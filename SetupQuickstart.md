# Setup Quickstart

## Get the code!
Clone the `tgmrank` and `tgmrank-frontend` repositories to the same directory

Expected directory structure:

```
├── SomeDirectory
    ├── tgmrank-frontend
    └── tgmrank
```

Enter the tgmrank directory
```
$ cd tgmrank
```

## (Optional) Cleanup Steps
```
$ docker system prune
$ docker image rm <imageId>
```

## (Optional) Skip Building the Backend
If you're only developing the frontend, comment out `services.web.build` from `docker-compose.override.yml`. Now you don't have to download the builder image (~2 GB) and build the entire and its dependencies.

```
...
  web:
    # build: .
    ports:
      - 1301:1301
...
```

## Do it
Run and build everything. Hopefully you don't have services currently listening on ports 80, 1300, or 5432.
```
$ docker-compose up --build -d
```

## (Optional) Get some test data
Connect to the postgresql database. The connection string should be `postgres://application_user:asdf123@localhost:5432/tgmscores`
Run your scripts!

## Stop the frontend service
```
$ docker-compose stop frontend
```

You should now be able to navigate to the `tgmrank-frontend` directory and run the frontend service locally! See `http://localhost:1234`

## Notes
* Many of the leaderboard responses are cached, so after drastic, unexpected database changes, you may need to flush the API cache. This can be done in the UI by logging in as an Admin and navigating to the admin panel.
* Deleting deleting the postgresql database and the embedded database can be done easily if you have rust/cargo installed. You'll need to install cargo-make `cargo install cargo-make` and run `cargo make reset_env`.
  * If you don't have `cargo`, you can look at `Makefile.toml` and try to run the steps manually.
  * Similarly, running the integration tests can be done with `cargo make itest`. This calls `reset_env`, so make sure you don't have any unsaved work in your databases.
