use std::str::FromStr;

use actix_cors::Cors;

use actix_session::config::PersistentSession;
use actix_session::{storage::CookieSessionStore, SessionMiddleware};
use actix_web::cookie::Key;
use actix_web::cookie::SameSite;
use actix_web::http::header;
use actix_web::http::Uri;

use crate::guards;
use crate::services::REMEMBER_ME_DURATION_TIME_V3;
use crate::utility::ServerError;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Settings {
    pub application: ApplicationSettings,
    pub email: EmailSettings,
    pub database: DatabaseSettings,
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub enum ApplicationEnvironment {
    Production,
    Staging,
    Development,
    Other(String),
}

impl FromStr for ApplicationEnvironment {
    type Err = ServerError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let env = match s.to_ascii_lowercase().as_ref() {
            "production" => Self::Production,
            "staging" => Self::Staging,
            "development" => Self::Development,
            _ => {
                warn!("Unexpected Application Environment: {}", s);
                Self::Other(s.to_string())
            }
        };

        Ok(env)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ApplicationSettings {
    pub version: String,
    pub environment: ApplicationEnvironment,
    pub host: String,
    pub port: u16,
    pub secret_key: String,
    pub external_url: String,
    pub gateway_url: Option<String>,
    pub api_url: String,
    pub additional_allowed_origins: String,
}

impl ApplicationSettings {
    pub fn is_production(&self) -> bool {
        self.environment == ApplicationEnvironment::Production
    }

    pub fn get_external_domain(&self) -> String {
        let uri = self
            .external_url
            .parse::<Uri>()
            .expect("Expected external URL to build cookie domain");
        uri.host()
            .expect("Expected domain host for cookie domain")
            .to_string()
    }

    fn get_additional_allowed_origins(&self) -> Vec<String> {
        self.additional_allowed_origins
            .split(';')
            .map(|s| s.to_string())
            .collect()
    }

    pub fn build_cors_handler(&self) -> Cors {
        let additional_origins = self.get_additional_allowed_origins();
        Cors::default()
            .allowed_origin(&self.external_url)
            .allowed_origin_fn(move |origin, _req_head| match origin.to_str() {
                Ok(origin) => {
                    let origin = origin.to_string();
                    additional_origins.iter().any(|o| o.contains(&origin))
                }
                Err(e) => {
                    warn!("Origin is invalid: {:?} - {}", origin, e);
                    false
                }
            })
            .allowed_methods(vec!["GET", "POST", "PUT", "PATCH", "DELETE"])
            .allowed_headers(vec![
                header::AUTHORIZATION,
                header::ACCEPT,
                header::CONTENT_TYPE,
            ])
            .supports_credentials()
            .max_age(3600)
    }

    pub fn build_identity_service(&self) -> SessionMiddleware<CookieSessionStore> {
        SessionMiddleware::builder(
            CookieSessionStore::default(),
            Key::derive_from(self.secret_key.as_bytes()),
        )
        .cookie_secure(self.is_production())
        .cookie_name(guards::USER_COOKIE_ID.to_owned())
        .cookie_path("/".to_owned())
        .cookie_domain(Some(self.get_external_domain()))
        .cookie_same_site(SameSite::Strict)
        .session_lifecycle(PersistentSession::default().session_ttl(*REMEMBER_ME_DURATION_TIME_V3))
        .build()
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct EmailSettings {
    pub disable: bool,
    pub server: String,
    pub username: String,
    pub password: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DatabaseSettings {
    pub connection_string: String,
}

impl Settings {
    pub fn new(settings_filename: &str) -> anyhow::Result<Self> {
        dotenv::from_filename(settings_filename).ok();

        Ok(Self {
            application: ApplicationSettings {
                version: env!("CARGO_PKG_VERSION").to_string(),
                environment: ApplicationEnvironment::from_str(&std::env::var("APP_ENVIRONMENT")?)?,
                host: std::env::var("APP_HOST")?,
                port: std::env::var("APP_PORT")?.parse()?,
                secret_key: std::env::var("APP_SECRET_KEY")?,
                external_url: std::env::var("APP_EXTERNAL_URL")?,
                gateway_url: std::env::var("APP_GATEWAY_URL").ok(),
                api_url: std::env::var("APP_API_URL")?,
                additional_allowed_origins: std::env::var("APP_ADDITIONAL_ALLOWED_ORIGINS")?,
            },
            email: EmailSettings {
                disable: std::env::var("EMAIL_DISABLE")?.parse()?,
                server: std::env::var("EMAIL_SERVER")?,
                username: std::env::var("EMAIL_USERNAME")?,
                password: std::env::var("EMAIL_PASSWORD")?,
            },
            database: DatabaseSettings {
                connection_string: std::env::var("DATABASE_CONNECTION_STRING")?,
            },
        })
    }
}
