use actix_web::HttpRequest;

const API_KEY_HEADER_NAME: &str = "X-Api-Key";

pub struct RequestApiKey<'a> {
    pub key: &'a str,
}

impl<'a> RequestApiKey<'a> {
    pub fn from_request(req: &'a HttpRequest) -> Option<RequestApiKey<'a>> {
        req.headers().get(API_KEY_HEADER_NAME).and_then(|api_key| {
            Some(Self {
                key: api_key.to_str().ok()?,
            })
        })
    }
}
