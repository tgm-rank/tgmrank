use bb8_postgres::PostgresConnectionManager;

use crate::modules::DatabaseSettings;

pub type DbPool2 =
    bb8_postgres::bb8::Pool<PostgresConnectionManager<bb8_postgres::tokio_postgres::NoTls>>;
pub type DbConnection2<'a> = bb8_postgres::bb8::PooledConnection<
    'a,
    PostgresConnectionManager<bb8_postgres::tokio_postgres::NoTls>,
>;

pub type DbPool = sqlx::PgPool;
pub type DbConnection = sqlx::PgConnection;

const DATABASE_POOL_CAPACITY: usize = 16;

impl DatabaseSettings {
    pub async fn make_sqlx_connection_pool(&self) -> DbPool {
        sqlx::pool::PoolOptions::<sqlx::Postgres>::new()
            .min_connections(1)
            .max_connections(DATABASE_POOL_CAPACITY as u32)
            .connect(&self.connection_string)
            .await
            .expect("Could not build database connection pool")
    }

    pub async fn make_connection_pool(&self) -> DbPool2 {
        let manager = bb8_postgres::PostgresConnectionManager::new_from_stringlike(
            &self.connection_string,
            bb8_postgres::tokio_postgres::NoTls,
        )
        .expect("Could not create postgres connection manager");

        bb8_postgres::bb8::Pool::builder()
            .build(manager)
            .await
            .expect("Could not create connection pool")
    }
}
