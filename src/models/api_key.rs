#[derive(Debug)]
pub struct ApiKeyEntity {
    pub api_key_id: i32,
    pub description: String,
    pub key: String,
}
