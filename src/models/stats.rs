pub struct MedalStats {
    pub player_id: i32,
    pub total: i64,
    pub gold_count: i64,
    pub gold_modes: Option<Vec<i32>>,
    pub silver_count: i64,
    pub silver_modes: Option<Vec<i32>>,
    pub bronze_count: i64,
    pub bronze_modes: Option<Vec<i32>>,
}

pub struct SubmissionStats {
    pub player_id: i32,
    pub submissions: Vec<i64>,
}
