#[derive(Debug)]
pub struct LocationEntity {
    pub location_id: i32,
    pub num_code: i32,
    pub name: String,
    pub alpha_2: String,
    pub alpha_3: String,
}
