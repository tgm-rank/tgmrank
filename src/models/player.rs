use time::OffsetDateTime;

use crate::models::Role;
use std::fmt::Display;

#[derive(sqlx::FromRow, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlayerNotificationSettingsEntity {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub announce_rivals: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub announce_overtake: Option<bool>,
}

#[derive(sqlx::FromRow, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlayerSettingsEntity {
    pub notifications: PlayerNotificationSettingsEntity,
}

#[derive(sqlx::FromRow, Debug, Clone, Eq, PartialEq)]
pub struct PlayerEntity {
    pub player_id: i32,
    pub player_name: String,
    pub email: Option<String>,
    pub password: String,
    pub player_role: Role,
    pub active: bool,
    pub avatar_file: Option<String>,
    pub location_id: Option<i32>,
    pub location: Option<String>,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime,
}

#[derive(Debug, Deserialize, Clone)]
pub struct PlayerEntity2 {
    pub player_id: i32,
    pub player_name: String,
    pub avatar_file: Option<String>,
    pub location_id: Option<i32>,
    pub location: Option<String>,
}

// TODO: Evaluate when we can remove this custom Decode implementation.
// See https://github.com/launchbadge/sqlx/issues/1268 and https://github.com/launchbadge/sqlx/issues/1031
impl ::sqlx::decode::Decode<'_, ::sqlx::Postgres> for PlayerEntity2 {
    fn decode(
        value: ::sqlx::postgres::PgValueRef<'_>,
    ) -> ::std::result::Result<
        Self,
        ::std::boxed::Box<
            dyn ::std::error::Error + 'static + ::std::marker::Send + ::std::marker::Sync,
        >,
    > {
        let mut decoder = ::sqlx::postgres::types::PgRecordDecoder::new(value)?;
        let player_id = decoder.try_decode::<i32>()?;
        let player_name = decoder.try_decode::<String>()?;
        let avatar_file = decoder.try_decode::<Option<String>>()?;
        let location_id = decoder.try_decode::<Option<i32>>()?;
        let location = decoder.try_decode::<Option<String>>()?;
        ::std::result::Result::Ok(PlayerEntity2 {
            player_id,
            player_name,
            avatar_file,
            location_id,
            location,
        })
    }
}
impl ::sqlx::Type<::sqlx::Postgres> for PlayerEntity2 {
    fn type_info() -> ::sqlx::postgres::PgTypeInfo {
        ::sqlx::postgres::PgTypeInfo::with_name("PlayerEntity2")
    }
}

pub struct NewPlayerEntity {
    pub player_name: String,
    pub email: Option<String>,
    pub password: String,
}

pub struct AccountClaimEntity {
    pub account_id: i32,
    pub claim_key: String,
    pub claim_value: String,
}

pub enum ClaimKey {
    CanUpdateRoles,
    CanUpdateScores,
    CanVerifyScores,
    CanManageAccounts,
}

impl Display for ClaimKey {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match *self {
            Self::CanUpdateRoles => "can_update_roles",
            Self::CanUpdateScores => "can_update_scores",
            Self::CanVerifyScores => "can_verify_scores",
            Self::CanManageAccounts => "can_manage_accounts",
        })
    }
}
