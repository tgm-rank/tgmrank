#[derive(Debug, Clone, Eq, PartialEq)]
pub struct BadgeEntity {
    pub badge_id: i32,
    pub name: String,
}

pub struct NewBadgeEntity<'a> {
    pub name: &'a str,
    pub updated_by: i32,
}
