pub struct SentMail {
    pub email: String,
    pub account_id: Option<i32>,
    pub subject: String,
    pub body: String,
}
