use std::collections::hash_map::DefaultHasher;
use std::convert::TryFrom;
use std::hash::{Hash, Hasher};
use std::iter::{Skip, Take};

use itertools::Itertools;
use serde::de::Error;
use serde::Deserialize;
use time::OffsetDateTime;
use validator::Validate;

use crate::controllers::game_controller::GameRankingQuery;
use crate::controllers::mode_controller::ModeRankingQuery;
use crate::models::{RankingAlgorithm, ScoreStatus};
use crate::services::CacheKey;
use crate::utility::TgmRankError;
use crate::utility::WrappedInt;

fn normalize_wrapped_int_slice(s: &[WrappedInt]) -> Vec<i32> {
    normalize_slice(&s.iter().map(|s| s.0).collect_vec())
}

fn normalize_slice<T: std::cmp::Ord + std::clone::Clone>(s: &[T]) -> Vec<T> {
    s.iter().sorted().dedup().cloned().collect_vec()
}

fn from_str<'de, D, S>(deserializer: D) -> Result<Option<S>, D::Error>
where
    D: serde::Deserializer<'de>,
    S: std::str::FromStr,
{
    if let Ok(s) = <&str as serde::Deserialize>::deserialize(deserializer) {
        return S::from_str(s)
            .map(Some)
            .map_err(|_| D::Error::custom("could not parse string"));
    }

    Ok(None)
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Hash, Deserialize)]
pub struct LocationFilter {
    pub locations: Option<Vec<String>>,
}

impl From<Option<Vec<String>>> for LocationFilter {
    fn from(query: Option<Vec<String>>) -> Self {
        Self { locations: query }
    }
}

#[derive(Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PaginationQuery {
    #[serde(deserialize_with = "from_str")]
    pub page: Option<i32>,
    #[serde(deserialize_with = "from_str")]
    pub page_size: Option<i32>,
}

#[derive(Debug, Clone, Default, PartialEq, Eq, Hash, Deserialize, Validate)]
pub struct PaginationFilter {
    #[validate(range(min = 1, message = "Page must be a positive integer"))]
    pub page: Option<i32>,
    #[validate(range(min = 1, message = "Page size must be a positive integer"))]
    pub page_size: Option<i32>,
}

impl PaginationFilter {
    pub fn paginate<I>(&self, iter: I) -> Take<Skip<I>>
    where
        I: Iterator,
        I::Item: Sized,
    {
        match (self.page, self.page_size) {
            (Some(page), Some(page_size)) => {
                let skip = if page == 0 { 0 } else { (page - 1) * page_size } as usize;
                let take = page_size as usize;
                iter.skip(skip).take(take)
            }
            (None, Some(page_size)) => iter.skip(0).take(page_size as usize),
            _ => iter.skip(0).take(1000000),
        }
    }
}

impl From<Option<&PaginationQuery>> for PaginationFilter {
    fn from(query: Option<&PaginationQuery>) -> Self {
        Self {
            page: match query.and_then(|p| p.page) {
                Some(page) if page < 0 => Some(0),
                Some(page) => Some(page),
                _ => None,
            },
            page_size: query.and_then(|p| p.page_size),
        }
    }
}

#[derive(Clone, Default, PartialEq, Eq, Hash, Deserialize, Validate)]
pub struct GameFilter {
    #[validate(nested)]
    pub pagination: PaginationFilter,
}

impl CacheKey for GameFilter {
    fn prefix() -> &'static str {
        "Games"
    }
}

impl From<GenericQuery> for GameFilter {
    fn from(query: GenericQuery) -> Self {
        Self {
            pagination: PaginationFilter::from(query.pagination.as_ref()),
        }
    }
}

#[derive(Clone, PartialEq, Eq, Hash, Deserialize)]
pub struct PlayerFilter {
    pub id: Option<i32>,
    pub name: Option<String>,
    pub as_of: Option<OffsetDateTime>,
}

impl Default for PlayerFilter {
    fn default() -> Self {
        Self {
            id: None,
            name: None,
            as_of: Some(OffsetDateTime::now_utc()),
        }
    }
}

impl CacheKey for PlayerFilter {
    fn prefix() -> &'static str {
        "PlayerScores"
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Hash, Deserialize, Validate)]
pub struct GameRankingFilter {
    pub id: Option<String>,
    pub name: Option<String>,
    pub player_name: Option<String>,
    #[validate(length(max = 6, message = "Too many player ids"))]
    pub player_ids: Option<Vec<i32>>,
    pub mode_ids: Option<Vec<i32>>,
    pub as_of: Option<OffsetDateTime>,
    #[validate(nested)]
    pub pagination: PaginationFilter,
    pub location: LocationFilter,
    pub algorithm: RankingAlgorithm,
}

impl TryFrom<GameRankingQuery> for GameRankingFilter {
    type Error = TgmRankError;

    fn try_from(query: GameRankingQuery) -> Result<Self, Self::Error> {
        Ok(Self {
            id: None,
            name: query.name,
            player_name: query.player_name,
            player_ids: query.player_ids.as_deref().map(normalize_wrapped_int_slice),
            mode_ids: query.mode_ids.as_deref().map(normalize_wrapped_int_slice),
            as_of: match query.as_of {
                Some(s) => Some(
                    OffsetDateTime::parse(&s, &time::format_description::well_known::Rfc3339)
                        .map_err(TgmRankError::from_std)?,
                ),
                _ => None,
            },
            pagination: query.pagination.as_ref().into(),
            location: query.locations.into(),
            algorithm: query.algorithm.unwrap_or_default(),
        })
    }
}

impl CacheKey for GameRankingFilter {
    fn prefix() -> &'static str {
        "GameRanking"
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Hash, Deserialize, Validate)]
pub struct ModeRankingFilter {
    pub id: Option<i32>,
    pub as_of: Option<OffsetDateTime>,
    pub player_name: Option<String>,
    #[validate(length(max = 6, message = "Too many player ids"))]
    pub player_ids: Option<Vec<i32>>,
    #[validate(nested)]
    pub pagination: PaginationFilter,
    pub location: LocationFilter,
    pub algorithm: RankingAlgorithm,
}

impl TryFrom<ModeRankingQuery> for ModeRankingFilter {
    type Error = TgmRankError;

    fn try_from(query: ModeRankingQuery) -> Result<Self, Self::Error> {
        Ok(Self {
            id: None,
            as_of: match query.as_of {
                Some(s) => Some(
                    OffsetDateTime::parse(&s, &time::format_description::well_known::Rfc3339)
                        .map_err(TgmRankError::from_std)?,
                ),
                _ => None,
            },
            player_name: query.player_name,
            player_ids: query.player_ids.as_deref().map(normalize_wrapped_int_slice),
            pagination: query.pagination.as_ref().into(),
            location: query.locations.into(),
            algorithm: query.algorithm.unwrap_or_default(),
        })
    }
}

impl CacheKey for ModeRankingFilter {
    fn prefix() -> &'static str {
        "ModeRanking"
    }

    fn get_params(&self) -> Vec<u8> {
        vec![]
    }
}

#[derive(Deserialize)]
pub struct GradeFilter {
    pub mode_id: i32,
}

#[derive(Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RecentActivityQuery {
    pub score_id: Option<i32>,
    pub rank_threshold: Option<i32>,
    #[serde(default, with = "crate::utility::opt_stringified_list")]
    pub filter_mode_ids: Option<Vec<WrappedInt>>,
    #[serde(flatten)]
    pub filter: GenericQuery,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Default, Deserialize)]
pub struct RecentActivityFilter {
    pub score_id: Option<i32>,
    pub rank_threshold: Option<i32>,
    pub filter_mode_ids: Option<Vec<i32>>,
    pub filter: GenericFilter,
}

impl TryFrom<RecentActivityQuery> for RecentActivityFilter {
    type Error = TgmRankError;

    fn try_from(query: RecentActivityQuery) -> Result<Self, Self::Error> {
        Ok(Self {
            score_id: query.score_id,
            rank_threshold: query.rank_threshold,
            filter_mode_ids: query
                .filter_mode_ids
                .as_deref()
                .map(normalize_wrapped_int_slice),
            filter: GenericFilter::try_from(query.filter)?,
        })
    }
}

impl CacheKey for RecentActivityFilter {
    fn prefix() -> &'static str {
        "RecentActivity"
    }
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Hash, Deserialize)]
pub struct ModeFilter {
    pub mode_ids: Option<Vec<i32>>,
}

impl From<Vec<i32>> for ModeFilter {
    fn from(query: Vec<i32>) -> Self {
        Self {
            mode_ids: Some(query),
        }
    }
}

#[derive(Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GenericQuery {
    pub player_name: Option<String>,
    pub as_of: Option<OffsetDateTime>,
    #[serde(default, with = "crate::utility::opt_stringified_list")]
    pub mode_ids: Option<Vec<WrappedInt>>,
    #[serde(flatten)]
    pub pagination: Option<PaginationQuery>,
    #[serde(default, with = "crate::utility::opt_stringified_list")]
    pub locations: Option<Vec<String>>,
    #[serde(default, with = "crate::utility::opt_stringified_list")]
    pub score_status: Option<Vec<ScoreStatus>>,
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Hash, Deserialize)]
pub struct GenericFilter {
    pub player_name: Option<String>,
    pub as_of: Option<OffsetDateTime>,
    pub pagination: PaginationFilter,
    pub location: LocationFilter,
    pub mode: ModeFilter,
    pub score_status: Option<Vec<ScoreStatus>>,
}

impl TryFrom<GenericQuery> for GenericFilter {
    type Error = TgmRankError;

    fn try_from(query: GenericQuery) -> Result<Self, Self::Error> {
        Ok(Self {
            player_name: query.player_name,
            as_of: query.as_of,
            pagination: query.pagination.as_ref().into(),
            location: query.locations.into(),
            mode: match query.mode_ids {
                Some(mode_ids) => ModeFilter::from(normalize_wrapped_int_slice(&mode_ids)),
                _ => ModeFilter::default(),
            },
            score_status: query.score_status,
        })
    }
}

impl GenericFilter {
    pub fn key(&self, prefix: &str) -> Vec<u8> {
        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        let key_hash = hasher.finish();

        let mut k = prefix.as_bytes().to_vec();
        k.append(&mut key_hash.to_be_bytes().to_vec());

        k
    }
}
