#[derive(sqlx::Type, Debug, PartialEq)]
#[sqlx(type_name = "recent_activity_type")]
#[sqlx(rename_all = "snake_case")]
pub enum RecentActivityType {
    Mode,
    Game,
    Overall,
}

#[derive(Debug)]
pub struct RecentActivityEntity {
    pub activity_type: RecentActivityType,
    pub id: i32,
    pub current_score_id: i32,
    pub current_ranking: Option<i32>,
    pub current_ranking_points: Option<i32>,
    pub prev_score_id: Option<i32>,
    pub prev_ranking: Option<i32>,
    pub prev_ranking_points: Option<i32>,
    pub passed_player_ids: Vec<i32>,
}

pub struct CombinedRecentActivity {
    pub mode_activity: RecentActivityEntity,
    pub game_activity: RecentActivityEntity,
    pub overall_activity: RecentActivityEntity,
}
