use time::OffsetDateTime;

use super::ScoreStatus;

pub struct AchievementEntity {
    pub achievement_id: i32,
    pub description: String,
    pub game_id: Option<i32>,
    pub mode_id: Option<i32>,
    pub sort_order: i32,
}

pub struct AccountAchievementEntity {
    pub account_achievement_id: i32,
    pub account_id: i32,
    pub achievement_id: i32,
    pub score_id: Option<i32>,
    pub achieved_at: Option<OffsetDateTime>,
    pub status: Option<ScoreStatus>,
}
