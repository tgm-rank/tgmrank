use crate::models::ModeEntity;

#[derive(Clone, Eq, PartialEq)]
pub struct GameEntity {
    pub game_id: i32,
    pub game_name: String,
    pub short_name: String,
    pub subtitle: String,
    pub modes: Option<Vec<ModeEntity>>,
}

pub enum Game {
    Overall = 0,
    Tgm1,
    Tgm2,
    Tgm3,
}
