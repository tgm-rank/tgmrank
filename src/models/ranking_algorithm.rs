use std::fmt;
use std::str::FromStr;

#[derive(Debug, Eq, PartialEq, PartialOrd, Clone, Serialize, Deserialize, Hash, sqlx::Type)]
#[sqlx(type_name = "ranking_algorithm")]
#[sqlx(rename_all = "snake_case")]
#[serde(rename_all = "snake_case")]
pub enum RankingAlgorithm {
    Naive,
    Percentile,
    SimpleMax,
}

impl Default for RankingAlgorithm {
    fn default() -> Self {
        Self::Naive
    }
}

impl fmt::Display for RankingAlgorithm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(match *self {
            RankingAlgorithm::Naive => "naive",
            RankingAlgorithm::Percentile => "percentile",
            RankingAlgorithm::SimpleMax => "simple_max",
        })
    }
}

impl FromStr for RankingAlgorithm {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<RankingAlgorithm, &'static str> {
        match s.to_lowercase().as_str() {
            "naive" => Ok(RankingAlgorithm::Naive),
            "percentile" => Ok(RankingAlgorithm::Percentile),
            "simple_max" => Ok(RankingAlgorithm::SimpleMax),
            _ => Err("Unrecognized enum variant"),
        }
    }
}
