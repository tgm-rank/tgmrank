use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct ResetPasswordForm {
    #[validate(length(min = 8, message = "Password is too short"))]
    pub password: String,
}

#[cfg(test)]
mod tests {
    use validator::Validate;

    use crate::models::forms::reset_password_form::ResetPasswordForm;

    #[test]
    fn reset_password_must_not_be_too_short() {
        let model = ResetPasswordForm {
            password: String::from("abcd"),
        };

        assert_eq!(model.validate().is_ok(), false);
    }
}
