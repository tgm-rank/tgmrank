use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct LoginForm {
    pub username: String,
    pub password: String,
    pub remember: Option<bool>,
}
