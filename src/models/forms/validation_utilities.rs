use once_cell::sync::Lazy;
use regex::Regex;

pub static USERNAME_RE: Lazy<Regex> = Lazy::new(|| {
    Regex::new(
        r"^[a-zA-Z0-9_\u3040-\u309Fー\u30A0-\u30FF]+( [a-zA-Z0-9_\u3040-\u309Fー\u30A0-\u30FF]+)*$",
    )
    .unwrap()
});
