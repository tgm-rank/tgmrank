use crate::models::ScoreStatus;

#[derive(Debug, Serialize, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct VerifyScoreForm {
    pub status: ScoreStatus,
    #[validate(length(max = 500, message = "Comment is too long"))]
    pub comment: Option<String>,
}
