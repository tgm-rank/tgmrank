use crate::models::forms::validation_utilities::USERNAME_RE;

#[derive(Serialize, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct SimpleUserRegisterForm {
    #[validate(length(
        min = 3,
        max = 20,
        message = "Username must be between 3 and 20 characters"
    ))]
    #[validate(regex(path = *USERNAME_RE, message = "Username is invalid"))]
    pub username: String,
}

#[derive(Debug, Serialize, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct UserRegisterForm {
    #[validate(length(
        min = 3,
        max = 20,
        message = "Username must be between 3 and 20 characters"
    ))]
    #[validate(regex(path = *USERNAME_RE, message = "Username is invalid"))]
    pub username: String,
    #[validate(email(message = "Email is not valid"))]
    pub email: String,
    #[validate(length(min = 8, message = "Password is too short"))]
    pub password: String,
}

#[cfg(test)]
mod tests {
    use crate::models::forms::user_register_form::UserRegisterForm;
    use validator::Validate;

    const MIN_USERNAME_LENGTH: i32 = 3;
    const MAX_USERNAME_LENGTH: i32 = 20;
    const MIN_PASSWORD_LENGTH: i32 = 8;
    const VALID_EMAIL: &str = "asdf@asdf.com";

    fn generate_string(length: i32) -> String {
        (0..length).map(|_| 'a').collect::<String>()
    }

    fn make_valid_register_model() -> UserRegisterForm {
        UserRegisterForm {
            username: generate_string(MIN_USERNAME_LENGTH),
            email: String::from(VALID_EMAIL),
            password: generate_string(MIN_PASSWORD_LENGTH),
        }
    }

    #[test]
    fn register_model_should_be_validated() {
        let model = make_valid_register_model();

        assert_eq!(model.validate().is_ok(), true);
    }

    #[test]
    fn register_username_should_not_be_too_short() {
        for i in 0..MIN_USERNAME_LENGTH {
            let mut model = make_valid_register_model();
            model.username = generate_string(i);

            assert_eq!(model.validate().is_ok(), false);
        }
    }

    #[test]
    fn register_username_should_not_be_too_long() {
        let mut model = make_valid_register_model();
        model.username = generate_string(MAX_USERNAME_LENGTH);
        assert_eq!(model.validate().is_ok(), true);

        let mut model = make_valid_register_model();
        model.username = generate_string(MAX_USERNAME_LENGTH + 1);
        assert_eq!(model.validate().is_ok(), false);
    }

    #[test]
    fn register_username_should_only_contain_letters_numbers_spaces_underscores_and_jp_kana_for_whatever_reason(
    ) {
        let bad_usernames = vec!["Hello\nWorld", "ÀBCD"];

        for username in bad_usernames {
            let mut model = make_valid_register_model();
            model.username = String::from(username);

            assert_eq!(model.validate().is_ok(), false)
        }

        let good_usernames = vec!["HelloWorld", "トリカン", "ひらがな", "________"];

        for username in good_usernames {
            let mut model = make_valid_register_model();
            model.username = String::from(username);

            assert_eq!(model.validate().is_ok(), true)
        }
    }

    #[test]
    fn register_username_should_only_allow_single_spaces_between_words() {
        let bad_usernames = vec!["Hello  World", " Hello", "Hello "];

        for username in bad_usernames {
            let mut model = make_valid_register_model();
            model.username = String::from(username);

            assert_eq!(model.validate().is_ok(), false)
        }

        let good_usernames = vec!["Hello World"];

        for username in good_usernames {
            let mut model = make_valid_register_model();
            model.username = String::from(username);

            assert_eq!(model.validate().is_ok(), true)
        }
    }

    #[test]
    fn register_email_should_be_valid() {
        let mut model = make_valid_register_model();
        model.email = String::from("This ain't an email");

        assert_eq!(model.validate().is_ok(), false);
    }

    #[test]
    fn register_password_should_not_be_too_short() {
        for i in 0..MIN_PASSWORD_LENGTH {
            let mut model = make_valid_register_model();
            model.password = generate_string(i);

            assert_eq!(model.validate().is_ok(), false);
        }
    }
}
