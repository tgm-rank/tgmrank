use crate::models::forms::validation_utilities::USERNAME_RE;

#[derive(Default, Serialize, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct PlayerUpdateForm {
    pub current_password: Option<String>,
    #[validate(length(min = 8, message = "Password is too short"))]
    pub new_password: Option<String>,
    #[validate(email(message = "Email is not valid"))]
    pub new_email_address: Option<String>,
}

#[derive(Default, Serialize, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct PlayerLocationUpdateForm {
    #[validate(length(
        min = 2,
        max = 3,
        message = "Location code is not valid. Expected ISO 3166-1 alpha-2 or alpha-3 code."
    ))]
    pub new_location: Option<String>,
}

#[derive(Default, Serialize, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct PlayerUsernameUpdateForm {
    #[validate(length(
        min = 3,
        max = 20,
        message = "Username must be between 3 and 20 characters"
    ))]
    #[validate(regex(path = *USERNAME_RE, message = "Username is invalid"))]
    pub username: String,
}
