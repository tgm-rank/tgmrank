pub struct PaginationForm {
    pub page: Option<i64>,
    pub page_size: Option<i64>,
}
