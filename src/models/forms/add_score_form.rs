use itertools::Itertools;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use time::Time;
use validator::{Validate, ValidationError};

use crate::models::ProofType;
use crate::utility::{parse_playtime, validate_playtime_str};

pub static LINK_WHITELIST: Lazy<Vec<&str>> = Lazy::new(|| vec!["tetrisconcept.net"]);

#[derive(Clone, Serialize, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct ProofLinkForm {
    #[serde(rename = "type")]
    pub proof_type: ProofType,
    #[validate(url(message = "Invalid proof url"))]
    pub link: String,
}

#[derive(Default, Serialize, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct UpdateScoreForm {
    pub score_id: i32,
    #[validate(length(max = 500, message = "Comment is too long"))]
    pub comment: Option<String>,
    #[validate(nested)]
    #[validate(length(min = 1))]
    pub proof: Vec<ProofLinkForm>,
    pub platform_id: Option<i32>,
    pub dry_run: Option<bool>,
}

impl UpdateScoreForm {
    pub fn get_comment(&self) -> Option<String> {
        self.comment.as_ref().map(|s| get_comment(s))
    }
}

#[derive(Default, Clone, Serialize, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct AddScoreForm {
    pub mode_id: i32,
    pub grade_id: Option<i32>,
    #[validate(range(min = 0, max = 9999, message = "Level is out of range"))]
    pub level: Option<i32>,
    #[validate(custom(
        function = "AddScoreForm::validate_time_string",
        message = "Invalid time format"
    ))]
    pub playtime: Option<String>,
    #[validate(range(min = 0, max = 999999999, message = "Score is out of range"))]
    pub score: Option<i32>,
    #[validate(nested)]
    #[validate(length(min = 1))]
    pub proof: Vec<ProofLinkForm>,
    #[validate(length(max = 500, message = "Comment is too long"))]
    pub comment: Option<String>,
    pub player_id: Option<i32>,
    pub platform_id: Option<i32>,
    pub dry_run: Option<bool>,
}

impl AddScoreForm {
    pub fn get_comment(&self) -> Option<String> {
        self.comment.as_ref().map(|s| get_comment(s))
    }

    fn validate_time_string(time_string: &str) -> Result<(), ValidationError> {
        if time_string.is_empty() || validate_playtime_str(time_string) {
            Ok(())
        } else {
            Err(ValidationError::new("Invalid time format"))
        }
    }

    pub fn get_playtime(&self) -> Option<Time> {
        if let Some(time) = &self.playtime {
            parse_playtime(time).ok()
        } else {
            None
        }
    }
}

fn get_comment(s: &str) -> String {
    s.trim()
        .split('\n')
        .filter(|s| !s.trim().is_empty())
        .collect_vec()
        .join("\n")
}

#[cfg(test)]
mod tests {
    use super::get_comment;
    use crate::models::forms::{AddScoreForm, UpdateScoreForm};

    use validator::Validate;

    #[test]
    fn get_comment_should_trim_whitespace() {
        let s = "   \n\t hello  \n\t  ";
        let comment = get_comment(s);

        assert_eq!(comment, "hello");
    }

    #[test]
    fn get_comment_should_collapse_newlines() {
        let s = "one\n\n\n\ntwo\n\n\nthree";
        let comment = get_comment(s);

        assert_eq!(comment, "one\ntwo\nthree");
    }

    #[test]
    fn add_score_form_should_require_at_least_one_proof_entry() {
        let form = AddScoreForm {
            proof: vec![],
            ..Default::default()
        };

        let validation_result = form.validate();
        assert!(validation_result.is_err());

        let errors = validation_result.unwrap_err();
        assert!(errors
            .to_string()
            .contains("proof: Validation error: length"));
    }

    #[test]
    fn update_score_form_should_require_at_least_one_proof_entry() {
        let form = UpdateScoreForm {
            proof: vec![],
            ..Default::default()
        };

        let validation_result = form.validate();
        assert!(validation_result.is_err());

        let errors = validation_result.unwrap_err();
        assert!(errors
            .to_string()
            .contains("proof: Validation error: length"));
    }
}
