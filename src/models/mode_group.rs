pub struct ModeGroupEntity {
    pub mode_group_id: i32,
    pub game_id: i32,
    pub name: String,
    pub description: String,
    pub mode_ids: Vec<i32>,
}
