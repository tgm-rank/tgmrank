use crate::models::integration_target::IntegrationTarget;

#[derive(Default)]
pub struct ExternalIntegrationEntity {
    pub integration_id: Option<i32>,
    pub target: IntegrationTarget,
    pub player_id: i32,
    pub external_key: String,
}
