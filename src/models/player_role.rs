use std::fmt;
use std::str::FromStr;

use serde::{Deserialize, Serialize};

#[derive(Debug, Eq, PartialEq, PartialOrd, Clone, Serialize, Deserialize, sqlx::Type)]
#[sqlx(type_name = "player_role_type")]
#[sqlx(rename_all = "snake_case")]
#[serde(rename_all = "snake_case")]
pub enum Role {
    Banned,
    User,
    Verifier,
    Admin,
}

impl Role {
    pub fn can_log_in(&self) -> bool {
        self > &Role::Banned
    }

    pub fn can_verify_scores(&self) -> bool {
        self >= &Role::Verifier
    }

    pub fn is_admin(&self) -> bool {
        self == &Role::Admin
    }

    pub fn get_all_roles() -> Vec<Role> {
        vec![Role::Banned, Role::User, Role::Verifier, Role::Admin]
    }
}

impl fmt::Display for Role {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(match *self {
            Role::Banned => "banned",
            Role::User => "user",
            Role::Verifier => "verifier",
            Role::Admin => "admin",
        })
    }
}

impl FromStr for Role {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Role, &'static str> {
        match s.to_lowercase().as_str() {
            "banned" => Ok(Role::Banned),
            "user" => Ok(Role::User),
            "verifier" => Ok(Role::Verifier),
            "admin" => Ok(Role::Admin),
            _ => Err("Unrecognized enum variant"),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::models::player_role::Role;

    #[test]
    fn can_log_in_returns_false_if_banned() {
        assert_eq!(Role::Banned.can_log_in(), false);
    }

    #[test]
    fn can_log_in_returns_true_if_not_banned() {
        assert_eq!(Role::User.can_log_in(), true);
        assert_eq!(Role::Verifier.can_log_in(), true);
        assert_eq!(Role::Admin.can_log_in(), true);
    }

    #[test]
    fn can_verify_scores_should_not_accept_banned_role() {
        assert_eq!(Role::Banned.can_verify_scores(), false);
    }

    #[test]
    fn can_verify_scores_should_not_accept_user_role() {
        assert_eq!(Role::User.can_verify_scores(), false);
    }

    #[test]
    fn can_verify_scores_should_accept_verifiers_and_admins() {
        assert_eq!(Role::Verifier.can_verify_scores(), true);
        assert_eq!(Role::Admin.can_verify_scores(), true);
    }

    #[test]
    fn get_all_roles() {
        let roles = Role::get_all_roles();

        assert!(roles.contains(&Role::Banned));
        assert!(roles.contains(&Role::User));
        assert!(roles.contains(&Role::Verifier));
        assert!(roles.contains(&Role::Admin));
    }
}
