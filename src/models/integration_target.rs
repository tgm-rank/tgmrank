use std::fmt;
use std::str::FromStr;

#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize, sqlx::Type)]
#[sqlx(type_name = "integration_target")]
#[sqlx(rename_all = "snake_case")]
#[serde(rename_all = "snake_case")]
pub enum IntegrationTarget {
    Discord,
}

impl Default for IntegrationTarget {
    fn default() -> Self {
        IntegrationTarget::Discord
    }
}

impl fmt::Display for IntegrationTarget {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(match *self {
            IntegrationTarget::Discord => "discord",
        })
    }
}

impl FromStr for IntegrationTarget {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<IntegrationTarget, &'static str> {
        match s.to_lowercase().as_str() {
            "discord" => Ok(IntegrationTarget::Discord),
            _ => Err("Unrecognized enum variant"),
        }
    }
}
