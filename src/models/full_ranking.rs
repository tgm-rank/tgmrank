use crate::models::FullScoreEntity;
use sqlx::types::Json;

pub struct ModeRankingEntity {
    pub score_id: i32,
    pub player_id: i32,
    pub game_id: i32,
    pub mode_id: i32,
    pub ranking: Option<i32>,
    pub ranking_points: Option<i32>,
}

pub struct FullModeRankingEntity {
    pub player_id: i32,
    pub game_id: i32,
    pub mode_id: i32,
    pub ranking: Option<i32>,
    pub ranking_points: Option<i32>,
    pub score: Json<FullScoreEntity>,
}
