use std::fmt::Display;

use sqlx::postgres::types::PgRange;
use time::OffsetDateTime;

#[derive(Hash, Eq, PartialEq)]
pub enum ModeTagRef {
    Main,
    Extended,
    Event,
    Carnival,
    Doom,
}

impl Display for ModeTagRef {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match *self {
            Self::Main => "MAIN",
            Self::Extended => "EXTENDED",
            Self::Event => "EVENT",
            Self::Carnival => "CARNIVAL",
            Self::Doom => "DOOM",
        })
    }
}

#[derive(Hash, Eq, PartialEq)]
pub enum StandardModeGroup {
    Main,
    Extended,
}

impl Display for StandardModeGroup {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match *self {
            Self::Main => "main",
            Self::Extended => "extended",
        })
    }
}

#[derive(Clone, Eq, PartialEq, Deserialize)]
pub struct ModeEntity {
    pub mode_id: i32,
    pub game_id: i32,
    pub mode_name: String,
    pub is_ranked: bool,
    pub weight: i32,
    pub margin: i32,
    pub link: Option<String>,
    pub grade_sort: Option<i16>,
    pub score_sort: Option<i16>,
    pub level_sort: Option<i16>,
    pub time_sort: Option<i16>,
    pub sort_order: i32,
    pub description: Option<String>,
    pub submission_range_start: Option<OffsetDateTime>,
    pub submission_range_end: Option<OffsetDateTime>,
    pub tags: Option<Vec<String>>,
}

impl ::sqlx::decode::Decode<'_, ::sqlx::Postgres> for ModeEntity {
    fn decode(
        value: ::sqlx::postgres::PgValueRef<'_>,
    ) -> ::std::result::Result<
        Self,
        ::std::boxed::Box<
            dyn ::std::error::Error + 'static + ::std::marker::Send + ::std::marker::Sync,
        >,
    > {
        let mut decoder = ::sqlx::postgres::types::PgRecordDecoder::new(value)?;
        let mode_id = decoder.try_decode::<i32>()?;
        let game_id = decoder.try_decode::<i32>()?;
        let mode_name = decoder.try_decode::<String>()?;
        let is_ranked = decoder.try_decode::<bool>()?;
        let weight = decoder.try_decode::<i32>()?;
        let margin = decoder.try_decode::<i32>()?;
        let link = decoder.try_decode::<Option<String>>()?;
        let grade_sort = decoder.try_decode::<Option<i16>>()?;
        let score_sort = decoder.try_decode::<Option<i16>>()?;
        let level_sort = decoder.try_decode::<Option<i16>>()?;
        let time_sort = decoder.try_decode::<Option<i16>>()?;
        let sort_order = decoder.try_decode::<i32>()?;
        let description = decoder.try_decode::<Option<String>>()?;
        let submission_range_start = decoder.try_decode::<Option<OffsetDateTime>>()?;
        let submission_range_end = decoder.try_decode::<Option<OffsetDateTime>>()?;
        let tags = decoder.try_decode::<Option<Vec<String>>>()?;
        ::std::result::Result::Ok(ModeEntity {
            mode_id,
            game_id,
            mode_name,
            is_ranked,
            weight,
            margin,
            link,
            grade_sort,
            score_sort,
            level_sort,
            time_sort,
            sort_order,
            description,
            submission_range_start,
            submission_range_end,
            tags,
        })
    }
}

impl ::sqlx::Type<::sqlx::Postgres> for ModeEntity {
    fn type_info() -> ::sqlx::postgres::PgTypeInfo {
        ::sqlx::postgres::PgTypeInfo::with_name("ModeEntity")
    }
}

pub struct NewModeEntity {
    pub game_id: i32,
    pub mode_name: String,
    pub is_ranked: bool,
    pub weight: i32,
    pub margin: i32,
    pub link: Option<String>,
    pub grade_sort: Option<i16>,
    pub score_sort: Option<i16>,
    pub level_sort: Option<i16>,
    pub time_sort: Option<i16>,
    pub sort_order: i32,
    pub description: Option<String>,
    pub submission_range: PgRange<OffsetDateTime>,
    pub tags: Option<Vec<String>>,
}

// TODO: We need to get rid of this :D
// These values _should_ never change in the database.
#[derive(Eq, PartialEq, Hash)]
pub enum Mode {
    Tgm1Master = 1,
    Tgm120g,
    Tgm1Big,
    Tgm1Reverse,
    Tgm1SecretGrade,
    TapMaster,
    TapDeath,
    TapDeathSeries,
    TapDoubles,
    TapBig,
    TapItem,
    TapNormal,
    TapTgmplus,
    TapSecretGrade,
    TiMasterClassic,
    TiShiraseClassic,
    TiEasyClassic,
    TiSakuraClassic,
    TiMasterSecretGradeClassic,
    TiMasterWorld,
    TiShiraseWorld,
    TiEasyWorld,
    TiSakuraWorld,
    TiMasterSecretGradeWorld,
    TiBigClassic,
    TiBigWorld,
    TiShiraseSecretGradeClassic,
    TiShiraseSecretGradeWorld,
    Tgm1ScoreAttack,
    TiQualifiedMasterClassic,
    TiQualifiedMasterWorld,
    TapBigNormal = 38,
    TapTgmPlusItem = 39,
    ChallengeSeriesTgmPlusItem = 40,
    TgmPlusSecretGrade = 41,
    DeathSecretGrade = 42,
    CarnivalOfDoom = 45,
    Tgm120gSeriesOf3 = 50,
    TapMasterBigItemEvent = 51,
    TapMasterBigItem = 52,
    SegatetTheGranEvent = 53,
    Tgm1BigCaravanEvent = 55,
    Tgm1AvoidTheSingle = 61,
}

// ...could be better
impl Mode {
    pub fn from_i32(mode_id: i32) -> Option<Self> {
        match mode_id {
            1 => Some(Mode::Tgm1Master),
            2 => Some(Mode::Tgm120g),
            3 => Some(Mode::Tgm1Big),
            4 => Some(Mode::Tgm1Reverse),
            5 => Some(Mode::Tgm1SecretGrade),
            6 => Some(Mode::TapMaster),
            7 => Some(Mode::TapDeath),
            8 => Some(Mode::TapDeathSeries),
            9 => Some(Mode::TapDoubles),
            10 => Some(Mode::TapBig),
            11 => Some(Mode::TapItem),
            12 => Some(Mode::TapNormal),
            13 => Some(Mode::TapTgmplus),
            14 => Some(Mode::TapSecretGrade),
            15 => Some(Mode::TiMasterClassic),
            16 => Some(Mode::TiShiraseClassic),
            17 => Some(Mode::TiEasyClassic),
            18 => Some(Mode::TiSakuraClassic),
            19 => Some(Mode::TiMasterSecretGradeClassic),
            20 => Some(Mode::TiMasterWorld),
            21 => Some(Mode::TiShiraseWorld),
            22 => Some(Mode::TiEasyWorld),
            23 => Some(Mode::TiSakuraWorld),
            24 => Some(Mode::TiMasterSecretGradeWorld),
            25 => Some(Mode::TiBigClassic),
            26 => Some(Mode::TiBigWorld),
            27 => Some(Mode::TiShiraseSecretGradeClassic),
            28 => Some(Mode::TiShiraseSecretGradeWorld),
            29 => Some(Mode::Tgm1ScoreAttack),
            30 => Some(Mode::TiQualifiedMasterClassic),
            31 => Some(Mode::TiQualifiedMasterWorld),
            38 => Some(Mode::TapBigNormal),
            39 => Some(Mode::TapTgmPlusItem),
            40 => Some(Mode::ChallengeSeriesTgmPlusItem),
            41 => Some(Mode::TgmPlusSecretGrade),
            42 => Some(Mode::DeathSecretGrade),
            45 => Some(Mode::CarnivalOfDoom),
            50 => Some(Mode::Tgm120gSeriesOf3),
            51 => Some(Mode::TapMasterBigItemEvent),
            52 => Some(Mode::TapMasterBigItem),
            53 => Some(Mode::SegatetTheGranEvent),
            55 => Some(Mode::Tgm1BigCaravanEvent),
            61 => Some(Mode::Tgm1AvoidTheSingle),
            _ => None,
        }
    }
}
