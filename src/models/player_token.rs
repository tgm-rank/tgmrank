use time::OffsetDateTime;

use crate::models::user_token_type::UserTokenType;

#[derive(Debug)]
pub struct PlayerTokenEntity {
    pub token_id: i32,
    pub player_id: i32,
    pub token_type: UserTokenType,
    pub token: String,
    pub expiration_date: OffsetDateTime,
}

impl PlayerTokenEntity {
    pub fn verify(&self, token_string: &str, token_type: &UserTokenType) -> bool {
        let now = OffsetDateTime::now_utc();
        token_type == &self.token_type && token_string == self.token && now <= self.expiration_date
    }
}

#[derive(Debug)]
pub struct NewPlayerTokenEntity {
    pub player_id: i32,
    pub token_type: UserTokenType,
    pub token: String,
    pub expiration_date: OffsetDateTime,
}

#[cfg(test)]
mod tests {
    use time::{Duration, OffsetDateTime};

    use crate::models::player_token::PlayerTokenEntity;
    use crate::models::user_token_type::UserTokenType;

    fn make_player_token() -> PlayerTokenEntity {
        PlayerTokenEntity {
            token_id: 0,
            player_id: 0,
            token_type: UserTokenType::ForgotPassword,
            token: String::from("1234"),
            expiration_date: OffsetDateTime::now_utc() + Duration::days(1),
        }
    }

    #[test]
    fn verify_should_return_true_when_token_is_not_expired_and_token_with_type_matches() {
        let token = make_player_token();
        assert_eq!(token.verify(&token.token, &token.token_type), true);
    }

    #[test]
    fn verify_should_return_false_if_already_expired() {
        let mut token = make_player_token();
        token.expiration_date = OffsetDateTime::now_utc() - Duration::days(1);

        assert_eq!(token.verify(&token.token, &token.token_type), false);
    }

    #[test]
    fn verify_should_return_false_if_token_is_different_type() {
        let input_token_type = UserTokenType::Activation;
        let token = make_player_token();

        assert_eq!(token.verify(&token.token, &input_token_type), false);
    }

    #[test]
    fn verify_should_return_false_if_token_value_does_not_match() {
        let input_token = String::from("asdf");

        let mut token = make_player_token();
        token.token = String::from("123");

        assert_eq!(token.verify(&input_token, &token.token_type), false);
    }
}
