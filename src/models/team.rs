use crate::models::PlayerEntity2;

#[derive(Debug, Clone)]
pub struct TeamEntity {
    pub team_id: i32,
    pub name: String,
    pub members: Vec<PlayerEntity2>,
}
