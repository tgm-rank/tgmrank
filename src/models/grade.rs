use crate::models::grade_line_type::GradeLine;

#[derive(Debug, Clone, Eq, PartialEq, Deserialize, sqlx::Type)]
pub struct GradeEntity {
    pub grade_id: i32,
    pub grade_display: String,
    pub mode_id: i32,
    pub line: Option<GradeLine>,
    pub sort_order: i32,
}
