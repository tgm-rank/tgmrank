use std::fmt;
use std::str::FromStr;

#[derive(Debug, Eq, PartialEq, Clone, Deserialize, sqlx::Type)]
#[serde(rename_all = "snake_case")]
#[sqlx(type_name = "varchar")]
// TODO: Reverted back to using VARCHAR until https://github.com/launchbadge/sqlx/issues/1920 is fixed
#[sqlx(rename_all = "snake_case")]
pub enum GradeLine {
    Green,
    Orange,
}

impl fmt::Display for GradeLine {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(match *self {
            GradeLine::Green => "green",
            GradeLine::Orange => "orange",
        })
    }
}

impl FromStr for GradeLine {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<GradeLine, &'static str> {
        match s.to_lowercase().as_str() {
            "orange" => Ok(GradeLine::Orange),
            "green" => Ok(GradeLine::Green),
            _ => Err("Unrecognized enum variant"),
        }
    }
}
