use crate::models::ProofType;

#[derive(Debug, Clone, Eq, PartialEq, Deserialize, sqlx::Type)]
pub struct ProofEntity {
    pub proof_id: i32,
    pub score_id: i32,
    pub proof_type: ProofType,
    pub link: String,
}

pub struct NewProofEntity<'a> {
    pub score_id: i32,
    pub proof_type: &'a ProofType,
    pub link: &'a str,
}
