use time::{Date, OffsetDateTime, Time};

use crate::models::{GradeEntity, PlatformEntity, PlayerEntity2, ProofEntity, ScoreStatus};
use sqlx::types::Json;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ScoreEntity {
    pub score_id: i32,
    pub grade_id: Option<i32>,
    pub level: Option<i32>,
    pub playtime: Option<Time>,
    pub score: Option<i32>,
    pub status: ScoreStatus,
    pub comment: Option<String>,
    pub player_id: i32,
    pub mode_id: i32,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime,
    pub updated_by: i32,
    pub platform_id: Option<i32>,
}

#[derive(Debug, Deserialize)]
pub struct VerificationEntity {
    pub verification_comment: Option<String>,
    pub verified_by: Json<PlayerEntity2>,
    pub verified_on: OffsetDateTime,
}

#[derive(Debug, Clone)]
pub struct FullScoreEntity {
    pub score_id: i32,
    pub grade: Option<GradeEntity>,
    pub level: Option<i32>,
    pub playtime: Option<Time>,
    pub score: Option<i32>,
    pub status: ScoreStatus,
    pub comment: Option<String>,
    pub player: PlayerEntity2,
    pub mode_id: i32,
    pub created_at: OffsetDateTime,
    pub updated_at: OffsetDateTime,
    pub updated_by: i32,
    pub platform: Option<PlatformEntity>,
    pub verification_comment: Option<String>,
    pub verified_on: Option<OffsetDateTime>,
    pub verifier: Option<PlayerEntity2>,
    pub proof: Option<Vec<ProofEntity>>,
}

#[derive(Debug)]
pub struct NewScore {
    pub grade_id: Option<i32>,
    pub level: Option<i32>,
    pub playtime: Option<Time>,
    pub score: Option<i32>,
    pub status: ScoreStatus,
    pub comment: Option<String>,
    pub player_id: i32,
    pub mode_id: i32,
    pub updated_by: i32,
    pub platform_id: Option<i32>,
}

pub struct UpdateScoreEntity {
    pub score_id: i32,
    pub comment: Option<String>,
    pub updated_by: i32,
    pub platform_id: Option<i32>,
}

pub struct VerifyScoreEntity<'a> {
    pub status: &'a ScoreStatus,
    pub verification_comment: Option<&'a str>,
    pub verified_by: i32,
}

pub struct ActivityCalendarEntity {
    pub date: Date,
    pub score_count: i64,
    pub score_ids: Vec<i32>,
}
