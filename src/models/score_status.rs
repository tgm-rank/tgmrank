use sqlx::postgres::PgTypeInfo;
use std::convert::TryFrom;
use std::fmt;
use std::str::FromStr;

#[derive(
    Debug,
    Eq,
    PartialEq,
    Ord,
    PartialOrd,
    Hash,
    Clone,
    Serialize,
    Deserialize,
    sqlx::Type,
    postgres_types::ToSql,
)]
#[postgres(name = "score_status")]
#[sqlx(type_name = "score_status")]
#[sqlx(rename_all = "snake_case")]
#[serde(rename_all = "snake_case", try_from = "String")]
pub enum ScoreStatus {
    #[postgres(name = "unverified")]
    Unverified,
    #[postgres(name = "pending")]
    Pending,
    #[postgres(name = "legacy")]
    Legacy,
    #[postgres(name = "rejected")]
    Rejected,
    #[postgres(name = "verified")]
    Verified,
    #[postgres(name = "accepted")]
    Accepted,
}

static ALL_STATUSES: &[ScoreStatus] = &[
    ScoreStatus::Unverified,
    ScoreStatus::Pending,
    ScoreStatus::Legacy,
    ScoreStatus::Rejected,
    ScoreStatus::Verified,
    ScoreStatus::Accepted,
];

impl ScoreStatus {
    pub fn get_all() -> &'static [ScoreStatus] {
        ALL_STATUSES
    }
}

impl fmt::Display for ScoreStatus {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(match *self {
            ScoreStatus::Unverified => "unverified",
            ScoreStatus::Pending => "pending",
            ScoreStatus::Legacy => "legacy",
            ScoreStatus::Rejected => "rejected",
            ScoreStatus::Verified => "verified",
            ScoreStatus::Accepted => "accepted",
        })
    }
}

impl FromStr for ScoreStatus {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<ScoreStatus, &'static str> {
        match s.to_lowercase().as_str() {
            "unverified" => Ok(ScoreStatus::Unverified),
            "pending" => Ok(ScoreStatus::Pending),
            "legacy" => Ok(ScoreStatus::Legacy),
            "rejected" => Ok(ScoreStatus::Rejected),
            "verified" => Ok(ScoreStatus::Verified),
            "accepted" => Ok(ScoreStatus::Accepted),
            _ => Err("Unrecognized enum variant"),
        }
    }
}

impl TryFrom<String> for ScoreStatus {
    type Error = &'static str;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let status = Self::from_str(&value)?;
        Ok(status)
    }
}

// Workaround to pass in an array of ScoreStatus to sqlx
// https://github.com/launchbadge/sqlx/issues/298#issuecomment-908511000
// TODO: Evaluate when we can remove this
#[derive(sqlx::Encode)]
pub struct ScoreStatusesWrapper<'a>(pub &'a [ScoreStatus]);

impl sqlx::Type<sqlx::Postgres> for ScoreStatusesWrapper<'_> {
    fn type_info() -> PgTypeInfo {
        PgTypeInfo::with_name("_score_status")
    }
}
