use crate::models::PlayerEntity2;

#[derive(Clone)]
pub struct FullRankingEntity {
    pub rank_id: i32,
    pub game_id: i32,
    pub player_id: i32,
    pub score_count: i32,
    pub ranking: i32,
    pub ranking_points: Option<i32>,
    pub player: PlayerEntity2,
}
