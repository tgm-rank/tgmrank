#[derive(Debug, Clone, Eq, PartialEq, Deserialize)]
pub struct PlatformEntity {
    pub platform_id: i32,
    pub platform_name: String,
    pub mode_id: Option<i32>,
    pub game_id: Option<i32>,
}

pub struct NewPlatformEntity {
    pub platform_name: String,
    pub mode_id: i32,
}

// TODO: Evaluate when we can remove this custom Decode implementation.
// See https://github.com/launchbadge/sqlx/issues/1268 and https://github.com/launchbadge/sqlx/issues/1031
impl ::sqlx::decode::Decode<'_, ::sqlx::Postgres> for PlatformEntity {
    fn decode(
        value: ::sqlx::postgres::PgValueRef<'_>,
    ) -> ::std::result::Result<
        Self,
        ::std::boxed::Box<
            dyn ::std::error::Error + 'static + ::std::marker::Send + ::std::marker::Sync,
        >,
    > {
        let mut decoder = ::sqlx::postgres::types::PgRecordDecoder::new(value)?;
        let platform_id = decoder.try_decode::<i32>()?;
        let platform_name = decoder.try_decode::<String>()?;
        let mode_id = decoder.try_decode::<Option<i32>>()?;
        let game_id = decoder.try_decode::<Option<i32>>()?;
        ::std::result::Result::Ok(PlatformEntity {
            platform_id,
            platform_name,
            mode_id,
            game_id,
        })
    }
}
impl ::sqlx::Type<::sqlx::Postgres> for PlatformEntity {
    fn type_info() -> ::sqlx::postgres::PgTypeInfo {
        ::sqlx::postgres::PgTypeInfo::with_name("PlatformEntity")
    }
}
