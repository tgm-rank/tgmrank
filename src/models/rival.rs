pub struct RivalEntity {
    pub rival_id: i32,
    pub player_id: i32,
    pub rival_player_id: i32,
}
