use time::OffsetDateTime;
use uuid::Uuid;

pub struct Session {
    pub session_id: i32,
    pub session_key: Uuid,
    pub user_id: i32,
    pub expires: OffsetDateTime,
}

impl Session {
    pub fn is_expired(&self) -> bool {
        OffsetDateTime::now_utc() > self.expires
    }
}

pub struct NewSession {
    pub session_key: Uuid,
    pub user_id: i32,
    pub expires: OffsetDateTime,
}

#[cfg(test)]
mod tests {
    use time::Duration;

    use super::*;

    #[test]
    fn is_expired_should_return_true_if_current_datetime_is_past_expiration_datetime() {
        let session = Session {
            session_id: 0,
            session_key: Uuid::new_v4(),
            user_id: 0,
            expires: OffsetDateTime::now_utc() - Duration::seconds(10),
        };

        assert!(session.is_expired());
    }
}
