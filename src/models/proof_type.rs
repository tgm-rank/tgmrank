use std::convert::TryFrom;
use std::fmt;
use std::str::FromStr;

#[derive(Debug, Eq, PartialEq, PartialOrd, Clone, Serialize, Deserialize, Hash, sqlx::Type)]
#[sqlx(type_name = "varchar")]
// TODO: Reverted back to using VARCHAR until https://github.com/launchbadge/sqlx/issues/1920 is fixed
#[sqlx(rename_all = "snake_case")]
#[serde(rename_all = "snake_case", try_from = "String")]
pub enum ProofType {
    Image,
    Video,
    Other,
}

impl fmt::Display for ProofType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(match *self {
            ProofType::Image => "image",
            ProofType::Video => "video",
            ProofType::Other => "other",
        })
    }
}

impl FromStr for ProofType {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<ProofType, &'static str> {
        match s.to_lowercase().as_str() {
            "image" => Ok(ProofType::Image),
            "video" => Ok(ProofType::Video),
            "other" => Ok(ProofType::Other),
            _ => Err("Unrecognized enum variant"),
        }
    }
}

impl TryFrom<String> for ProofType {
    type Error = &'static str;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let status = Self::from_str(&value)?;
        Ok(status)
    }
}
