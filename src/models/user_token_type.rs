use std::fmt;

use std::str::FromStr;

#[derive(Debug, PartialEq, Clone, sqlx::Type)]
#[sqlx(type_name = "user_token_type")]
#[sqlx(rename_all = "snake_case")]
pub enum UserTokenType {
    Activation,
    ForgotPassword,
}

impl fmt::Display for UserTokenType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(match *self {
            UserTokenType::Activation => "activation",
            UserTokenType::ForgotPassword => "forgot_password",
        })
    }
}

impl FromStr for UserTokenType {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<UserTokenType, &'static str> {
        match s.to_lowercase().as_str() {
            "activation" => Ok(UserTokenType::Activation),
            "forgot_password" => Ok(UserTokenType::ForgotPassword),
            _ => Err("Unrecognized enum variant"),
        }
    }
}
