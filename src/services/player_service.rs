use std::thread::sleep;

use actix_web::web;
use itertools::Itertools;
use once_cell::sync::Lazy;
use regex::Regex;

use crate::controllers::models::BadgeModel;
use crate::guards::UserCookie;
use crate::models::BadgeEntity;
use crate::models::{NewBadgeEntity, PlayerEntity2, PlayerSettingsEntity};
use crate::modules::DbConnection;
use crate::utility::TgmRankError::ScoreValidationRuleError;
use crate::utility::{
    AuthenticationError, ClientError, TgmRankError, TgmRankResult, ValidationRuleError,
};
use crate::{
    controllers::models::PlayerBadgeModel,
    models::{
        LocationEntity, NewPlayerEntity, NewPlayerTokenEntity, PlayerEntity, PlayerTokenEntity,
        RivalEntity, Role, UserTokenType,
    },
};
use crate::{controllers::models::PlayerUsernameHistoryModel, services::LoginService};
use crate::{
    controllers::models::{AccountAchievementModel, PlayerModel},
    models::AccountAchievementEntity,
};
use time::{Duration, OffsetDateTime};

static USERNAME_CHANGE_TIMEOUT_DURATION: Lazy<time::Duration> = Lazy::new(|| Duration::days(60));
pub const MAX_RIVALS: usize = 5;

pub struct PlayerService();

impl PlayerService {
    pub async fn get_all(db: &mut DbConnection) -> TgmRankResult<Vec<PlayerModel>> {
        let players = PlayerEntity::get_all(db)
            .await?
            .into_iter()
            .sorted_by_key(|p| p.player_name.to_string().to_lowercase())
            .map(|p| PlayerModel {
                player_id: p.player_id,
                player_name: p.player_name,
                avatar: p.avatar_file,
                games: None,
                location: p.location,
            })
            .collect();

        Ok(players)
    }

    pub async fn get_all_with_scores(db: &mut DbConnection) -> TgmRankResult<Vec<PlayerModel>> {
        let players = PlayerEntity2::get_all_with_scores(db).await?;
        let player_models = players.into_iter().map(PlayerModel::from).collect();

        Ok(player_models)
    }

    pub async fn find_by_id(db: &mut DbConnection, id: i32) -> TgmRankResult<PlayerEntity> {
        PlayerEntity::get_by_id(db, id).await
    }

    pub async fn find_player(
        db: &mut DbConnection,
        username_or_email: &str,
        allow_email: bool,
    ) -> TgmRankResult<PlayerEntity> {
        PlayerEntity::get_by_name(db, username_or_email, allow_email).await
    }

    pub async fn player_authenticated(
        db: &mut DbConnection,
        username: &str,
        password: &str,
    ) -> TgmRankResult<PlayerEntity> {
        let player = Self::find_player(db, username, true)
            .await
            .map_err(|_| AuthenticationError::InvalidUsername)?;

        let result = Self::check_password_hash(player, password.to_string()).await;

        if result.is_err() {
            web::block(move || {
                sleep(std::time::Duration::from_millis(1000));
            })
            .await
            .map_err(TgmRankError::from_std)?;
        }

        result
    }

    pub async fn check_password_hash(
        player: PlayerEntity,
        password_hash: String,
    ) -> TgmRankResult<PlayerEntity> {
        if player.password.trim().is_empty() {
            return Err(AuthenticationError::AccountIncomplete.into());
        }

        if !player.player_role.can_log_in() {
            return Err(AuthenticationError::AccountBanned.into());
        }

        let stored_password_hash = player.password.clone();
        let password_hashes_match = web::block(move || -> TgmRankResult<bool> {
            bcrypt::verify(&password_hash, &stored_password_hash)
                .map_err(|e| AuthenticationError::BcryptError(e).into())
        })
        .await
        .map_err(TgmRankError::from_std)??;

        if !password_hashes_match {
            return Err(AuthenticationError::InvalidPassword.into());
        }

        Ok(player)
    }

    pub async fn register(
        db: &mut DbConnection,
        player: NewPlayerEntity,
    ) -> TgmRankResult<PlayerEntity> {
        let player_by_name = Self::find_player(db, &player.player_name, false).await;
        if player_by_name.is_ok() {
            return Err(AuthenticationError::AccountDuplicate.into());
        }

        if let Some(email) = player.email.as_deref() {
            let player_by_email = Self::find_player(db, email, true).await;
            if player_by_email.is_ok() {
                return Err(AuthenticationError::AccountDuplicate.into());
            }
        }

        PlayerEntity::insert(db, player)
            .await
            .map_err(|_| AuthenticationError::RegistrationError.into())
    }

    fn filter_username_history(account_history: &[PlayerEntity]) -> Vec<&PlayerEntity> {
        let mut history: Vec<&PlayerEntity> = account_history
            .iter()
            .tuple_windows::<(_, _)>()
            .filter(|(a, b)| a.player_name != b.player_name)
            .map(|(a, _)| a)
            .collect();

        if let Some(last) = account_history.last() {
            history.push(last);
        }

        history
    }

    fn is_able_to_update_username(account_history: &[PlayerEntity]) -> bool {
        let username_history = Self::filter_username_history(account_history);

        if username_history.len() > 1 {
            let last_username_change_datetime = Self::filter_username_history(account_history)
                .first()
                .map(|a| a.updated_at);

            if let Some(last_username_change_datetime) = last_username_change_datetime {
                return OffsetDateTime::now_utc() - last_username_change_datetime
                    > *USERNAME_CHANGE_TIMEOUT_DURATION;
            }
        }

        true
    }

    pub async fn get_username_history(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<Vec<PlayerUsernameHistoryModel>> {
        let history = PlayerEntity::account_history(db, player_id).await?;
        let history = Self::filter_username_history(&history);

        let history_model = history
            .into_iter()
            .map(PlayerUsernameHistoryModel::from)
            .collect_vec();

        Ok(history_model)
    }

    pub async fn update_username(
        db: &mut DbConnection,
        player: PlayerEntity,
        username: &str,
    ) -> TgmRankResult<PlayerEntity> {
        if player.player_name == username {
            return Err(ClientError::UsernameUnchanged.into());
        }

        let history = PlayerEntity::account_history(db, player.player_id).await?;

        if !Self::is_able_to_update_username(&history) {
            return Err(ClientError::TooFrequentUsernameChange.into());
        }

        let player = PlayerEntity {
            player_name: username.to_string(),
            ..player
        };

        PlayerEntity::update(db, player).await
    }

    pub async fn update_avatar(
        db: &mut DbConnection,
        player: PlayerEntity,
        avatar_file: &str,
    ) -> TgmRankResult<PlayerEntity> {
        let player = PlayerEntity {
            avatar_file: Some(avatar_file.to_string()),
            ..player
        };
        PlayerEntity::update(db, player).await
    }

    pub async fn update_email(
        db: &mut DbConnection,
        player: PlayerEntity,
        new_email: &str,
    ) -> TgmRankResult<PlayerEntity> {
        let player = PlayerEntity {
            email: Some(new_email.to_string()),
            ..player
        };
        PlayerEntity::update(db, player).await
    }

    pub async fn update_password(
        db: &mut DbConnection,
        player: PlayerEntity,
        new_password: &str,
    ) -> TgmRankResult<PlayerEntity> {
        if new_password.trim().is_empty() {
            return Err(AuthenticationError::InvalidPassword.into());
        }

        let hashed_password = PlayerService::gen_password(new_password);
        let player = PlayerEntity {
            password: hashed_password,
            ..player
        };

        PlayerEntity::update(db, player).await
    }

    pub async fn update_role(
        db: &mut DbConnection,
        player: PlayerEntity,
        new_role: Role,
    ) -> TgmRankResult<PlayerEntity> {
        let player = PlayerEntity {
            player_role: new_role,
            ..player
        };

        let player = PlayerEntity::update(db, player).await?;
        LoginService::delete_user_sessions(db, player.player_id, &[]).await?;

        Ok(player)
    }

    pub async fn update_location(
        db: &mut DbConnection,
        player: PlayerEntity,
        location_code: Option<&str>,
    ) -> TgmRankResult<PlayerEntity> {
        let new_location_id = if let Some(code) = location_code {
            Some(
                LocationEntity::get_by_alpha_code(db, code)
                    .await
                    .map_err(|e| {
                        ScoreValidationRuleError(
                            ValidationRuleError::CustomFieldError {
                                field_name: "newLocation".to_string(),
                                message: e.to_string(),
                            }
                            .into(),
                        )
                    })?
                    .location_id,
            )
        } else {
            None
        };

        let player = PlayerEntity {
            location_id: new_location_id,
            ..player
        };

        let player = PlayerEntity::update(db, player).await?;
        Ok(player)
    }

    pub async fn get_settings(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<PlayerSettingsEntity> {
        PlayerEntity::get_settings(db, player_id).await
    }

    pub async fn update_settings(
        db: &mut DbConnection,
        player_id: i32,
        settings: &PlayerSettingsEntity,
    ) -> TgmRankResult<PlayerEntity> {
        PlayerEntity::update_settings(db, player_id, settings).await
    }

    pub fn gen_password(password: &str) -> String {
        bcrypt::hash(password, bcrypt::DEFAULT_COST).unwrap()
    }

    pub async fn get_forgot_password_token(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<PlayerTokenEntity> {
        PlayerTokenEntity::get_by_player(db, player_id, &UserTokenType::ForgotPassword).await
    }

    pub fn gen_token(
        player_id: i32,
        some_string: &str,
        token_type: UserTokenType,
    ) -> NewPlayerTokenEntity {
        let bcrypt_prefix_re: Regex = Regex::new(r"^\$.{2}\$\d+\$").unwrap();
        let punctuation_remover_re: Regex = Regex::new(r"[./$]").unwrap();

        let raw_token = bcrypt::hash(some_string, bcrypt::DEFAULT_COST).unwrap();
        let raw_token = bcrypt_prefix_re.replace_all(&raw_token, "").to_string();
        let token = punctuation_remover_re
            .replace_all(&raw_token, "")
            .to_string();
        let expiration_date = OffsetDateTime::now_utc() + Duration::days(1);

        NewPlayerTokenEntity {
            player_id,
            token_type,
            token,
            expiration_date,
        }
    }

    pub async fn upsert_token(
        db: &mut DbConnection,
        token: NewPlayerTokenEntity,
    ) -> TgmRankResult<PlayerTokenEntity> {
        PlayerTokenEntity::upsert(db, token).await
    }

    pub async fn delete_token(
        db: &mut DbConnection,
        token: PlayerTokenEntity,
    ) -> TgmRankResult<usize> {
        let deleted = PlayerTokenEntity::delete(db, token).await?;
        Ok(deleted.len())
    }

    pub async fn get_rivals(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<Vec<PlayerModel>> {
        let rivals = RivalEntity::get_by_player_id(db, player_id).await?;
        let player_ids = rivals.into_iter().map(|r| r.rival_player_id).collect_vec();

        Self::get_players_by_ids_in_order(db, &player_ids).await
    }

    pub async fn get_reverse_rivals(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<Vec<PlayerModel>> {
        let rivals = RivalEntity::get_reverse_rivals_by_player_id(db, player_id).await?;
        let player_ids = rivals.into_iter().map(|r| r.player_id).collect_vec();

        Self::get_players_by_ids_in_order(db, &player_ids).await
    }

    async fn get_players_by_ids_in_order(
        db: &mut DbConnection,
        player_ids: &[i32],
    ) -> TgmRankResult<Vec<PlayerModel>> {
        let player_entities = PlayerEntity::get_by_ids(db, player_ids).await?;

        let player_models = player_ids
            .iter()
            .filter_map(|player_id| {
                player_entities
                    .iter()
                    .find(|entity| &entity.player_id == player_id)
                    .cloned()
            })
            .map_into()
            .collect();

        Ok(player_models)
    }

    pub async fn add_rival(
        db: &mut DbConnection,
        player_id: i32,
        rival_player_id: i32,
    ) -> TgmRankResult<PlayerModel> {
        let current_rivals = Self::get_rivals(db, player_id).await?;
        if current_rivals.len() >= MAX_RIVALS {
            return Err(ClientError::RivalLimitError(MAX_RIVALS).into());
        }

        let rival = RivalEntity::insert(
            db,
            RivalEntity {
                rival_id: 0,
                player_id,
                rival_player_id,
            },
        )
        .await?;

        let player = PlayerEntity::get_by_id(db, rival.rival_player_id).await?;

        Ok(player.into())
    }

    pub async fn delete_rival(
        db: &mut DbConnection,
        player_id: i32,
        rival_player_id: i32,
    ) -> TgmRankResult<RivalEntity> {
        let deleted = RivalEntity::delete_by_player_ids(db, player_id, rival_player_id).await?;

        Ok(deleted)
    }

    pub async fn get_achievements(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<Vec<AccountAchievementModel>> {
        let achievements =
            AccountAchievementEntity::get_achievements_for_players(db, &[player_id]).await?;
        Ok(achievements
            .into_iter()
            .map(AccountAchievementModel::from)
            .collect_vec())
    }

    pub async fn get_badges(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<PlayerBadgeModel> {
        let badges = BadgeEntity::get_badges_for_player(db, player_id).await?;

        Ok(PlayerBadgeModel {
            badges: badges.into_iter().map(|b| b.name).collect(),
        })
    }

    pub async fn get_all_badges(db: &mut DbConnection) -> TgmRankResult<Vec<BadgeModel>> {
        let badges = BadgeEntity::get_all(db).await?;
        Ok(badges.into_iter().map(BadgeModel::from).collect_vec())
    }

    pub async fn add_badge_for_player(
        user: &UserCookie,
        db: &mut DbConnection,
        player_id: i32,
        badge_id: i32,
    ) -> TgmRankResult<()> {
        BadgeEntity::add_badge_for_player(user, db, player_id, badge_id).await?;
        Ok(())
    }

    pub async fn remove_badge_for_player(
        db: &mut DbConnection,
        player_id: i32,
        badge_id: i32,
    ) -> TgmRankResult<()> {
        BadgeEntity::remove_badge(db, player_id, badge_id).await?;
        Ok(())
    }

    pub async fn create_badge(
        user: &UserCookie,
        db: &mut DbConnection,
        badge_name: &str,
    ) -> TgmRankResult<BadgeModel> {
        let badge = BadgeEntity::insert_badge(
            db,
            &NewBadgeEntity {
                name: badge_name,
                updated_by: user.user_id,
            },
        )
        .await?;
        Ok(BadgeModel::from(badge))
    }
}

#[cfg(test)]
mod tests {
    use super::{Duration, OffsetDateTime};
    use super::{PlayerEntity, PlayerService, USERNAME_CHANGE_TIMEOUT_DURATION};

    mod is_able_to_update_username {
        use super::*;

        #[test]
        fn should_return_true_when_history_has_fewer_than_2_elements() {
            let history = vec![];
            assert_eq!(PlayerService::is_able_to_update_username(&history), true);

            let history = vec![PlayerEntity::default()];
            assert_eq!(PlayerService::is_able_to_update_username(&history), true);
        }

        #[test]
        fn should_return_true_when_history_has_no_username_changes_even_within_timeout_range() {
            let history = vec![
                PlayerEntity {
                    player_name: "hello".to_string(),
                    updated_at: OffsetDateTime::now_utc() - Duration::days(1),
                    ..Default::default()
                },
                PlayerEntity {
                    player_name: "hello".to_string(),
                    updated_at: OffsetDateTime::now_utc() - Duration::days(2),
                    ..Default::default()
                },
            ];
            assert_eq!(PlayerService::is_able_to_update_username(&history), true);
        }

        #[test]
        fn should_return_false_when_history_has_username_change_from_within_timeout_range() {
            let history = vec![
                PlayerEntity {
                    player_name: "second".to_string(),
                    updated_at: OffsetDateTime::now_utc() - *USERNAME_CHANGE_TIMEOUT_DURATION
                        + Duration::seconds(1),
                    ..Default::default()
                },
                PlayerEntity {
                    player_name: "first".to_string(),
                    updated_at: OffsetDateTime::now_utc() - Duration::days(200),
                    ..Default::default()
                },
            ];
            assert_eq!(PlayerService::is_able_to_update_username(&history), false);
        }

        #[test]
        fn should_return_true_when_history_has_username_change_from_outside_timeout_range() {
            let history = vec![
                PlayerEntity {
                    player_name: "second".to_string(),
                    updated_at: OffsetDateTime::now_utc()
                        - *USERNAME_CHANGE_TIMEOUT_DURATION
                        - Duration::milliseconds(100),
                    ..Default::default()
                },
                PlayerEntity {
                    player_name: "first".to_string(),
                    updated_at: OffsetDateTime::now_utc() - Duration::days(200),
                    ..Default::default()
                },
            ];
            assert_eq!(PlayerService::is_able_to_update_username(&history), true);
        }
    }
}
