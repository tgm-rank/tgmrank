use fnv::FnvHashMap;
use itertools::Itertools;
use time::Date;

use crate::models::ModeEntity;
use crate::models::ModeRankingEntity;
use crate::models::RecentActivityType;
use crate::models::{filters::RecentActivityFilter, RecentActivityEntity};
use crate::models::{ActivityCalendarEntity, PlayerEntity};
use crate::modules::DbConnection;
use crate::modules::DbConnection2;
use crate::repositories::RecentActivityRepository;
use crate::services::RankingService;
use crate::utility::{TgmRankError, TgmRankResult};
use crate::{
    controllers::models::{
        ActivityCalendarModel, ModeRankingModel, PlayerModel, RankDeltaModel, RankDiffModel,
        RecentActivityModel,
    },
    models::CombinedRecentActivity,
};

pub struct RecentActivityService {}

impl RecentActivityService {
    pub async fn update_recent_activity_by_mode_group(
        db: &mut DbConnection,
        mode_group: String,
        score_ids: &[i32],
    ) -> TgmRankResult<()> {
        RecentActivityRepository::update_recent_activity_by_mode_group(db, &mode_group, score_ids)
            .await?;
        Ok(())
    }

    pub async fn update_recent_activity(
        db: &mut DbConnection,
        mode_ids: &[i32],
        score_ids: &[i32],
    ) -> TgmRankResult<()> {
        RecentActivityRepository::update_recent_activity(db, mode_ids, score_ids).await?;
        Ok(())
    }

    pub async fn get_score_ids(
        db: &DbConnection2<'_>,
        filter: &RecentActivityFilter,
    ) -> TgmRankResult<Vec<i32>> {
        RecentActivityRepository::get_score_ids(&db, filter).await
    }

    pub async fn get_recent_activity(
        db: &mut DbConnection,
        score_ids: &[i32],
        filter: &RecentActivityFilter,
    ) -> TgmRankResult<Vec<RecentActivityModel>> {
        let recent_activity = Self::get_recent_activity_internal(db, filter, &score_ids).await?;
        Ok(recent_activity)
    }

    async fn make_score_dictionary(
        db: &mut DbConnection,
        recent_activity: &[RecentActivityEntity],
        filter: &RecentActivityFilter,
    ) -> TgmRankResult<FnvHashMap<i32, ModeRankingModel>> {
        let score_ids = recent_activity
            .iter()
            .map(|a| a.current_score_id)
            .chain(recent_activity.iter().filter_map(|a| a.prev_score_id))
            .collect_vec();

        let scores = ModeRankingEntity::get_ranked_scores(db, &score_ids, false).await?;
        let scores = RankingService::make_mode_ranking(db, scores, &filter.filter, true).await?;

        Ok(scores.into_iter().map(|r| (r.score_id, r)).collect())
    }

    async fn make_passed_players_dictionary(
        db: &mut DbConnection,
        recent_activity: &[RecentActivityEntity],
    ) -> TgmRankResult<FnvHashMap<i32, PlayerEntity>> {
        let player_ids = recent_activity
            .iter()
            .flat_map(|a| -> &[i32] { a.passed_player_ids.as_ref() })
            .copied()
            .collect_vec();

        let players = PlayerEntity::get_by_ids(db, player_ids.as_ref()).await?;

        Ok(players.into_iter().map(|p| (p.player_id, p)).collect())
    }

    async fn get_recent_activity_internal(
        db: &mut DbConnection,
        filter: &RecentActivityFilter,
        score_ids: &[i32],
    ) -> TgmRankResult<Vec<RecentActivityModel>> {
        let mode_ids =
            ModeEntity::get_ranked_mode_ids(db, filter.filter.mode.mode_ids.as_deref()).await?;

        Self::update_recent_activity(db, &mode_ids, score_ids).await?;

        let recent_activity =
            RecentActivityRepository::recent_activity(db, score_ids, &mode_ids).await?;

        let score_dictionary = Self::make_score_dictionary(db, &recent_activity, filter).await?;
        let player_dictionary = Self::make_passed_players_dictionary(db, &recent_activity).await?;

        let mut combined_recent_activity = vec![];
        for (score_id, mut ra) in recent_activity
            .into_iter()
            .chunk_by(|ra| ra.current_score_id)
            .into_iter()
        {
            combined_recent_activity.push(CombinedRecentActivity {
                mode_activity: ra
                    .find(|ra| ra.activity_type == RecentActivityType::Mode)
                    .ok_or(TgmRankError::GenericError {
                        message: format!("Mode recent activity is missing for {}", score_id),
                    })?,
                game_activity: ra
                    .find(|ra| ra.activity_type == RecentActivityType::Game)
                    .ok_or(TgmRankError::GenericError {
                        message: format!("Game recent activity is missing for {}", score_id),
                    })?,
                overall_activity: ra
                    .find(|ra| ra.activity_type == RecentActivityType::Overall)
                    .ok_or(TgmRankError::GenericError {
                        message: format!("Overall recent activity is missing for {}", score_id),
                    })?,
            });
        }

        let mut recent_activity = Vec::new();
        for activity in combined_recent_activity {
            let current_score = score_dictionary
                .get(&activity.mode_activity.current_score_id)
                .ok_or_else(|| TgmRankError::GenericError {
                    message: "Score could not be found for recent activity".into(),
                })?;

            let prev_score = if let Some(prev_score_id) = activity.mode_activity.prev_score_id {
                Some(score_dictionary.get(&prev_score_id).ok_or_else(|| {
                    TgmRankError::GenericError {
                        message: "Prev score could not be found for recent activity".into(),
                    }
                })?)
            } else {
                None
            };

            let current_score = ModeRankingModel {
                rank: activity.mode_activity.current_ranking,
                ranking_points: activity.mode_activity.current_ranking_points,
                ..current_score.clone()
            };

            let prev_score = prev_score.map(|s| ModeRankingModel {
                rank: activity.mode_activity.prev_ranking,
                ranking_points: activity.mode_activity.prev_ranking_points,
                ..s.clone()
            });

            recent_activity.push(RecentActivityModel {
                game_id: activity.game_activity.id,
                mode_id: activity.mode_activity.id,
                score: current_score,
                previous_score: prev_score,
                delta: RankDeltaModel {
                    overall: RankDiffModel {
                        players_offset: Some(Self::map_passed_players(
                            &activity.overall_activity.passed_player_ids,
                            &player_dictionary,
                        )),
                        ..RankDiffModel::from(activity.overall_activity)
                    },
                    game: RankDiffModel {
                        players_offset: Some(Self::map_passed_players(
                            &activity.game_activity.passed_player_ids,
                            &player_dictionary,
                        )),
                        ..RankDiffModel::from(activity.game_activity)
                    },
                    mode: RankDiffModel {
                        players_offset: Some(Self::map_passed_players(
                            &activity.mode_activity.passed_player_ids,
                            &player_dictionary,
                        )),
                        ..RankDiffModel::from(activity.mode_activity)
                    },
                },
            });
        }

        Ok(recent_activity)
    }

    fn map_passed_players(
        passed_player_ids: &[i32],
        player_dictionary: &FnvHashMap<i32, PlayerEntity>,
    ) -> Vec<PlayerModel> {
        passed_player_ids
            .iter()
            .map(|player_id| {
                PlayerModel::from(
                    player_dictionary
                        .get(player_id)
                        .cloned()
                        .expect("Could not find player for player id for recent activity"),
                )
            })
            .collect_vec()
    }

    pub async fn get_calendar(
        db: &mut DbConnection,
        from: &Date,
        to: &Date,
    ) -> TgmRankResult<Vec<ActivityCalendarModel>> {
        let activity = ActivityCalendarEntity::get_calendar(db, from, to).await?;
        let activity_models = activity
            .into_iter()
            .map(ActivityCalendarModel::from)
            .collect_vec();

        Ok(activity_models)
    }
}

#[cfg(test)]
mod tests {}
