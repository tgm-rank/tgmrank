use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

use crate::models::{PlayerEntity, ScoreEntity};
use crate::utility::TgmRankResult;

#[derive(Debug)]
pub enum CacheEvent<'a> {
    PlayerUpdate(&'a PlayerEntity),
    ScoreUpdate(&'a ScoreEntity),
    Initialize,
    Flush,
}

pub struct StaticCache {
}

impl StaticCache {
    pub async fn notify(&self, event: CacheEvent<'_>) -> TgmRankResult<()> {
        match &event {
            CacheEvent::PlayerUpdate(p) => {
                info!("Cache: Handling player Update Event {}", p.player_id)
            }
            CacheEvent::ScoreUpdate(s) => info!("Cache: Handling score update {}", s.score_id),
            CacheEvent::Initialize => info!("Cache: Handling initialize"),
            CacheEvent::Flush => info!("Cache: Handling flush event"),
        }

        // if let Some(service) = &self.cloudflare_service {
        //     service.on_notify(&event).await?;
        // }

        Ok(())
    }
}

pub trait CacheKey: Hash {
    fn prefix() -> &'static str;
    fn get_params(&self) -> Vec<u8> {
        vec![]
    }

    fn key(&self) -> Vec<u8> {
        let mut hasher = DefaultHasher::new();
        self.hash(&mut hasher);
        let key_hash = hasher.finish();

        let mut cache_key = Self::prefix().as_bytes().to_vec();
        cache_key.append(&mut self.get_params());
        cache_key.append(&mut key_hash.to_be_bytes().to_vec());

        cache_key
    }
}
