use lettre::message::Mailbox;
use lettre::transport::smtp::authentication::{Credentials, Mechanism};
use lettre::transport::smtp::extension::ClientId;
use lettre::transport::stub::AsyncStubTransport;
use lettre::{AsyncSmtpTransport, AsyncTransport, Message, Tokio1Executor};

use crate::models::{PlayerEntity, SentMail};
use crate::modules::Settings;
use crate::utility::{TgmRankError, TgmRankResult, UriEncodedString};
use sqlx::PgConnection;

pub struct EmailService();

impl EmailService {
    pub async fn send_forgot_password_email(
        transaction: &mut PgConnection,
        settings: &Settings,
        player: &PlayerEntity,
        token_str: &str,
    ) -> TgmRankResult<()> {
        let email = player.email.as_ref().unwrap();
        const FORGOT_PASSWORD_SUBJECT: &str = "Password Reset for theabsolute.plus";
        let forgot_email_body = format!(
            "Hi {0},\n\nClick this link to reset your password for theabsolute.plus: {1}/reset-password/{2}/{3}\n\nThis link will expire in 24 hours.",
            player.player_name, settings.application.external_url, UriEncodedString(&player.player_name), &token_str
        );

        Self::send_email(
            settings,
            email.to_string(),
            FORGOT_PASSWORD_SUBJECT.to_string(),
            forgot_email_body.to_string(),
        )
        .await?;

        SentMail {
            email: email.to_string(),
            account_id: Some(player.player_id),
            subject: FORGOT_PASSWORD_SUBJECT.to_string(),
            body: forgot_email_body,
        }
        .insert(transaction)
        .await?;

        Ok(())
    }

    pub async fn send_password_was_reset_email(
        transaction: &mut PgConnection,
        settings: &Settings,
        player: &PlayerEntity,
    ) -> TgmRankResult<()> {
        let email = player.email.as_ref().unwrap();
        const PASSWORD_WAS_RESET_SUBJECT: &str = "Your Password For theabsolute.plus Has Changed";
        let password_was_reset_body = format!("Hi {0},\n\nYour password for theabsolute.plus has changed.\n\nIf you did not change your password, then your account may have been compromised. Please reset your password by going to the following link: {1}/forgot-password\n\nIf you did change your password, then you do not need to do anything.\n\nHave a good day!",
                                              player.player_name,
                                              settings
                                                  .application.external_url);

        Self::send_email(
            settings,
            email.to_string(),
            PASSWORD_WAS_RESET_SUBJECT.to_string(),
            password_was_reset_body.to_string(),
        )
        .await?;

        SentMail {
            email: email.to_string(),
            account_id: Some(player.player_id),
            subject: PASSWORD_WAS_RESET_SUBJECT.to_string(),
            body: password_was_reset_body,
        }
        .insert(transaction)
        .await?;

        Ok(())
    }

    pub async fn send_rejected_score_email(
        transaction: &mut PgConnection,
        settings: &Settings,
        account: &PlayerEntity,
        score_id: i32,
    ) -> TgmRankResult<()> {
        let email = account.email.as_ref().unwrap();

        const REJECTION_SUBJECT: &str = "Your Submission on theabsolute.plus Was Rejected";
        let score_rejection_body = format!("Hi {0},\n\nThe following submission on theabsolute.plus was rejected: {1}/score/{2}\n\nPlease review our updated proof policy ({1}/about/proof) for your future submissions.\n\nHave a good day!",
                                           account.player_name,
                                           settings
                                                  .application.external_url, score_id);

        Self::send_email(
            settings,
            email.to_string(),
            REJECTION_SUBJECT.to_string(),
            score_rejection_body.to_string(),
        )
        .await?;

        SentMail {
            email: email.to_string(),
            account_id: Some(account.player_id),
            subject: REJECTION_SUBJECT.to_string(),
            body: score_rejection_body,
        }
        .insert(transaction)
        .await?;

        Ok(())
    }

    async fn send_email(
        settings: &Settings,
        to: String,
        subject: String,
        text: String,
    ) -> TgmRankResult<()> {
        let email = Message::builder()
            .from(Mailbox::new(
                Some("TGM Rank".to_string()),
                settings
                    .email
                    .username
                    .parse()
                    .map_err(TgmRankError::from_std)?,
            ))
            .to(Mailbox::new(
                None,
                to.parse().map_err(TgmRankError::from_std)?,
            ))
            .subject(&subject)
            .body(text.to_string())
            .map_err(TgmRankError::from_std)?;

        if settings.email.disable {
            let client = AsyncStubTransport::new_ok();
            client.send(email).await.map_err(TgmRankError::from_std)?;
        } else {
            let credentials = Credentials::new(
                settings.email.username.clone(),
                settings.email.password.clone(),
            );

            let client = AsyncSmtpTransport::<Tokio1Executor>::relay(&settings.email.server)
                .map_err(TgmRankError::from_std)?
                .credentials(credentials)
                .hello_name(ClientId::Domain(settings.application.get_external_domain()))
                .authentication(vec![Mechanism::Plain])
                .build();

            client.send(email).await.map_err(TgmRankError::from_std)?;
        }

        info!(
            "Sending email to {}\nSubject: {}\nText: {}\n",
            &to, &subject, &text
        );

        Ok(())
    }
}
