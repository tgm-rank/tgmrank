use fnv::FnvHashMap;
use itertools::Itertools;

use crate::controllers::models::{
    GameModel, ModeInformationModel, ModeRankingModel, OverallRankingEntryModel,
};
use crate::models::filters::{
    GameFilter, GameRankingFilter, GenericFilter, ModeRankingFilter, PlayerFilter,
};
use crate::models::{
    FullRankingEntity, FullScoreEntity, GameEntity, GradeEntity, ModeEntity, ModeRankingEntity,
};
use crate::modules::{DbConnection, DbPool};
use crate::utility::{ClientError, TgmRankError, TgmRankResult};

pub struct RankingService {}

impl RankingService {
    pub async fn get_grades(db: &DbPool, mode_id: i32) -> TgmRankResult<Vec<GradeEntity>> {
        let grades = GradeEntity::get_for_mode(&mut *db.acquire().await?, mode_id).await?;
        Ok(grades)
    }

    pub async fn get_detailed_score(
        db: &mut DbConnection,
        score_id: i32,
        filter: &GenericFilter,
    ) -> TgmRankResult<ModeRankingModel> {
        let ranking_entity = ModeRankingEntity::get_score(&mut *db, score_id, filter).await?;

        let score = FullScoreEntity::get_by_ids(db, &[score_id], filter)
            .await?
            .swap_remove(0);

        let score_model = ModeRankingModel::from_components(ranking_entity, score, true);

        Ok(score_model)
    }

    pub async fn get_game_ranking(
        db: &mut DbConnection,
        filter: &GameRankingFilter,
    ) -> TgmRankResult<Vec<OverallRankingEntryModel>> {
        let game_id = filter.id.as_deref().unwrap_or("0");
        let ranking = Self::make_overall_ranking_model(db, game_id, filter).await?;

        Ok(ranking)
    }

    pub async fn get_all_game_rankings(
        db: &mut DbConnection,
        filter: &GameRankingFilter,
    ) -> TgmRankResult<Vec<OverallRankingEntryModel>> {
        let game_ids = GameEntity::get_all(db)
            .await?
            .into_iter()
            .map(|g| g.game_id)
            .collect_vec();

        let rankings = FullRankingEntity::get_for_game(db, &game_ids, filter)
            .await?
            .into_iter()
            .map(OverallRankingEntryModel::from)
            .collect_vec();

        Ok(rankings)
    }

    async fn make_overall_ranking_model(
        db: &mut DbConnection,
        game_id: &str,
        filter: &GameRankingFilter,
    ) -> TgmRankResult<Vec<OverallRankingEntryModel>> {
        let games = GameEntity::get_all(db).await?;
        let game = games
            .iter()
            .find(|g| {
                g.game_id.to_string() == game_id
                    || g.short_name.to_ascii_lowercase() == game_id.to_ascii_lowercase()
            })
            .ok_or(ClientError::InvalidGameId)?;

        let ranking = FullRankingEntity::get_for_game(db, &[game.game_id], filter).await?;

        let aggregated_ranking = ranking
            .into_iter()
            .map(OverallRankingEntryModel::from)
            .collect();

        Ok(aggregated_ranking)
    }

    pub async fn get_mode_ranking(
        db: &mut DbConnection,
        filter: &ModeRankingFilter,
    ) -> TgmRankResult<Vec<ModeRankingModel>> {
        if let Some(mode_id) = filter.id {
            let _mode = ModeEntity::get_by_id(&mut *db, mode_id).await?;
        } else {
            return Err(TgmRankError::GenericClientError {
                message: "Mode id is required".to_string(),
            });
        }

        let ranking = ModeRankingEntity::get_mode_ranking(db, filter).await?;

        let scores = Self::make_mode_ranking(
            db,
            ranking,
            &GenericFilter {
                as_of: filter.as_of,
                ..Default::default()
            },
            true,
        )
        .await?;
        Ok(scores)
    }

    pub async fn make_mode_ranking(
        mut db: &mut DbConnection,
        ranking: Vec<ModeRankingEntity>,
        filter: &GenericFilter,
        include_player: bool,
    ) -> TgmRankResult<Vec<ModeRankingModel>> {
        let score_ids = ranking.iter().map(|r| r.score_id).collect_vec();

        let scores = FullScoreEntity::get_by_ids(&mut db, &score_ids, filter).await?;

        let score_dictionary: FnvHashMap<i32, FullScoreEntity> =
            scores.into_iter().map(|s| (s.score_id, s)).collect();

        let mut scores = vec![];
        for r in ranking.into_iter() {
            if let Some(score) = score_dictionary.get(&r.score_id) {
                scores.push(ModeRankingModel::from_components(
                    r,
                    (*score).clone(),
                    include_player,
                ));
            } else {
                warn!("Score {} was not found for ranking", r.score_id)
            }
        }

        Ok(scores)
    }

    pub async fn get_player_scores(
        db: &mut DbConnection,
        filter: &PlayerFilter,
    ) -> TgmRankResult<Vec<ModeRankingModel>> {
        let player_scores = ModeRankingEntity::get_player_scores(db, filter).await?;
        let models: Vec<ModeRankingModel> = Self::make_mode_ranking(
            db,
            player_scores,
            &GenericFilter {
                as_of: filter.as_of,
                ..Default::default()
            },
            false,
        )
        .await?;
        Ok(models)
    }

    pub async fn get_pbs_for_players(
        db: &mut DbConnection,
        filter: &ModeRankingFilter,
    ) -> TgmRankResult<Vec<ModeRankingModel>> {
        let scores_for_players = ModeRankingEntity::get_mode_ranking(db, filter).await?;
        let models: Vec<ModeRankingModel> = Self::make_mode_ranking(
            db,
            scores_for_players,
            &GenericFilter {
                as_of: filter.as_of,
                ..Default::default()
            },
            true,
        )
        .await?;
        Ok(models)
    }

    pub async fn get_mode_description(
        db: &mut DbConnection,
        mode_id: i32,
    ) -> TgmRankResult<ModeInformationModel> {
        let mode_description = ModeEntity::get_by_id(db, mode_id).await?;
        Ok(mode_description.into())
    }

    pub async fn get_game_modes(
        db: &mut DbConnection,
        _filter: &GameFilter,
    ) -> TgmRankResult<Vec<GameModel>> {
        let games = GameEntity::get_all(db).await?;
        let game_models = games.into_iter().map(GameModel::from).collect_vec();

        Ok(game_models)
    }
}
