use time::OffsetDateTime;

use crate::modules::DbConnection;
use crate::utility::TgmRankResult;
use crate::{models::ScoreEntity, services::score_validation_rules::VALIDATION_MAP};
use crate::{
    models::{GradeEntity, Mode, ModeEntity, NewScore},
    utility::ScoreValidationError,
};

use super::score_validation_rules::VALIDATION_BY_TAGS_MAP;

pub type ValidationFn = fn(&ValidationContext, &NewScore) -> TgmRankResult<()>;

pub struct ValidationContext {
    pub grades: Vec<GradeEntity>,
}

pub struct ScoreValidationService();

impl ScoreValidationService {
    fn validate_mode(mode: &ModeEntity) -> TgmRankResult<()> {
        let now = OffsetDateTime::now_utc();
        if let Some(start) = mode.submission_range_start {
            if start > now {
                return Err(ScoreValidationError::TooEarlySubmission.into());
            }
        }

        if let Some(end) = mode.submission_range_end {
            if end < now {
                return Err(ScoreValidationError::TooLateSubmission.into());
            }
        }

        Ok(())
    }

    async fn validate_duplicate(db: &mut DbConnection, score: &NewScore) -> TgmRankResult<()> {
        if ScoreEntity::is_duplicate(db, score).await? {
            return Err(ScoreValidationError::DuplicateScore.into());
        }

        Ok(())
    }

    pub async fn validate(db: &mut DbConnection, score: &NewScore) -> TgmRankResult<()> {
        // let mode_id = Mode::from_i32(score.mode_id).ok_or(TgmRankError::GenericClientError {
        //     message: "Invalid mode".into(),
        // })?;

        let mode = ModeEntity::get_by_id(db, score.mode_id).await?;
        Self::validate_mode(&mode)?;

        Self::validate_duplicate(db, score).await?;

        let grades = GradeEntity::get_for_mode(db, score.mode_id).await?;
        let context = ValidationContext { grades };

        if let Some(mode_id) = Mode::from_i32(mode.mode_id) {
            if let Some(validator) = VALIDATION_MAP.get(&mode_id) {
                validator(&context, score)?;
            }
        }

        if let Some(tags) = mode.tags {
            for tag in tags {
                if let Some(validator) = VALIDATION_BY_TAGS_MAP.get(&tag) {
                    validator(&context, score)?;
                }
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    mod validate_mode {
        use time::Duration;

        use crate::utility::TgmRankError;

        use super::*;

        impl Default for ModeEntity {
            fn default() -> Self {
                Self {
                    mode_id: 1,
                    game_id: 1,
                    mode_name: "Hello World".to_string(),
                    is_ranked: false,
                    weight: 0,
                    margin: 0,
                    link: None,
                    grade_sort: None,
                    score_sort: None,
                    level_sort: None,
                    time_sort: None,
                    sort_order: 0,
                    description: None,
                    submission_range_start: None,
                    submission_range_end: None,
                    tags: None,
                }
            }
        }

        #[test]
        fn submission_before_submission_start_date_returns_an_error() {
            let expired_mode = ModeEntity {
                submission_range_start: Some(OffsetDateTime::now_utc() + Duration::seconds(10)),
                submission_range_end: None,
                ..Default::default()
            };

            let result = ScoreValidationService::validate_mode(&expired_mode);
            assert!(matches!(
                result.expect_err("Did not get an error"),
                TgmRankError::ScoreValidationError(ScoreValidationError::TooEarlySubmission)
            ));
        }

        #[test]
        fn unlocked_but_expired_mode_returns_an_error() {
            let expired_mode = ModeEntity {
                submission_range_start: None,
                submission_range_end: Some(OffsetDateTime::now_utc() - Duration::seconds(10)),
                ..Default::default()
            };

            let result = ScoreValidationService::validate_mode(&expired_mode);
            assert!(matches!(
                result.expect_err("Did not get an error"),
                TgmRankError::ScoreValidationError(ScoreValidationError::TooLateSubmission)
            ));
        }

        #[test]
        fn unexpired_mode_is_good() {
            let mode = ModeEntity {
                submission_range_start: None,
                submission_range_end: Some(OffsetDateTime::now_utc() + Duration::seconds(10)),
                ..Default::default()
            };

            let result = ScoreValidationService::validate_mode(&mode);
            assert!(result.is_ok());
        }

        #[test]
        fn unlocked_mode_without_expiration_date_is_good() {
            let mode = ModeEntity {
                submission_range_start: None,
                submission_range_end: None,
                ..Default::default()
            };

            let result = ScoreValidationService::validate_mode(&mode);
            assert!(result.is_ok());
        }
    }
}
