use crate::controllers::models::{RankingMedalsModel, SubmissionStatsModel};
use crate::models::{JsonRow, MedalStats, PlayerEntity, SubmissionStats};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;
use fnv::FnvHashMap;

pub struct StatsService {}

impl StatsService {
    async fn player_lookup(
        db: &mut DbConnection,
        player_ids: &[i32],
    ) -> TgmRankResult<FnvHashMap<i32, PlayerEntity>> {
        let player_entity_lookup: FnvHashMap<i32, PlayerEntity> =
            PlayerEntity::get_by_ids(db, player_ids)
                .await?
                .into_iter()
                .map(|p| (p.player_id, p))
                .collect();

        Ok(player_entity_lookup)
    }

    pub async fn get_submission_stats(
        db: &mut DbConnection,
    ) -> TgmRankResult<Vec<SubmissionStatsModel>> {
        let submission_info = SubmissionStats::get_submission_stats(db).await?;

        let player_ids = submission_info
            .iter()
            .map(|p| p.player_id)
            .collect::<Vec<i32>>();

        let mut player_entity_lookup = Self::player_lookup(db, &player_ids).await?;

        let models = submission_info
            .into_iter()
            .map(|s| SubmissionStatsModel {
                player: player_entity_lookup.remove(&s.player_id).unwrap().into(),
                submissions: s
                    .submissions
                    .into_iter()
                    .map(|count| count as i32)
                    .collect(),
            })
            .collect();

        Ok(models)
    }

    pub async fn get_ranking_medals(
        db: &mut DbConnection,
    ) -> TgmRankResult<Vec<RankingMedalsModel>> {
        let player_medals = MedalStats::get_medals(db).await?;

        let player_ids = player_medals
            .iter()
            .map(|p| p.player_id)
            .collect::<Vec<i32>>();

        let mut player_entity_lookup = Self::player_lookup(db, &player_ids).await?;

        let models = player_medals
            .into_iter()
            .map(|m| RankingMedalsModel {
                player: player_entity_lookup.remove(&m.player_id).unwrap().into(),
                total: m.total as i32,
                gold_count: m.gold_count as i32,
                gold_modes: m.gold_modes.unwrap_or_default(),
                silver_count: m.silver_count as i32,
                silver_modes: m.silver_modes.unwrap_or_default(),
                bronze_count: m.bronze_count as i32,
                bronze_modes: m.bronze_modes.unwrap_or_default(),
            })
            .collect();

        Ok(models)
    }

    pub async fn medals_by_location(db: &mut DbConnection) -> TgmRankResult<JsonRow> {
        let rows = sqlx::query_as!(
            JsonRow,
            r#"
SELECT COALESCE(JSONB_AGG(a.data), '[]'::JSONB) AS "json!"
FROM (
	SELECT
		JSONB_BUILD_OBJECT(
			'country', MAX(l.alpha_2),
			'total', COUNT(DISTINCT r.mode_id),
			'gold_count', COUNT(DISTINCT r.mode_id) FILTER (WHERE r.ranking = 1),
			'gold_modes', ARRAY_AGG(DISTINCT r.mode_id) FILTER (WHERE r.ranking = 1),
			'silver_count', COUNT(DISTINCT r.mode_id) FILTER (WHERE r.ranking = 2),
			'silver_modes', ARRAY_AGG(DISTINCT r.mode_id) FILTER (WHERE r.ranking = 2),
			'bronze_count', COUNT(DISTINCT r.mode_id) FILTER (WHERE r.ranking = 3),
			'bronze_modes', ARRAY_AGG(DISTINCT r.mode_id) FILTER (WHERE r.ranking = 3)
		) AS data
	FROM mode_ranking(
		(
			SELECT array_agg(m.mode_id)
			FROM MODE m
			WHERE NOT (m.mode_id = ANY((SELECT array_agg(mt.mode_id) FROM mode_tag mt WHERE tag_name = 'EVENT')::integer[]))
		)
	) r
	JOIN account a USING (player_id)
	JOIN LOCATION l USING (location_id)
	WHERE r.ranking <= 3
	GROUP BY l.location_id
	ORDER BY
		COUNT(DISTINCT r.mode_id) DESC,
		COUNT(DISTINCT r.mode_id) FILTER (WHERE r.ranking = 1) DESC,
		COUNT(DISTINCT r.mode_id) FILTER (WHERE r.ranking = 2) DESC,
		COUNT(DISTINCT r.mode_id) FILTER (WHERE r.ranking = 3) DESC
) a;
            "#
        ).fetch_one(db)
            .await?;

        Ok(rows)
    }
}
