use fnv::FnvHashMap;
use once_cell::sync::Lazy;
use time::Time;

use crate::models::{GradeEntity, GradeLine, Mode, ModeTagRef, NewScore};
use crate::utility::{
    GradeRuleError, LevelRuleError, ScoreRuleError, TgmRankError, TgmRankResult, TimeRuleError,
    ValidationRuleError,
};

use super::score_validation_service::{ValidationContext, ValidationFn};

pub static VALIDATION_MAP: Lazy<FnvHashMap<Mode, ValidationFn>> = Lazy::new(|| {
    let mut validation_map = FnvHashMap::default();

    validation_map.insert(Mode::Tgm1Master, validate_tgm1 as ValidationFn);
    validation_map.insert(Mode::Tgm120g, validate_tgm1 as ValidationFn);
    validation_map.insert(Mode::Tgm1Big, validate_tgm1 as ValidationFn);
    validation_map.insert(Mode::Tgm1Reverse, validate_tgm1 as ValidationFn);
    validation_map.insert(Mode::Tgm1SecretGrade, validate_secret_grade as ValidationFn);

    validation_map.insert(Mode::TapMaster, validate_tap_master as ValidationFn);
    validation_map.insert(Mode::TapDeath, validate_tap_death as ValidationFn);
    validation_map.insert(
        Mode::TapDeathSeries,
        validate_tap_death_series_of_5 as ValidationFn,
    );
    validation_map.insert(Mode::TapDoubles, validate_tap_doubles as ValidationFn);
    validation_map.insert(Mode::TapBig, validate_tap_big as ValidationFn);
    validation_map.insert(Mode::TapItem, validate_tap_item as ValidationFn);
    validation_map.insert(Mode::TapNormal, validate_tap_normal as ValidationFn);
    validation_map.insert(Mode::TapTgmplus, validate_tap_tgmplus as ValidationFn);
    validation_map.insert(Mode::TapSecretGrade, validate_secret_grade as ValidationFn);

    validation_map.insert(Mode::TiMasterClassic, validate_ti_master as ValidationFn);
    validation_map.insert(Mode::TiShiraseClassic, validate_ti_shirase as ValidationFn);
    validation_map.insert(Mode::TiEasyClassic, validate_ti_easy as ValidationFn);
    validation_map.insert(Mode::TiSakuraClassic, validate_ti_sakura as ValidationFn);
    validation_map.insert(
        Mode::TiMasterSecretGradeClassic,
        validate_secret_grade as ValidationFn,
    );
    validation_map.insert(
        Mode::TiShiraseSecretGradeClassic,
        validate_secret_grade as ValidationFn,
    );
    validation_map.insert(Mode::TiBigClassic, validate_ti_big as ValidationFn);

    validation_map.insert(Mode::TiMasterWorld, validate_ti_master as ValidationFn);
    validation_map.insert(Mode::TiShiraseWorld, validate_ti_shirase as ValidationFn);
    validation_map.insert(Mode::TiEasyWorld, validate_ti_easy as ValidationFn);
    validation_map.insert(Mode::TiSakuraWorld, validate_ti_sakura as ValidationFn);
    validation_map.insert(
        Mode::TiMasterSecretGradeWorld,
        validate_secret_grade as ValidationFn,
    );
    validation_map.insert(
        Mode::TiShiraseSecretGradeWorld,
        validate_secret_grade as ValidationFn,
    );
    validation_map.insert(Mode::TiBigWorld, validate_ti_big as ValidationFn);

    validation_map.insert(
        Mode::Tgm1ScoreAttack,
        validate_tgm1_score_attack as ValidationFn,
    );

    validation_map.insert(
        Mode::TiQualifiedMasterClassic,
        validate_ti_qualified_master as ValidationFn,
    );
    validation_map.insert(
        Mode::TiQualifiedMasterWorld,
        validate_ti_qualified_master as ValidationFn,
    );

    validation_map.insert(Mode::TapBigNormal, validate_tap_normal_big as ValidationFn);
    validation_map.insert(
        Mode::TapTgmPlusItem,
        validate_tap_tgmplus_item as ValidationFn,
    );

    validation_map.insert(
        Mode::ChallengeSeriesTgmPlusItem,
        validate_tap_tgmplus_item as ValidationFn,
    );

    validation_map.insert(
        Mode::TgmPlusSecretGrade,
        validate_secret_grade as ValidationFn,
    );

    validation_map.insert(
        Mode::DeathSecretGrade,
        validate_secret_grade as ValidationFn,
    );

    validation_map.insert(Mode::CarnivalOfDoom, validate_ti_shirase as ValidationFn);

    validation_map.insert(
        Mode::Tgm120gSeriesOf3,
        validate_tgm1_20g_series_of_3 as ValidationFn,
    );

    validation_map.insert(
        Mode::TapMasterBigItemEvent,
        validate_big_grade_optional as ValidationFn,
    );

    validation_map.insert(
        Mode::TapMasterBigItem,
        validate_big_grade_optional as ValidationFn,
    );

    validation_map.insert(Mode::SegatetTheGranEvent, validate_tgm1 as ValidationFn);

    validation_map.insert(
        Mode::Tgm1BigCaravanEvent,
        validation_tgm1_big_caravan as ValidationFn,
    );

    validation_map.insert(
        Mode::Tgm1AvoidTheSingle,
        validation_tgm1_avoid_the_single as ValidationFn,
    );

    validation_map
});

pub static VALIDATION_BY_TAGS_MAP: Lazy<FnvHashMap<String, ValidationFn>> = Lazy::new(|| {
    let mut validation_map = FnvHashMap::default();

    validation_map.insert(
        ModeTagRef::Carnival.to_string(),
        validate_tap_death_carnival as ValidationFn,
    );

    validation_map
});

fn process_rule_results(rule_results: Vec<Result<(), ValidationRuleError>>) -> TgmRankResult<()> {
    let errors: Vec<ValidationRuleError> = rule_results
        .into_iter()
        .filter(|r| r.is_err())
        .map(|r| r.unwrap_err())
        .collect();
    if !errors.is_empty() {
        return Err(TgmRankError::ScoreValidationRuleError(
            errors.as_slice().into(),
        ));
    }

    Ok(())
}

fn validate_tgm1(context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let min_time_for_gm = Time::from_hms(0, 6, 0).map_err(TgmRankError::from_std)?;
    let max_time_for_gm = Time::from_hms(0, 13, 30).map_err(TgmRankError::from_std)?;

    let rules = vec![
        score.validate_time_required().map(|_| ()),
        score.validate_grade_required().map(|_| ()),
        score.validate_level_required().map(|_| ()),
        score.validate_grade(context).map(|_| ()),
        score.validate_level(0, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "GM", None)?, 999, 999),
        score.validate_grade_time(
            find_grade(context, score.mode_id, "GM", None)?,
            Some(min_time_for_gm),
            Some(max_time_for_gm),
        ),
        score.validate_grade_score(find_grade(context, score.mode_id, "9", None)?, 0, 399),
        score.validate_grade_score(find_grade(context, score.mode_id, "8", None)?, 400, 799),
        score.validate_grade_score(find_grade(context, score.mode_id, "7", None)?, 800, 1_399),
        score.validate_grade_score(find_grade(context, score.mode_id, "6", None)?, 1_400, 1_999),
        score.validate_grade_score(find_grade(context, score.mode_id, "5", None)?, 2_000, 3_499),
        score.validate_grade_score(find_grade(context, score.mode_id, "4", None)?, 3_500, 5_499),
        score.validate_grade_score(find_grade(context, score.mode_id, "3", None)?, 5_500, 7_999),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "2", None)?,
            8_000,
            11_999,
        ),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "1", None)?,
            12_000,
            15_999,
        ),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "S1", None)?,
            16_000,
            21_999,
        ),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "S2", None)?,
            22_000,
            29_999,
        ),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "S3", None)?,
            30_000,
            39_999,
        ),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "S4", None)?,
            40_000,
            51_999,
        ),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "S5", None)?,
            52_000,
            65_999,
        ),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "S6", None)?,
            66_000,
            81_999,
        ),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "S7", None)?,
            82_000,
            99_999,
        ),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "S8", None)?,
            100_000,
            119_999,
        ),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "S9", None)?,
            120_000,
            999_999,
        ),
        score.validate_grade_score(
            find_grade(context, score.mode_id, "GM", None)?,
            126_000,
            999_999,
        ),
    ];

    process_rule_results(rules)
}

fn validate_tgm1_20g_series_of_3(
    context: &ValidationContext,
    score: &NewScore,
) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time_required().map(|_| ()),
        score.validate_grade_required().map(|_| ()),
        score.validate_level_required().map(|_| ()),
        score.validate_grade(context).map(|_| ()),
        score.validate_level(15, 2997),
        score.validate_grade_level(find_grade(context, score.mode_id, "—", None)?, 15, 2996),
        score.validate_grade_level(
            find_grade(context, score.mode_id, "3xClear", None)?,
            2997,
            2997,
        ),
        score.validate_grade_level(
            find_grade(context, score.mode_id, "3xGM", None)?,
            2997,
            2997,
        ),
    ];

    process_rule_results(rules)
}

fn validate_tgm1_score_attack(_context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![score.validate_score_required().map(|_| ())];

    process_rule_results(rules)
}

fn validate_secret_grade(context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let eyeballed_level_lower_bound = 48;

    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_level(5, 999).map(|_| ()),
        score.validate_grade(context).map(|_| ()),
        score.validate_grade_level(
            find_grade(context, score.mode_id, "GM", None)?,
            eyeballed_level_lower_bound,
            999,
        ),
    ];

    process_rule_results(rules)
}

fn validate_tap_master(context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let too_ridiculous_clear_time = Some(Time::from_hms(0, 5, 0).map_err(TgmRankError::from_std)?);
    let m_roll_cutoff_time = Some(Time::from_hms(0, 8, 45).map_err(TgmRankError::from_std)?);

    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_grade(context).map(|_| ()),
        score.validate_level(5, 999).map(|_| ()),
        score.validate_line_level(context, 999, 999),
        score.validate_grade_time(
            find_grade(context, score.mode_id, "M", Some(GradeLine::Green))?,
            too_ridiculous_clear_time,
            m_roll_cutoff_time,
        ),
        score.validate_grade_time(
            find_grade(context, score.mode_id, "GM", Some(GradeLine::Green))?,
            too_ridiculous_clear_time,
            m_roll_cutoff_time,
        ),
        score.validate_grade_time(
            find_grade(context, score.mode_id, "GM", Some(GradeLine::Orange))?,
            too_ridiculous_clear_time,
            m_roll_cutoff_time,
        ),
    ];

    process_rule_results(rules)
}

fn validate_tap_death(context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_grade(context).map(|_| ()),
        score.validate_level(5, 999).map(|_| ()),
        score.validate_grade_level(find_grade(context, score.mode_id, "—", None)?, 0, 500),
        score.validate_grade_level(find_grade(context, score.mode_id, "M", None)?, 501, 998),
        score.validate_grade_level(find_grade(context, score.mode_id, "GM", None)?, 999, 999),
    ];

    process_rule_results(rules)
}

fn validate_tap_death_carnival(
    _context: &ValidationContext,
    score: &NewScore,
) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_level(5, 999).map(|_| ()),
    ];

    process_rule_results(rules)
}

fn validate_tap_death_series_of_5(
    _context: &ValidationContext,
    score: &NewScore,
) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_level(25, 4995).map(|_| ()),
        score.validate_time_empty(),
        score.validate_score_empty(),
        score.validate_grade_empty(),
    ];

    process_rule_results(rules)
}

fn validate_tap_doubles(_context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_level(5, 300).map(|_| ()),
        score.validate_grade_empty(),
        score.validate_score_empty(),
    ];

    process_rule_results(rules)
}

fn validate_tap_big(context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_grade(context).map(|_| ()),
        score.validate_level(3, 999),
    ];

    process_rule_results(rules)
}

fn validate_big_grade_optional(
    _context: &ValidationContext,
    score: &NewScore,
) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_level(3, 999),
    ];

    process_rule_results(rules)
}

fn validate_tap_item(context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_grade(context).map(|_| ()),
        score.validate_level(5, 999),
    ];

    process_rule_results(rules)
}

fn validate_tap_normal(_context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let playtime_validation = if score.playtime.is_some() {
        score.validate_time().map(|_| ())
    } else {
        Ok(())
    };

    let level_validation = if score.level.is_some() {
        score.validate_level(5, 300).map(|_| ())
    } else {
        Ok(())
    };

    let rules = vec![
        playtime_validation,
        level_validation,
        score.validate_score(0, 1_000_000),
        score.validate_grade_empty(),
    ];

    process_rule_results(rules)
}

fn validate_tap_normal_big(_context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_level_optional(0, 300).map(|_| ()),
        score.validate_score(0, 2_000_000),
        score.validate_grade_empty(),
    ];

    process_rule_results(rules)
}

fn validate_tap_tgmplus(_context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_level(5, 999).map(|_| ()),
        score.validate_grade_empty(),
        score.validate_time_required().map(|_| ()),
    ];

    process_rule_results(rules)
}

fn validate_tap_tgmplus_item(_context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_level(5, 999).map(|_| ()),
        score.validate_grade_empty(),
        score.validate_time_required().map(|_| ()),
    ];

    process_rule_results(rules)
}

fn validate_ti_master(context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_grade(context).map(|_| ()),
        score.validate_level(5, 999).map(|_| ()),
        score.validate_score_empty(),
        score.validate_grade_level(find_grade(context, score.mode_id, "m9", None)?, 999, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "M", None)?, 999, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "MK", None)?, 999, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "MV", None)?, 999, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "MO", None)?, 999, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "MM", None)?, 999, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "GM", None)?, 999, 999),
    ];

    process_rule_results(rules)
}

fn validate_ti_qualified_master(
    context: &ValidationContext,
    score: &NewScore,
) -> TgmRankResult<()> {
    let rules = vec![score.validate_grade(context).map(|_| ())];

    process_rule_results(rules)
}

fn validate_ti_shirase(context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_grade(context).map(|_| ()),
        score.validate_level(0, 1300).map(|_| ()),
        score.validate_score_empty(),
        score.validate_grade_level(
            find_grade(context, score.mode_id, "S13", Some(GradeLine::Green))?,
            1300,
            1300,
        ),
        score.validate_grade_level(
            find_grade(context, score.mode_id, "S13", Some(GradeLine::Orange))?,
            1300,
            1300,
        ),
    ];

    process_rule_results(rules)
}

fn validate_ti_easy(_context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_level(5, 200).map(|_| ()),
        score.validate_score(0, 3000).map(|_| ()),
        score.validate_grade_empty(),
    ];

    process_rule_results(rules)
}

fn validate_ti_sakura(context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_grade(context).map(|_| ()),
        score.validate_level(0, 100).map(|_| ()),
        score.validate_score_empty(),
        score.validate_grade_level(find_grade(context, score.mode_id, "ALL", None)?, 100, 100),
    ];

    process_rule_results(rules)
}

fn validate_ti_big(context: &ValidationContext, score: &NewScore) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time().map(|_| ()),
        score.validate_grade(context).map(|_| ()),
        score.validate_level(3, 999).map(|_| ()),
        score.validate_score_empty(),
        score.validate_grade_level(find_grade(context, score.mode_id, "m9", None)?, 999, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "M", None)?, 999, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "MK", None)?, 999, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "MV", None)?, 999, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "MO", None)?, 999, 999),
        score.validate_grade_level(find_grade(context, score.mode_id, "MM", None)?, 999, 999),
    ];

    process_rule_results(rules)
}

fn validation_tgm1_big_caravan(
    _context: &ValidationContext,
    score: &NewScore,
) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time_empty().map(|_| ()),
        score.validate_grade_empty().map(|_| ()),
        score.validate_level_empty().map(|_| ()),
        score.validate_score(0, 9999999),
    ];

    process_rule_results(rules)
}

fn validation_tgm1_avoid_the_single(
    _context: &ValidationContext,
    score: &NewScore,
) -> TgmRankResult<()> {
    let rules = vec![
        score.validate_time_empty().map(|_| ()),
        score.validate_grade_empty().map(|_| ()),
        score.validate_level_empty().map(|_| ()),
        score.validate_score(0, 500),
    ];

    process_rule_results(rules)
}

fn find_grade<'a>(
    context: &'a ValidationContext,
    mode_id: i32,
    grade_display: &str,
    line: Option<GradeLine>,
) -> TgmRankResult<&'a GradeEntity> {
    context
        .grades
        .iter()
        .find(|g| g.mode_id == mode_id && g.grade_display == grade_display && g.line == line)
        .ok_or_else(|| TgmRankError::InternalServerError("Could not find matching grade".into()))
}

impl NewScore {
    fn validate_grade_required(&self) -> Result<i32, ValidationRuleError> {
        self.grade_id.ok_or_else(|| GradeRuleError::Required.into())
    }

    fn validate_time_required(&self) -> Result<Time, ValidationRuleError> {
        self.playtime.ok_or_else(|| TimeRuleError::Required.into())
    }

    fn validate_level_required(&self) -> Result<i32, ValidationRuleError> {
        self.level.ok_or_else(|| LevelRuleError::Required.into())
    }

    fn validate_score_required(&self) -> Result<i32, ValidationRuleError> {
        self.score.ok_or_else(|| ScoreRuleError::Required.into())
    }

    fn validate_empty<T>(value: Option<T>) -> Result<(), ()> {
        match value {
            Some(_) => Err(()),
            None => Ok(()),
        }
    }

    fn validate_grade_empty(&self) -> Result<(), ValidationRuleError> {
        NewScore::validate_empty(self.grade_id).map_err(|_| GradeRuleError::MustBeEmpty.into())
    }

    #[allow(dead_code)]
    fn validate_time_empty(&self) -> Result<(), ValidationRuleError> {
        NewScore::validate_empty(self.playtime).map_err(|_| TimeRuleError::MustBeEmpty.into())
    }

    #[allow(dead_code)]
    fn validate_level_empty(&self) -> Result<(), ValidationRuleError> {
        NewScore::validate_empty(self.level).map_err(|_| LevelRuleError::MustBeEmpty.into())
    }

    fn validate_score_empty(&self) -> Result<(), ValidationRuleError> {
        NewScore::validate_empty(self.score).map_err(|_| ScoreRuleError::MustBeEmpty.into())
    }

    fn validate_time(&self) -> Result<Time, ValidationRuleError> {
        self.validate_time_required()
    }

    fn validate_level_optional(
        &self,
        level_lower_bound: i32,
        level_upper_bound: i32,
    ) -> Result<(), ValidationRuleError> {
        if let Some(level) = self.level {
            let level_is_in_bounds = level >= level_lower_bound && level <= level_upper_bound;
            if !level_is_in_bounds {
                return Err(LevelRuleError::Invalid.into());
            }
        }

        Ok(())
    }

    fn validate_level(
        &self,
        level_lower_bound: i32,
        level_upper_bound: i32,
    ) -> Result<(), ValidationRuleError> {
        let level = self.validate_level_required()?;

        let level_is_in_bounds = level >= level_lower_bound && level <= level_upper_bound;
        if !level_is_in_bounds {
            return Err(LevelRuleError::Invalid.into());
        }

        Ok(())
    }

    fn _validate_score_optional(
        &self,
        score_lower_bound: i32,
        score_upper_bound: i32,
    ) -> Result<(), ValidationRuleError> {
        if let Some(score) = self.score {
            let score_is_in_bounds = score >= score_lower_bound && score <= score_upper_bound;
            if !score_is_in_bounds {
                return Err(ScoreRuleError::Invalid.into());
            }
        }

        Ok(())
    }

    fn validate_score(
        &self,
        score_lower_bound: i32,
        score_upper_bound: i32,
    ) -> Result<(), ValidationRuleError> {
        let score = self.validate_score_required()?;

        let score_is_in_bounds = score >= score_lower_bound && score <= score_upper_bound;
        if !score_is_in_bounds {
            return Err(ScoreRuleError::Invalid.into());
        }

        Ok(())
    }

    fn validate_grade<'a>(
        &self,
        context: &'a ValidationContext,
    ) -> Result<&'a GradeEntity, ValidationRuleError> {
        let grade_id = self.validate_grade_required()?;

        let grade = context
            .grades
            .iter()
            .find(|g| g.grade_id == grade_id)
            .ok_or(GradeRuleError::Invalid)?;

        Ok(grade)
    }

    fn validate_grade_time(
        &self,
        grade: &GradeEntity,
        time_lower_bound: Option<Time>,
        time_upper_bound: Option<Time>,
    ) -> Result<(), ValidationRuleError> {
        let grade_id = self.validate_grade_required()?;

        let time = self.validate_time_required()?;

        let time_is_too_fast = time_lower_bound.filter(|&lb| time < lb).is_some();
        let time_is_too_slow = time_upper_bound.filter(|&ub| time > ub).is_some();
        if grade_id == grade.grade_id && (time_is_too_fast || time_is_too_slow) {
            return Err(TimeRuleError::InvalidForGrade.into());
        }

        Ok(())
    }

    fn validate_grade_score(
        &self,
        grade: &GradeEntity,
        score_lower_bound: i32,
        score_upper_bound: i32,
    ) -> Result<(), ValidationRuleError> {
        let grade_id = self.validate_grade_required()?;
        if let Some(score) = self.score {
            let score_is_in_bounds = score >= score_lower_bound && score <= score_upper_bound;
            if grade_id == grade.grade_id && !score_is_in_bounds {
                return Err(ScoreRuleError::InvalidForGrade.into());
            }
        }

        Ok(())
    }

    fn validate_grade_level(
        &self,
        grade: &GradeEntity,
        level_lower_bound: i32,
        level_upper_bound: i32,
    ) -> Result<(), ValidationRuleError> {
        let grade_id = self.validate_grade_required()?;
        let level_is_in_bounds = self.validate_level(level_lower_bound, level_upper_bound);
        if grade_id == grade.grade_id && level_is_in_bounds.is_err() {
            return Err(LevelRuleError::InvalidForGrade.into());
        }

        Ok(())
    }

    fn validate_line_level(
        &self,
        context: &ValidationContext,
        level_lower_bound: i32,
        level_upper_bound: i32,
    ) -> Result<(), ValidationRuleError> {
        let grade_id = self.validate_grade_required()?;
        let level_is_in_bounds = self.validate_level(level_lower_bound, level_upper_bound);

        let grade_has_line = context
            .grades
            .iter()
            .find(|g| g.grade_id == grade_id)
            .and_then(|g| g.line.as_ref())
            .is_some();

        if !grade_has_line && level_is_in_bounds.is_ok() {
            return Err(LevelRuleError::InvalidForGrade.into());
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use time::{Duration, Time};

    use crate::models::{GradeEntity, NewScore, ScoreStatus};
    use crate::services::ValidationContext;

    fn make_valid_score_model() -> NewScore {
        NewScore {
            grade_id: None,
            level: None,
            playtime: None,
            score: None,
            status: ScoreStatus::Pending,
            comment: None,
            player_id: 0,
            mode_id: 0,
            updated_by: 0,
            platform_id: None,
        }
    }

    #[test]
    fn validate_time_returns_err_when_score_does_not_have_playtime() {
        let score = make_valid_score_model();
        let result = score.validate_time();

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_time_returns_ok_when_score_has_playtime() {
        let score = NewScore {
            playtime: Time::from_hms(0, 1, 1).ok(),
            ..make_valid_score_model()
        };
        let result = score.validate_time();

        assert_eq!(result.is_ok(), true);
    }

    #[test]
    fn validate_level_returns_err_when_level_is_too_low() {
        let level_lower_bound = 10;
        let level_upper_bound = 100;
        let score = NewScore {
            level: Some(level_lower_bound - 1),
            ..make_valid_score_model()
        };
        let result = score.validate_level(level_lower_bound, level_upper_bound);

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_level_returns_err_when_level_is_too_high() {
        let level_lower_bound = 10;
        let level_upper_bound = 100;
        let score = NewScore {
            level: Some(level_upper_bound + 1),
            ..make_valid_score_model()
        };
        let result = score.validate_level(level_lower_bound, level_upper_bound);

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_level_returns_ok_when_level_is_just_right() {
        let level_lower_bound = 10;
        let level_upper_bound = 100;
        let score = NewScore {
            level: Some(level_lower_bound),
            ..make_valid_score_model()
        };
        let result = score.validate_level(level_lower_bound, level_upper_bound);

        assert_eq!(result.is_ok(), true);
    }

    #[test]
    fn validate_level_returns_err_when_level_is_none() {
        let score = NewScore {
            level: None,
            ..make_valid_score_model()
        };
        let result = score.validate_level(0, 0);

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_score_returns_err_when_score_is_too_low() {
        let score_lower_bound = 10;
        let score_upper_bound = 100;
        let score = NewScore {
            score: Some(score_lower_bound - 1),
            ..make_valid_score_model()
        };
        let result = score.validate_score(score_lower_bound, score_upper_bound);

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_score_returns_err_when_score_is_too_high() {
        let score_lower_bound = 10;
        let score_upper_bound = 100;
        let score = NewScore {
            score: Some(score_upper_bound + 1),
            ..make_valid_score_model()
        };
        let result = score.validate_score(score_lower_bound, score_upper_bound);

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_score_returns_ok_when_score_is_just_right() {
        let score_lower_bound = 10;
        let score_upper_bound = 100;
        let score = NewScore {
            score: Some(score_lower_bound),
            ..make_valid_score_model()
        };
        let result = score.validate_score(score_lower_bound, score_upper_bound);

        assert_eq!(result.is_ok(), true);
    }

    #[test]
    fn validate_score_returns_err_when_score_is_none() {
        let score = NewScore {
            score: None,
            ..make_valid_score_model()
        };
        let result = score.validate_score(0, 0);

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_grade_returns_err_when_grade_is_none() {
        let context = ValidationContext { grades: vec![] };

        let score = NewScore {
            grade_id: None,
            ..make_valid_score_model()
        };
        let result = score.validate_grade(&context);

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_grade_returns_err_when_no_grades_are_found() {
        let context = ValidationContext { grades: vec![] };

        let score = NewScore {
            grade_id: Some(10),
            ..make_valid_score_model()
        };
        let result = score.validate_grade(&context);

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_grade_returns_err_when_no_matching_grade_is_found() {
        let non_matching_grade = GradeEntity {
            grade_id: 123,
            grade_display: "".into(),
            mode_id: 123,
            line: None,
            sort_order: 6,
        };

        let context = ValidationContext {
            grades: vec![non_matching_grade],
        };

        let score = NewScore {
            grade_id: Some(10),
            ..make_valid_score_model()
        };
        let result = score.validate_grade(&context);

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_grade_returns_ok_when_matching_grade_is_found() {
        let any_matching_grade_id = 72;

        let matching_grade = GradeEntity {
            grade_id: any_matching_grade_id,
            grade_display: "".into(),
            mode_id: 123,
            line: None,
            sort_order: 6,
        };

        let context = ValidationContext {
            grades: vec![matching_grade],
        };

        let score = NewScore {
            grade_id: Some(any_matching_grade_id),
            ..make_valid_score_model()
        };
        let result = score.validate_grade(&context);

        assert_eq!(result.is_ok(), true);
    }

    #[test]
    fn validate_grade_time_returns_err_when_time_is_too_slow() {
        let any_matching_grade_id = 123;

        let matching_grade = GradeEntity {
            grade_id: any_matching_grade_id,
            grade_display: "Z".into(),
            mode_id: 123,
            line: None,
            sort_order: 6,
        };

        let time_lower_bound = Time::from_hms(5, 5, 5).unwrap();
        let time_upper_bound = Time::from_hms(10, 10, 10).unwrap();
        let score = NewScore {
            grade_id: Some(any_matching_grade_id),
            playtime: Some(time_upper_bound + Duration::milliseconds(1)),
            ..make_valid_score_model()
        };

        let result = score.validate_grade_time(
            &matching_grade,
            Some(time_lower_bound),
            Some(time_upper_bound),
        );

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_grade_time_returns_err_when_time_is_too_fast() {
        let any_matching_grade_id = 123;

        let matching_grade = GradeEntity {
            grade_id: any_matching_grade_id,
            grade_display: "X".into(),
            mode_id: 123,
            line: None,
            sort_order: 6,
        };

        let time_lower_bound = Time::from_hms(5, 5, 5).unwrap();
        let time_upper_bound = Time::from_hms(10, 10, 10).unwrap();
        let score = NewScore {
            grade_id: Some(any_matching_grade_id),
            playtime: Some(time_lower_bound - Duration::seconds(1)),
            ..make_valid_score_model()
        };

        let result = score.validate_grade_time(
            &matching_grade,
            Some(time_lower_bound),
            Some(time_upper_bound),
        );

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_grade_time_does_not_check_lower_bound_when_lower_bound_is_none() {
        let any_matching_grade_id = 123;

        let matching_grade = GradeEntity {
            grade_id: any_matching_grade_id,
            grade_display: "Y".into(),
            mode_id: 123,
            line: None,
            sort_order: 6,
        };

        let time_lower_bound = Time::from_hms(5, 5, 5).unwrap();
        let time_upper_bound = Time::from_hms(10, 10, 10).unwrap();
        let score = NewScore {
            grade_id: Some(any_matching_grade_id),
            playtime: Some(time_lower_bound - Duration::seconds(1)),
            ..make_valid_score_model()
        };

        let result = score.validate_grade_time(&matching_grade, None, Some(time_upper_bound));

        assert_eq!(result.is_ok(), true);
    }

    #[test]
    fn validate_grade_score_returns_ok_when_score_is_some_and_is_big_enough() {
        let any_matching_grade_id = 123;

        let matching_grade = GradeEntity {
            grade_id: any_matching_grade_id,
            grade_display: "W".into(),
            mode_id: 123,
            line: None,
            sort_order: 6,
        };

        let score_lower_bound = 10;
        let score_upper_bound = 123_456_789;
        let score = NewScore {
            grade_id: Some(any_matching_grade_id),
            score: Some(score_lower_bound + 1),
            ..make_valid_score_model()
        };

        let result =
            score.validate_grade_score(&matching_grade, score_lower_bound, score_upper_bound);

        assert_eq!(result.is_ok(), true);
    }

    #[test]
    fn validate_grade_score_returns_ok_when_level_is_some_and_is_in_bounds() {
        let any_matching_grade_id = 123;

        let matching_grade = GradeEntity {
            grade_id: any_matching_grade_id,
            grade_display: "T".into(),
            mode_id: 123,
            line: None,
            sort_order: 6,
        };

        let level_lower_bound = 10;
        let level_upper_bound = 100;
        let score = NewScore {
            grade_id: Some(any_matching_grade_id),
            level: Some(level_lower_bound + 1),
            ..make_valid_score_model()
        };

        let result =
            score.validate_grade_level(&matching_grade, level_lower_bound, level_upper_bound);

        assert_eq!(result.is_ok(), true);
    }

    #[test]
    fn validate_grade_empty_returns_correct_values_when_grade_is_empty() {
        let score = NewScore {
            grade_id: None,
            ..make_valid_score_model()
        };
        let result = score.validate_grade_empty();

        assert_eq!(result.is_ok(), true);

        let score = NewScore {
            grade_id: Some(10),
            ..make_valid_score_model()
        };
        let result = score.validate_grade_empty();

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_level_empty_returns_correct_values_when_level_is_empty() {
        let score = NewScore {
            level: None,
            ..make_valid_score_model()
        };
        let result = score.validate_level_empty();

        assert_eq!(result.is_ok(), true);

        let score = NewScore {
            level: Some(123),
            ..make_valid_score_model()
        };
        let result = score.validate_level_empty();

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_time_empty_returns_correct_values_when_time_is_empty() {
        let score = NewScore {
            playtime: None,
            ..make_valid_score_model()
        };
        let result = score.validate_time_empty();

        assert_eq!(result.is_ok(), true);

        let score = NewScore {
            playtime: Time::from_hms(0, 2, 18).ok(),
            ..make_valid_score_model()
        };
        let result = score.validate_time_empty();

        assert_eq!(result.is_err(), true);
    }

    #[test]
    fn validate_score_empty_returns_correct_values_when_score_is_empty() {
        let score = NewScore {
            score: None,
            ..make_valid_score_model()
        };
        let result = score.validate_score_empty();

        assert_eq!(result.is_ok(), true);

        let score = NewScore {
            score: Some(99999),
            ..make_valid_score_model()
        };
        let result = score.validate_score_empty();

        assert_eq!(result.is_err(), true);
    }
}
