use actix_identity::Identity;
use actix_web::dev::Extensions;
use actix_web::{HttpMessage, HttpRequest};
use once_cell::sync::Lazy;
use time::{Duration, OffsetDateTime};
use uuid::Uuid;

use crate::guards::UserCookie;
use crate::models::forms::LoginForm;
use crate::models::{AccountClaimEntity, NewSession, PlayerEntity, Session};
use crate::modules::DbConnection;
use crate::services::PlayerService;
use crate::utility::{AuthenticationError, TgmRankResult};

pub static REMEMBER_ME_DURATION_TIME_V3: Lazy<actix_web::cookie::time::Duration> =
    Lazy::new(|| actix_web::cookie::time::Duration::days(180));

pub static REMEMBER_ME_DURATION: Lazy<time::Duration> = Lazy::new(|| time::Duration::days(180));
static FORGET_ME_DURATION: Lazy<time::Duration> = Lazy::new(|| Duration::days(1));

pub struct LoginService();

impl LoginService {
    pub async fn login(
        db: &mut DbConnection,
        login_form: LoginForm,
        request: &HttpRequest,
    ) -> TgmRankResult<PlayerEntity> {
        let player =
            PlayerService::player_authenticated(db, &login_form.username, &login_form.password)
                .await?;

        let expiration = if let Some(true) = login_form.remember {
            OffsetDateTime::now_utc() + *REMEMBER_ME_DURATION
        } else {
            OffsetDateTime::now_utc() + *FORGET_ME_DURATION
        };

        let session = NewSession {
            user_id: player.player_id,
            session_key: Uuid::new_v4(),
            expires: expiration,
        };

        let cookie = UserCookie {
            user_id: player.player_id,
            user_role: player.player_role.clone(),
            session_key: session.session_key,
        };

        Identity::login(&request.extensions(), cookie.to_string()).unwrap();
        Session::insert(db, session).await?;

        Ok(player)
    }

    pub async fn logout(db: &mut DbConnection, id: Identity) -> TgmRankResult<()> {
        let user_cookie = UserCookie::from_identity(&id)?;
        id.logout();

        // TODO: Check session is actually deleted?
        LoginService::delete_single_session(db, user_cookie.user_id, user_cookie.session_key)
            .await?;

        Ok(())
    }

    pub async fn is_user(db: &mut DbConnection, id: Identity) -> TgmRankResult<UserCookie> {
        let (user_cookie, _) = LoginService::validate_session(db, id).await?;
        Ok(user_cookie)
    }

    pub async fn is_admin(db: &mut DbConnection, id: Identity) -> TgmRankResult<UserCookie> {
        LoginService::is_authorized(db, id, &|user: &UserCookie| user.user_role.is_admin()).await
    }

    pub async fn can_verify_scores(
        db: &mut DbConnection,
        id: Identity,
    ) -> TgmRankResult<UserCookie> {
        LoginService::is_authorized(db, id, &|user: &UserCookie| {
            user.user_role.can_verify_scores()
        })
        .await
    }

    pub async fn is_authorized(
        db: &mut DbConnection,
        id: Identity,
        condition: &dyn Fn(&UserCookie) -> bool,
    ) -> TgmRankResult<UserCookie> {
        let (user_cookie, _) = LoginService::validate_session(db, id).await?;

        if !condition(&user_cookie) {
            return Err(AuthenticationError::UnauthorizedForOperation.into());
        }

        Ok(user_cookie)
    }

    pub async fn validate_session(
        db: &mut DbConnection,
        id: Identity,
    ) -> TgmRankResult<(UserCookie, PlayerEntity)> {
        let user_cookie = UserCookie::from_identity(&id)?;
        let session = Session::get_session_by_key(db, user_cookie.session_key).await;

        if session.is_err() {
            id.logout();
            return Err(AuthenticationError::UnauthorizedForOperation.into());
        }

        let session = session?;
        if session.is_expired() {
            LoginService::delete_single_session(db, session.user_id, session.session_key).await?;
            id.logout();
            return Err(AuthenticationError::SessionExpired.into());
        }

        let player = PlayerEntity::get_by_id(db, user_cookie.user_id).await?;
        let user_cookie = LoginService::refresh_identity(&id, user_cookie, &player);

        Ok((user_cookie, player))
    }

    fn refresh_identity(_id: &Identity, user: UserCookie, player: &PlayerEntity) -> UserCookie {
        if user.user_role != player.player_role {
            let updated_user = UserCookie {
                user_role: player.player_role.clone(),
                ..user
            };

            Identity::login(&Extensions::new(), user.to_string()).unwrap();
            return updated_user;
        }

        user
    }

    pub async fn delete_user_sessions(
        db: &mut DbConnection,
        user_id: i32,
        exceptions: &[Uuid],
    ) -> TgmRankResult<()> {
        Session::delete_player_sessions(db, user_id, exceptions).await?;
        Session::delete_expired_sessions(db).await?;
        Ok(())
    }

    async fn delete_single_session(
        db: &mut DbConnection,
        user_id: i32,
        session_key: Uuid,
    ) -> TgmRankResult<()> {
        Session::delete_session(db, user_id, session_key).await?;
        Session::delete_expired_sessions(db).await?;
        Ok(())
    }

    pub async fn validate_claim(
        db: &mut DbConnection,
        id: Identity,
        condition: &dyn Fn(&'_ PlayerEntity, &'_ AccountClaimEntity) -> bool,
    ) -> TgmRankResult<(PlayerEntity, Vec<AccountClaimEntity>)> {
        let (user_cookie, account) = LoginService::validate_session(db, id).await?;

        let claims = AccountClaimEntity::get_claims(db, user_cookie.user_id).await?;
        claims
            .iter()
            .find(|c| condition(&account, c))
            .ok_or(AuthenticationError::UnauthorizedForOperation)?;

        Ok((account, claims))
    }
}
