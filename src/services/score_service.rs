use actix_web::http::Uri;
use itertools::Itertools;
use validator::Validate;

use crate::models::{
    forms::{AddScoreForm, ProofLinkForm, UpdateScoreForm, VerifyScoreForm},
    AccountAchievementEntity, FullScoreEntity, StandardModeGroup, VerifyScoreEntity,
};
use crate::models::{
    NewProofEntity, NewScore, ProofEntity, ProofType, ScoreEntity, ScoreStatus, UpdateScoreEntity,
};
use crate::modules::{DbConnection, Settings};
use crate::services::{EmailService, PlayerService, RecentActivityService, ScoreValidationService};
use crate::utility::{
    AuthenticationError, ClientError, ProofError, TgmRankError, TgmRankResult, ValidationRuleError,
};
use crate::{controllers::models::AchievementModel, models::filters::GenericFilter};
use crate::{guards::UserCookie, models::AchievementEntity};

const VALID_CONTENT_TYPES: [mime::Mime; 4] = [
    mime::IMAGE_BMP,
    mime::IMAGE_GIF,
    mime::IMAGE_JPEG,
    mime::IMAGE_PNG,
];

// TODO: Read this data from somewhere
const VALID_VIDEO_DOMAINS: [&str; 3] = ["youtu.be", "youtube.com", "twitch.tv"];

pub struct ScoreService();

impl ScoreService {
    pub fn get_score_statuses() -> TgmRankResult<&'static [ScoreStatus]> {
        Ok(ScoreStatus::get_all())
    }

    fn validate_image_content_type(content_type: &mime::Mime) -> Result<(), ProofError> {
        let content_type_is_valid = VALID_CONTENT_TYPES.contains(content_type);

        match content_type_is_valid {
            true => Ok(()),
            false => Err(ProofError::UnsupportedImageType(content_type.to_string())),
        }
    }

    fn validate_video_host(uri: &Uri) -> Result<(), ProofError> {
        if let Some(host) = uri.host() {
            let is_valid = VALID_VIDEO_DOMAINS
                .iter()
                .any(|domain| host.contains(domain));
            if !is_valid {
                return Err(ProofError::UnsupportedVideoSite);
            }
            Ok(())
        } else {
            Err(ProofError::InvalidHostName)
        }
    }

    fn validate_video_content_type(content_type: &mime::Mime) -> Result<(), ProofError> {
        if content_type.type_() != mime::VIDEO && content_type != &mime::APPLICATION_OCTET_STREAM {
            return Err(ProofError::UnsupportedVideoType(content_type.to_string()));
        }

        Ok(())
    }

    // This is to handle sites that don't, for example, support HTTP HEAD requests.
    // Probably doesn't need to be configurable right now :D
    async fn validate_specific_link(
        client: &reqwest::Client,
        uri: &Uri,
    ) -> Result<Option<bool>, ProofError> {
        if let Some(host) = uri.host() {
            println!("{:?} {}", uri, uri.to_string());
            if host.contains("streamable") {
                let response = client
                    .get(format!("https://api.streamable.com/oembed.json"))
                    .query(&[("url", uri.to_string())])
                    .send()
                    .await
                    .map_err(|e| {
                        error!("{:?}", e);
                        ProofError::BrokenLink
                    })?;

                return Ok(Some(response.status().is_success()));
            }
        }

        Ok(None)
    }

    async fn validate_link(
        form: &ProofLinkForm,
        client: &reqwest::Client,
    ) -> Result<(), ProofError> {
        match Self::validate_specific_link(&client, &form.link.parse().unwrap()).await {
            Ok(Some(true)) => return Ok(()),
            Ok(Some(false)) => return Err(ProofError::BrokenLink),
            Err(e) => return Err(e),
            _ => {}
        };

        match &form.proof_type {
            ProofType::Other => {}
            ProofType::Image => {
                let mime_type = get_link_content_type(client, form).await?;
                Self::validate_image_content_type(&mime_type)?;
            }
            ProofType::Video => {
                let mime_type = get_link_content_type(client, form).await?;
                match (mime_type.type_(), mime_type.subtype()) {
                    (mime::TEXT, mime::HTML) => {
                        Self::validate_video_host(&form.link.parse().unwrap())?
                    }
                    _ => Self::validate_video_content_type(&mime_type)?,
                }
            }
        }

        Ok(())
    }

    async fn validate_proof(proofs: &[ProofLinkForm]) -> TgmRankResult<()> {
        let mut proof_errors = Vec::new();
        for (index, proof) in proofs.iter().enumerate() {
            let client = reqwest::Client::builder()
                .cookie_store(true)
                .build()
                .map_err(TgmRankError::from_std)?;

            let proof_validation_result = Self::validate_link(proof, &client).await;

            if let Err(e) = proof_validation_result {
                proof_errors.push(ValidationRuleError::ProofLinkError(index, e));
            }
        }

        if !proof_errors.is_empty() {
            return Err(TgmRankError::ScoreValidationRuleError(
                proof_errors.as_slice().into(),
            ));
        }

        Ok(())
    }

    fn get_submission_player_id(user: &UserCookie, score: &AddScoreForm) -> TgmRankResult<i32> {
        if !user.user_role.is_admin()
            && score.player_id.is_some()
            && score.player_id.unwrap() != user.user_id
        {
            return Err(AuthenticationError::UnauthorizedForOperation.into());
        }

        let player_id = score
            .player_id
            .or(Some(user.user_id))
            .ok_or(AuthenticationError::UnauthorizedForOperation)?;

        Ok(player_id)
    }

    fn make_insertable_score(user: &UserCookie, score: &AddScoreForm) -> TgmRankResult<NewScore> {
        let player_id = Self::get_submission_player_id(user, score)?;

        Ok(NewScore {
            grade_id: score.grade_id,
            level: score.level,
            playtime: score.get_playtime(),
            score: score.score,
            status: ScoreStatus::Pending,
            comment: score.get_comment(),
            player_id,
            mode_id: score.mode_id,
            updated_by: user.user_id,
            platform_id: score.platform_id,
        })
    }

    fn make_updatable_score(user: &UserCookie, score: &UpdateScoreForm) -> UpdateScoreEntity {
        UpdateScoreEntity {
            score_id: score.score_id,
            comment: score.get_comment(),
            updated_by: user.user_id,
            platform_id: score.platform_id,
        }
    }

    async fn refresh_materialized_views(mut db: &mut DbConnection) -> TgmRankResult<()> {
        FullScoreEntity::refresh_score_entry_history(&mut db).await?;

        Ok(())
    }

    pub async fn insert_from_form(
        mut db: &mut DbConnection,
        user: &UserCookie,
        score: &AddScoreForm,
    ) -> TgmRankResult<ScoreEntity> {
        score.validate()?;
        Self::validate_proof(&score.proof).await?;

        let score_to_insert = Self::make_insertable_score(user, &score)?;

        ScoreValidationService::validate(db, &score_to_insert).await?;

        let insert_result = ScoreEntity::insert(&mut db, score_to_insert).await?;

        Self::refresh_materialized_views(&mut db).await?;

        let proof: Vec<NewProofEntity> = score
            .proof
            .iter()
            .map(|p| NewProofEntity {
                score_id: insert_result.score_id,
                proof_type: &p.proof_type,
                link: p.link.trim(),
            })
            .unique_by(|p| (p.proof_type, p.link))
            .collect_vec();

        if !proof.is_empty() {
            ProofEntity::insert_many(&mut db, proof.iter().collect_vec().as_slice()).await?;
        }

        AccountAchievementEntity::update_for_players(&mut db, &[insert_result.player_id]).await?;

        RecentActivityService::update_recent_activity_by_mode_group(
            &mut db,
            StandardModeGroup::Main.to_string(),
            &[insert_result.score_id],
        )
        .await?;

        RecentActivityService::update_recent_activity_by_mode_group(
            &mut db,
            StandardModeGroup::Extended.to_string(),
            &[insert_result.score_id],
        )
        .await?;

        Ok(insert_result)
    }

    pub async fn update_from_form(
        mut db: &mut DbConnection,
        user: &UserCookie,
        score: &UpdateScoreForm,
    ) -> TgmRankResult<ScoreEntity> {
        score.validate()?;
        Self::validate_proof(&score.proof).await?;

        let current_score =
            FullScoreEntity::get_by_ids(db, &[score.score_id], &GenericFilter::default())
                .await?
                .swap_remove(0);

        Self::user_can_update_score(user, &current_score)?;

        let update_model = ScoreService::make_updatable_score(user, &score);

        let update_result = update_model.update(&mut db).await?;

        Self::refresh_materialized_views(&mut db).await?;

        let proof: Vec<NewProofEntity> = score
            .proof
            .iter()
            .map(|p| NewProofEntity {
                score_id: update_result.score_id,
                proof_type: &p.proof_type,
                link: p.link.trim(),
            })
            .collect_vec();

        if !proof.is_empty() {
            // TODO: Match proof when updating?
            let current_proof =
                ProofEntity::get_by_score_id(&mut db, &[update_result.score_id]).await?;

            let (to_delete, to_add) = Self::dedup_proof(&current_proof, &proof);

            ProofEntity::delete_many(&mut db, &to_delete.iter().map(|d| d.proof_id).collect_vec())
                .await?;
            ProofEntity::insert_many(&mut db, to_add.as_slice()).await?;
        }

        AccountAchievementEntity::update_for_players(&mut db, &[update_result.player_id]).await?;

        // transaction.commit().await?;

        Ok(update_result)
    }

    fn dedup_proof<'a, 'b>(
        current_proof: &'b [ProofEntity],
        new_proof: &'a [NewProofEntity],
    ) -> (Vec<&'b ProofEntity>, Vec<&'a NewProofEntity<'a>>) {
        fn proof_compare(cp: &ProofEntity, np: &NewProofEntity) -> bool {
            &cp.proof_type == np.proof_type && cp.link == np.link && cp.score_id == np.score_id
        }

        let mut to_add: Vec<&NewProofEntity> = vec![];
        for np in new_proof {
            if !current_proof.iter().any(|cp| proof_compare(cp, np)) {
                to_add.push(np);
            }
        }

        let mut to_delete: Vec<&ProofEntity> = vec![];
        for cp in current_proof {
            if !new_proof.iter().any(|np| proof_compare(cp, np)) {
                to_delete.push(cp);
            }
        }

        (to_delete, to_add)
    }

    fn user_can_update_score(
        user: &UserCookie,
        current_score: &FullScoreEntity,
    ) -> TgmRankResult<()> {
        if !user.user_role.can_verify_scores() && user.user_id != current_score.player.player_id {
            return Err(AuthenticationError::UnauthorizedForOperation.into());
        }

        Ok(())
    }

    pub async fn get_achievement_list(
        db: &mut DbConnection,
    ) -> TgmRankResult<Vec<AchievementModel>> {
        let achievements = AchievementEntity::get_all(db)
            .await?
            .into_iter()
            .map(AchievementModel::from)
            .collect_vec();
        Ok(achievements)
    }

    pub async fn verify_score(
        mut db: &mut DbConnection,
        settings: &Settings,
        verifier: &UserCookie,
        score_id: i32,
        form: &VerifyScoreForm,
    ) -> TgmRankResult<ScoreEntity> {
        form.validate()?;

        let current_score = FullScoreEntity::get_by_ids(db, &[score_id], &GenericFilter::default())
            .await?
            .swap_remove(0);

        Self::can_verify_score(&verifier, current_score.player.player_id, &form.status)?;

        let update = VerifyScoreEntity {
            status: &form.status,
            verification_comment: form.comment.as_deref(),
            verified_by: verifier.user_id,
        };

        update.upsert_vote(db, score_id).await?;
        let score = update.update(db, score_id).await?;

        Self::refresh_materialized_views(&mut db).await?;

        if Self::should_send_rejection_notification(current_score.status, update.status) {
            let account = PlayerService::find_by_id(db, score.player_id).await?;
            if account.email.is_some() {
                EmailService::send_rejected_score_email(db, settings, &account, score.score_id)
                    .await?;
            } else {
                info!(
                    "Did not send score rejection email to {}, account is not set up",
                    account.player_name
                );
            }
        }

        AccountAchievementEntity::update_for_players(&mut db, &[score.player_id]).await?;

        Ok(score)
    }

    fn can_verify_score(
        user: &UserCookie,
        score_player_id: i32,
        target_status: &ScoreStatus,
    ) -> TgmRankResult<()> {
        if !user.user_role.can_verify_scores() {
            return Err(AuthenticationError::UnauthorizedForOperation.into());
        }

        // TODO: Think about whether should also disallow un-rejecting own scores
        if user.user_id == score_player_id
            && (target_status == &ScoreStatus::Verified || target_status == &ScoreStatus::Accepted)
        {
            return Err(ClientError::CannotVerifyOwnScore.into());
        }

        Ok(())
    }

    pub fn should_send_rejection_notification(
        current_status: ScoreStatus,
        update_status: &ScoreStatus,
    ) -> bool {
        update_status == &ScoreStatus::Rejected && current_status != ScoreStatus::Rejected
    }
}

async fn get_link_content_type(
    client: &reqwest::Client,
    form: &ProofLinkForm,
) -> Result<mime::Mime, ProofError> {
    let response = client.head(&form.link).send().await.map_err(|e| {
        error!("{:?}", e);
        ProofError::BrokenLink
    })?;
    let mime_type = response
        .headers()
        .get(reqwest::header::CONTENT_TYPE)
        .ok_or(ProofError::NoContentType)?
        .to_str()
        .map_err(|_e| ProofError::NoContentType)?
        .parse::<mime::Mime>()
        .map_err(|e| ProofError::InvalidContentType(e.to_string()))?;
    Ok(mime_type)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::models::Role;

    impl Default for UserCookie {
        fn default() -> Self {
            UserCookie {
                user_id: 123,
                user_role: Role::User,
                session_key: Default::default(),
            }
        }
    }

    mod validate_image_content_type {
        use super::*;
        use std::str::FromStr;

        #[test]
        fn accepts_valid_content_types() {
            for content_type in &VALID_CONTENT_TYPES {
                let result = ScoreService::validate_image_content_type(content_type);
                assert!(result.is_ok());
            }
        }

        #[test]
        fn rejects_other_content_types() {
            let other_content_types = vec![
                mime::APPLICATION_JSON,
                mime::APPLICATION_PDF,
                mime::Mime::from_str("hello/world").unwrap(),
                mime::Mime::from_str("video/mp4").unwrap(),
            ];

            for content_type in other_content_types {
                let result = ScoreService::validate_image_content_type(&content_type);
                assert!(result.is_err(), "Did not get an error");
                assert!(matches!(
                    result.unwrap_err(),
                    ProofError::UnsupportedImageType(_)
                ));
            }
        }
    }

    mod validate_video_content_type {
        use super::*;
        use std::str::FromStr;

        #[test]
        fn accepts_common_video_types() {
            let some_common_video_content_types =
                vec!["video/mp4", "video/webm", "video/x-flv", "video/x-msvideo"]
                    .into_iter()
                    .map(|ct| ct.parse::<mime::Mime>().unwrap());

            for content_type in some_common_video_content_types {
                let result = ScoreService::validate_video_content_type(&content_type);
                assert!(result.is_ok());
            }
        }

        #[test]
        fn accepts_application_octet_stream() {
            let content_type = mime::Mime::from_str("application/octet-stream").unwrap();
            let result = ScoreService::validate_video_content_type(&content_type);
            assert!(result.is_ok());
        }

        #[test]
        fn rejects_other_content_types() {
            let other_content_types = vec![
                mime::APPLICATION_JSON,
                mime::APPLICATION_PDF,
                mime::IMAGE_PNG,
                mime::Mime::from_str("hello/world").unwrap(),
            ];

            for content_type in other_content_types {
                let result = ScoreService::validate_video_content_type(&content_type);
                assert!(result.is_err(), "Did not get an error");
                assert!(matches!(
                    result.unwrap_err(),
                    ProofError::UnsupportedVideoType(_)
                ));
            }
        }
    }

    mod validate_video_host {
        use super::*;

        #[test]
        fn accepts_valid_hosts() {
            for domain in &VALID_VIDEO_DOMAINS {
                let uri = Uri::from_static(domain);
                let result = ScoreService::validate_video_host(&uri);
                assert!(result.is_ok());
            }
        }

        #[test]
        fn rejects_other_hosts() {
            let uri = Uri::from_static("my.video.site.com");
            let result = ScoreService::validate_video_host(&uri);
            assert!(result.is_err());
            assert!(matches!(
                result.expect_err("Did not get an error"),
                ProofError::UnsupportedVideoSite
            ));
        }

        #[test]
        fn rejects_relative_paths() {
            let uri = Uri::from_static("/hello/world");
            let result = ScoreService::validate_video_host(&uri);
            assert!(matches!(
                result.expect_err("Did not get an error"),
                ProofError::InvalidHostName
            ));
        }
    }

    mod make_insertable_score {
        use super::*;

        #[test]
        fn should_return_unauthorized_when_non_admins_try_to_submit_for_other_users() {
            let user = UserCookie {
                user_id: 123,
                ..Default::default()
            };

            let form = AddScoreForm {
                player_id: Some(848),
                ..Default::default()
            };

            let result = ScoreService::make_insertable_score(&user, &form);
            assert!(result.is_err());
            assert!(matches!(
                result.expect_err("Did not get an error"),
                TgmRankError::AuthenticationError(AuthenticationError::UnauthorizedForOperation)
            ));
        }

        #[test]
        fn should_submit_for_player_on_form_for_admins() {
            let submit_for_user_id = 460;
            let user = UserCookie {
                user_id: 123,
                user_role: Role::Admin,
                ..Default::default()
            };

            let form = AddScoreForm {
                player_id: Some(submit_for_user_id),
                ..Default::default()
            };

            let result = ScoreService::make_insertable_score(&user, &form);
            let score_to_add = result.expect("Expected Ok, Got an Err");
            assert_eq!(score_to_add.player_id, submit_for_user_id);
        }

        #[test]
        fn should_submit_logged_in_user() {
            let user = UserCookie {
                user_id: 123,
                ..Default::default()
            };

            let form = AddScoreForm::default();

            let result = ScoreService::make_insertable_score(&user, &form);
            assert!(result.is_ok());

            let score_to_add = result.expect("Expected Ok, Got an Err");
            assert_eq!(score_to_add.player_id, user.user_id);
        }

        #[test]
        fn should_set_status_to_pending() {
            let user = UserCookie::default();
            let form = AddScoreForm::default();

            let result = ScoreService::make_insertable_score(&user, &form);
            assert!(result.is_ok());

            let score_to_add = result.expect("Expected Ok, Got an Err");
            assert_eq!(score_to_add.status, ScoreStatus::Pending);
        }

        #[test]
        fn should_set_updated_by_to_logged_in_user() {
            let user = UserCookie {
                user_id: 52,
                ..Default::default()
            };
            let form = AddScoreForm::default();

            let result = ScoreService::make_insertable_score(&user, &form);
            assert!(result.is_ok());

            let score_to_add = result.expect("Expected Ok, Got an Err");
            assert_eq!(score_to_add.updated_by, user.user_id);
        }
    }

    mod make_updatable_score {
        use super::*;

        #[test]
        fn should_use_user_id_for_updated_by() {
            let user_id = 57;
            let user = UserCookie {
                user_id,
                ..Default::default()
            };
            let form = UpdateScoreForm::default();

            let result = ScoreService::make_updatable_score(&user, &form);
            assert_eq!(result.updated_by, user.user_id);
        }
    }

    mod user_can_update_score {
        use super::*;

        #[test]
        fn should_return_ok_when_role_can_verify_scores() {
            let user = UserCookie {
                user_role: Role::Verifier,
                ..Default::default()
            };

            let can_update =
                ScoreService::user_can_update_score(&user, &FullScoreEntity::default());
            assert!(can_update.is_ok());
        }

        #[test]
        fn should_return_err_when_user_tries_to_update_another_users_score() {
            let user = UserCookie {
                user_id: 9,
                ..Default::default()
            };

            let can_update =
                ScoreService::user_can_update_score(&user, &FullScoreEntity::default());
            assert!(matches!(
                can_update.expect_err("Did not get an error"),
                TgmRankError::AuthenticationError(AuthenticationError::UnauthorizedForOperation)
            ))
        }
    }

    mod can_verify_score {
        use super::*;
        use crate::utility::ClientError;

        #[test]
        fn should_return_ok_when_role_can_verify_scores() {
            let user = UserCookie {
                user_id: 6,
                user_role: Role::Verifier,
                ..Default::default()
            };

            let can_update = ScoreService::can_verify_score(&user, 102, &ScoreStatus::Verified);
            assert!(can_update.is_ok());
        }

        #[test]
        fn should_return_err_when_user_cannot_verify_scores() {
            let user = UserCookie {
                user_id: 9,
                user_role: Role::User,
                ..Default::default()
            };

            let can_update = ScoreService::can_verify_score(&user, 8534, &ScoreStatus::Verified);

            assert!(matches!(
                can_update.expect_err("Did not get an error"),
                TgmRankError::AuthenticationError(AuthenticationError::UnauthorizedForOperation)
            ))
        }

        #[test]
        fn should_return_err_when_verifier_tries_to_verify_their_own_score() {
            let user = UserCookie {
                user_id: 9,
                user_role: Role::Admin,
                ..Default::default()
            };

            let can_update =
                ScoreService::can_verify_score(&user, user.user_id, &ScoreStatus::Verified);

            assert!(matches!(
                can_update.expect_err("Did not get an error"),
                TgmRankError::ClientError(ClientError::CannotVerifyOwnScore)
            ));

            let can_update =
                ScoreService::can_verify_score(&user, user.user_id, &ScoreStatus::Accepted);

            assert!(matches!(
                can_update.expect_err("Did not get an error"),
                TgmRankError::ClientError(ClientError::CannotVerifyOwnScore)
            ))
        }

        #[test]
        fn should_return_ok_when_verifier_tries_to_reject_their_own_score() {
            let user = UserCookie {
                user_id: 9,
                user_role: Role::Admin,
                ..Default::default()
            };

            let can_update =
                ScoreService::can_verify_score(&user, user.user_id, &ScoreStatus::Rejected);

            assert!(can_update.is_ok());
        }
    }

    mod dedup_proof {
        use super::*;

        #[test]
        fn should_handle_new_proof_not_in_existing_proof() {
            let current_proof = vec![];
            let new_proof: Vec<NewProofEntity> = vec![NewProofEntity {
                score_id: 123,
                proof_type: &ProofType::Image,
                link: "new-link",
            }];

            let (to_delete, to_add) = ScoreService::dedup_proof(&current_proof, &new_proof);
            assert!(to_delete.is_empty());
            assert_eq!(to_add.len(), 1);
            assert_eq!(to_add[0].link, "new-link");
        }

        #[test]
        fn should_handle_current_proof_not_in_new_proof() {
            let current_proof = vec![ProofEntity {
                proof_id: 123,
                score_id: 456,
                proof_type: ProofType::Image,
                link: "new-link".to_string(),
            }];
            let new_proof: Vec<NewProofEntity> = vec![];

            let (to_delete, to_add) = ScoreService::dedup_proof(&current_proof, &new_proof);
            assert_eq!(to_delete.len(), 1);
            assert_eq!(to_delete[0].link, "new-link");
            assert!(to_add.is_empty());
        }

        #[test]
        fn should_handle_matching_proof() {
            let current_proof = vec![ProofEntity {
                proof_id: 123,
                score_id: 456,
                proof_type: ProofType::Image,
                link: "new-link".to_string(),
            }];
            let new_proof: Vec<NewProofEntity> = vec![NewProofEntity {
                score_id: 456,
                proof_type: &ProofType::Image,
                link: "new-link",
            }];

            let (to_delete, to_add) = ScoreService::dedup_proof(&current_proof, &new_proof);
            assert!(to_delete.is_empty());
            assert!(to_add.is_empty());
        }
    }

    mod should_send_rejection_notification {
        use super::*;

        #[test]
        fn should_return_true_when_status_changes_to_rejected_from_non_rejected_status() {
            let examples = vec![
                ScoreStatus::Verified,
                ScoreStatus::Pending,
                ScoreStatus::Legacy,
                ScoreStatus::Unverified,
            ];

            for input in examples {
                let result =
                    ScoreService::should_send_rejection_notification(input, &ScoreStatus::Rejected);

                assert_eq!(result, true);
            }
        }

        #[test]
        fn should_return_false_when_status_stays_rejected() {
            let result = ScoreService::should_send_rejection_notification(
                ScoreStatus::Rejected,
                &ScoreStatus::Rejected,
            );

            assert_eq!(result, false);
        }

        #[test]
        fn should_return_false_when_updating_to_verified() {
            let result = ScoreService::should_send_rejection_notification(
                ScoreStatus::Rejected,
                &ScoreStatus::Verified,
            );

            assert_eq!(result, false);
        }
    }
}
