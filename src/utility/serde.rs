use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::fmt::{Display, Formatter};

#[derive(Clone)]
pub struct WrappedInt(pub i32);

impl From<i32> for WrappedInt {
    fn from(value: i32) -> Self {
        Self(value)
    }
}

impl Display for WrappedInt {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self.0))
    }
}

impl Serialize for WrappedInt {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(&self.0.to_string())
    }
}

impl<'de> Deserialize<'de> for WrappedInt {
    fn deserialize<D>(deserializer: D) -> Result<WrappedInt, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        let i = &s.parse::<i32>().map_err(serde::de::Error::custom)?;
        Ok(WrappedInt(*i))
    }
}

pub mod stringified_list {
    use itertools::Itertools;
    use serde::{
        de::{self, IntoDeserializer},
        Serializer,
    };
    use std::fmt;
    use std::fmt::Display;

    pub fn deserialize<'de, D, I>(deserializer: D) -> Result<Vec<I>, D::Error>
    where
        D: de::Deserializer<'de>,
        I: de::DeserializeOwned,
    {
        struct StringVecVisitor<I>(std::marker::PhantomData<I>);

        impl<'de, I> de::Visitor<'de> for StringVecVisitor<I>
        where
            I: de::DeserializeOwned,
        {
            type Value = Vec<I>;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("a string containing a list")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: de::Error,
            {
                let mut ids = Vec::new();
                for id in v.split(',') {
                    let id = I::deserialize(id.trim().into_deserializer())?;
                    ids.push(id);
                }
                Ok(ids)
            }
        }

        deserializer.deserialize_any(StringVecVisitor(std::marker::PhantomData::<I>))
    }

    pub fn serialize<S, I>(items: &[I], serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
        I: Display,
    {
        let s = items
            .iter()
            .map(|item| item.to_string())
            .collect_vec()
            .join(",");
        serializer.serialize_str(&s)
    }
}

pub mod opt_stringified_list {
    use itertools::Itertools;
    use serde::{
        de::{self},
        Serializer,
    };
    use std::fmt::Display;

    pub fn deserialize<'de, D, I>(deserializer: D) -> Result<Option<Vec<I>>, D::Error>
    where
        D: de::Deserializer<'de>,
        I: de::DeserializeOwned,
    {
        if let Ok(v) = super::stringified_list::deserialize(deserializer) {
            Ok(Some(v))
        } else {
            Ok(None)
        }
    }

    pub fn serialize<S, I>(items: &Option<Vec<I>>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
        I: Display,
    {
        if let Some(items) = items {
            let s = items
                .iter()
                .map(|item| item.to_string())
                .collect_vec()
                .join(",");
            serializer.serialize_str(&s)
        } else {
            serializer.serialize_none()
        }
    }
}

#[cfg(test)]
mod tests {
    use super::WrappedInt;
    use crate::models::ScoreStatus;
    use crate::utility::test_helpers::tests::TestResult;
    use serde::{Deserialize, Serialize};
    use serde_json::json;

    #[derive(Deserialize, Serialize)]
    pub struct ThingWithInts {
        #[serde(with = "super::stringified_list")]
        pub test: Vec<WrappedInt>,
    }

    #[derive(Deserialize, Serialize)]
    pub struct CustomThing {
        #[serde(with = "super::stringified_list")]
        pub test: Vec<ScoreStatus>,
    }

    #[derive(Deserialize, Serialize)]
    pub struct CustomThingWithOption {
        #[serde(default, with = "super::opt_stringified_list")]
        pub test: Option<Vec<ScoreStatus>>,
    }

    #[test]
    fn stringified_list_should_work_with_custom_objects() -> TestResult<()> {
        let json = json!({
            "test": "rejected , accepted, legacy "
        });

        let thing: CustomThing = serde_json::from_str(&json.to_string())?;

        assert_eq!(thing.test.len(), 3);
        assert_eq!(thing.test[0], ScoreStatus::Rejected);
        assert_eq!(thing.test[1], ScoreStatus::Accepted);
        assert_eq!(thing.test[2], ScoreStatus::Legacy);

        let restringified = serde_json::to_string(&thing)?;
        assert!(restringified
            .as_str()
            .contains("\"rejected,accepted,legacy\""));

        Ok(())
    }

    #[test]
    fn deserialize_stringified_list_should_work_with_integers() -> TestResult<()> {
        let json = json!({
            "test": "10 , 20, 30 "
        });

        let thing: ThingWithInts = serde_json::from_str(&json.to_string())?;

        assert_eq!(thing.test.len(), 3);
        assert_eq!(thing.test[0].0, 10);
        assert_eq!(thing.test[1].0, 20);
        assert_eq!(thing.test[2].0, 30);

        let restringified = serde_json::to_string(&thing)?;
        assert!(restringified.as_str().contains("\"10,20,30\""));

        Ok(())
    }

    #[test]
    fn deserialize_stringified_list_should_work_for_options() -> TestResult<()> {
        let json = json!({
            "test": "rejected , accepted, legacy "
        });

        let thing: CustomThingWithOption = serde_json::from_str(&json.to_string())?;

        assert!(thing.test.is_some());

        if let Some(test) = &thing.test {
            assert_eq!(test.len(), 3);
            assert_eq!(test[0], ScoreStatus::Rejected);
            assert_eq!(test[1], ScoreStatus::Accepted);
            assert_eq!(test[2], ScoreStatus::Legacy);
        }

        let restringified = serde_json::to_string(&thing)?;
        assert!(restringified
            .as_str()
            .contains("\"rejected,accepted,legacy\""));

        Ok(())
    }

    #[test]
    fn deserialize_stringified_list_should_work_for_options_when_field_is_missing() -> TestResult<()>
    {
        let json = json!({});

        let thing: CustomThingWithOption = serde_json::from_str(&json.to_string())?;

        assert!(thing.test.is_none());

        let restringified = serde_json::to_string(&thing)?;
        assert!(restringified.as_str().contains("null"));

        Ok(())
    }
}
