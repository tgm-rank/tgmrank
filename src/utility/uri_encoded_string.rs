use std::fmt;

use actix_web::http::header::http_percent_encode;

pub struct UriEncodedString<'a>(pub &'a str);

impl fmt::Display for UriEncodedString<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        http_percent_encode(f, self.0.as_bytes())
    }
}
