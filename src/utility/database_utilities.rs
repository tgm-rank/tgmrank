use std::time::Duration;

use actix_web::web;
use futures::Future;

use crate::utility::{TgmRankError, TgmRankResult};

pub async fn try_connect<T, F, R>(
    connection_label: &str,
    connect_fn: &F,
    tries: i32,
) -> TgmRankResult<T>
where
    F: Fn() -> R,
    R: Future<Output = TgmRankResult<T>>,
{
    for attempt in 1..=tries {
        match connect_fn().await {
            Ok(inner) => {
                info!(
                    "Connected to {} after {} attempt(s)",
                    connection_label, attempt
                );
                return Ok(inner);
            }
            Err(err) => {
                warn!(
                    "Failed to connect to {} (attempt {}): {}",
                    connection_label, attempt, err
                );

                web::block(|| -> TgmRankResult<()> {
                    std::thread::sleep(Duration::from_secs(1));
                    Ok(())
                })
                .await
                .map_err(TgmRankError::from_std)??;
            }
        }
    }

    Err(TgmRankError::InternalServerError(format!(
        "Could not connect to {}",
        connection_label
    )))
}
