//pub struct CachedFile(NamedFile);
//
//impl CachedFile {
//    pub fn new(path: &str, file: PathBuf) -> Option<Self> {
//        CachedFile::from_path(&Path::new(path).join(file))
//    }
//
//    pub fn from_path(path: &Path) -> Option<Self> {
//        NamedFile::open(path).ok().map(CachedFile)
//    }
//}

//impl<'r> Responder<'r> for CachedFile {
//    fn respond_to(self, req: &Request) -> Result<'r> {
//        Response::build_from(self.0.respond_to(req)?)
//            .raw_header("Cache-control", "max-age=86400") //  24h (24*60*60)
//            .ok()
//    }
//}
