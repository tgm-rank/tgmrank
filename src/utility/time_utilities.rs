use once_cell::sync::Lazy;
use regex::Regex;
use time::Time;

static TIME_RE: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"^(?P<M>[0-5]?[0-9]):(?P<S>[0-5][0-9])[:.](?P<CS>\d{2})$").unwrap());

pub fn validate_playtime_str(s: &str) -> bool {
    TIME_RE.is_match(s)
}

pub fn parse_playtime(s: &str) -> Result<Time, time::error::TryFromParsed> {
    match TIME_RE.captures(s) {
        Some(captures) => {
            let minutes = &captures["M"];
            let seconds = &captures["S"];
            let centiseconds = &captures["CS"];

            let new_time = Time::from_hms_milli(
                0,
                minutes.parse().unwrap(),
                seconds.parse().unwrap(),
                centiseconds.parse::<u16>().unwrap() * 10,
            )?;

            Ok(new_time)
        }
        None => Err(time::error::TryFromParsed::InsufficientInformation),
    }
}

pub fn playtime_string(time: &Time) -> String {
    format!(
        "00:{:0>2}:{:0>2}.{:0>3}",
        time.minute(),
        time.second(),
        time.millisecond()
    )
}

// time::serde::format_description!(date_serde, Date, "[year]-[month]-[day]");

// time::serde::format_description!(datetime_serde, OffsetDateTime, "[year]-[month]-[day]");

// pub mod datetime_serde {
//     use serde::{Deserialize, Deserializer};
//     use time::OffsetDateTime;

//     pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<OffsetDateTime>, D::Error>
//     where
//         D: Deserializer<'de>,
//     {
//         if let Ok(s) = String::deserialize(deserializer) {
//             let dt = OffsetDateTime::parse(&s, &time::format_description::well_known::Rfc3339)
//                 .map_err(serde::de::Error::custom)?;

//             return Ok(Some(dt));
//         }

//         Ok(None)
//     }

//     pub fn deserialize_dt<'de, D>(deserializer: D) -> Result<OffsetDateTime, D::Error>
//     where
//         D: Deserializer<'de>,
//     {
//         let s = String::deserialize(deserializer)?;
//         let dt =
//             OffsetDateTime::parse(&s, &time::format_description::well_known::Rfc3339).map_err(serde::de::Error::custom)?;

//         Ok(dt)
//     }
// }

#[cfg(test)]
mod tests {
    use crate::utility::test_helpers::tests::TestResult;
    use crate::utility::time_utilities;

    #[test]
    fn validate_playtime_str_should_reject_stuff() -> TestResult<()> {
        let invalid_times = vec![
            "",
            "1",
            "a",
            "1:",
            "4:3.2",
            "a:3.2",
            "4:b.2",
            "4:3.c",
            "100:12:12",
            "12:100:12",
            "12:34.567",
            "60:12.23",
            "12:60.34",
            "12:34.5",
        ];

        for s in &invalid_times {
            assert_eq!(time_utilities::validate_playtime_str(s), false);
        }

        Ok(())
    }

    #[test]
    fn validate_playtime_str_should_accept_stuff() -> TestResult<()> {
        let valid_times = vec!["4:11.32", "04:11.32", "04:11:32"];

        for s in &valid_times {
            assert_eq!(time_utilities::validate_playtime_str(s), true);
        }

        Ok(())
    }

    #[test]
    fn get_playtime_should_parse_minutes() -> TestResult<()> {
        let time = time_utilities::parse_playtime("05:00.00")?;
        assert_eq!(time.minute(), 5);

        Ok(())
    }

    #[test]
    fn get_playtime_should_parse_seconds() -> TestResult<()> {
        let time = time_utilities::parse_playtime("00:23.00")?;
        assert_eq!(time.second(), 23);

        Ok(())
    }

    #[test]
    fn get_playtime_should_parse_milliseconds() -> TestResult<()> {
        let time = time_utilities::parse_playtime("00:00.05")?;
        assert_eq!(time.millisecond(), 50);

        Ok(())
    }

    #[test]
    fn get_playtime_should_parse_milliseconds_with_colon_separator() -> TestResult<()> {
        let time = time_utilities::parse_playtime("00:00:95")?;
        assert_eq!(time.millisecond(), 950);

        Ok(())
    }

    #[test]
    fn playtime_string_should_format_minutes_seconds_and_centiseconds() -> TestResult<()> {
        let time = time::Time::from_hms_milli(0, 5, 4, 50)?;
        let time_str = time_utilities::playtime_string(&time);

        assert_eq!(time_str, "00:05:04.050");

        Ok(())
    }

    #[test]
    fn playtime_string_should_ignore_hours() -> TestResult<()> {
        let time = time::Time::from_hms_milli(2, 5, 4, 100)?;
        let time_str = time_utilities::playtime_string(&time);

        assert_eq!(time_str, "00:05:04.100");

        Ok(())
    }

    // mod date_serde {
    //     use serde::{Deserialize, Serialize};
    //     use serde_json::json;
    //     use time::Date;

    //     use crate::utility::test_helpers::tests::TestResult;

    //     #[test]
    //     fn serialize_date_should_have_year_month_day() -> TestResult<()> {
    //         #[derive(Serialize)]
    //         pub struct Test {
    //             #[serde(with = "crate::utility::time_utilities::date_serde")]
    //             pub date: Date,
    //         }

    //         let year = 2323;
    //         let month = 12;
    //         let day = 9;

    //         let thing = Test {
    //             date: Date::try_from_ymd(year, month, day)?,
    //         };
    //         let date_str = serde_json::to_string(&thing)?;

    //         assert_eq!(&date_str, r#"{"date":"2323-12-09"}"#);

    //         Ok(())
    //     }

    //     #[test]
    //     fn deserialize_date_should_work() -> TestResult<()> {
    //         #[derive(Deserialize)]
    //         pub struct Test {
    //             #[serde(with = "crate::utility::time_utilities::date_serde")]
    //             pub date: Date,
    //         }

    //         let json = json!({
    //             "date": "2020-04-25"
    //         });

    //         let thing: Test = serde_json::from_str(&json.to_string())?;

    //         assert_eq!(thing.date.year(), 2020);
    //         assert_eq!(thing.date.month(), 4);
    //         assert_eq!(thing.date.day(), 25);

    //         Ok(())
    //     }
    // }

    // mod datetime_serde {
    //     use crate::utility::test_helpers::tests::TestResult;
    //     use serde::Deserialize;
    //     use serde_json::json;

    //     use time::OffsetDateTime;

    //     #[derive(Deserialize)]
    //     pub struct Test {
    //         #[serde(
    //             deserialize_with = "crate::utility::time_utilities::datetime_serde::deserialize"
    //         )]
    //         pub dt: Option<OffsetDateTime>,
    //     }

    //     #[test]
    //     fn deserialize_should_work() -> TestResult<()> {
    //         let json = json!({
    //             "dt": "2021-08-19T23:58:34.759088+03:00"
    //         });

    //         let thing: Test = serde_json::from_str(&json.to_string())?;

    //         assert!(thing.dt.is_some());

    //         let dt = thing.dt.unwrap();
    //         assert_eq!(dt.year(), 2021);
    //         assert_eq!(dt.month(), 8);
    //         assert_eq!(dt.day(), 19);
    //         assert_eq!(dt.hour(), 23);
    //         assert_eq!(dt.minute(), 58);
    //         assert_eq!(dt.second(), 34);
    //         assert_eq!(dt.offset().as_hours(), 3);

    //         Ok(())
    //     }

    //     #[test]
    //     fn deserialize_should_deserialize_null_as_none() -> TestResult<()> {
    //         let json = json!({ "dt": null });
    //         let thing = serde_json::from_str::<Test>(&json.to_string());

    //         assert!(thing.is_ok());
    //         assert!(thing.unwrap().dt.is_none());

    //         Ok(())
    //     }
    // }
}
