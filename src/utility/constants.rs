use actix_web::http::header;
use actix_web::http::header::HeaderName;

pub const CDN_CACHE_CONTROL_HEADER: (HeaderName, &str) =
    (header::CACHE_CONTROL, "max-age=0, s-maxage=21600");
