use std::{
    sync::{Arc, RwLock},
    thread::LocalKey,
};

pub trait CachedItem<T> {
    fn current(&'static self) -> Arc<T>;
    fn replace(&'static self, new_item: T) -> Arc<T>;
}

impl<T> CachedItem<T> for LocalKey<RwLock<Arc<T>>> {
    fn current(&'static self) -> Arc<T> {
        self.with(|i| i.read().unwrap().clone())
    }

    fn replace(&'static self, new_item: T) -> Arc<T> {
        self.with(|t| *t.write().unwrap() = Arc::new(new_item));
        self.current()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn current_should_keep_current_value_after_replacement() {
        thread_local! {
            static ITEM: RwLock<Arc<i32>> = RwLock::new(Arc::new(1));
        }

        let x = ITEM.current();
        let new_item = ITEM.replace(2);

        assert_eq!(x.as_ref(), &1);
        assert_eq!(new_item.as_ref(), &2);
    }
}
