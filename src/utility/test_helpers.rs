#[cfg(test)]
pub mod tests {
    use crate::controllers::models::{
        ModeRankingModel, PlayerModel, RankDeltaModel, RankDiffModel, RecentActivityModel,
    };
    use crate::models::{PlayerEntity, Role, ScoreEntity, ScoreStatus};

    pub type TestResult<T> = Result<T, anyhow::Error>;

    mod model_defaults {
        use super::*;
        use time::OffsetDateTime;

        impl Default for RecentActivityModel {
            fn default() -> Self {
                Self {
                    game_id: 0,
                    mode_id: 0,
                    score: ModeRankingModel {
                        score_id: 0,
                        game_id: 0,
                        mode_id: 0,
                        player: Some(PlayerModel {
                            player_id: 0,
                            player_name: "".to_string(),
                            games: None,
                            avatar: None,
                            location: None,
                        }),
                        level: None,
                        playtime: None,
                        grade: None,
                        proof: vec![],
                        comment: None,
                        score: None,
                        rank: None,
                        ranking_points: None,
                        status: ScoreStatus::Unverified,
                        created_at: OffsetDateTime::now_utc().to_string(),
                        verification: None,
                        platform: None,
                    },
                    previous_score: None,
                    delta: RankDeltaModel {
                        overall: RankDiffModel {
                            rank: None,
                            ranking_points: None,
                            previous_rank: None,
                            previous_ranking_points: None,
                            players_offset: Some(vec![]),
                        },
                        game: RankDiffModel {
                            rank: None,
                            ranking_points: None,
                            previous_rank: None,
                            previous_ranking_points: None,
                            players_offset: Some(vec![]),
                        },
                        mode: RankDiffModel {
                            rank: None,
                            ranking_points: None,
                            previous_rank: None,
                            previous_ranking_points: None,
                            players_offset: Some(vec![]),
                        },
                    },
                }
            }
        }
    }

    mod entity_defaults {
        use super::*;
        use crate::models::{FullScoreEntity, PlayerEntity2};
        use time::OffsetDateTime;

        impl Default for PlayerEntity {
            fn default() -> Self {
                Self {
                    player_id: 0,
                    player_name: "".to_string(),
                    email: None,
                    password: "".to_string(),
                    player_role: Role::User,
                    active: false,
                    avatar_file: None,
                    location_id: None,
                    location: None,
                    created_at: OffsetDateTime::now_utc(),
                    updated_at: OffsetDateTime::now_utc(),
                }
            }
        }

        impl Default for PlayerEntity2 {
            fn default() -> Self {
                Self {
                    player_id: 0,
                    player_name: "".to_string(),
                    avatar_file: None,
                    location_id: None,
                    location: None,
                }
            }
        }

        impl Default for FullScoreEntity {
            fn default() -> Self {
                Self {
                    score_id: 0,
                    grade: None,
                    level: None,
                    playtime: None,
                    score: None,
                    status: ScoreStatus::Unverified,
                    comment: None,
                    player: PlayerEntity2::default(),
                    mode_id: 0,
                    created_at: OffsetDateTime::now_utc(),
                    updated_at: OffsetDateTime::now_utc(),
                    updated_by: 0,
                    platform: None,
                    verification_comment: None,
                    verified_on: None,
                    verifier: None,
                    proof: None,
                }
            }
        }

        impl Default for ScoreEntity {
            fn default() -> Self {
                Self {
                    score_id: 0,
                    grade_id: None,
                    level: None,
                    playtime: None,
                    score: None,
                    status: ScoreStatus::Unverified,
                    comment: None,
                    player_id: 0,
                    mode_id: 0,
                    created_at: OffsetDateTime::now_utc(),
                    updated_at: OffsetDateTime::now_utc(),
                    updated_by: 0,
                    platform_id: None,
                }
            }
        }
    }
}
