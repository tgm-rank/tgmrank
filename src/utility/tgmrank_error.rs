use std::fmt::Debug;

use actix_web::http::StatusCode;
use actix_web::{HttpResponse, ResponseError};
use itertools::Itertools;
use once_cell::sync::Lazy;
use regex::{Captures, Regex};
use thiserror::Error;
use validator::{ValidationErrors, ValidationErrorsKind};

use crate::controllers::models::ErrorModel;

pub static SNAKE_CASE_RE: Lazy<Regex> = Lazy::new(|| Regex::new(r"(_[a-z])").unwrap());

pub type TgmRankResult<T> = Result<T, TgmRankError>;
//pub type TgmRankResult<T> = anyhow::Result<T>;

#[derive(Error, Debug, Clone, PartialEq)]
pub enum ScoreRuleError {
    #[error("Score is a required field")]
    Required,
    #[error("Score must be empty")]
    MustBeEmpty,
    #[error("Invalid score for this mode")]
    Invalid,
    #[error("Invalid score for grade")]
    InvalidForGrade,
}

#[derive(Error, Debug, Clone, PartialEq)]
pub enum GradeRuleError {
    #[error("Grade is a required field")]
    Required,
    #[error("Grade must be empty")]
    MustBeEmpty,
    #[error("Invalid grade for this mode")]
    Invalid,
}

#[derive(Error, Debug, Clone, PartialEq)]
pub enum TimeRuleError {
    #[error("Time is a required field")]
    Required,
    #[error("Time must be empty")]
    MustBeEmpty,
    #[error("Invalid time for this mode")]
    Invalid,
    #[error("Invalid time for grade")]
    InvalidForGrade,
}

#[derive(Error, Debug, Clone, PartialEq)]
pub enum LevelRuleError {
    #[error("Level is a required field")]
    Required,
    #[error("Level must be empty")]
    MustBeEmpty,
    #[error("Invalid level for this mode")]
    Invalid,
    #[error("Invalid level for grade")]
    InvalidForGrade,
}

#[derive(Error, Debug, Clone, PartialEq)]
pub enum ProofError {
    #[error("Broken proof link")]
    BrokenLink,
    #[error("Error reading proof content-type.")]
    NoContentType,
    #[error("Invalid Content Type {0}")]
    InvalidContentType(String),
    #[error(
        "Unsupported image content type ({0}). Please verify this is a direct link to a bmp, gif, jpg, png file."
    )]
    UnsupportedImageType(String),
    #[error("Unsupported video site. Please use youtube or twitch.")]
    UnsupportedVideoSite,
    #[error("Unsupported video content type ({0}). Please verify this is a direct link to a video file.")]
    UnsupportedVideoType(String),
    #[error("Invalid host name.")]
    InvalidHostName,
}

#[derive(Error, Debug, Clone, PartialEq)]
pub enum ValidationRuleError {
    #[error("{0}")]
    ScoreRuleError(#[from] ScoreRuleError),
    #[error("{0}")]
    GradeRuleError(#[from] GradeRuleError),
    #[error("{0}")]
    TimeRuleError(#[from] TimeRuleError),
    #[error("{0}")]
    LevelRuleError(#[from] LevelRuleError),
    #[error("Invalid proof link: {1}")]
    ProofLinkError(usize, ProofError),
    #[error("{message}")]
    CustomFieldError { field_name: String, message: String },
}

impl ValidationRuleError {
    pub fn get_field_name(&self) -> String {
        match self {
            ValidationRuleError::ScoreRuleError(..) => "score".to_string(),
            ValidationRuleError::GradeRuleError(..) => "grade".to_string(),
            ValidationRuleError::TimeRuleError(..) => "playtime".to_string(),
            ValidationRuleError::LevelRuleError(..) => "level".to_string(),
            ValidationRuleError::ProofLinkError(index, ..) => format!("proof[{}].link", index),
            ValidationRuleError::CustomFieldError { field_name, .. } => field_name.to_string(),
        }
    }
}

#[derive(Debug)]
pub struct ValidationRuleErrors(Vec<ValidationRuleError>);

impl From<&[ValidationRuleError]> for ValidationRuleErrors {
    fn from(i: &[ValidationRuleError]) -> Self {
        Self(i.to_vec())
    }
}

impl From<ValidationRuleError> for ValidationRuleErrors {
    fn from(error: ValidationRuleError) -> Self {
        Self(vec![error])
    }
}

#[derive(Error, Debug)]
pub enum ScoreValidationError {
    #[error("Duplicate Score")]
    DuplicateScore,
    #[error("Mode is locked for submissions")]
    ModeIsLocked,
    #[error("Leaderboard is not open for submissions yet")]
    TooEarlySubmission,
    #[error("Leaderboard is closed for submissions")]
    TooLateSubmission,
}

#[derive(Error, Debug)]
pub enum ClientError {
    #[error("Invalid Content Type")]
    InvalidContentType,
    #[error("File is too large")]
    FileTooLarge,
    #[error("Invalid form data")]
    InvalidFormData,
    #[error("Maximum rival limit ({0}) reached")]
    RivalLimitError(usize),
    #[error("Username was changed too recently")]
    TooFrequentUsernameChange,
    #[error("Username unchanged")]
    UsernameUnchanged,
    #[error("Cannot add self as rival")]
    CannotAddSelfAsRival,
    #[error("Invalid game id")]
    InvalidGameId,
    #[error("Unexpected query string parameters")]
    ShouldNotHaveQueryString,
    #[error("Cannot verify own score")]
    CannotVerifyOwnScore,
}

#[derive(Error, Debug)]
pub enum ServerError {
    #[error("Invalid cache state")]
    CacheInvalid,
    #[error("Could not find value in cache")]
    CacheMiss,
    #[error("Feature not configured: {0}")]
    NotConfigured(&'static str),
    #[error(r#"Invalid Setting Value: "{value}""#)]
    InvalidSetting { value: String },
}

// TODO Rename this, authentication vs authorization
#[allow(dead_code)]
#[derive(Error, Debug)]
pub enum AuthenticationError {
    #[error("Invalid Username or Password")]
    InvalidUsername,
    #[error("Invalid Username or Password")]
    InvalidPassword,
    #[error("Account is not set up. Please contact an administrator.")]
    AccountIncomplete,
    #[error("Account is locked")]
    AccountLocked,
    #[error("Account is banned")]
    AccountBanned,
    #[error("Could not register user")]
    RegistrationError,
    #[error("Username already taken")]
    AccountDuplicate,
    #[error("Internal Error")]
    BcryptError(#[from] bcrypt::BcryptError),
    #[error("Unauthorized")]
    UnauthorizedForOperation,
    #[error("Session Expired")]
    SessionExpired,
    #[error("Unauthorized to overwrite email address")]
    CannotUpdateEmailAddress,
}

// TODO: At what point do I just use anyhow?
#[allow(dead_code)]
#[derive(Error, Debug)]
pub enum TgmRankError {
    #[error("{message}")]
    GenericError { message: String },
    #[error("{message}")]
    GenericClientError { message: String },
    #[error("{0}")]
    ClientError(#[from] ClientError),
    #[error("{0}")]
    ServerError(#[from] ServerError),
    #[error("{0}")]
    InternalServerError(String),
    #[error("Not Found")]
    NotFoundError,
    #[error("Route Error: {message}")]
    RouteNotFoundError { message: String },
    #[error("Not Implemented")]
    NotImplementedError,
    #[error("{0}")]
    AuthenticationError(#[from] AuthenticationError),
    #[error("Database Error")]
    SqlxError(#[from] sqlx::error::Error),
    #[error("SQLX Migration Error: {0}")]
    SqlxMigrationError(#[from] sqlx::migrate::MigrateError),
    #[error("Serializer Error: {0}")]
    SerializerError(#[from] serde_json::error::Error),
    #[error("Validation Error")]
    ValidationError(#[from] ValidationErrors),
    // TODO: Combine these error types into an enum?
    #[error("Validation Error")]
    ScoreValidationRuleError(ValidationRuleErrors),
    #[error("Validation Error: {0}")]
    ScoreValidationError(#[from] ScoreValidationError),
    #[error("Tokio postgres Error: {0}")]
    TokioPostgresError(#[from] bb8_postgres::tokio_postgres::Error),
    #[error("bb8 RunError: {0}")]
    Bb8RunError(#[from] bb8_postgres::bb8::RunError<bb8_postgres::tokio_postgres::Error>),
    #[error("reqwest Error: {0}")]
    ReqwestError(#[from] reqwest::Error),
    #[error(transparent)]
    Other(#[from] anyhow::Error),
}

impl TgmRankError {
    pub fn from_std<E>(e: E) -> Self
    where
        E: std::error::Error + Send + Sync + 'static,
    {
        TgmRankError::Other(anyhow::Error::new(e))
    }

    pub fn from_any(e: anyhow::Error) -> Self {
        TgmRankError::Other(e)
    }
}

impl TgmRankError {
    fn http_status(&self) -> StatusCode {
        match *self {
            TgmRankError::GenericError { .. } => StatusCode::INTERNAL_SERVER_ERROR,
            TgmRankError::GenericClientError { .. } => StatusCode::BAD_REQUEST,
            TgmRankError::ClientError(_) => StatusCode::BAD_REQUEST,
            TgmRankError::ServerError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            TgmRankError::InternalServerError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            TgmRankError::NotFoundError => StatusCode::NOT_FOUND,
            TgmRankError::RouteNotFoundError { .. } => StatusCode::NOT_FOUND,
            TgmRankError::NotImplementedError => StatusCode::NOT_IMPLEMENTED,
            TgmRankError::AuthenticationError(_) => StatusCode::UNAUTHORIZED,
            TgmRankError::SqlxError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            TgmRankError::SqlxMigrationError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            TgmRankError::SerializerError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            TgmRankError::ValidationError(_) => StatusCode::BAD_REQUEST,
            TgmRankError::ScoreValidationRuleError(_) => StatusCode::BAD_REQUEST,
            TgmRankError::ScoreValidationError(_) => StatusCode::BAD_REQUEST,
            TgmRankError::TokioPostgresError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            TgmRankError::Bb8RunError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            TgmRankError::ReqwestError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            TgmRankError::Other(_) => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }

    fn error_model(&self) -> ErrorModel {
        let top_level_message = self.to_string();
        match self {
            TgmRankError::ValidationError(errors) => Self::map_validation_errors(
                ErrorModel {
                    error: Some(top_level_message),
                    ..Default::default()
                },
                errors,
            ),
            TgmRankError::ScoreValidationRuleError(errors) => {
                let field_errors_model = errors
                    .0
                    .iter()
                    .dedup()
                    .map(|error| ErrorModel {
                        field: Some(error.get_field_name()),
                        details: Some(error.to_string()),
                        ..Default::default()
                    })
                    .collect();

                ErrorModel {
                    error: Some(top_level_message),
                    data_errors: Some(field_errors_model),
                    ..Default::default()
                }
            }
            TgmRankError::SqlxError(inner_error) => {
                error!("SQLX Error: {}", inner_error);
                ErrorModel {
                    error: Some(top_level_message),
                    ..Default::default()
                }
            }
            _ => ErrorModel {
                error: Some(top_level_message),
                ..Default::default()
            },
        }
    }

    fn snake_case_to_camel_case(s: &str) -> String {
        SNAKE_CASE_RE
            .replace_all(s, |c: &Captures| {
                c[0].trim_start_matches('_').to_ascii_uppercase()
            })
            .to_string()
    }

    fn map_validation_errors(error_model: ErrorModel, errors: &ValidationErrors) -> ErrorModel {
        let mut data_errors: Vec<ErrorModel> = Vec::new();
        for (field, err) in errors.errors() {
            let field_name = Self::snake_case_to_camel_case(field);
            match err {
                ValidationErrorsKind::Struct(more_errors) => {
                    let e = Self::map_validation_errors(
                        ErrorModel {
                            field: Some(field_name),
                            ..ErrorModel::default()
                        },
                        more_errors,
                    );

                    // Decided to exclude struct error level in the hierarchy
                    // data_errors.push(e);

                    if let Some(struct_data_errors) = e.data_errors {
                        for struct_data_error in struct_data_errors {
                            data_errors.push(struct_data_error)
                        }
                    }
                }
                ValidationErrorsKind::List(more_errors) => {
                    for (index, errors) in more_errors.iter() {
                        // This only handles one inner layer
                        let inner_errors =
                            Self::map_validation_errors(ErrorModel::default(), errors);

                        if let Some(inner_errors) = inner_errors.data_errors {
                            for inner_data_error in inner_errors {
                                data_errors.push(ErrorModel {
                                    field: inner_data_error.field.map(|field| {
                                        format!("{}[{}].{}", field_name, index, field)
                                    }),
                                    ..inner_data_error
                                })
                            }
                        }
                    }
                }
                ValidationErrorsKind::Field(field_errors) => {
                    // let _inner_errors = field_errors.iter().map(|e| ErrorModel {
                    //     error: e.message.as_ref().map(|message| message.to_string()),
                    //     ..ErrorModel::default()
                    // });

                    let error_string = field_errors
                        .iter()
                        .filter_map(|e| e.message.as_ref())
                        .map(|e| e.to_string())
                        .join(", ");

                    data_errors.push(ErrorModel {
                        field: Some(field_name),
                        details: Some(error_string),
                        ..Default::default()
                    });
                }
            }
        }

        ErrorModel {
            data_errors: if data_errors.is_empty() {
                None
            } else {
                Some(data_errors)
            },
            ..error_model
        }
    }
}

impl ResponseError for TgmRankError {
    fn error_response(&self) -> HttpResponse {
        let json_string =
            serde_json::to_string(&self.error_model()).expect("Could not serialize error to json");

        error!("Caught Error: {}", json_string);

        HttpResponse::build(self.http_status())
            .content_type("application/json")
            .body(json_string)
    }
}
