pub trait AsCsv {
    fn as_csv<T>(&self) -> Option<Vec<T>>
    where
        T: std::str::FromStr;
}

impl AsCsv for &str {
    fn as_csv<T>(&self) -> Option<Vec<T>>
    where
        T: std::str::FromStr,
    {
        if !self.trim().is_empty() {
            let mut acc = vec![];
            for s in self.split(',') {
                let item = s.trim().parse::<T>().ok()?;
                acc.push(item)
            }
            if acc.is_empty() {
                return None;
            } else {
                return Some(acc);
            }
        }

        None
    }
}

impl<S> AsCsv for Option<S>
where
    S: AsRef<str>,
{
    fn as_csv<T>(&self) -> Option<Vec<T>>
    where
        T: std::str::FromStr,
    {
        match self {
            Some(ref s) => s.as_ref().as_csv(),
            _ => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::AsCsv;

    #[test]
    fn as_csv_should_accept_single_entry() {
        let s = "hello";
        let csv: Option<Vec<String>> = s.as_csv();

        assert!(csv.is_some());

        let csv = csv.unwrap();
        assert_eq!(csv.len(), 1);
        assert_eq!(csv[0], "hello");
    }

    #[test]
    fn as_csv_should_split_string() {
        let s = "hello,hi,howdy";
        let csv: Option<Vec<String>> = s.as_csv();

        assert!(csv.is_some());

        let csv = csv.unwrap();
        assert_eq!(csv.len(), 3);
        assert_eq!(csv[0], "hello");
        assert_eq!(csv[1], "hi");
        assert_eq!(csv[2], "howdy");
    }

    #[test]
    fn as_csv_should_trim_entries() {
        let s = " hello , hi , howdy ";
        let csv: Option<Vec<String>> = s.as_csv();

        assert!(csv.is_some());

        let csv = csv.unwrap();
        assert_eq!(csv.len(), 3);
        assert_eq!(csv[0], "hello");
        assert_eq!(csv[1], "hi");
        assert_eq!(csv[2], "howdy");
    }

    #[test]
    fn as_csv_should_return_none_when_empty() {
        let s = "";
        let csv: Option<Vec<String>> = s.as_csv();

        assert!(csv.is_none());
    }

    #[test]
    fn as_csv_should_return_none_when_entries_cannot_be_parsed_properly() {
        let s = "1,2,three";
        let csv: Option<Vec<i32>> = s.as_csv();

        assert!(csv.is_none());
    }
}
