#![feature(proc_macro_hygiene, stmt_expr_attributes)]
#![forbid(unsafe_code)]

extern crate openssl;
extern crate openssl_probe;

extern crate actix_web;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate validator_derive;

use actix_identity::IdentityMiddleware;
use actix_web::web::{Data, PathConfig, QueryConfig};
use actix_web::{middleware, web, App, HttpServer};

use crate::modules::Settings;
use crate::utility::{TgmRankError, TgmRankResult};

pub mod guards;

pub mod controllers;
pub mod models;
pub mod modules;
pub mod repositories;
pub mod services;
pub mod utility;

pub async fn run_service() -> TgmRankResult<()> {
    std::env::set_var(
        "RUST_LOG",
        "warn,tgmrank=info,actix_web=info,actix_server=info,actix_web::middleware::logger=warn,sqlx::query=warn",
    );
    env_logger::init();

    let settings = Settings::new("Settings.env").map_err(TgmRankError::from_any)?;
    info!("\n{}", serde_json::to_string_pretty(&settings)?);

    let port = settings.application.port;

    let pool = settings.database.make_sqlx_connection_pool().await;
    sqlx::migrate!("./migrations")
        .set_ignore_missing(true)
        .run(&pool)
        .await?;

    let db_pool = web::Data::new(pool);

    let pool2 = settings.database.make_connection_pool().await;
    let bb8_pool = web::Data::new(pool2);

    HttpServer::new(move || {
        App::new()
            .wrap(settings.application.build_cors_handler())
            .wrap(IdentityMiddleware::default())
            .wrap(settings.application.build_identity_service())
            .wrap(middleware::NormalizePath::trim())
            .wrap(middleware::Logger::default())
            .wrap(middleware::DefaultHeaders::new().add(("Cache-Control", "no-store")))
            .app_data(Data::new(settings.to_owned()))
            .app_data(web::JsonConfig::default().limit(4096))
            .app_data(db_pool.to_owned())
            .app_data(bb8_pool.to_owned())
            .app_data(PathConfig::default().error_handler(|err, _req| {
                TgmRankError::RouteNotFoundError {
                    message: err.to_string(),
                }
                .into()
            }))
            .app_data(QueryConfig::default().error_handler(|err, _req| {
                TgmRankError::RouteNotFoundError {
                    message: err.to_string(),
                }
                .into()
            }))
            .service(
                web::scope("/v1")
                    .configure(&controllers::admin_controller::routes)
                    .configure(&controllers::avatar_controller::routes)
                    .configure(&controllers::badge_controller::routes)
                    .configure(&controllers::forgot_password_controller::routes)
                    .configure(&controllers::game_controller::routes)
                    .configure(&controllers::integration_controller::routes)
                    .configure(&controllers::location_controller::routes)
                    .configure(&controllers::login_controller::routes)
                    .configure(&controllers::mode_controller::routes)
                    .configure(&controllers::player_controller::routes)
                    .configure(&controllers::proxy_controller::routes)
                    .configure(&controllers::score_controller::routes)
            )
            .default_service(web::route().to(&controllers::catcher_controller::not_found))
    })
    .bind(("0.0.0.0", port))
    .map_err(TgmRankError::from_std)?
    .run()
    .await
    .map_err(TgmRankError::from_std)
}
