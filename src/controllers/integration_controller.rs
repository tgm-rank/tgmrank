use actix_identity::Identity;
use actix_web::web::ServiceConfig;
use actix_web::{delete, get, post, web, HttpRequest, HttpResponse, Responder};
use itertools::Itertools;

use crate::controllers::models::{ExternalIntegrationModel, GenericModel};
use crate::models::{ApiKeyEntity, ExternalIntegrationEntity, IntegrationTarget};
use crate::modules::{DbPool, RequestApiKey};
use crate::services::LoginService;
use crate::utility::{AuthenticationError, TgmRankError, TgmRankResult};

pub fn routes(config: &mut ServiceConfig) {
    config
        .service(add_integration_to_self)
        .service(remove_integration_from_self)
        .service(get_integration)
        .service(get_integrations);
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct IntegrationForm {
    target: IntegrationTarget,
    external_key: String,
}

#[post("/integration")]
async fn add_integration_to_self(
    id: Identity,
    db: web::Data<DbPool>,
    integration_form: web::Json<IntegrationForm>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let user = LoginService::is_user(db, id).await?;

    let form = integration_form.into_inner();

    let _integration = ExternalIntegrationEntity::upsert(
        db,
        ExternalIntegrationEntity {
            player_id: user.user_id,
            target: form.target,
            external_key: form.external_key,
            ..ExternalIntegrationEntity::default()
        },
    )
    .await?;

    Ok(HttpResponse::Ok().json(GenericModel { success: true }))
}

#[derive(Deserialize)]
pub struct IntegrationQuery {
    target: IntegrationTarget,
}

#[delete("/integration")]
async fn remove_integration_from_self(
    id: Identity,
    db: web::Data<DbPool>,
    web::Query(query): web::Query<IntegrationQuery>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let user = LoginService::is_user(db, id).await?;

    let existing_integration = ExternalIntegrationEntity::get_by_target(db, query.target)
        .await?
        .into_iter()
        .find(|i| i.player_id == user.user_id);

    if let Some(existing_integration) = existing_integration {
        let _integration = ExternalIntegrationEntity::upsert(
            db,
            ExternalIntegrationEntity {
                external_key: "".to_string(),
                ..existing_integration
            },
        )
        .await?;
    }

    Ok(HttpResponse::Ok().json(GenericModel { success: true }))
}

#[get("/integration")]
async fn get_integration(
    id: Identity,
    db: web::Data<DbPool>,
    web::Query(query): web::Query<IntegrationQuery>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let user = LoginService::is_admin(db, id).await?;

    let integrations = ExternalIntegrationEntity::get_by_target(db, query.target).await?;

    let has_integration = integrations
        .iter()
        .any(|integration| integration.player_id == user.user_id);

    if !has_integration {
        Err(TgmRankError::NotFoundError)
    } else {
        Ok(HttpResponse::Ok().json(GenericModel { success: true }))
    }
}

#[get("/integrations")]
async fn get_integrations(
    req: HttpRequest,
    id: Identity,
    db: web::Data<DbPool>,
    web::Query(query): web::Query<IntegrationQuery>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let admin = LoginService::is_admin(db, id).await;

    if admin.is_err() {
        let api_key = RequestApiKey::from_request(&req)
            .ok_or(AuthenticationError::UnauthorizedForOperation)?;

        ApiKeyEntity::find_by_key(db, api_key.key)
            .await
            .map_err(|_| AuthenticationError::UnauthorizedForOperation)?;
    }

    let integrations = ExternalIntegrationEntity::get_by_target(db, query.target).await?;

    let integration_model = integrations
        .into_iter()
        .map(|i| ExternalIntegrationModel {
            player_id: i.player_id,
            external_key: i.external_key,
        })
        .collect_vec();

    Ok(HttpResponse::Ok().json(integration_model))
}
