pub mod models;

pub mod admin_controller;
pub mod avatar_controller;
pub mod badge_controller;
pub mod catcher_controller;
pub mod forgot_password_controller;
pub mod frontend_stub_controller;
pub mod game_controller;
pub mod integration_controller;
pub mod location_controller;
pub mod login_controller;
pub mod mode_controller;
pub mod player_controller;
pub mod proxy_controller;
pub mod score_controller;
