use crate::utility::{TgmRankError, TgmRankResult};
use actix_web::HttpResponse;

pub async fn not_found() -> TgmRankResult<HttpResponse> {
    Err(TgmRankError::NotFoundError)
}
