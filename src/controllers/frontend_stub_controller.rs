//// These routes aren't actually mounted, but they're useful for upholding type guarantees with the uri! macro
//
//use actix_web::HttpResponse;
//
//#[allow(dead_code)]
//#[allow(unused_variables)]
//#[get("/reset-password/<username>/<reset_key>")]
//pub fn reset_password(username: String, reset_key: String) -> HttpResponse {
//    HttpResponse::Ok().body("")
//}
