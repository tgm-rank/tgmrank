use std::convert::TryInto;

use actix_web::web::ServiceConfig;
use actix_web::{get, web, HttpResponse, Responder};
use itertools::Itertools;
use validator::Validate;

use crate::controllers::models::{ModeModel, ModeRankingModel, PlatformModel};
use crate::models::filters::{ModeRankingFilter, PaginationQuery};
use crate::models::{ModeEntity, PlatformEntity, RankingAlgorithm};
use crate::modules::DbPool;
use crate::services::RankingService;
use crate::utility::{TgmRankResult, WrappedInt};

pub fn routes(config: &mut ServiceConfig) {
    config
        .service(get_mode_by_id)
        .service(get_mode_description)
        .service(get_platforms_for_mode)
        .service(get_ranking)
        .service(get_all_mode_rankings_by_game_and_mode);
}

#[get("/mode/{mode_id:\\d+}")]
async fn get_mode_by_id(
    db: web::Data<DbPool>,
    mode_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let mode_model = ModeEntity::get_by_id(&mut *db.acquire().await?, mode_id.into_inner())
        .await
        .map(ModeModel::from)?;

    Ok(HttpResponse::Ok().json(mode_model))
}

#[get("/mode/{mode_id:\\d+}/description")]
async fn get_mode_description(
    db: web::Data<DbPool>,
    mode_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let description =
        RankingService::get_mode_description(&mut *db.acquire().await?, mode_id.into_inner())
            .await?;
    Ok(HttpResponse::Ok().json(description))
}

#[get("/mode/{mode_id:\\d+}/platform")]
async fn get_platforms_for_mode(
    pool: web::Data<DbPool>,
    mode_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let platforms =
        PlatformEntity::get_for_mode(&mut *pool.acquire().await?, mode_id.into_inner()).await?;
    let platforms = platforms.into_iter().map(PlatformModel::from).collect_vec();

    Ok(HttpResponse::Ok().json(platforms))
}

#[derive(Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ModeRankingQuery {
    pub as_of: Option<String>,
    pub player_name: Option<String>,
    #[serde(default, with = "crate::utility::opt_stringified_list")]
    pub player_ids: Option<Vec<WrappedInt>>,
    #[serde(flatten)]
    pub pagination: Option<PaginationQuery>,
    #[serde(default, with = "crate::utility::opt_stringified_list")]
    pub locations: Option<Vec<String>>,
    pub algorithm: Option<RankingAlgorithm>,
}

async fn get_ranking_internal(
    db: &web::Data<DbPool>,
    filter: &ModeRankingFilter,
) -> TgmRankResult<Vec<ModeRankingModel>> {
    let db = &mut *db.acquire().await?;
    let mode_ranking = RankingService::get_mode_ranking(db, filter).await?;
    Ok(mode_ranking)
}

pub fn mode_ranking_path(mode_id: i32) -> String {
    format!("/v1/mode/{0}/ranking", mode_id)
}

#[get("/mode/{mode_id:\\d+}/ranking")]
async fn get_ranking(
    db: web::Data<DbPool>,
    mode_id: web::Path<i32>,
    web::Query(query): web::Query<ModeRankingQuery>,
) -> TgmRankResult<impl Responder> {
    let filter = ModeRankingFilter {
        id: Some(mode_id.into_inner()),
        ..query.try_into()?
    };
    filter.validate()?;

    let ranking = get_ranking_internal(&db, &filter).await?;

    Ok(HttpResponse::Ok().json(ranking))
}

#[get("/mode/ranking")]
async fn get_all_mode_rankings_by_game_and_mode(
    db: web::Data<DbPool>,
    web::Query(query): web::Query<ModeRankingQuery>,
) -> TgmRankResult<impl Responder> {
    let filter: ModeRankingFilter = query.try_into()?;
    filter.validate()?;

    let mode_ranking =
        RankingService::get_pbs_for_players(&mut *db.acquire().await?, &filter).await?;

    Ok(HttpResponse::Ok()
        .content_type("application/json")
        .json(&mode_ranking))
}
