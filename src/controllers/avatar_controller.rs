use std::fs;
use std::io::Write;
use std::path::Path;

use actix_identity::Identity;
use actix_multipart::{Field, Multipart};
use actix_web::web::ServiceConfig;
use actix_web::{post, web, HttpResponse, Responder};
use futures::StreamExt;
use mime::{Mime, Name};
use uuid::Uuid;

use crate::controllers::models::LoginModel;
use crate::models::PlayerEntity;
use crate::modules::DbPool;
use crate::services::{LoginService, PlayerService};
use crate::utility::{ClientError, TgmRankError, TgmRankResult};

pub const AVATAR_DIRECTORY: &str = "public/avatar/";
pub const AVATAR_FILE_FORM_KEY: &str = "avatarFile";
pub const AVATAR_SIZE_LIMIT: usize = 100_000;
// const DEFAULT_AVATAR: &str = "public/default_avatar.svg";

pub fn routes(config: &mut ServiceConfig) {
    config.service(upload_avatar);
}

//// In prod, nginx accesses the avatar directory directly
//#[get("/avatar/<file..>")]
//pub fn get_avatar(file: PathBuf) -> Option<Content<CachedFile>> {
//    let file_path = Path::new(AVATAR_DIRECTORY).join(file);
//    if !file_path.exists() {
//        return CachedFile::from_path(&Path::new(DEFAULT_AVATAR))
//            .map(|file| Content(ContentType::SVG, file));
//    }
//
//    if let Some(extension) = file_path.extension() {
//        let extension = extension.to_str().unwrap();
//        return ContentType::from_extension(extension).and_then(|content_type| {
//            CachedFile::from_path(&file_path).map(|file| Content(content_type, file))
//        });
//    }
//
//    CachedFile::from_path(&Path::new(DEFAULT_AVATAR)).map(|file| Content(ContentType::SVG, file))
//}

#[post("/avatar")]
pub async fn upload_avatar<'r>(
    id: Identity,
    mut multipart: Multipart,
    db: web::Data<DbPool>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let user = LoginService::is_user(db, id).await?;

    while let Some(item) = multipart.next().await {
        let mut field = item.map_err(|_| TgmRankError::GenericError {
            message: "Form error".to_string(),
        })?;

        let content_data = field.content_disposition();

        if content_data.and_then(|c| Some(c.is_form_data())) != Some(true)
            || content_data.and_then(|c| c.get_name()) != Some(AVATAR_FILE_FORM_KEY)
        {
            return Err(ClientError::InvalidFormData.into());
        }

        let extension = get_extension(field.content_type().unwrap())?;
        let file_id = Uuid::new_v4().to_string();
        let filename = format!("{}.{}", file_id, extension);
        let filepath = Path::new(AVATAR_DIRECTORY).join(&filename);

        let buffer = read_file_from_chunks(&mut field).await?;

        web::block(move || -> TgmRankResult<()> {
            let mut f = std::fs::File::create(filepath).map_err(TgmRankError::from_std)?;
            f.write_all(&buffer).map_err(TgmRankError::from_std)?;

            Ok(())
        })
        .await
        .map_err(TgmRankError::from_std)??;

        let player = PlayerEntity::get_by_id(db, user.user_id).await?;

        if let Some(old_avatar) = &player.avatar_file {
            let old_avatar_path = Path::new(AVATAR_DIRECTORY).join(old_avatar);

            if old_avatar_path.exists() {
                web::block(move || -> TgmRankResult<()> {
                    fs::remove_file(&old_avatar_path).map_err(|_| TgmRankError::GenericError {
                        message: "Could not remove old avatar".into(),
                    })?;

                    Ok(())
                })
                .await
                .map_err(TgmRankError::from_std)??;
            }
        }

        let _player = PlayerService::update_avatar(db, player, &filename).await?;
    }

    let player = PlayerEntity::get_by_id(db, user.user_id).await?;
    let model_response = LoginModel::from(player);
    Ok(HttpResponse::Ok().json(model_response))
}

async fn read_file_from_chunks(field: &mut Field) -> TgmRankResult<Vec<u8>> {
    let mut buffer: Vec<u8> = Vec::new();
    while let Some(chunk) = field.next().await {
        let data = chunk.map_err(|_| TgmRankError::GenericError {
            message: "Form error".to_string(),
        })?;

        buffer.write_all(&data).map_err(TgmRankError::from_std)?;

        if buffer.len() >= AVATAR_SIZE_LIMIT {
            return Err(ClientError::FileTooLarge.into());
        }
    }

    Ok(buffer)
}

fn get_extension(mime: &Mime) -> TgmRankResult<&str> {
    let accepted_mime_names: Vec<Name> = vec![mime::BMP, mime::GIF, mime::JPEG, mime::PNG];
    accepted_mime_names
        .iter()
        .find(|n| n.as_str() == mime.subtype().as_str().to_lowercase())
        .map(|n| n.as_str())
        .ok_or_else(|| ClientError::InvalidContentType.into())
}
