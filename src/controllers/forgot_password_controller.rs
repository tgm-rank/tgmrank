use actix_web::web::ServiceConfig;
use actix_web::{post, web, HttpResponse, Responder};
use validator::Validate;

use crate::controllers::models::GenericModel;
use crate::models::forms::{ForgotPasswordForm, ResetPasswordForm};
use crate::models::{PlayerEntity, UserTokenType};
use crate::modules::DbPool;
use crate::modules::Settings;
use crate::services::{EmailService, LoginService, PlayerService};
use crate::utility::{AuthenticationError, TgmRankResult};

pub fn routes(config: &mut ServiceConfig) {
    config
        .service(forgot_password_form_submit)
        .service(reset_password_process);
}

#[post("/forgot_password")]
pub async fn forgot_password_form_submit(
    settings: web::Data<Settings>,
    db: web::Data<DbPool>,
    forgot_password_form: web::Json<ForgotPasswordForm>,
) -> TgmRankResult<impl Responder> {
    let query = &*forgot_password_form.username;

    let player = PlayerService::find_player(&mut *db.acquire().await?, query, true).await;

    if let Ok(player) = player {
        if player.email.is_some() {
            let token = PlayerService::gen_token(
                player.player_id,
                &player.player_name,
                UserTokenType::ForgotPassword,
            );

            let mut transaction = db.begin().await?;

            EmailService::send_forgot_password_email(
                &mut transaction,
                &settings,
                &player,
                &token.token,
            )
            .await?;

            PlayerService::upsert_token(&mut transaction, token).await?;

            transaction.commit().await?;
        } else {
            return Err(AuthenticationError::AccountIncomplete.into());
        }
    }

    Ok(HttpResponse::Ok().json(GenericModel { success: true }))
}

#[post("/reset_password/{username}/{reset_key}")]
pub async fn reset_password_process(
    db: web::Data<DbPool>,
    path: web::Path<(String, String)>,
    reset_password_form: web::Json<ResetPasswordForm>,
) -> TgmRankResult<impl Responder> {
    let (username, reset_key) = path.into_inner();

    let reset_password_model = &*reset_password_form;
    reset_password_model.validate()?;

    let db = &mut *db.acquire().await?;
    let player = PlayerEntity::get_by_name(db, &username, true).await?;
    let player_id = player.player_id;

    let player_token = PlayerService::get_forgot_password_token(db, player.player_id).await?;

    if player_token.verify(&reset_key, &UserTokenType::ForgotPassword) {
        PlayerService::update_password(db, player, &reset_password_model.password).await?;
        PlayerService::delete_token(db, player_token).await?;
    } else {
        return Err(AuthenticationError::UnauthorizedForOperation.into());
    }

    LoginService::delete_user_sessions(db, player_id, &[]).await?;

    Ok(HttpResponse::Ok().json(GenericModel { success: true }))
}
