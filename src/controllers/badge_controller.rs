use crate::modules::DbPool;
use crate::services::{LoginService, PlayerService};
use crate::utility::TgmRankResult;
use actix_identity::Identity;
use actix_web::web::ServiceConfig;
use actix_web::{get, post, web, HttpResponse, Responder};
use validator::{Validate, ValidationError};

pub fn routes(config: &mut ServiceConfig) {
    config.service(get_all_badges).service(create_badge);
}

#[get("/badge")]
async fn get_all_badges(db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let badges = PlayerService::get_all_badges(db).await?;
    Ok(HttpResponse::Ok().json(badges))
}

#[derive(Serialize, Deserialize, Validate)]
pub struct NewBadgeForm {
    #[validate(
        length(min = 1, message = "Badge name is too short"),
        custom(function = "validate_badge_name")
    )]
    pub name: String,
}

fn validate_badge_name(name: &str) -> Result<(), ValidationError> {
    if name.trim().is_empty() {
        return Err(ValidationError::new("Badge name cannot be empty"));
    }

    Ok(())
}

#[post("/badge")]
async fn create_badge(
    id: Identity,
    db: web::Data<DbPool>,
    form: web::Json<NewBadgeForm>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let user = LoginService::is_admin(db, id).await?;
    form.validate()?;

    let badge = PlayerService::create_badge(&user, db, form.name.trim()).await?;
    Ok(HttpResponse::Ok().json(badge))
}
