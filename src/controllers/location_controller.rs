use actix_web::web::ServiceConfig;
use actix_web::{get, web, HttpResponse, Responder};
use itertools::Itertools;

use crate::controllers::models::LocationModel;
use crate::{models::LocationEntity, modules::DbPool, utility::TgmRankResult};

pub fn routes(config: &mut ServiceConfig) {
    config
        .service(get_all_locations)
        .service(get_all_locations_cached);
}

#[get("/location")]
async fn get_all_locations(db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let locations = LocationEntity::get_all(&mut *db.acquire().await?).await?;
    let location_models = locations.iter().map(LocationModel::from).collect_vec();

    Ok(HttpResponse::Ok().json(location_models))
}

#[get("/cache/location")]
async fn get_all_locations_cached(db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let locations = LocationEntity::get_all(&mut *db.acquire().await?).await?;
    let location_models = locations.iter().map(LocationModel::from).collect_vec();

    Ok(HttpResponse::Ok()
        .append_header(crate::utility::constants::CDN_CACHE_CONTROL_HEADER)
        .json(location_models))
}
