use std::collections::HashMap;
use std::convert::TryInto;

use actix_web::web::ServiceConfig;
use actix_web::{get, web, HttpResponse, Responder};
use itertools::Itertools;
use validator::Validate;

use crate::models::filters::{GameFilter, GameRankingFilter, PaginationQuery};
use crate::models::{GameEntity, ModeGroupEntity, RankingAlgorithm};
use crate::modules::DbPool;
use crate::services::RankingService;
use crate::utility::{ClientError, TgmRankResult, WrappedInt};
use crate::{controllers::models::GameModel, models::filters::GenericQuery};

pub fn routes(config: &mut ServiceConfig) {
    config
        .service(get_all_games)
        .service(get_rankings_for_all_games)
        .service(get_game_by_id)
        .service(get_ranking_by_game)
        .service(get_ranking_with_group)
        .service(get_ranking_with_group_for_player);
}

#[get("/game")]
async fn get_all_games(
    db: web::Data<DbPool>,
    web::Query(query): web::Query<GenericQuery>,
) -> TgmRankResult<impl Responder> {
    let filter: GameFilter = query.into();
    filter.validate()?;

    let games = RankingService::get_game_modes(&mut *db.acquire().await?, &filter).await?;

    Ok(HttpResponse::Ok().json(games))
}

#[get("/game/{game_id}")]
async fn get_game_by_id(
    db: web::Data<DbPool>,
    game_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let game = GameEntity::get_by_id(&mut *db.acquire().await?, game_id.into_inner()).await?;
    let game_model: GameModel = game.into();

    Ok(HttpResponse::Ok().json(game_model))
}

#[derive(Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GameRankingQuery {
    pub name: Option<String>,
    pub player_name: Option<String>,
    #[serde(default, with = "crate::utility::opt_stringified_list")]
    pub player_ids: Option<Vec<WrappedInt>>,
    #[serde(default, with = "crate::utility::opt_stringified_list")]
    pub mode_ids: Option<Vec<WrappedInt>>,
    pub as_of: Option<String>,
    #[serde(flatten)]
    pub pagination: Option<PaginationQuery>,
    #[serde(default, with = "crate::utility::opt_stringified_list")]
    pub locations: Option<Vec<String>>,
    pub algorithm: Option<RankingAlgorithm>,
}

#[get("/game/ranking")]
async fn get_rankings_for_all_games(
    db: web::Data<DbPool>,
    web::Query(query): web::Query<GameRankingQuery>,
) -> TgmRankResult<impl Responder> {
    let filter: GameRankingFilter = query.try_into()?;
    filter.validate()?;

    let rankings =
        RankingService::get_all_game_rankings(&mut *db.acquire().await?, &filter).await?;

    Ok(HttpResponse::Ok().json(rankings))
}

#[get("/game/{game}/ranking")]
async fn get_ranking_by_game(
    db: web::Data<DbPool>,
    game: web::Path<String>,
    web::Query(query): web::Query<GameRankingQuery>,
) -> TgmRankResult<impl Responder> {
    let filter = GameRankingFilter {
        id: Some(game.into_inner()),
        ..query.try_into()?
    };
    filter.validate()?;

    let db = &mut *db.acquire().await?;

    let ranking_model = RankingService::get_game_ranking(db, &filter).await?;
    Ok(HttpResponse::Ok().json(ranking_model))
}

pub fn game_ranking_path(game_id: i32, group: &str) -> String {
    return format!("/v1/game/{0}/{1}/ranking", game_id, group);
}

#[get("/game/{game:\\d+}/{mode_group}/ranking")]
async fn get_ranking_with_group(
    db: web::Data<DbPool>,
    path: web::Path<(i32, String)>,
    query: web::Query<HashMap<String, String>>,
) -> TgmRankResult<impl Responder> {
    if !query.into_inner().is_empty() {
        return Err(ClientError::ShouldNotHaveQueryString.into());
    }

    let (game_id, mode_group) = path.into_inner();

    let db = &mut *db.acquire().await?;
    let mode_group = ModeGroupEntity::find_mode_group(db, game_id, &mode_group).await?;

    let filter = GameRankingFilter {
        id: Some(game_id.to_string()),
        mode_ids: Some(mode_group.mode_ids),
        ..Default::default()
    };
    filter.validate()?;

    let ranking_model = RankingService::get_game_ranking(db, &filter).await?;
    Ok(HttpResponse::Ok().json(ranking_model))
}

#[get("/game/{game:\\d+}/{mode_group}/ranking/player/{playerId:\\d+}")]
async fn get_ranking_with_group_for_player(
    db: web::Data<DbPool>,
    path: web::Path<(i32, String, i32)>,
    query: web::Query<HashMap<String, String>>,
) -> TgmRankResult<impl Responder> {
    if !query.into_inner().is_empty() {
        return Err(ClientError::ShouldNotHaveQueryString.into());
    }

    let (game_id, mode_group, player_id) = path.into_inner();

    let db = &mut *db.acquire().await?;
    let mode_group = ModeGroupEntity::find_mode_group(db, game_id, &mode_group).await?;

    let filter = GameRankingFilter {
        id: Some(game_id.to_string()),
        mode_ids: Some(mode_group.mode_ids),
        ..Default::default()
    };
    filter.validate()?;

    let ranking_model = RankingService::get_game_ranking(db, &filter).await?;
    let filtered_ranking_model = ranking_model
        .into_iter()
        .filter(|r| r.player.player_id == player_id)
        .collect_vec();

    Ok(HttpResponse::Ok().json(filtered_ranking_model))
}
