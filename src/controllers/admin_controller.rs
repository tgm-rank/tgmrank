use actix_identity::Identity;
use actix_web::{
    get,
    web::{self, ServiceConfig},
    HttpResponse, Responder,
};

use crate::{modules::DbPool, services::LoginService, utility::TgmRankResult};

use super::models::GenericModel;

pub fn routes(config: &mut ServiceConfig) {
    config.service(admin_refresh_ranking_cache);
}

#[get("/admin/refresh")]
async fn admin_refresh_ranking_cache(
    id: Identity,
    db: web::Data<DbPool>,
) -> TgmRankResult<impl Responder> {
    let _user = LoginService::is_admin(&mut *db.acquire().await?, id).await?;

    Ok(HttpResponse::Ok().json(GenericModel { success: true }))
}
