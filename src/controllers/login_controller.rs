use actix_identity::Identity;
use actix_web::web::ServiceConfig;
use actix_web::{get, post, web, HttpRequest, HttpResponse, Responder};
use validator::Validate;

use crate::models::forms::{LoginForm, SimpleUserRegisterForm, UserRegisterForm};
use crate::models::{NewPlayerEntity, PlayerEntity};
use crate::modules::DbPool;
use crate::services::{LoginService, PlayerService};
use crate::{
    controllers::models::{GenericModel, LoginModel},
    utility::{AuthenticationError::CannotUpdateEmailAddress, TgmRankError, TgmRankResult},
};

pub fn routes(config: &mut ServiceConfig) {
    config
        .service(login)
        .service(login_verify)
        .service(logout)
        .service(register)
        .service(simple_register)
        .service(claim_account);
}

#[post("/login")]
pub async fn login(
    request: HttpRequest,
    db: web::Data<DbPool>,
    user: web::Json<LoginForm>,
) -> TgmRankResult<impl Responder> {
    user.validate()?;

    let db = &mut *db.acquire().await?;
    let player = LoginService::login(db, user.into_inner(), &request).await?;

    let login_model = LoginModel::from(player);
    Ok(HttpResponse::Ok().json(login_model))
}

#[get("/logout")]
pub async fn logout(id: Identity, db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    LoginService::logout(db, id).await?;
    Ok(HttpResponse::Ok().json(GenericModel { success: true }))
}

#[get("/login/verify")]
pub async fn login_verify(
    db: web::Data<DbPool>,
    id: Option<Identity>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    if let Some(id) = id {
        if let Ok(user) = LoginService::is_user(db, id).await {
            if let Ok(player) = PlayerEntity::get_by_id(db, user.user_id).await {
                let login_model = LoginModel::from(player);
                return Ok(HttpResponse::Ok().json(login_model));
            }
        }
    }

    Ok(HttpResponse::Ok().json(GenericModel { success: false }))
}

#[post("/register")]
pub async fn register(
    db: web::Data<DbPool>,
    user: web::Json<UserRegisterForm>,
) -> TgmRankResult<impl Responder> {
    user.validate()?;

    let player_to_insert = NewPlayerEntity {
        player_name: user.username.to_string(),
        email: Some(user.email.to_string()),
        password: PlayerService::gen_password(&user.password),
    };

    let player = PlayerService::register(&mut *db.acquire().await?, player_to_insert).await?;

    let login_model = LoginModel::from(player);
    Ok(HttpResponse::Ok().json(login_model))
}

#[post("/register/simple")]
pub async fn simple_register(
    id: Identity,
    db: web::Data<DbPool>,
    user: web::Json<SimpleUserRegisterForm>,
) -> TgmRankResult<impl Responder> {
    user.validate()?;

    let db = &mut *db.acquire().await?;
    let _user = LoginService::is_admin(db, id).await?;

    let player_to_insert = NewPlayerEntity {
        player_name: user.username.to_string(),
        email: None,
        password: "".to_string(),
    };

    let player = PlayerService::register(db, player_to_insert).await?;

    let login_model = LoginModel::from(player);
    Ok(HttpResponse::Ok().json(login_model))
}

#[get("/register/claim/{username}/{email}")]
pub async fn claim_account(
    id: Identity,
    db: web::Data<DbPool>,
    info: web::Path<(String, String)>,
) -> TgmRankResult<impl Responder> {
    let (username, email) = info.into_inner();

    let db = &mut *db.acquire().await?;
    let _user = LoginService::is_admin(db, id).await?;

    let player = PlayerService::find_player(db, &username, false)
        .await
        .map_err(|_| TgmRankError::NotFoundError)?;

    if player.player_role.is_admin() {
        return Err(TgmRankError::AuthenticationError(CannotUpdateEmailAddress));
    }

    let player = PlayerService::update_email(db, player, &email)
        .await
        .map_err(|_| TgmRankError::GenericError {
            message: format!("Could not set email {} for user {}", email, username),
        })?;

    let login_model = LoginModel::from(player);
    Ok(HttpResponse::Ok().json(login_model))
}
