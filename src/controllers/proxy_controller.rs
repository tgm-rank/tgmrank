use crate::utility::{TgmRankError, TgmRankResult};
use actix_web::web::ServiceConfig;
use actix_web::{post, web, HttpResponseBuilder, Responder};
use anyhow::anyhow;

pub fn routes(config: &mut ServiceConfig) {
    config.service(discord_webhook_proxy);
}

#[post("/proxy/discord/webhook/{webhook_id}/{webhook_token}")]
async fn discord_webhook_proxy(
    path: web::Path<(String, String)>,
    body: String,
) -> TgmRankResult<impl Responder> {
    let (webhook_id, webhook_token) = path.into_inner();

    let client = reqwest::Client::builder()
        .build()
        .map_err(TgmRankError::from_std)?;

    let url = format!(
        "https://discord.com/api/webhooks/{webhook_id}/{webhook_token}",
        webhook_id = webhook_id,
        webhook_token = webhook_token
    );

    let proxy_response = client
        .post(url)
        .query(&[("wait", "true")])
        .body(body)
        .header(reqwest::header::CONTENT_TYPE, "application/json")
        .send()
        .await?;

    let status_code = actix_web::http::StatusCode::from_u16(proxy_response.status().as_u16())
        .map_err(|e| anyhow!("Invalid status code: {}", e))?;

    let proxy_response_body = proxy_response.text().await?;

    let response = HttpResponseBuilder::new(status_code)
        .content_type("application/json")
        .body(proxy_response_body);

    Ok(response)
}
