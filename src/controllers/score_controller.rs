use std::convert::TryFrom;

use crate::models::filters::{GenericFilter, RecentActivityFilter, RecentActivityQuery};
use crate::models::forms::{AddScoreForm, UpdateScoreForm, VerifyScoreForm};
use crate::models::{GradeEntity, ProofEntity};
use crate::modules::{DbPool, DbPool2, Settings};
use crate::services::{
    LoginService, RankingService, RecentActivityService, ScoreService, StatsService,
};
use crate::utility::TgmRankResult;
use crate::{controllers::models::*, models::Role};
use actix_identity::Identity;
use actix_web::web::ServiceConfig;
use actix_web::{get, patch, post, web, HttpResponse, Responder};
use itertools::Itertools;
use time::Date;

pub fn routes(config: &mut ServiceConfig) {
    config
        .service(get_submission_info)
        .service(get_ranking_medals)
        .service(get_ranking_medals_by_location)
        .service(get_recent_activity)
        .service(get_activity_calendar)
        .service(get_score)
        .service(get_grade)
        .service(get_grade_for_mode)
        .service(get_grades_by_mode_path)
        .service(get_pong)
        .service(get_version)
        .service(get_score_statuses)
        .service(get_roles)
        .service(add_score)
        .service(update_score)
        .service(verify_score)
        .service(get_achievement_list)
        .service(get_recent_proof);
}

#[get("/score/{score_id:\\d+}")]
async fn get_score(
    db: web::Data<DbPool>,
    score_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let query = GenericFilter {
        ..Default::default()
    };

    let score_id = score_id.into_inner();

    let db = &mut *db.acquire().await?;

    let score = RankingService::get_detailed_score(db, score_id, &query).await?;
    Ok(HttpResponse::Ok().json(score))
}

#[get("/grade/{grade_id}")]
async fn get_grade(
    db: web::Data<DbPool>,
    grade_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let grade = GradeEntity::get_by_id(&mut *db.acquire().await?, grade_id.into_inner()).await?;
    let grade_model = GradeModel::from(grade);

    Ok(HttpResponse::Ok().json(grade_model))
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ModeIdQuery {
    mode: i32,
}

#[get("/grade")]
async fn get_grade_for_mode(
    pool: web::Data<DbPool>,
    web::Query(query): web::Query<ModeIdQuery>,
) -> TgmRankResult<impl Responder> {
    let grades = GradeEntity::get_for_mode(&mut *pool.acquire().await?, query.mode).await?;
    let grades = grades.into_iter().map(GradeModel::from).collect_vec();

    Ok(HttpResponse::Ok().json(grades))
}

#[get("/grade/mode/{mode_id}")]
async fn get_grades_by_mode_path(
    pool: web::Data<DbPool>,
    mode_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let mode_id = mode_id.into_inner();
    let grades = GradeEntity::get_for_mode(&mut *pool.acquire().await?, mode_id).await?;
    let grades = grades.into_iter().map(GradeModel::from).collect_vec();

    Ok(HttpResponse::Ok().json(grades))
}

#[get("/ping")]
async fn get_pong() -> TgmRankResult<impl Responder> {
    let pong = PingModel {
        message: "pong".into(),
    };
    Ok(HttpResponse::Ok().json(pong))
}

#[get("/version")]
async fn get_version(settings: web::Data<Settings>) -> TgmRankResult<impl Responder> {
    Ok(HttpResponse::Ok().json(VersionModel {
        version: settings.application.version.clone(),
    }))
}

#[get("/score/activity")]
async fn get_recent_activity(
    db: web::Data<DbPool>,
    db2: web::Data<DbPool2>,
    web::Query(query): web::Query<RecentActivityQuery>,
) -> TgmRankResult<impl Responder> {
    let filter = RecentActivityFilter::try_from(query)?;

    let db_connection = db2.get().await?;
    let score_ids = RecentActivityService::get_score_ids(&db_connection, &filter).await?;

    let mut transaction = db.begin().await?;

    let recent_activity =
        RecentActivityService::get_recent_activity(&mut transaction, &score_ids, &filter).await?;

    transaction.commit().await?;

    Ok(HttpResponse::Ok().json(recent_activity))
}

#[derive(Deserialize)]
pub struct ActivityCalendarQuery {
    pub from: Date,
    pub to: Date,
}

#[get("/score/calendar")]
async fn get_activity_calendar(
    db: web::Data<DbPool>,
    web::Query(query): web::Query<ActivityCalendarQuery>,
) -> TgmRankResult<impl Responder> {
    let activity =
        RecentActivityService::get_calendar(&mut *db.acquire().await?, &query.from, &query.to)
            .await?;

    Ok(HttpResponse::Ok().json(activity))
}

#[get("/scoreStatus")]
async fn get_score_statuses() -> TgmRankResult<impl Responder> {
    let statuses = ScoreService::get_score_statuses()?;
    Ok(HttpResponse::Ok().json(statuses))
}

#[get("/roles")]
async fn get_roles() -> TgmRankResult<impl Responder> {
    let roles = Role::get_all_roles();
    Ok(HttpResponse::Ok().json(roles))
}

#[post("/score")]
async fn add_score(
    id: Identity,
    db: web::Data<DbPool>,
    score_form: web::Json<AddScoreForm>,
) -> TgmRankResult<impl Responder> {
    let user = LoginService::is_user(&mut *db.acquire().await?, id).await?;

    let score = score_form.into_inner();

    let mut transaction = db.begin().await?;

    let insert_result = ScoreService::insert_from_form(&mut transaction, &user, &score).await?;
    let ranked_score = RankingService::get_detailed_score(
        &mut transaction,
        insert_result.score_id,
        &GenericFilter::default(),
    )
    .await?;

    if let Some(true) = &score.dry_run {
        transaction.rollback().await?
    } else {
        transaction.commit().await?;
    }

    Ok(HttpResponse::Ok().json(ranked_score))
}

#[patch("/score")]
async fn update_score(
    id: Identity,
    db: web::Data<DbPool>,
    score_form: web::Json<UpdateScoreForm>,
) -> TgmRankResult<impl Responder> {
    let user = LoginService::is_user(&mut *db.acquire().await?, id).await?;

    let score = score_form.into_inner();

    let mut transaction = db.begin().await?;
    let update_result = ScoreService::update_from_form(&mut transaction, &user, &score).await?;

    let ranked_score = RankingService::get_detailed_score(
        &mut transaction,
        update_result.score_id,
        &GenericFilter::default(),
    )
    .await?;

    if let Some(true) = &score.dry_run {
        transaction.rollback().await?
    } else {
        transaction.commit().await?;
    }

    Ok(HttpResponse::Ok().json(ranked_score))
}

#[post("/score/{score_id:\\d+}/verify")]
async fn verify_score(
    id: Identity,
    db: web::Data<DbPool>,
    settings: web::Data<Settings>,
    score_id: web::Path<i32>,
    verify_form: web::Json<VerifyScoreForm>,
) -> TgmRankResult<impl Responder> {
    let mut transaction = db.begin().await?;
    let user = LoginService::can_verify_scores(&mut transaction, id).await?;

    let verify_score_form = verify_form.into_inner();

    let update_result = ScoreService::verify_score(
        &mut transaction,
        &settings,
        &user,
        score_id.into_inner(),
        &verify_score_form,
    )
    .await?;

    let ranked_score = RankingService::get_detailed_score(
        &mut transaction,
        update_result.score_id,
        &GenericFilter::default(),
    )
    .await?;

    transaction.commit().await?;

    Ok(HttpResponse::Ok().json(ranked_score))
}

#[get("/achievement")]
async fn get_achievement_list(db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let achievements = ScoreService::get_achievement_list(&mut *db.acquire().await?).await?;
    Ok(HttpResponse::Ok().json(achievements))
}

#[get("/score/statistics/submission")]
async fn get_submission_info(db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let models = StatsService::get_submission_stats(&mut *db.acquire().await?).await?;
    Ok(HttpResponse::Ok().json(models))
}

#[get("/score/statistics/medal")]
async fn get_ranking_medals(db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let models = StatsService::get_ranking_medals(&mut *db.acquire().await?).await?;
    Ok(HttpResponse::Ok().json(models))
}

#[get("/score/statistics/medal-countries")]
async fn get_ranking_medals_by_location(db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let medals = StatsService::medals_by_location(&mut *db.acquire().await?).await?;
    Ok(HttpResponse::Ok()
        .content_type("application/json")
        .body(medals.json.to_string()))
}

#[get("/proof/recent")]
async fn get_recent_proof(db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let proof = ProofEntity::get_recent_proof(&mut *db.acquire().await?).await?;
    let proof_models = proof.into_iter().map(FullProofModel::from).collect_vec();
    Ok(HttpResponse::Ok().json(proof_models))
}
