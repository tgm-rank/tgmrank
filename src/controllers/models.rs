use std::collections::HashMap;

use itertools::Itertools;
use serde::{Deserialize, Serialize};
use time::Date;

use crate::models::{AccountAchievementEntity, AchievementEntity, RecentActivityEntity};
use crate::models::{
    ActivityCalendarEntity, GameEntity, GradeEntity, LocationEntity, ModeEntity, ModeRankingEntity,
    PlayerEntity, ProofEntity, ProofType, ScoreStatus,
};
use crate::models::{
    BadgeEntity, FullRankingEntity, FullScoreEntity, PlatformEntity, PlayerEntity2, TeamEntity,
    VerificationEntity,
};
use crate::{models::Role, utility::playtime_string};

#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ErrorModel {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub error: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub field: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub details: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub data_errors: Option<Vec<ErrorModel>>,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GenericModel {
    pub success: bool,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LoginModel {
    pub user_id: i32,
    pub username: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub avatar: Option<String>,
    pub role: Role,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub location: Option<String>,
}

impl From<PlayerEntity> for LoginModel {
    fn from(player: PlayerEntity) -> Self {
        Self {
            user_id: player.player_id,
            username: player.player_name,
            avatar: player.avatar_file,
            role: player.player_role,
            location: player.location,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SubmissionRange {
    #[serde(skip_serializing_if = "Option::is_none")]
    start: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    end: Option<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ModeModel {
    pub mode_id: i32,
    pub game_id: i32,
    pub mode_name: String,
    pub link: Option<String>,
    pub sort_order: i32,
    pub grade_required: ScoreFieldRequired,
    pub score_required: ScoreFieldRequired,
    pub level_required: ScoreFieldRequired,
    pub time_required: ScoreFieldRequired,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub submission_range: Option<SubmissionRange>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tags: Option<Vec<String>>,
}

impl From<ModeEntity> for ModeModel {
    fn from(mode: ModeEntity) -> Self {
        let submission_range = SubmissionRange {
            start: mode.submission_range_start.and_then(|dt| {
                dt.format(&time::format_description::well_known::Rfc3339)
                    .ok()
            }),
            end: mode.submission_range_end.and_then(|dt| {
                dt.format(&time::format_description::well_known::Rfc3339)
                    .ok()
            }),
        };

        let submission_range = if submission_range.start.is_none() && submission_range.end.is_none()
        {
            None
        } else {
            Some(submission_range)
        };

        Self {
            mode_id: mode.mode_id,
            game_id: mode.game_id,
            mode_name: mode.mode_name.to_string(),
            link: mode.link,
            sort_order: mode.sort_order,
            grade_required: ScoreFieldRequired::from(mode.grade_sort),
            score_required: ScoreFieldRequired::from(mode.score_sort),
            level_required: ScoreFieldRequired::from(mode.level_sort),
            time_required: ScoreFieldRequired::from(mode.time_sort),
            submission_range,
            tags: mode.tags,
        }
    }
}

#[derive(Default, Serialize)]
pub struct ModeInformationModel {
    pub description: Option<String>,
}

impl From<ModeEntity> for ModeInformationModel {
    fn from(m: ModeEntity) -> Self {
        Self {
            description: m.description,
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GameModel {
    pub game_id: i32,
    pub game_name: String,
    pub short_name: String,
    pub subtitle: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub modes: Option<Vec<ModeModel>>,
}

impl GameModel {
    pub fn make_game_models(games: Vec<GameEntity>, modes: Vec<ModeEntity>) -> Vec<Self> {
        let mut game_modes: HashMap<i32, Vec<ModeModel>> = modes
            .into_iter()
            .map(|m| (m.game_id, m.into()))
            .into_group_map();

        games
            .into_iter()
            .map(|g| GameModel {
                modes: game_modes.remove(&g.game_id),
                ..g.into()
            })
            .collect_vec()
    }
}

impl From<GameEntity> for GameModel {
    fn from(game: GameEntity) -> Self {
        Self {
            game_id: game.game_id,
            game_name: game.game_name.to_string(),
            short_name: game.short_name.to_string(),
            subtitle: game.subtitle,
            modes: game
                .modes
                .map(|modes| modes.into_iter().map(ModeModel::from).collect_vec()),
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct OverallRankingEntryModel {
    pub rank_id: i32,
    pub game_id: i32,
    pub player: PlayerModel,
    pub score_count: i32,
    pub rank: i32,
    pub ranking_points: Option<i32>,
}

impl From<FullRankingEntity> for OverallRankingEntryModel {
    fn from(r: FullRankingEntity) -> Self {
        Self {
            rank_id: r.rank_id,
            game_id: r.game_id,
            player: PlayerModel::from(r.player),
            score_count: r.score_count,
            rank: r.ranking,
            ranking_points: r.ranking_points,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ScoreStatusVerificationModel {
    pub comment: Option<String>,
    pub verified_by: PlayerModel,
    pub verified_on: String,
}

impl From<VerificationEntity> for ScoreStatusVerificationModel {
    fn from(v: VerificationEntity) -> Self {
        Self {
            comment: v.verification_comment,
            verified_by: v.verified_by.0.into(),
            verified_on: v
                .verified_on
                .format(&time::format_description::well_known::Rfc3339)
                .expect("Invalid time format"),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlatformModel {
    pub platform_id: i32,
    pub platform_name: String,
}

impl From<PlatformEntity> for PlatformModel {
    fn from(p: PlatformEntity) -> Self {
        Self {
            platform_id: p.platform_id,
            platform_name: p.platform_name,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ModeRankingModel {
    pub score_id: i32,
    pub game_id: i32,
    pub mode_id: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub player: Option<PlayerModel>,
    pub level: Option<i32>,
    pub playtime: Option<String>,
    pub grade: Option<GradeModel>,
    pub proof: Vec<ProofModel>,
    pub comment: Option<String>,
    pub score: Option<i32>,
    pub rank: Option<i32>,
    pub ranking_points: Option<i32>,
    pub status: ScoreStatus,
    pub created_at: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub verification: Option<ScoreStatusVerificationModel>,
    pub platform: Option<PlatformModel>,
}

impl ModeRankingModel {
    pub fn from_components(
        r: ModeRankingEntity,
        score: FullScoreEntity,
        include_player: bool,
    ) -> Self {
        Self {
            score_id: score.score_id,
            game_id: r.game_id,
            mode_id: r.mode_id,
            player: if include_player {
                Some(score.player.into())
            } else {
                None
            },
            level: score.level,
            playtime: score.playtime.map(|time| playtime_string(&time)),
            score: score.score,
            grade: score.grade.map(|g| g.into()),
            proof: score
                .proof
                .unwrap_or_default()
                .into_iter()
                .map(|p| p.into())
                .collect_vec(),
            comment: score.comment,
            rank: r.ranking,
            ranking_points: r.ranking_points,
            status: score.status,
            created_at: score
                .created_at
                .format(&time::format_description::well_known::Rfc3339)
                .expect("Invalid time format"),
            verification: match (score.verifier, score.verified_on) {
                (Some(verifier), Some(verified_on)) => Some(ScoreStatusVerificationModel {
                    comment: score.verification_comment,
                    verified_by: verifier.into(),
                    verified_on: verified_on
                        .format(&time::format_description::well_known::Rfc3339)
                        .expect("Invalid time format"),
                }),
                _ => None,
            },
            platform: score.platform.map(|p| p.into()),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlayerModel {
    pub player_id: i32,
    pub player_name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub games: Option<Vec<GameModel>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub avatar: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub location: Option<String>,
}

impl From<PlayerEntity> for PlayerModel {
    fn from(player: PlayerEntity) -> Self {
        Self {
            player_id: player.player_id,
            player_name: player.player_name,
            games: None,
            avatar: player.avatar_file,
            location: player.location,
        }
    }
}

impl From<PlayerEntity2> for PlayerModel {
    fn from(player: PlayerEntity2) -> Self {
        Self {
            player_id: player.player_id,
            player_name: player.player_name,
            games: None,
            avatar: player.avatar_file,
            location: player.location,
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ScoreModel {
    pub details: ModeRankingModel,
    pub game: GameModel,
    pub mode: ModeModel,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GradeModel {
    grade_id: i32,
    grade_display: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    line: Option<String>,
    grade_order: i32,
}

impl From<GradeEntity> for GradeModel {
    fn from(grade: GradeEntity) -> Self {
        Self {
            grade_id: grade.grade_id,
            grade_display: grade.grade_display,
            line: grade.line.map(|line| line.to_string()),
            grade_order: grade.sort_order,
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FullProofModel {
    pub proof_id: i32,
    pub score_id: i32,
    pub proof_type: ProofType,
    pub link: String,
}

impl From<ProofEntity> for FullProofModel {
    fn from(proof: ProofEntity) -> Self {
        Self {
            proof_id: proof.proof_id,
            score_id: proof.score_id,
            proof_type: proof.proof_type,
            link: proof.link,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ProofModel {
    pub proof_id: i32,
    #[serde(rename = "type")]
    pub proof_type: ProofType,
    pub link: String,
}

impl From<ProofEntity> for ProofModel {
    fn from(proof: ProofEntity) -> Self {
        Self {
            proof_id: proof.proof_id,
            proof_type: proof.proof_type,
            link: proof.link,
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PingModel {
    pub message: String,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct VersionModel {
    pub version: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RankDiffModel {
    pub rank: Option<i32>,
    pub ranking_points: Option<i32>,
    pub previous_rank: Option<i32>,
    pub previous_ranking_points: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub players_offset: Option<Vec<PlayerModel>>,
}

impl From<RecentActivityEntity> for RankDiffModel {
    fn from(activity: RecentActivityEntity) -> Self {
        Self {
            rank: activity.current_ranking,
            ranking_points: activity.current_ranking_points,
            previous_rank: activity.prev_ranking,
            previous_ranking_points: activity.prev_ranking_points,
            players_offset: None,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RankDeltaModel {
    pub overall: RankDiffModel,
    pub game: RankDiffModel,
    pub mode: RankDiffModel,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RecentActivityModel {
    pub game_id: i32,
    pub mode_id: i32,
    pub score: ModeRankingModel,
    pub previous_score: Option<ModeRankingModel>,
    pub delta: RankDeltaModel,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ActivityCalendarModel {
    pub day: Date,
    pub value: i32,
    pub score_ids: Vec<i32>,
}

impl From<ActivityCalendarEntity> for ActivityCalendarModel {
    fn from(e: ActivityCalendarEntity) -> Self {
        Self {
            day: e.date,
            value: e.score_count as i32,
            score_ids: e.score_ids,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ScoreFieldRequired {
    Hidden,
    Optional,
    Required,
}

impl From<Option<i16>> for ScoreFieldRequired {
    fn from(field_sort: Option<i16>) -> Self {
        match field_sort {
            Some(1) => ScoreFieldRequired::Required,
            Some(-1) => ScoreFieldRequired::Required,
            Some(0) => ScoreFieldRequired::Optional,
            Some(_) => ScoreFieldRequired::Hidden,
            None => ScoreFieldRequired::Hidden,
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ExternalIntegrationModel {
    pub player_id: i32,
    pub external_key: String,
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LocationModel<'a> {
    pub alpha_2: &'a str,
    pub alpha_3: &'a str,
    pub name: &'a str,
}

impl<'a> From<&'a LocationEntity> for LocationModel<'a> {
    fn from(location: &'a LocationEntity) -> Self {
        Self {
            alpha_2: &location.alpha_2,
            alpha_3: &location.alpha_3,
            name: &location.name,
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AchievementModel {
    pub achievement_id: i32,
    pub description: String,
    pub game_id: Option<i32>,
    pub mode_id: Option<i32>,
    pub sort_order: i32,
}

impl From<AchievementEntity> for AchievementModel {
    fn from(achievement: AchievementEntity) -> Self {
        Self {
            achievement_id: achievement.achievement_id,
            description: achievement.description,
            game_id: achievement.game_id,
            mode_id: achievement.mode_id,
            sort_order: achievement.sort_order,
        }
    }
}

#[derive(Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AccountAchievementModel {
    pub achievement_id: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub score_id: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub achieved_at: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<ScoreStatus>,
}

impl From<AccountAchievementEntity> for AccountAchievementModel {
    fn from(aa: AccountAchievementEntity) -> Self {
        Self {
            achievement_id: aa.achievement_id,
            score_id: aa.score_id,
            achieved_at: aa.achieved_at.and_then(|achieved_at| {
                achieved_at
                    .format(&time::format_description::well_known::Rfc3339)
                    .ok()
            }),
            status: aa.status,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct BadgeModel {
    pub badge_id: i32,
    pub name: String,
}

impl From<BadgeEntity> for BadgeModel {
    fn from(e: BadgeEntity) -> Self {
        Self {
            badge_id: e.badge_id,
            name: e.name,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlayerBadgeModel {
    pub badges: Vec<String>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlayerUsernameHistoryModel {
    pub username: String,
    pub updated_at: String,
}

impl From<&PlayerEntity> for PlayerUsernameHistoryModel {
    fn from(p: &PlayerEntity) -> Self {
        Self {
            username: p.player_name.to_string(),
            updated_at: p
                .updated_at
                .format(&time::format_description::well_known::Rfc3339)
                .expect("Invalid time format"),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SubmissionStatsModel {
    pub player: PlayerModel,
    pub submissions: Vec<i32>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RankingMedalsModel {
    pub player: PlayerModel,
    pub total: i32,
    pub gold_count: i32,
    pub gold_modes: Vec<i32>,
    pub silver_count: i32,
    pub silver_modes: Vec<i32>,
    pub bronze_count: i32,
    pub bronze_modes: Vec<i32>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TeamModel {
    pub team_id: i32,
    pub name: String,
    pub members: Vec<PlayerModel>,
}

impl From<TeamEntity> for TeamModel {
    fn from(t: TeamEntity) -> Self {
        Self {
            team_id: t.team_id,
            name: t.name,
            members: t.members.into_iter().map(PlayerModel::from).collect(),
        }
    }
}
