use actix_identity::Identity;
use actix_web::web::ServiceConfig;
use actix_web::{delete, get, patch, post, put, web, HttpResponse, Responder};
use validator::Validate;

use crate::models::{
    forms::{PlayerLocationUpdateForm, PlayerUpdateForm},
    Role,
};
use crate::models::{PlayerEntity, PlayerSettingsEntity};
use crate::modules::{DbPool, Settings};
use crate::services::{EmailService, LoginService, PlayerService, RankingService};
use crate::utility::{AuthenticationError, TgmRankError, TgmRankResult};
use crate::{
    controllers::models::{GenericModel, LoginModel, PlayerModel},
    models::forms::PlayerUsernameUpdateForm,
};
use crate::{models::filters::PlayerFilter, utility::ClientError};

pub fn routes(config: &mut ServiceConfig) {
    config
        .service(update_player)
        .service(update_player_location)
        .service(get_player_settings)
        .service(update_player_settings)
        .service(get_player_username_history)
        .service(update_player_username)
        .service(get_players)
        .service(get_ranked_players)
        .service(get_player_rivals)
        .service(get_player_reverse_rivals)
        .service(get_player_rival)
        .service(post_player_rival)
        .service(delete_player_rival)
        .service(get_player_achievements)
        .service(get_player_badges)
        .service(add_badge)
        .service(remove_badge)
        .service(admin_get_player_role)
        .service(admin_update_player_role)
        .service(get_player_by_name)
        .service(get_scores_for_player_by_name)
        .service(get_player)
        .service(get_scores_for_player);
}

async fn get_player_scores(
    db: web::Data<DbPool>,
    filter: PlayerFilter,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;

    let player_scores = RankingService::get_player_scores(db, &filter).await?;
    Ok(HttpResponse::Ok().json(player_scores))
}

#[derive(Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlayerQuery {
    player_name: String,
}

#[get("/player")]
async fn get_player_by_name(
    db: web::Data<DbPool>,
    web::Query(query): web::Query<PlayerQuery>,
) -> TgmRankResult<impl Responder> {
    let player =
        PlayerService::find_player(&mut *db.acquire().await?, &query.player_name, false).await?;
    let model: PlayerModel = player.into();

    Ok(HttpResponse::Ok().json(model))
}

#[get("/player/scores")]
async fn get_scores_for_player_by_name(
    db: web::Data<DbPool>,
    web::Query(query): web::Query<PlayerQuery>,
) -> TgmRankResult<impl Responder> {
    let filter = PlayerFilter {
        name: Some(query.player_name),
        ..Default::default()
    };

    get_player_scores(db, filter).await
}

pub fn player_scores_path(player_id: i32) -> String {
    format!("/v1/player/{0}/scores", player_id)
}

#[get("/player/{player_id}/scores")]
async fn get_scores_for_player(
    db: web::Data<DbPool>,
    player_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let filter = PlayerFilter {
        id: Some(player_id.into_inner()),
        ..Default::default()
    };

    get_player_scores(db, filter).await
}

#[get("/player/{player_id}")]
async fn get_player(
    db: web::Data<DbPool>,
    player_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let player =
        PlayerService::find_by_id(&mut *db.acquire().await?, player_id.into_inner()).await?;
    let model: PlayerModel = player.into();

    Ok(HttpResponse::Ok().json(model))
}

#[patch("/player")]
async fn update_player(
    id: Identity,
    settings: web::Data<Settings>,
    db: web::Data<DbPool>,
    update_form: web::Json<PlayerUpdateForm>,
) -> TgmRankResult<impl Responder> {
    update_form.validate()?;

    let db_connection = &mut *db.acquire().await?;
    let user = LoginService::is_user(db_connection, id).await?;
    let player = PlayerEntity::get_by_id(db_connection, user.user_id).await?;
    let player = match (&update_form.current_password, &update_form.new_password) {
        (Some(current_password), Some(new_password)) if !new_password.is_empty() => {
            let mut transaction = db.begin().await?;

            let player =
                PlayerService::check_password_hash(player, current_password.to_string()).await?;
            let player =
                PlayerService::update_password(&mut transaction, player, new_password).await?;

            if player.email.is_some() {
                EmailService::send_password_was_reset_email(&mut transaction, &settings, &player)
                    .await?;
            }

            transaction.commit().await?;

            player
        }
        _ => player,
    };

    LoginService::delete_user_sessions(db_connection, player.player_id, &[user.session_key])
        .await?;

    Ok(HttpResponse::Ok().json(LoginModel::from(player)))
}

#[get("/player/{player_id}/username")]
async fn get_player_username_history(
    db: web::Data<DbPool>,
    player_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let username_history =
        PlayerService::get_username_history(&mut *db.acquire().await?, player_id.into_inner())
            .await?;

    Ok(HttpResponse::Ok().json(username_history))
}

#[put("/player/username")]
async fn update_player_username(
    id: Identity,
    db: web::Data<DbPool>,
    update_form: web::Json<PlayerUsernameUpdateForm>,
) -> TgmRankResult<impl Responder> {
    update_form.validate()?;

    let db = &mut *db.acquire().await?;
    let user = LoginService::is_user(db, id).await?;
    let player = PlayerEntity::get_by_id(db, user.user_id).await?;
    let player = PlayerService::update_username(db, player, &update_form.username).await?;

    Ok(HttpResponse::Ok().json(LoginModel::from(player)))
}

#[put("/player/location")]
async fn update_player_location(
    id: Identity,
    db: web::Data<DbPool>,
    update_form: web::Json<PlayerLocationUpdateForm>,
) -> TgmRankResult<impl Responder> {
    update_form.validate()?;

    let db = &mut *db.acquire().await?;
    let user = LoginService::is_user(db, id).await?;
    let player = PlayerEntity::get_by_id(db, user.user_id).await?;
    let player =
        PlayerService::update_location(db, player, update_form.new_location.as_deref()).await?;

    Ok(HttpResponse::Ok().json(LoginModel::from(player)))
}

#[get("/player/settings")]
async fn get_player_settings(id: Identity, db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let user = LoginService::is_user(db, id).await?;
    let settings = PlayerService::get_settings(db, user.user_id).await?;

    Ok(HttpResponse::Ok().json(settings))
}

#[put("/player/settings")]
async fn update_player_settings(
    id: Identity,
    db: web::Data<DbPool>,
    update_form: web::Json<PlayerSettingsEntity>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let user = LoginService::is_user(db, id).await?;
    let player = PlayerService::update_settings(db, user.user_id, &update_form).await?;

    Ok(HttpResponse::Ok().json(LoginModel::from(player)))
}

#[get("/player/{player_id}/rivals")]
async fn get_player_rivals(
    db: web::Data<DbPool>,
    player_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let rivals = PlayerService::get_rivals(db, player_id.into_inner()).await?;
    Ok(HttpResponse::Ok().json(rivals))
}

#[get("/player/{player_id}/rivaledBy")]
async fn get_player_reverse_rivals(
    db: web::Data<DbPool>,
    player_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let rivals =
        PlayerService::get_reverse_rivals(&mut *db.acquire().await?, player_id.into_inner())
            .await?;
    Ok(HttpResponse::Ok().json(rivals))
}

#[get("/player/{player_id}/rival/player/{rival_player_id}")]
async fn get_player_rival(
    db: web::Data<DbPool>,
    path: web::Path<(i32, i32)>,
) -> TgmRankResult<impl Responder> {
    let (player_id, rival_player_id) = path.into_inner();

    let db = &mut *db.acquire().await?;
    let rivals = PlayerService::get_rivals(db, player_id).await?;
    let rival_player = rivals
        .iter()
        .find(|r| r.player_id == rival_player_id)
        .ok_or(TgmRankError::NotFoundError)?;

    Ok(HttpResponse::Ok().json(rival_player))
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RivalForm {
    pub rival_player_id: i32,
}

#[post("/player/{player_id}/rival")]
async fn post_player_rival(
    id: Identity,
    db: web::Data<DbPool>,
    player_id: web::Path<i32>,
    rival_form: web::Json<RivalForm>,
) -> TgmRankResult<impl Responder> {
    let player_id = player_id.into_inner();

    let db = &mut *db.acquire().await?;
    let user = LoginService::is_user(db, id).await?;
    if user.user_id != player_id {
        return Err(AuthenticationError::UnauthorizedForOperation.into());
    }

    if player_id == rival_form.rival_player_id {
        return Err(ClientError::CannotAddSelfAsRival.into());
    }

    let rival_player = PlayerService::add_rival(db, player_id, rival_form.rival_player_id).await?;

    Ok(HttpResponse::Ok().json(rival_player))
}

#[delete("/player/{player_id}/rival/player/{rival_player_id}")]
async fn delete_player_rival(
    id: Identity,
    db: web::Data<DbPool>,
    path: web::Path<(i32, i32)>,
) -> TgmRankResult<impl Responder> {
    let (player_id, rival_player_id) = path.into_inner();

    let db = &mut *db.acquire().await?;
    let user = LoginService::is_user(db, id).await?;
    if user.user_id != player_id {
        return Err(AuthenticationError::UnauthorizedForOperation.into());
    }

    PlayerService::delete_rival(db, player_id, rival_player_id).await?;

    Ok(HttpResponse::Ok().json(GenericModel { success: true }))
}

#[get("/players")]
async fn get_players(db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let players = PlayerService::get_all(&mut *db.acquire().await?).await?;

    Ok(HttpResponse::Ok().json(players))
}

pub fn ranked_player_list_path() -> &'static str {
    "/v1/players/ranked"
}

#[get("/players/ranked")]
async fn get_ranked_players(db: web::Data<DbPool>) -> TgmRankResult<impl Responder> {
    let players = PlayerService::get_all_with_scores(&mut *db.acquire().await?).await?;

    Ok(HttpResponse::Ok().json(players))
}

#[get("/player/{player_id}/achievement")]
async fn get_player_achievements(
    db: web::Data<DbPool>,
    player_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let achievements =
        PlayerService::get_achievements(&mut *db.acquire().await?, player_id.into_inner()).await?;
    Ok(HttpResponse::Ok().json(achievements))
}

#[get("/player/{player_id}/badge")]
async fn get_player_badges(
    db: web::Data<DbPool>,
    player_id: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let badge_model =
        PlayerService::get_badges(&mut *db.acquire().await?, player_id.into_inner()).await?;
    Ok(HttpResponse::Ok().json(badge_model))
}

#[post("/player/{player_id}/badge/{badge_id}")]
async fn add_badge(
    id: Identity,
    db: web::Data<DbPool>,
    path: web::Path<(i32, i32)>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let user = LoginService::is_admin(db, id).await?;
    let (player_id, badge_id) = path.into_inner();
    let badge_model = PlayerService::add_badge_for_player(&user, db, player_id, badge_id).await?;
    Ok(HttpResponse::Ok().json(badge_model))
}

#[delete("/player/{player_id}/badge/{badge_id}")]
async fn remove_badge(
    id: Identity,
    db: web::Data<DbPool>,
    path: web::Path<(i32, i32)>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let _user = LoginService::is_admin(db, id).await?;
    let (player_id, badge_id) = path.into_inner();
    let badge_model = PlayerService::remove_badge_for_player(db, player_id, badge_id).await?;
    Ok(HttpResponse::Ok().json(badge_model))
}

#[get("/player/{player_id}/role")]
async fn admin_get_player_role(
    id: Identity,
    db: web::Data<DbPool>,
    path: web::Path<i32>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let _user = LoginService::is_admin(db, id).await?;

    let player_id = path.into_inner();
    let player = PlayerService::find_by_id(db, player_id).await?;

    Ok(HttpResponse::Ok().json(player.player_role))
}

#[post("/player/{player_id}/role/{player_role}")]
async fn admin_update_player_role(
    id: Identity,
    db: web::Data<DbPool>,
    path: web::Path<(i32, Role)>,
) -> TgmRankResult<impl Responder> {
    let db = &mut *db.acquire().await?;
    let _user = LoginService::is_admin(db, id).await?;

    let (player_id, player_role) = path.into_inner();

    let player_to_update = PlayerService::find_by_id(db, player_id).await?;

    if player_role == Role::Admin || player_to_update.player_role == Role::Admin {
        return Err(AuthenticationError::UnauthorizedForOperation.into());
    }

    let player = PlayerService::update_role(db, player_to_update, player_role).await?;
    let player_model: PlayerModel = player.into();

    Ok(HttpResponse::Ok().json(player_model))
}
