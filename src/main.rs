extern crate tgmrank;

#[actix_web::main]
async fn main() -> tgmrank::utility::TgmRankResult<()> {
    tgmrank::run_service().await
}
