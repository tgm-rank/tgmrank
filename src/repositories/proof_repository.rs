use crate::models::{NewProofEntity, ProofEntity, ProofType};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl ProofEntity {
    pub async fn get_by_score_id(
        db: &mut DbConnection,
        score_ids: &[i32],
    ) -> TgmRankResult<Vec<Self>> {
        let proof = sqlx::query_as!(
            Self,
            r#"
SELECT proof_id, score_id, proof_type as "proof_type: ProofType", link
FROM public.proof
WHERE score_id = ANY($1)
ORDER BY score_id, proof_id"#,
            score_ids
        )
        .fetch_all(db)
        .await?;

        Ok(proof)
    }

    pub async fn get_recent_proof(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let proof = sqlx::query_as!(
            Self,
            r#"
SELECT proof_id, score_id, proof_type as "proof_type: ProofType", link
FROM public.proof
WHERE created_at > current_timestamp - '14 days'::INTERVAL;"#
        )
        .fetch_all(db)
        .await?;

        Ok(proof)
    }

    pub async fn insert(db: &mut DbConnection, proof: &NewProofEntity<'_>) -> TgmRankResult<Self> {
        let proof = sqlx::query_as!(
            Self,
            r#"INSERT INTO public.proof (score_id, proof_type, link)
        VALUES ($1, $2::proof_type, $3)
        RETURNING proof_id, score_id, proof_type as "proof_type: ProofType", link"#,
            proof.score_id,
            proof.proof_type as &ProofType,
            proof.link
        )
        .fetch_one(db)
        .await?;

        Ok(proof)
    }

    pub async fn insert_many(
        db: &mut DbConnection,
        proof: &[&'_ NewProofEntity<'_>],
    ) -> TgmRankResult<Vec<Self>> {
        let mut proof_result = Vec::new();
        for p in proof.iter() {
            let proof_entity = Self::insert(db, p).await?;
            proof_result.push(proof_entity);
        }

        Ok(proof_result)
    }

    pub async fn delete_for_score(
        db: &mut DbConnection,
        score_id: i32,
    ) -> TgmRankResult<Vec<Self>> {
        let deleted_proof = sqlx::query_as!(
            Self,
            r#"DELETE FROM public.proof WHERE score_id = $1
        RETURNING proof_id, score_id, proof_type as "proof_type: ProofType", link"#,
            score_id
        )
        .fetch_all(db)
        .await?;

        Ok(deleted_proof)
    }

    pub async fn delete_many(db: &mut DbConnection, proof_ids: &[i32]) -> TgmRankResult<Vec<Self>> {
        let deleted_proof = sqlx::query_as!(
            Self,
            r#"DELETE FROM public.proof WHERE proof_id = ANY($1)
        RETURNING proof_id, score_id, proof_type as "proof_type: ProofType", link"#,
            proof_ids
        )
        .fetch_all(db)
        .await?;

        Ok(deleted_proof)
    }
}
