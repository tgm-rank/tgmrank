use crate::models::filters::{GenericFilter, ModeRankingFilter, PlayerFilter};
use crate::models::{ModeRankingEntity, RankingAlgorithm};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl ModeRankingEntity {
    pub async fn refresh_current_mode_ranking(db: &mut DbConnection) -> TgmRankResult<()> {
        sqlx::query("REFRESH MATERIALIZED VIEW current_mode_ranking;")
            .execute(db)
            .await?;

        Ok(())
    }

    pub async fn get_ranked_scores(
        db: &mut DbConnection,
        score_ids: &[i32],
        retrieve_as_of: bool,
    ) -> TgmRankResult<Vec<Self>> {
        let scores = sqlx::query_as!(
            Self,
            r#"
SELECT
    s.score_id as "score_id!",
    s.player_id as "player_id!",
    g.game_id as "game_id!",
    m.mode_id as "mode_id!",
    r.ranking as "ranking?",
    r.ranking_points as "ranking_points?"
FROM public.score_entry s
JOIN public.player p ON s.player_id = p.player_id
JOIN public.mode m ON s.mode_id = m.mode_id
JOIN public.game g ON m.game_id = g.game_id
LEFT JOIN public.mode_ranking(
        NULL,
        CASE WHEN $2 THEN s.created_at ELSE current_timestamp end
    ) r
    ON r.score_id = s.score_id
WHERE s.score_id = ANY($1)"#,
            score_ids,
            retrieve_as_of
        )
        .fetch_all(db)
        .await?;

        Ok(scores)
    }

    pub async fn get_player_scores(
        db: &mut DbConnection,
        filter: &PlayerFilter,
    ) -> TgmRankResult<Vec<Self>> {
        let scores = sqlx::query_as!(
            Self,
            r#"
select
    s.score_id as "score_id!",
    s.player_id as "player_id!",
    g.game_id as "game_id!",
    mo.mode_id as "mode_id!",
    r.ranking as "ranking?",
    r.ranking_points as "ranking_points?"
FROM public.score_entry__as_of2(
        CASE WHEN $1::TIMESTAMPTZ IS NOT NULL THEN $1 ELSE CURRENT_TIMESTAMP END
     ) s
JOIN public.player p USING (player_id)
JOIN public.mode mo USING (mode_id)
JOIN public.game g USING (game_id)
LEFT JOIN public.mode_ranking(NULL, CASE WHEN $1::TIMESTAMPTZ IS NOT NULL THEN $1 ELSE CURRENT_TIMESTAMP END) r
    USING (score_id)
WHERE p.player_id = $2 OR p.player_name ILIKE $3
ORDER BY g.game_id, mo.mode_id, s.rank_order_key;"#,
            filter.as_of,
            filter.id,
            filter.name
        )
        .fetch_all(db)
        .await?;

        Ok(scores)
    }

    pub async fn get_mode_ranking(
        db: &mut DbConnection,
        filter: &ModeRankingFilter,
    ) -> TgmRankResult<Vec<Self>> {
        let scores = sqlx::query_as!(
            Self,
            r#"
SELECT
    r.score_id as "score_id!",
    r.player_id as "player_id!",
    g.game_id as "game_id!",
    m.mode_id as "mode_id!",
    r.ranking as "ranking?",
    r.ranking_points as "ranking_points?"
FROM public.mode_ranking(
    CASE WHEN $2::INTEGER IS NULL THEN NULL ELSE ARRAY[$2]::INTEGER[] END,
    CASE WHEN $1::TIMESTAMPTZ IS NOT NULL THEN $1 ELSE CURRENT_TIMESTAMP END, $8::ranking_algorithm
) r
JOIN public.player p ON r.player_id = p.player_id
JOIN public.mode m ON r.mode_id = m.mode_id
JOIN public.game g ON m.game_id = g.game_id
WHERE
    r.ranking IS NOT NULL AND
    ($2::integer IS NULL OR r.mode_id = $2::integer) AND
    (($3::varchar is NULL AND $4::integer[] IS NULL) OR
         p.player_name ILIKE $3::varchar OR
         p.player_id = ANY($4::integer[])) AND
    ($7::varchar[] IS NULL OR p.location = ANY($7::varchar[]))
ORDER BY r.ranking NULLS LAST, p.player_name
LIMIT coalesce($5, 1000000)
OFFSET (coalesce($5, 1000000) * ($6 - 1))"#,
            filter.as_of,
            filter.id,
            filter.player_name,
            filter.player_ids.as_deref(),
            filter.pagination.page_size,
            filter.pagination.page,
            filter.location.locations.as_deref(),
            filter.algorithm.clone() as RankingAlgorithm,
        )
        .fetch_all(db)
        .await?;

        Ok(scores)
    }

    pub async fn get_score(
        db: &mut DbConnection,
        score_id: i32,
        filter: &GenericFilter,
    ) -> TgmRankResult<Self> {
        let scores = sqlx::query_as!(
            Self,
            r#"
select
    s.score_id as "score_id!",
    s.player_id as "player_id!",
    g.game_id as "game_id!",
    m.mode_id as "mode_id!",
    r.ranking as "ranking?",
    r.ranking_points as "ranking_points?"
FROM public.score_entry__as_of2(
        CASE WHEN $1::TIMESTAMPTZ IS NOT NULL THEN $1 ELSE CURRENT_TIMESTAMP END
     ) s
JOIN public.player p ON s.player_id = p.player_id
JOIN public.mode m ON s.mode_id = m.mode_id
JOIN public.game g ON m.game_id = g.game_id
LEFT JOIN public.mode_ranking(ARRAY[s.mode_id], CASE WHEN $1::TIMESTAMPTZ IS NOT NULL THEN $1 ELSE CURRENT_TIMESTAMP END) r
    ON r.score_id = s.score_id
WHERE s.score_id = $2"#,
            filter.as_of,
            score_id
        )
            .fetch_one(db)
            .await?;

        Ok(scores)
    }
}
