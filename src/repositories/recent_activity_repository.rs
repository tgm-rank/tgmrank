use crate::models::filters::RecentActivityFilter;
use crate::models::{RecentActivityEntity, RecentActivityType};
use crate::modules::{DbConnection, DbConnection2};
use crate::utility::TgmRankResult;

pub struct RecentActivityRepository {}

impl RecentActivityRepository {
    pub async fn get_score_ids(
        db: &DbConnection2<'_>,
        filter: &RecentActivityFilter,
    ) -> TgmRankResult<Vec<i32>> {
        let default_recent_activity_statuses = vec![];
        let score_statuses = &filter
            .filter
            .score_status
            .as_ref()
            .unwrap_or(&default_recent_activity_statuses);

        let score_ids = db
            .query(
                r#"
        SELECT score_id
        FROM public.score_entry s
        JOIN public.player p USING (player_id)
        LEFT JOIN public.recent_activity ra USING (score_id)
        WHERE status = ANY($9::score_status[]) and
            ($5::INTEGER IS NULL OR s.score_id = $5::INTEGER) AND
            ($6::INTEGER[] IS NULL OR s.mode_id = ANY($6::INTEGER[])) AND
            ($7::INTEGER IS NULL OR (ra.ranking <= $7::INTEGER AND ra.mode_ids = $8::INTEGER[])) AND
            ($1::VARCHAR IS NULL OR p.player_name ILIKE $1) AND
            ($4::VARCHAR[] IS NULL OR p.location = ANY($4::VARCHAR[]))
        GROUP BY score_id
        ORDER BY s.created_at desc
        LIMIT least(coalesce($2, 10), 20)
        OFFSET (least(coalesce($2, 10), 20) * ($3 - 1))"#,
                &[
                    &filter.filter.player_name,
                    &filter.filter.pagination.page_size,
                    &filter.filter.pagination.page,
                    &filter.filter.location.locations.as_deref(),
                    &filter.score_id,
                    &filter.filter_mode_ids.as_deref(),
                    &filter.rank_threshold,
                    &filter.filter.mode.mode_ids.as_deref(),
                    &score_statuses.as_slice(),
                ],
            )
            .await?;

        let score_ids = score_ids.into_iter().map(|row| row.get(0)).collect();

        Ok(score_ids)
    }

    pub async fn update_recent_activity(
        db: &mut DbConnection,
        ranked_mode_ids: &[i32],
        score_ids: &[i32],
    ) -> TgmRankResult<()> {
        sqlx::query!(
            r#"SELECT public.cache_recent_activity($1, $2);"#,
            ranked_mode_ids,
            score_ids
        )
        .execute(db)
        .await?;

        Ok(())
    }

    pub async fn update_recent_activity_by_mode_group(
        db: &mut DbConnection,
        mode_group: &str,
        score_ids: &[i32],
    ) -> TgmRankResult<()> {
        sqlx::query!(
            r#"SELECT public.cache_recent_activity(
                (SELECT mg.mode_ids FROM public.mode_group mg WHERE mg."name" = $1 AND game_id = 0),
                $2
            );"#,
            mode_group,
            score_ids
        )
        .execute(db)
        .await?;

        Ok(())
    }

    pub async fn recent_activity(
        db: &mut DbConnection,
        score_ids: &[i32],
        mode_ids: &[i32],
    ) -> TgmRankResult<Vec<RecentActivityEntity>> {
        let activity = sqlx::query_as!(
            RecentActivityEntity,
            r#"
SELECT
    ra.activity_type AS "activity_type!: RecentActivityType",
    ra.game_or_mode_id AS "id!",
    ra.score_id AS "current_score_id!",
    ra.ranking AS current_ranking,
    ra.ranking_points AS current_ranking_points,
    ra.prev_score_id AS prev_score_id,
    ra.prev_ranking AS prev_ranking,
    ra.prev_ranking_points AS prev_ranking_points,
    ra.passed_player_ids AS "passed_player_ids!"
FROM public.recent_activity ra
JOIN public.score_entry se USING (score_id)
WHERE ra.score_id = ANY($1) AND ra.mode_ids = $2
ORDER BY se.created_at DESC, se.score_id, ra.activity_type;"#,
            score_ids,
            mode_ids,
        )
        .fetch_all(db)
        .await?;

        Ok(activity)
    }
}
