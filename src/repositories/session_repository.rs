use uuid::Uuid;

use crate::models::{NewSession, Session};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl Session {
    pub async fn get_by_id(db: &mut DbConnection, session_id: i32) -> TgmRankResult<Self> {
        let session = sqlx::query_as!(
            Session,
            r#"SELECT session_id, session_key, user_id, expires FROM public.sessions WHERE session_id = $1"#,
            session_id
        )
        .fetch_one(db)
        .await?;
        Ok(session)
    }

    pub async fn get_sessions_for_player(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<Vec<Self>> {
        let sessions = sqlx::query_as!(
            Session,
            r#"SELECT session_id, session_key, user_id, expires FROM public.sessions WHERE user_id = $1"#,
            player_id
        )
        .fetch_all(db)
        .await?;
        Ok(sessions)
    }

    pub async fn get_session_by_key(
        db: &mut DbConnection,
        session_key: Uuid,
    ) -> TgmRankResult<Session> {
        let session = sqlx::query_as!(
            Session,
            r#"SELECT session_id, session_key, user_id, expires FROM public.sessions WHERE session_key = $1"#,
            session_key
        )
        .fetch_one(db)
        .await?;
        Ok(session)
    }

    pub async fn delete_session(
        db: &mut DbConnection,
        player_id: i32,
        session_key: Uuid,
    ) -> TgmRankResult<()> {
        let _ = sqlx::query!(
            r#"DELETE FROM public.sessions WHERE user_id = $1 AND session_key = $2"#,
            player_id,
            session_key,
        )
        .execute(db)
        .await?;
        Ok(())
    }

    pub async fn delete_expired_sessions(db: &mut DbConnection) -> TgmRankResult<()> {
        let _ = sqlx::query!(r#"DELETE FROM public.sessions WHERE expires < CURRENT_TIMESTAMP"#)
            .execute(db)
            .await?;
        Ok(())
    }

    pub async fn delete_player_sessions(
        db: &mut DbConnection,
        player_id: i32,
        exceptions: &[Uuid],
    ) -> TgmRankResult<()> {
        let _ = sqlx::query!(
            r#"DELETE FROM public.sessions WHERE user_id = $1 AND NOT (session_key = ANY($2))"#,
            player_id,
            &exceptions
        )
        .execute(db)
        .await?;
        Ok(())
    }

    pub async fn insert(db: &mut DbConnection, new_session: NewSession) -> TgmRankResult<Session> {
        let session = sqlx::query_as!(
            Session,
            r#"INSERT INTO public.sessions
(session_key, user_id, expires)
VALUES($1, $2, $3)
RETURNING session_id, session_key, user_id, expires"#,
            new_session.session_key,
            new_session.user_id,
            new_session.expires
        )
        .fetch_one(db)
        .await?;
        Ok(session)
    }
}
