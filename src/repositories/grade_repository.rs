use crate::models::{GradeEntity, GradeLine};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl GradeEntity {
    pub async fn get_by_id(db: &mut DbConnection, grade_id: i32) -> TgmRankResult<Self> {
        let grade = sqlx::query_as!(
            Self,
            r#"
SELECT grade_id, grade_display, mode_id, line as "line: GradeLine", sort_order
FROM public.grade
WHERE grade_id = $1"#,
            grade_id
        )
        .fetch_one(db)
        .await?;
        Ok(grade)
    }

    pub async fn get_by_ids(db: &mut DbConnection, grade_ids: &[i32]) -> TgmRankResult<Vec<Self>> {
        let grade = sqlx::query_as!(
            Self,
            r#"
SELECT grade_id, grade_display, mode_id, line as "line: GradeLine", sort_order
FROM public.grade
WHERE grade_id = ANY($1)"#,
            grade_ids
        )
        .fetch_all(db)
        .await?;
        Ok(grade)
    }

    pub async fn get_for_mode(db: &mut DbConnection, mode_id: i32) -> TgmRankResult<Vec<Self>> {
        let grades = sqlx::query_as!(
            Self,
            r#"
SELECT grade_id, grade_display, mode_id, line as "line: GradeLine", sort_order
FROM public.grade
WHERE mode_id = $1"#,
            mode_id
        )
        .fetch_all(db)
        .await?;
        Ok(grades)
    }
}
