use crate::models::{NewPlatformEntity, PlatformEntity};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl PlatformEntity {
    pub async fn insert(
        db: &mut DbConnection,
        new_platform: &NewPlatformEntity,
    ) -> TgmRankResult<Self> {
        let p = sqlx::query_as!(
            Self,
            r#"
INSERT INTO public.platform (platform_name, mode_id)
VALUES ($1, $2)
RETURNING platform_id,
platform_name,
mode_id,
game_id
"#,
            new_platform.platform_name,
            new_platform.mode_id,
        )
        .fetch_one(&mut *db)
        .await?;

        Ok(p)
    }

    pub async fn get_for_mode(db: &mut DbConnection, mode_id: i32) -> TgmRankResult<Vec<Self>> {
        let platforms = sqlx::query_as!(
            Self,
            r#"
SELECT platform_id, platform_name, mode_id, game_id
FROM public.platform
WHERE mode_id = $1 OR game_id = (SELECT game_id FROM public."mode" m WHERE m.mode_id = $1)
ORDER BY platform_name"#,
            mode_id
        )
        .fetch_all(db)
        .await?;
        Ok(platforms)
    }
}
