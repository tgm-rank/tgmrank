use crate::models::RivalEntity;
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

pub struct RivalRepository {}

impl RivalEntity {
    pub async fn get_by_player_id(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<Vec<Self>> {
        let rivals = sqlx::query_as!(
            Self,
            r#"SELECT rival_id, player_id, rival_player_id FROM public.rival WHERE player_id = $1 ORDER BY created_at"#,
            player_id
        )
        .fetch_all(db)
        .await?;

        Ok(rivals)
    }

    pub async fn get_reverse_rivals_by_player_id(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<Vec<Self>> {
        let rivals = sqlx::query_as!(
            Self,
            r#"SELECT r.rival_id, r.player_id, r.rival_player_id
            FROM public.rival r
            JOIN public.player p ON r.player_id = p.player_id
            WHERE rival_player_id = $1
            ORDER BY LOWER(p.player_name);"#,
            player_id
        )
        .fetch_all(db)
        .await?;

        Ok(rivals)
    }

    pub async fn get_by_rival_player_id(
        db: &mut DbConnection,
        rival_player_id: i32,
    ) -> TgmRankResult<Vec<RivalEntity>> {
        let rivals = sqlx::query_as!(
            Self,
            r#"SELECT rival_id, player_id, rival_player_id FROM public.rival WHERE rival_player_id = $1"#,
            rival_player_id
        )
        .fetch_all(db)
        .await?;

        Ok(rivals)
    }

    pub async fn insert(db: &mut DbConnection, entity: RivalEntity) -> TgmRankResult<RivalEntity> {
        let rival = sqlx::query_as!(
            Self,
            r#"INSERT INTO public.rival (player_id, rival_player_id)
        VALUES ($1, $2)
        ON CONFLICT (player_id, rival_player_id)
        DO UPDATE SET rival_player_id = EXCLUDED.rival_player_id
        RETURNING rival_id, player_id, rival_player_id"#,
            entity.player_id,
            entity.rival_player_id
        )
        .fetch_one(db)
        .await?;

        Ok(rival)
    }

    pub async fn delete(db: &mut DbConnection, rival_id: i32) -> TgmRankResult<Self> {
        let rival = sqlx::query_as!(
            Self,
            r#"DELETE FROM public.rival WHERE rival_id = $1 RETURNING rival_id, player_id, rival_player_id"#,
            rival_id
        )
        .fetch_one(db)
        .await?;

        Ok(rival)
    }

    pub async fn delete_by_player_ids(
        db: &mut DbConnection,
        player_id: i32,
        rival_player_id: i32,
    ) -> TgmRankResult<Self> {
        let rival = sqlx::query_as!(
            Self,
            r#"DELETE FROM public.rival WHERE player_id = $1 AND rival_player_id = $2 RETURNING rival_id, player_id, rival_player_id"#,
            player_id,
            rival_player_id
        )
        .fetch_one(db)
        .await?;

        Ok(rival)
    }
}
