use crate::models::filters::PlayerFilter;
use crate::models::{
    AccountClaimEntity, NewPlayerEntity, PlayerEntity, PlayerEntity2, PlayerSettingsEntity, Role,
};
use crate::modules::DbConnection;
use crate::utility::{TgmRankError, TgmRankResult};

impl PlayerEntity {
    pub async fn get_all(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let players = sqlx::query_as!(
            Self,
            r#"SELECT
    player_id as "player_id!: i32",
    player_name as "player_name!",
    email,
    password as "password!",
    player_role as "player_role!: Role",
    avatar_file,
    active as "active!: bool",
    l.location_id as "location_id?",
    l.alpha_2 as "location?",
    created_at as "created_at!",
    updated_at as "updated_at!"
FROM public.account a
LEFT JOIN location l ON a.location_id = l.location_id"#
        )
        .fetch_all(db)
        .await?;
        Ok(players)
    }

    pub async fn get(db: &mut DbConnection, filter: &PlayerFilter) -> TgmRankResult<Self> {
        let player = if let Some(player_id) = &filter.id {
            Self::get_by_id(db, *player_id).await
        } else if let Some(name) = &filter.name {
            Self::get_by_name(db, name, false).await
        } else {
            Err(TgmRankError::NotFoundError)
        }?;

        Ok(player)
    }

    pub async fn get_by_id(db: &mut DbConnection, player_id: i32) -> TgmRankResult<Self> {
        let mut players = Self::get_by_ids(db, &[player_id]).await?;
        if players.is_empty() {
            return Err(TgmRankError::NotFoundError);
        }
        Ok(players.swap_remove(0))
    }

    pub async fn get_by_name(
        db: &mut DbConnection,
        name: &str,
        search_email: bool,
    ) -> TgmRankResult<Self> {
        let player = sqlx::query_as!(
            Self,
            r#"
SELECT
    player_id,
    player_name,
    email,
    password,
    player_role as "player_role: Role",
    avatar_file,
    active,
    l.location_id as "location_id?",
    l.alpha_2 as "location?",
    created_at,
    updated_at
FROM public.account a
LEFT JOIN public.location l ON a.location_id = l.location_id
WHERE player_name ILIKE $1 OR ($2 = true AND email ILIKE $1)"#,
            name,
            search_email
        )
        .fetch_one(db)
        .await?;

        Ok(player)
    }

    pub async fn insert(
        db: &mut DbConnection,
        player: NewPlayerEntity,
    ) -> TgmRankResult<PlayerEntity> {
        let player_id = sqlx::query!(
            r#"INSERT INTO public.account
            (player_name, email, password)
        VALUES($1, $2, $3)
        RETURNING player_id"#,
            player.player_name,
            player.email,
            player.password
        )
        .fetch_one(&mut *db)
        .await?
        .player_id;

        let player = Self::get_by_id(db, player_id).await?;
        Ok(player)
    }

    pub async fn update(
        db: &mut DbConnection,
        player: PlayerEntity,
    ) -> TgmRankResult<PlayerEntity> {
        let row = sqlx::query!(
            r#"UPDATE public.account
            SET player_name = $2,
            email = $3,
            password = $4,
            avatar_file = $5,
            player_role = $6::player_role_type,
            location_id = $7
        WHERE player_id = $1
        RETURNING player_id"#,
            player.player_id,
            player.player_name,
            player.email,
            player.password,
            player.avatar_file,
            player.player_role as Role,
            player.location_id,
        )
        .fetch_one(&mut *db)
        .await?;

        let player = Self::get_by_id(db, row.player_id).await?;
        Ok(player)
    }

    pub async fn update_settings(
        db: &mut DbConnection,
        player_id: i32,
        player_settings: &PlayerSettingsEntity,
    ) -> TgmRankResult<PlayerEntity> {
        let row = sqlx::query!(
            r#"UPDATE account
            SET settings = jsonb_merge_recurse(settings, $2::JSONB)
        WHERE player_id = $1
        RETURNING player_id"#,
            player_id,
            sqlx::types::Json(player_settings) as _,
        )
        .fetch_one(&mut *db)
        .await?;

        let player = Self::get_by_id(db, row.player_id).await?;
        Ok(player)
    }

    pub async fn get_settings(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<PlayerSettingsEntity> {
        let settings = sqlx::query!(
            r#"SELECT settings AS "settings: sqlx::types::Json<PlayerSettingsEntity>" FROM account WHERE player_id = $1"#,
            player_id
        )
        .fetch_one(db)
        .await?
        .settings.0;

        Ok(settings)
    }

    pub async fn get_by_ids(
        db: &mut DbConnection,
        player_ids: &[i32],
    ) -> TgmRankResult<Vec<PlayerEntity>> {
        // TODO: Figure out why this query needed extra type annotations
        let players = sqlx::query_as!(
            Self,
            r#"
SELECT
    player_id as "player_id!",
    player_name as "player_name!",
    email,
    password as "password!",
    player_role as "player_role!: Role",
    avatar_file as "avatar_file?",
    active as "active!",
    l.location_id as "location_id?",
    l.alpha_2 as "location?",
    created_at as "created_at!",
    updated_at as "updated_at!"
FROM public.account a
LEFT JOIN public.location l USING (location_id)
WHERE player_id = ANY($1::integer[])"#,
            player_ids,
        )
        .fetch_all(db)
        .await?;

        Ok(players)
    }

    pub async fn account_history(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<Vec<Self>> {
        let history = sqlx::query_as!(
            Self,
            r#"
SELECT
    player_id as "player_id!: i32",
    player_name as "player_name!",
    email,
    password as "password!",
    player_role as "player_role!: Role",
    avatar_file,
    active as "active!",
    l.location_id as "location_id?",
    l.alpha_2 as "location?",
    created_at as "created_at!",
    updated_at as "updated_at!"
FROM public.account_with_history a
LEFT JOIN public.location l ON a.location_id = l.location_id
WHERE player_id = $1
ORDER BY updated_at DESC"#,
            player_id,
        )
        .fetch_all(db)
        .await?;

        Ok(history)
    }
}

impl AccountClaimEntity {
    pub async fn insert_claim(
        db: &mut DbConnection,
        new_claim: &AccountClaimEntity,
    ) -> TgmRankResult<Self> {
        let claim = sqlx::query_as!(
            Self,
            r#"
INSERT INTO public.account_claim (account_id, claim_key, claim_value)
VALUES ($1, $2, $3)
RETURNING account_id, claim_key, claim_value;"#,
            new_claim.account_id,
            new_claim.claim_key,
            new_claim.claim_value,
        )
        .fetch_one(db)
        .await?;

        Ok(claim)
    }

    pub async fn insert_claims(
        db: &mut DbConnection,
        new_claims: &[AccountClaimEntity],
    ) -> TgmRankResult<()> {
        for claim in new_claims {
            Self::insert_claim(db, claim).await?;
        }

        Ok(())
    }

    pub async fn get_claims(db: &mut DbConnection, account_id: i32) -> TgmRankResult<Vec<Self>> {
        let claims = sqlx::query_as!(
            Self,
            r#"
SELECT account_id, claim_key, claim_value
FROM account_claim
WHERE account_id = $1;
            "#,
            account_id
        )
        .fetch_all(db)
        .await?;

        Ok(claims)
    }
}

impl PlayerEntity2 {
    pub async fn get_all_with_scores(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let players = sqlx::query_as!(
            Self,
            r#"SELECT
    player_id as "player_id!",
    player_name as "player_name!",
    avatar_file,
    location_id,
    location
FROM public.player a
WHERE EXISTS(SELECT 1 FROM public.score_entry se WHERE public.is_ranked_score_status(se.status) AND se.player_id = a.player_id)
ORDER BY a.player_name;"#
        )
            .fetch_all(db)
            .await?;
        Ok(players)
    }
}
