use itertools::Itertools;

use crate::models::filters::GenericFilter;
use crate::models::{
    ActivityCalendarEntity, FullScoreEntity, GradeEntity, PlatformEntity, ProofEntity, ScoreStatus,
    VerifyScoreEntity,
};
use crate::models::{NewScore, PlayerEntity2, ScoreEntity, UpdateScoreEntity};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

use time::Date;

impl FullScoreEntity {
    pub async fn refresh_score_entry_history(db: &mut DbConnection) -> TgmRankResult<()> {
        sqlx::query("REFRESH MATERIALIZED VIEW score_entry_with_history;")
            .execute(db)
            .await?;

        Ok(())
    }

    pub async fn get_by_ids(
        db: &mut DbConnection,
        score_ids: &[i32],
        filter: &GenericFilter,
    ) -> TgmRankResult<Vec<Self>> {
        let scores = sqlx::query_as!(
            Self,
            r#"
SELECT
    score_id AS "score_id!",
    CASE WHEN g.grade_id IS NOT NULL
        THEN (g.grade_id, g.grade_display, g.mode_id, CAST(g.line AS VARCHAR), g.sort_order)
        ELSE NULL
        END AS "grade?: GradeEntity",
    level AS "level?",
    playtime AS "playtime?",
    score AS "score?",
    status AS "status!: ScoreStatus",
    comment AS "comment?",
    (score_account.player_id, score_account.player_name, score_account.avatar_file, score_account.location_id, score_account.location) AS "player!: PlayerEntity2",
    s.mode_id AS "mode_id!",
    s.created_at AS "created_at!: time::OffsetDateTime",
    s.updated_at AS "updated_at!: time::OffsetDateTime",
    updated_by AS "updated_by!: i32",
    CASE WHEN s.platform_id IS NOT NULL
        THEN (pl.platform_id, pl.platform_name, pl.mode_id, pl.game_id)
        ELSE NULL
    END AS "platform?: PlatformEntity",
    verification_comment,
    verified_on,
    CASE WHEN s.verified_by IS NOT NULL
        THEN (verifier_account.player_id, verifier_account.player_name, verifier_account.avatar_file, verifier_account.location_id, verifier_account.location)
        ELSE NULL
    END AS "verifier?: PlayerEntity2",
    p.proof_data AS "proof?: Vec<ProofEntity>"
FROM public.score_entry__as_of2(CASE WHEN $1::TIMESTAMPTZ IS NOT NULL THEN $1 ELSE CURRENT_TIMESTAMP END) s
JOIN public.player score_account USING (player_id)
LEFT JOIN public.player verifier_account ON verifier_account.player_id = s.verified_by
LEFT JOIN public.grade g USING (grade_id)
LEFT JOIN (
    SELECT p.score_id, array_agg((p.proof_id, p.score_id, CAST(p.proof_type AS VARCHAR), p.link) ORDER BY p.proof_id) AS proof_data
    FROM public.proof p
    WHERE p.score_id = ANY($2)
    GROUP BY p.score_id
) p USING (score_id)
LEFT JOIN public.platform pl USING (platform_id)
WHERE score_id = ANY($2)"#,
            filter.as_of,
            score_ids
        )
        .fetch_all(db)
        .await?;

        Ok(scores)
    }
}

impl ScoreEntity {
    pub async fn insert(db: &mut DbConnection, score: NewScore) -> TgmRankResult<ScoreEntity> {
        let score = sqlx::query_as!(
            Self,
            r#"INSERT INTO public.score_entry
(grade_id, "level", playtime, score, status, "comment", player_id, mode_id, updated_by, platform_id, rank_order_key)
VALUES($1, $2, $3, $4, $5::score_status, $6, $7, $8, $9, $10, (SELECT * FROM public.build_rank_order_key($8, $1, $4, $2, $3)))
RETURNING score_id, grade_id, level, playtime, score,
    status as "status: ScoreStatus", comment, player_id, mode_id, created_at, updated_at, updated_by, platform_id"#,
            score.grade_id,
            score.level,
            score.playtime,
            score.score,
            score.status as ScoreStatus,
            score.comment,
            score.player_id,
            score.mode_id,
            score.updated_by,
            score.platform_id,
        )
        .fetch_one(db)
        .await?;

        Ok(score)
    }

    pub async fn is_duplicate(db: &mut DbConnection, score: &NewScore) -> TgmRankResult<bool> {
        // TODO: Is this wise? Or should we just map the database error after attempting an insert. We would have to match on the score entry uniqueness constraint name
        let is_duplicate = sqlx::query!(
            r#"SELECT 1 as "dupe"
FROM public.score_entry se
WHERE
    se.status != 'rejected' AND
    se.mode_id = $1 AND
    se.player_id = $2 AND
    (($3::integer IS NULL AND se.grade_id IS NULL) OR se.grade_id = $3::integer) AND
    (($4::integer IS NULL AND se.level IS NULL) OR se.level = $4::integer) AND
    (($5::time(2) IS NULL AND se.playtime IS NULL) OR se.playtime = $5::time(2)) AND
    (($6::integer IS NULL AND se.score IS NULL) OR se.score = $6::integer)"#,
            &score.mode_id,
            &score.player_id,
            score.grade_id,
            score.level,
            score.playtime,
            score.score,
        )
        .fetch_optional(db)
        .await?;

        Ok(is_duplicate.is_some())
    }

    pub async fn get_player_scores_mode_ids(
        db: &mut DbConnection,
        account_id: i32,
    ) -> TgmRankResult<Vec<i32>> {
        let mode_ids = sqlx::query!(
            r#"
WITH mode_ids AS (
	SELECT DISTINCT mode_id FROM public.score_entry WHERE player_id = $1
)
SELECT m.mode_id AS "mode_id!"
FROM public.mode m
LEFT JOIN public.linked_mode lm USING (mode_id)
WHERE
	m.mode_id IN (SELECT mode_id FROM mode_ids) OR
	lm.linked_mode_id IN (SELECT mode_id FROM mode_ids);"#,
            account_id
        )
        .fetch_all(db)
        .await?
        .into_iter()
        .map(|record| record.mode_id)
        .collect_vec();

        Ok(mode_ids)
    }
}

impl ActivityCalendarEntity {
    pub async fn get_calendar(
        db: &mut DbConnection,
        from: &Date,
        to: &Date,
    ) -> TgmRankResult<Vec<Self>> {
        let activity = sqlx::query_as!(
            Self,
            r#"
SELECT
    created_at::date as "date!",
    COUNT(*) as "score_count!: i64",
    ARRAY_AGG(score_id) as "score_ids!"
FROM public.score_entry
WHERE public.is_ranked_score_status(status) AND created_at::date BETWEEN $1 AND $2
GROUP BY created_at::date"#,
            from,
            to
        )
        .fetch_all(db)
        .await?;

        Ok(activity)
    }
}

impl UpdateScoreEntity {
    pub async fn update(&self, db: &mut DbConnection) -> TgmRankResult<ScoreEntity> {
        // TODO Don't send update query at all if nothing has changed
        let score = sqlx::query_as!(
            ScoreEntity,
            r#"UPDATE public.score_entry
SET comment = COALESCE($2, comment),
    updated_by = $3,
    platform_id = $4
WHERE score_id = $1
RETURNING score_id, grade_id, level, playtime, score,
    status as "status: ScoreStatus", comment, player_id, mode_id, created_at, updated_at, updated_by, platform_id"#,
            &self.score_id,
            self.comment,
            &self.updated_by,
            self.platform_id
        )
            .fetch_one(db)
            .await?;

        Ok(score)
    }
}

impl VerifyScoreEntity<'_> {
    pub async fn update(&self, db: &mut DbConnection, score_id: i32) -> TgmRankResult<ScoreEntity> {
        let updated_score = sqlx::query_as!(
            ScoreEntity,
            r#"
UPDATE public.score_entry
SET status = $2::score_status,
    verification_comment = $3,
    verified_by = $4,
    verified_on = timezone('UTC'::text, CURRENT_TIMESTAMP),
    updated_by = $4
WHERE score_id = $1
RETURNING score_id, grade_id, level, playtime, score,
    status as "status: ScoreStatus", comment, player_id, mode_id, created_at, updated_at, updated_by, platform_id"#,
            score_id,
            self.status as &ScoreStatus,
            self.verification_comment,
            self.verified_by,
        )
            .fetch_one(db)
            .await?;

        Ok(updated_score)
    }

    pub async fn delete_vote(&self, db: &mut DbConnection, score_id: i32) -> TgmRankResult<()> {
        let _ = sqlx::query!(
            r#"DELETE FROM public.score_vote WHERE score_id = $1 AND account_id = $2"#,
            score_id,
            self.verified_by,
        )
        .execute(db)
        .await?;

        Ok(())
    }

    pub async fn upsert_vote(&self, db: &mut DbConnection, score_id: i32) -> TgmRankResult<()> {
        let _ = sqlx::query!(
            r#"
INSERT INTO public.score_vote (score_id, account_id, status, verification_comment)
VALUES ($1, $2, $3, $4)
ON CONFLICT (score_id, account_id)
DO UPDATE
SET status = EXCLUDED.status,
    verification_comment = EXCLUDED.verification_comment;"#,
            score_id,
            self.verified_by,
            self.status as &ScoreStatus,
            self.verification_comment,
        )
        .execute(db)
        .await?;

        Ok(())
    }
}
