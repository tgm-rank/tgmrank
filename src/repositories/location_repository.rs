use crate::models::LocationEntity;
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl LocationEntity {
    pub async fn get_all(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let locations = sqlx::query_as!(
            Self,
            r#"SELECT location_id, num_code, name, alpha_2, alpha_3 FROM public.location"#
        )
        .fetch_all(db)
        .await?;

        Ok(locations)
    }

    pub async fn get_by_alpha_code(db: &mut DbConnection, code: &str) -> TgmRankResult<Self> {
        let location = sqlx::query_as!(
            Self,
            r#"SELECT location_id, num_code, name, alpha_2, alpha_3 FROM public.location
WHERE alpha_2 ILIKE $1 OR alpha_3 ILIKE $1"#,
            code
        )
        .fetch_one(db)
        .await?;

        Ok(location)
    }

    pub async fn get_by_id(db: &mut DbConnection, location_id: i32) -> TgmRankResult<Self> {
        let location = sqlx::query_as!(
            Self,
            r#"SELECT location_id, num_code, name, alpha_2, alpha_3 FROM public.location
WHERE location_id = $1"#,
            location_id
        )
        .fetch_one(db)
        .await?;

        Ok(location)
    }
}
