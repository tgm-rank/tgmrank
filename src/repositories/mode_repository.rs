use itertools::Itertools;

use crate::models::{ModeEntity, ModeGroupEntity, NewModeEntity};
use crate::modules::DbConnection;
use crate::utility::{TgmRankError, TgmRankResult};

impl ModeEntity {
    pub async fn get_by_id(db: &mut DbConnection, mode_id: i32) -> TgmRankResult<Self> {
        let modes = Self::get_all(db).await?;
        let mode = modes
            .into_iter()
            .find(|m| m.mode_id == mode_id)
            .ok_or(TgmRankError::NotFoundError)?;

        Ok(mode)
    }

    pub async fn get_all(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let modes = sqlx::query_as!(
            Self,
            r#"
SELECT
    mode_id,
    game_id,
    mode_name,
    is_ranked,
    weight,
    margin,
    link,
    grade_sort,
    score_sort,
    level_sort,
    time_sort,
    sort_order,
    description,
    LOWER(submission_range) AS "submission_range_start",
    UPPER(submission_range) as "submission_range_end",
    (
        SELECT ARRAY_AGG(tag_name)
        FROM public.mode_tag
        WHERE mode_tag.mode_id = mode.mode_id
    ) AS tags
FROM public.mode
ORDER BY game_id, sort_order"#
        )
        .fetch_all(db)
        .await?;
        Ok(modes)
    }

    pub async fn get_by_ids(db: &mut DbConnection, mode_ids: &[i32]) -> TgmRankResult<Vec<Self>> {
        let modes = Self::get_all(db)
            .await?
            .into_iter()
            .filter(|mode| mode_ids.contains(&mode.mode_id))
            .collect_vec();

        Ok(modes)
    }

    pub async fn get_ranked_mode_ids(
        db: &mut DbConnection,
        mode_ids: Option<&[i32]>,
    ) -> TgmRankResult<Vec<i32>> {
        match &mode_ids {
            Some(mode_ids) if !mode_ids.is_empty() => Ok((*mode_ids).to_vec()),
            _ => Self::get_ranked_mode_ids_internal(db).await,
        }
    }

    async fn get_ranked_mode_ids_internal(db: &mut DbConnection) -> TgmRankResult<Vec<i32>> {
        let mode_ids = sqlx::query!(r#"SELECT mode_id FROM public.mode WHERE is_ranked = true"#)
            .fetch_all(db)
            .await?
            .into_iter()
            .map(|record| record.mode_id)
            .sorted()
            .collect_vec();

        Ok(mode_ids)
    }

    pub async fn insert_mode(
        db: &mut DbConnection,
        new_mode: &NewModeEntity,
    ) -> TgmRankResult<Self> {
        let modes = sqlx::query_as!(
            Self,
            r#"
INSERT INTO public.mode (game_id, mode_name, is_ranked, weight, margin, link, grade_sort, score_sort, level_sort, time_sort, sort_order, description, submission_range)
VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
RETURNING mode_id,
game_id,
mode_name,
is_ranked,
weight,
margin,
link,
grade_sort,
score_sort,
level_sort,
time_sort,
sort_order,
description,
LOWER(submission_range) AS "submission_range_start",
UPPER(submission_range) as "submission_range_end",
(
    SELECT ARRAY_AGG(tag_name)
    FROM public.mode_tag
    WHERE mode_tag.mode_id = mode.mode_id
) AS tags
"#,
            new_mode.game_id,
            new_mode.mode_name,
            new_mode.is_ranked,
            new_mode.weight,
            new_mode.margin,
            new_mode.link,
            new_mode.grade_sort,
            new_mode.score_sort,
            new_mode.level_sort,
            new_mode.time_sort,
            new_mode.sort_order,
            new_mode.description,
            new_mode.submission_range,
        )
            .fetch_one(&mut *db)
            .await?;

        if let Some(tags) = &new_mode.tags {
            Self::update_tags(db, modes.mode_id, tags).await?;
        }

        Self::add_alias(db, modes.mode_id).await?;

        Ok(modes)
    }

    pub async fn update_tags(
        db: &mut DbConnection,
        mode_id: i32,
        tags: &[String],
    ) -> TgmRankResult<()> {
        sqlx::query!(r#"DELETE FROM public.mode_tag WHERE mode_id = $1"#, mode_id)
            .execute(&mut *db)
            .await?;

        for tag in tags {
            sqlx::query!(
                r#"INSERT INTO public.mode_tag (mode_id, tag_name) VALUES ($1, $2)"#,
                mode_id,
                tag
            )
            .execute(&mut *db)
            .await?;
        }

        Ok(())
    }

    pub async fn add_alias(db: &mut DbConnection, mode_id: i32) -> TgmRankResult<()> {
        sqlx::query!(
            r#"INSERT INTO public.mode_alias (mode_id, alias) SELECT mode_id, public.slugify(mode_name) FROM mode WHERE mode_id = $1;"#,
            mode_id,
        )
        .execute(db)
        .await?;

        Ok(())
    }

    pub async fn link_modes(
        db: &mut DbConnection,
        mode_id: i32,
        linked_mode_id: i32,
    ) -> TgmRankResult<()> {
        sqlx::query!(
            r#"INSERT INTO public.linked_mode (mode_id, linked_id) VALUES ($1, $2)"#,
            mode_id,
            linked_mode_id
        )
        .execute(db)
        .await?;

        Ok(())
    }
}

impl ModeGroupEntity {
    pub async fn find_mode_group(
        db: &mut DbConnection,
        game_id: i32,
        name: &str,
    ) -> TgmRankResult<ModeGroupEntity> {
        let mode_group = sqlx::query_as!(
            Self,
            r#"
SELECT mg.mode_group_id, mg.game_id, mg.name, mg.description, mg.mode_ids
FROM public.mode_group mg
WHERE mg.game_id = $1 AND mg.name ILIKE $2;
            "#,
            game_id,
            name
        )
        .fetch_one(db)
        .await?;

        Ok(mode_group)
    }

    pub async fn get_groups_by_mode_ids(
        db: &mut DbConnection,
        mode_ids: &[i32],
    ) -> TgmRankResult<Vec<Self>> {
        let mode_groups = sqlx::query_as!(
            Self,
            r#"
SELECT mg.mode_group_id, mg.game_id, mg.name, mg.description, mg.mode_ids
FROM public.mode_group mg
WHERE $1 && mg.mode_ids;
            "#,
            mode_ids
        )
        .fetch_all(db)
        .await?;

        Ok(mode_groups)
    }
}
