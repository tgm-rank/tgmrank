use crate::models::{GameEntity, ModeEntity};
use crate::modules::DbConnection;
use crate::utility::{TgmRankError, TgmRankResult};

impl GameEntity {
    pub async fn get_all_real(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let games = sqlx::query_as!(
            Self,
            r#"
SELECT game_id, game_name, short_name, subtitle,
	COALESCE(ARRAY_AGG(
		(
		    SELECT mode_fields FROM
		    (
		        SELECT
		        m.mode_id,
		        m.game_id,
		        m.mode_name,
		        m.is_ranked,
		        m.weight,
		        m.margin,
		        m.link,
		        m.grade_sort,
		        m.score_sort,
		        m.level_sort,
		        m.time_sort,
		        m.sort_order,
		        m.description,
		        LOWER(m.submission_range) AS "submission_range_start?",
		        UPPER(m.submission_range) as "submission_range_end?",
		        mt.tags
            ) mode_fields
        )
		ORDER BY g.game_id, m.sort_order
	) FILTER (WHERE m.mode_id IS NOT NULL), '{}') AS "modes: Vec<ModeEntity>"
FROM public.game g
LEFT JOIN public.mode m USING (game_id)
LEFT JOIN (
	SELECT mode_id, array_agg(mt.tag_name) AS tags
	FROM public.mode_tag mt
	GROUP BY mode_id
) mt USING (mode_id)
GROUP BY g.game_id;
"#
        )
        .fetch_all(db)
        .await?;
        Ok(games)
    }

    pub async fn get_all(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let games_from_db = Self::get_all_real(db).await?;
        Ok(games_from_db)
    }

    pub async fn get_by_id(db: &mut DbConnection, game_id: i32) -> TgmRankResult<Self> {
        let mut games = Self::get_by_ids(db, &[game_id]).await?;

        if games.is_empty() {
            return Err(TgmRankError::NotFoundError);
        }

        Ok(games.swap_remove(0))
    }

    pub async fn get_by_ids(db: &mut DbConnection, game_ids: &[i32]) -> TgmRankResult<Vec<Self>> {
        let games = Self::get_all(db)
            .await?
            .into_iter()
            .filter(|game| game_ids.contains(&game.game_id))
            .collect();

        Ok(games)
    }
}
