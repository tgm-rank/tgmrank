use crate::guards::UserCookie;
use crate::models::{BadgeEntity, NewBadgeEntity};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl BadgeEntity {
    pub async fn get_all(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let badges = sqlx::query_as!(Self, r#"SELECT badge_id, name FROM public.badge"#)
            .fetch_all(db)
            .await?;
        Ok(badges)
    }

    pub async fn insert_badge(
        db: &mut DbConnection,
        new_badge: &NewBadgeEntity<'_>,
    ) -> TgmRankResult<Self> {
        let badge = sqlx::query_as!(
            Self,
            r#"INSERT INTO public.badge (name, updated_by) VALUES ($1, $2) RETURNING badge_id, name"#,
            new_badge.name,
            new_badge.updated_by
        )
        .fetch_one(db)
        .await?;

        Ok(badge)
    }

    pub async fn get_badges_for_player(
        db: &mut DbConnection,
        player_id: i32,
    ) -> TgmRankResult<Vec<Self>> {
        let badges = sqlx::query_as!(
            Self,
            r#"SELECT badge_id, name FROM badge b JOIN account_badge ab USING (badge_id) WHERE ab.account_id = $1 ORDER BY ab.sort_order, ab.created_at"#,
            player_id
        )
            .fetch_all(db)
            .await?;

        Ok(badges)
    }

    pub async fn add_badge_for_player(
        user: &UserCookie,
        db: &mut DbConnection,
        player_id: i32,
        badge_id: i32,
    ) -> TgmRankResult<Vec<Self>> {
        sqlx::query!(
            r#"INSERT INTO account_badge (account_id, badge_id, updated_by) VALUES ($1, $2, $3)"#,
            player_id,
            badge_id,
            user.user_id,
        )
        .execute(&mut *db)
        .await?;

        Self::get_badges_for_player(db, player_id).await
    }

    pub async fn remove_badge(
        db: &mut DbConnection,
        player_id: i32,
        badge_id: i32,
    ) -> TgmRankResult<()> {
        let _ = sqlx::query!(
            r#"DELETE FROM account_badge ab WHERE ab.account_id = $1 AND ab.badge_id = $2"#,
            player_id,
            badge_id
        )
        .execute(db)
        .await?;
        Ok(())
    }
}
