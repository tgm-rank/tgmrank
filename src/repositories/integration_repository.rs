use crate::models::{ExternalIntegrationEntity, IntegrationTarget};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl ExternalIntegrationEntity {
    pub async fn get_by_target(
        db: &mut DbConnection,
        target: IntegrationTarget,
    ) -> TgmRankResult<Vec<ExternalIntegrationEntity>> {
        let integrations = sqlx::query_as!(
            Self,
            r#"
SELECT integration_id as "integration_id?", target as "target: IntegrationTarget", player_id, external_key
FROM public.external_integration
WHERE target = $1"#,
            target as IntegrationTarget,
        )
        .fetch_all(db)
        .await?;

        Ok(integrations)
    }

    pub async fn upsert(
        db: &mut DbConnection,
        entity: ExternalIntegrationEntity,
    ) -> TgmRankResult<ExternalIntegrationEntity> {
        let integration = sqlx::query_as!(
            Self,
            r#"INSERT INTO public.external_integration (target, player_id, external_key)
            VALUES ($1, $2, $3)
            ON CONFLICT (target, player_id)
            DO UPDATE
                SET target = EXCLUDED.target,
                    external_key = EXCLUDED.external_key
            RETURNING integration_id as "integration_id?", target as "target: IntegrationTarget", player_id, external_key"#,
            entity.target as IntegrationTarget,
            entity.player_id,
            entity.external_key,
        )
        .fetch_one(db)
        .await?;

        Ok(integration)
    }
}
