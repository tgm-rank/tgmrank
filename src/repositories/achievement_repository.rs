use crate::modules::DbConnection;
use crate::{
    models::{AccountAchievementEntity, AchievementEntity, ScoreStatus},
    utility::TgmRankResult,
};

impl AchievementEntity {
    pub async fn get_all(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let achievements = sqlx::query_as!(
            Self,
            r#"SELECT achievement_id, description, game_id, mode_id, sort_order FROM public.achievement"#
        )
        .fetch_all(db)
        .await?;

        Ok(achievements)
    }
}

impl AccountAchievementEntity {
    pub async fn get_achievements_for_players(
        db: &mut DbConnection,
        player_ids: &[i32],
    ) -> TgmRankResult<Vec<Self>> {
        let achievements = sqlx::query_as!(
            Self,
            r#"
SELECT account_achievement_id, account_id, achievement_id, score_id, se.created_at AS "achieved_at?", se.status as "status?: ScoreStatus"
FROM public.account_achievement
LEFT JOIN public.score_entry se USING (score_id)
WHERE account_id = ANY($1)
ORDER BY account_id"#,
            player_ids
        )
        .fetch_all(db)
        .await?;

        Ok(achievements)
    }

    pub async fn update_for_players(
        db: &mut DbConnection,
        player_ids: &[i32],
    ) -> TgmRankResult<()> {
        sqlx::query!(
            r#"SELECT public.update_multiple_player_achievements($1);"#,
            player_ids
        )
        .execute(db)
        .await?;

        Ok(())
    }
}
