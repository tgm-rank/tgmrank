use crate::models::{PlayerEntity2, TeamEntity};
use crate::modules::DbConnection;
use crate::TgmRankResult;

impl TeamEntity {
    pub async fn get_teams_for_mode(
        db: &mut DbConnection,
        mode_id: i32,
    ) -> TgmRankResult<Vec<Self>> {
        let teams = sqlx::query_as!(
            Self,
            r#"
SELECT
    t.team_id as "team_id!",
    t.name as "name!",
    ARRAY_AGG((p.player_id, p.player_name, p.avatar_file, p.location_id, p.location)) AS "members!: Vec<PlayerEntity2>"
FROM public.team t
JOIN public.team_account ta USING (team_id)
JOIN public.player p ON ta.account_id = p.player_id
WHERE t.mode_id = $1
GROUP BY t.team_id;
            "#,
            mode_id
        )
            .fetch_all(db)
            .await?;

        Ok(teams)
    }
}
