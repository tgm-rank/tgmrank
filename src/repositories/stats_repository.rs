use crate::models::{MedalStats, SubmissionStats};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl MedalStats {
    pub async fn get_medals(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let rows = sqlx::query_as!(
            Self,
            r#"
SELECT
	r.player_id AS "player_id!",
	COUNT(*) AS "total!",
	COUNT(*) FILTER (WHERE r.ranking = 1) AS "gold_count!",
	ARRAY_AGG(r.mode_id) FILTER (WHERE r.ranking = 1) AS gold_modes,
	COUNT(*) FILTER (WHERE r.ranking = 2) AS "silver_count!",
	ARRAY_AGG(r.mode_id) FILTER (WHERE r.ranking = 2) AS silver_modes,
	COUNT(*) FILTER (WHERE r.ranking = 3) AS "bronze_count!",
	ARRAY_AGG(r.mode_id) FILTER (WHERE r.ranking = 3) AS bronze_modes
FROM public.mode_ranking(
	(
		SELECT array_agg(m.mode_id)
		FROM public.MODE m
		WHERE NOT (m.mode_id = ANY((SELECT array_agg(mt.mode_id) FROM public.mode_tag mt WHERE tag_name = 'EVENT')::integer[]))
	)
) r
WHERE r.ranking <= 3
GROUP BY r.player_id
ORDER BY
	COUNT(*) DESC,
	COUNT(*) FILTER (WHERE r.ranking = 1) DESC,
	COUNT(*) FILTER (WHERE r.ranking = 2) DESC,
	COUNT(*) FILTER (WHERE r.ranking = 3) DESC;
            "#
        ).fetch_all(db)
            .await?;

        Ok(rows)
    }
}

impl SubmissionStats {
    pub async fn get_submission_stats(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let rows = sqlx::query_as!(
            Self,
            r#"
SELECT
	a.player_id,
	ARRAY[
	    COUNT(*),
        COUNT(*) FILTER (WHERE g.game_id = 1),
        COUNT(*) FILTER (WHERE g.game_id = 2),
        COUNT(*) FILTER (WHERE g.game_id = 3)
    ] as "submissions!"
FROM public.score_entry se
JOIN public.MODE m USING (mode_id)
JOIN public.game g USING (game_id)
JOIN public.account a USING (player_id)
WHERE
	public.is_ranked_score_status(se.status) AND
	m.mode_id NOT IN (SELECT mode_id FROM public.mode_tag mt WHERE mt.tag_name = 'EVENT')
GROUP BY a.player_id
ORDER BY COUNT(*) DESC;
            "#
        )
        .fetch_all(db)
        .await?;

        Ok(rows)
    }
}
