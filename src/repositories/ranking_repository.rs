use time::OffsetDateTime;

use crate::models::filters::GameRankingFilter;
use crate::models::{FullRankingEntity, ModeEntity, PlayerEntity2, RankingAlgorithm};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl FullRankingEntity {
    pub async fn get_for_game(
        db: &mut DbConnection,
        game_ids: &[i32],
        filter: &GameRankingFilter,
    ) -> TgmRankResult<Vec<Self>> {
        let mode_ids = ModeEntity::get_ranked_mode_ids(db, filter.mode_ids.as_deref()).await?;

        let ranking = sqlx::query_as!(
            Self,
            r#"
SELECT r.rank_id as "rank_id!",
    r.game_id as "game_id!",
    r.player_id as "player_id!",
    r.score_count as "score_count!",
    r.ranking as "ranking!",
    r.ranking_points,
    (p.player_id, p.player_name, p.avatar_file, p.location_id, p.location) AS "player!: PlayerEntity2"
FROM public.game_ranking($1, $2, $9::ranking_algorithm) r
JOIN public.player p ON r.player_id = p.player_id
WHERE r.game_id = ANY($3) AND
    (($4::varchar is NULL AND $5::integer[] IS NULL) OR
         p.player_name ILIKE $4::varchar OR
         p.player_id = ANY($5::integer[])) AND
    ($8::varchar[] IS NULL OR p.location = ANY($8::varchar[]))
ORDER BY r.ranking NULLS LAST, p.player_name
LIMIT coalesce($6, 1000000)
OFFSET (coalesce($6, 1000000) * ($7 - 1))"#,
            &mode_ids,
            filter.as_of.unwrap_or_else(OffsetDateTime::now_utc),
            game_ids,
            filter.player_name,
            filter.player_ids.as_deref(),
            filter.pagination.page_size,
            filter.pagination.page,
            filter.location.locations.as_deref(),
            filter.algorithm.clone() as RankingAlgorithm
        )
        .fetch_all(db)
        .await?;

        Ok(ranking)
    }
}
