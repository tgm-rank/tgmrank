use crate::models::{NewPlayerTokenEntity, PlayerTokenEntity, UserTokenType};
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl PlayerTokenEntity {
    #[allow(dead_code)]
    async fn get_by_id(db: &mut DbConnection, token_id: i32) -> TgmRankResult<Self> {
        let token = sqlx::query_as!(
            Self,
            r#"SELECT token_id, player_id, token_type as "token_type: UserTokenType", token, expiration_date
FROM public.player_token WHERE token_id = $1"#,
            token_id
        )
        .fetch_one(db)
        .await?;

        Ok(token)
    }

    pub async fn get_by_player(
        db: &mut DbConnection,
        player_id: i32,
        token_type: &UserTokenType,
    ) -> TgmRankResult<Self> {
        let token = sqlx::query_as!(
            Self,
            r#"SELECT token_id, player_id, token_type as "token_type: UserTokenType", token, expiration_date
FROM public.player_token
WHERE player_id = $1 AND token_type = $2::user_token_type"#,
            player_id,
            token_type as &UserTokenType
        )
        .fetch_one(db)
        .await?;

        Ok(token)
    }

    pub async fn upsert(
        db: &mut DbConnection,
        token: NewPlayerTokenEntity,
    ) -> TgmRankResult<PlayerTokenEntity> {
        let token = sqlx::query_as!(
            Self,
            r#"INSERT INTO public.player_token (player_id, token_type, token, expiration_date)
VALUES ($1, $2::user_token_type, $3, $4)
ON CONFLICT (player_id, token_type)
DO UPDATE
SET token = EXCLUDED.token,
    expiration_date = EXCLUDED.expiration_date
RETURNING token_id, player_id, token_type as "token_type: UserTokenType", token, expiration_date;"#,
            token.player_id,
            token.token_type as UserTokenType,
            token.token,
            token.expiration_date
        )
        .fetch_one(db)
        .await?;

        Ok(token)
    }

    pub async fn delete(
        db: &mut DbConnection,
        token: PlayerTokenEntity,
    ) -> TgmRankResult<Vec<Self>> {
        let deleted_tokens = sqlx::query_as!(
            Self,
            r#"DELETE FROM public.player_token WHERE player_id = $1 AND token_type = $2::user_token_type
            RETURNING token_id, player_id, token_type as "token_type: UserTokenType", token, expiration_date;"#,
            token.player_id,
            token.token_type as UserTokenType
        )
        .fetch_all(db)
        .await?;

        Ok(deleted_tokens)
    }
}
