use crate::models::ApiKeyEntity;
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl ApiKeyEntity {
    pub async fn find_by_key(db: &mut DbConnection, key: &str) -> TgmRankResult<Self> {
        let api_key = sqlx::query_as!(
            Self,
            r#"SELECT api_key_id, description, key FROM public.api_key WHERE key = $1"#,
            key
        )
        .fetch_one(db)
        .await?;

        Ok(api_key)
    }
}
