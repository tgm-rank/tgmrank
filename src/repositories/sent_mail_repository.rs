use crate::models::SentMail;
use crate::modules::DbConnection;
use crate::utility::TgmRankResult;

impl SentMail {
    pub async fn insert(&self, db: &mut DbConnection) -> TgmRankResult<Self> {
        let inserted = sqlx::query_as!(
            Self,
            r#"INSERT INTO public.sent_mail (email, account_id, subject, body)
VALUES($1, $2, $3, $4)
RETURNING email, account_id, subject, body;"#,
            self.email,
            self.account_id,
            self.subject,
            self.body,
        )
        .fetch_one(db)
        .await?;

        Ok(inserted)
    }

    pub async fn fetch_all(db: &mut DbConnection) -> TgmRankResult<Vec<Self>> {
        let messages = sqlx::query_as!(
            Self,
            r#"SELECT email, account_id, subject, body FROM public.sent_mail;"#
        )
        .fetch_all(db)
        .await?;

        Ok(messages)
    }
}
