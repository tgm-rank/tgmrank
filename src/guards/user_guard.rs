use actix_identity::Identity;
use uuid::Uuid;

use crate::models::Role;
use crate::utility::{AuthenticationError, TgmRankError, TgmRankResult};
use std::str::FromStr;

pub static USER_COOKIE_ID: &str = "tgmrank_user";

#[derive(Clone, Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct UserCookie {
    pub user_id: i32,
    pub user_role: Role,
    pub session_key: Uuid,
}

impl FromStr for UserCookie {
    type Err = TgmRankError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let c = serde_json::from_str(s)?;
        Ok(c)
    }
}

impl UserCookie {
    pub fn from_identity(id: &Identity) -> TgmRankResult<Self> {
        let user_cookie = UserCookie::from_str(
            &id.id()
                .or_else(|_| Err(AuthenticationError::UnauthorizedForOperation))?,
        )
        .map_err(|_e| AuthenticationError::UnauthorizedForOperation)?;

        Ok(user_cookie)
    }
}

impl ToString for UserCookie {
    fn to_string(&self) -> String {
        serde_json::to_string(self).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use serde_json::json;
    use uuid::Uuid;

    use crate::guards::user_guard::UserCookie;
    use crate::models::Role;
    use std::str::FromStr;

    #[test]
    fn user_cookie_from_cookie_string_should_return_err_when_comprised_of_too_few_parts() {
        let cookie_str = json!({
            "user_id": 3,
        })
        .to_string();
        let user_cookie = UserCookie::from_str(&cookie_str);

        assert_eq!(user_cookie.is_err(), true);
    }

    #[test]
    fn user_cookie_from_cookie_string_should_return_none_when_role_part_is_invalid() {
        let cookie_str = json!({
            "user_id": 3,
            "role": "Some unknown role",
            "session_hash": "my session hash",
        })
        .to_string();
        let user_cookie = UserCookie::from_str(&cookie_str);

        assert_eq!(user_cookie.is_err(), true);
    }

    #[test]
    fn user_cookie_from_cookie_string_should_build_user_cookie_from_name_and_role() {
        let original_cookie = UserCookie {
            user_id: 123,
            user_role: Role::Admin,
            session_key: Uuid::new_v4(),
        };

        let cookie = UserCookie::from_str(&original_cookie.to_string());

        assert_eq!(cookie.is_ok(), true);

        let cookie = cookie.unwrap();
        assert_eq!(cookie.user_id, original_cookie.user_id);
        assert_eq!(cookie.user_role, original_cookie.user_role);
        assert_eq!(cookie.session_key, original_cookie.session_key);
    }
}
