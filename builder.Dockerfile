FROM clux/muslrust:nightly-2024-11-05 AS builder

WORKDIR /volume/tgmrank

COPY . .

RUN apt-get update && apt-get install -y clang-3.9
RUN cargo build --release
RUN cargo test --release --lib
