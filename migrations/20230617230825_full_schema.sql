do $outer$ begin
if not exists (
    select 1
    from information_schema.tables
    where table_schema = 'public'
    and table_name = 'score_entry')
then

--
-- PostgreSQL database dump
--

-- Dumped from database version 11.15
-- Dumped by pg_dump version 13.3

-- Started on 2023-06-17 16:44:32

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
-- PERFORM pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

create schema if not exists public;


--
-- TOC entry 3762 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 852 (class 1247 OID 16395)
-- Name: grade_line; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.grade_line AS ENUM (
    'green',
    'orange'
);


--
-- TOC entry 855 (class 1247 OID 16400)
-- Name: integration_target; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.integration_target AS ENUM (
    'discord'
);


--
-- TOC entry 858 (class 1247 OID 16404)
-- Name: player_role_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.player_role_type AS ENUM (
    'banned',
    'user',
    'verifier',
    'admin'
);


--
-- TOC entry 861 (class 1247 OID 16414)
-- Name: proof_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.proof_type AS ENUM (
    'image',
    'video',
    'other'
);


--
-- TOC entry 1004 (class 1247 OID 17886)
-- Name: ranking_algorithm; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.ranking_algorithm AS ENUM (
    'naive',
    'percentile'
);


--
-- TOC entry 864 (class 1247 OID 16422)
-- Name: ranking_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.ranking_type AS ENUM (
    'overall',
    'game',
    'mode'
);


--
-- TOC entry 959 (class 1247 OID 17610)
-- Name: recent_activity_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.recent_activity_type AS ENUM (
    'mode',
    'game',
    'overall'
);


--
-- TOC entry 1031 (class 1247 OID 18017)
-- Name: score_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.score_status AS ENUM (
    'unverified',
    'pending',
    'legacy',
    'rejected',
    'verified',
    'accepted'
);


--
-- TOC entry 867 (class 1247 OID 16442)
-- Name: user_token_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.user_token_type AS ENUM (
    'activation',
    'forgot_password'
);


--
-- TOC entry 461 (class 1255 OID 18128)
-- Name: build_rank_order_key(integer, integer, integer, integer, time without time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.build_rank_order_key(mode_id_param integer, grade_id_param integer, score integer, level integer, playtime time without time zone) RETURNS character varying
    LANGUAGE sql
    AS $$
    SELECT
        CASE
            WHEN mo.grade_sort = 1 THEN (SELECT LPAD(CAST(gr.sort_order AS VARCHAR), 10, '0') FROM grade gr WHERE gr.grade_id = grade_id_param)
            WHEN mo.grade_sort = -1 THEN (SELECT LPAD(CAST(1000000000 - gr.sort_order AS VARCHAR), 10, '0') FROM grade gr WHERE gr.grade_id = grade_id_param)
            ELSE '0'
        END
        || '-' ||
        CASE
            WHEN mo.score_sort = 1 THEN LPAD(CAST(score AS VARCHAR), 10, '0')
            WHEN mo.score_sort = -1 THEN LPAD(CAST(1000000000 - score AS VARCHAR), 10, '0')
            ELSE '0'
        END
        || '-' ||
        CASE
            WHEN mo.level_sort = 1 THEN LPAD(CAST(level AS VARCHAR), 10, '0')
            WHEN mo.level_sort = -1 THEN LPAD(CAST(1000000000 - level AS VARCHAR), 10, '0')
            ELSE '0'
        END
        || '-' ||
        CASE
            WHEN mo.time_sort = 1 THEN LPAD(CAST(ROUND(EXTRACT(EPOCH FROM playtime::interval) * 1000) AS VARCHAR), 10, '0')
            WHEN mo.time_sort = -1 THEN LPAD(CAST(ROUND(10000000000 - EXTRACT(EPOCH FROM playtime::interval) * 1000) AS VARCHAR), 10, '0')
            ELSE '0'
        END
    FROM mode mo
    WHERE mo.mode_id = mode_id_param
$$;


--
-- TOC entry 462 (class 1255 OID 18129)
-- Name: build_rank_order_key_batch(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.build_rank_order_key_batch() RETURNS void
    LANGUAGE sql
    AS $$
    UPDATE score_entry se
    SET rank_order_key = (SELECT * FROM build_rank_order_key(se.mode_id, se.grade_id, se.score, se.level, se.playtime));
$$;


--
-- TOC entry 464 (class 1255 OID 17689)
-- Name: cache_recent_activity(integer[], integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.cache_recent_activity(modes integer[], scores integer[]) RETURNS void
    LANGUAGE plpgsql
    SET join_collapse_limit TO '1'
    AS $$
DECLARE
    uncached_scores INTEGER[];
BEGIN
    SELECT array_agg(s.score_id)
    INTO uncached_scores
    FROM (SELECT UNNEST(scores) AS score_id) s
    LEFT JOIN (SELECT DISTINCT score_id FROM recent_activity r WHERE r.mode_ids = modes) r
        USING (score_id)
    WHERE r.score_id IS NULL;

    IF uncached_scores IS NOT NULL
    THEN
        INSERT INTO recent_activity
        (activity_type, mode_ids, game_or_mode_id, score_id, ranking, ranking_points, prev_score_id, prev_ranking, prev_ranking_points, passed_player_ids)
        WITH mode_ranking AS (
            SELECT se.score_id AS base_score_id, r.*
            FROM score_entry se
            JOIN LATERAL (
                SELECT player_id,
                    rcurrent.score_id AS current_score_id,
                    rcurrent.ranking AS current_ranking,
                    rcurrent.ranking_points AS current_ranking_points,
                    rprev.score_id AS prev_score_id,
                    rprev.ranking AS prev_ranking,
                    rprev.ranking_points AS prev_ranking_points
                FROM calculate_ranking(array[se.mode_id], se.created_at) rcurrent
                LEFT JOIN calculate_ranking(array[se.mode_id], se.created_at - interval '0.000001') rprev
                    USING (player_id)
            ) r ON TRUE
            WHERE se.score_id = ANY(uncached_scores)
        ),
        game_ranking AS (
            SELECT se.score_id AS base_score_id, r.*
            FROM score_entry se
            JOIN mode m USING (mode_id)
            JOIN game g USING (game_id)
            JOIN LATERAL (
                SELECT rcurrent.player_id,
                    rcurrent.ranking AS current_ranking,
                    rcurrent.ranking_points AS current_ranking_points,
                    rprev.ranking AS prev_ranking,
                    rprev.ranking_points AS prev_ranking_points
                FROM (SELECT * FROM calculate_aggregate_ranking(modes, se.created_at) ORDER BY player_id) rcurrent
                LEFT JOIN (SELECT * FROM calculate_aggregate_ranking(modes, se.created_at - interval '0.000001') ORDER BY player_id) rprev
                    ON rcurrent.player_id = rprev.player_id AND rprev.game_id = g.game_id
                WHERE rcurrent.game_id = g.game_id
            ) r ON TRUE
            WHERE se.score_id =  ANY(uncached_scores)
        ),
        overall_ranking AS (
            SELECT se.score_id AS base_score_id, r.*
            FROM score_entry se
            JOIN LATERAL (
                SELECT rcurrent.player_id,
                    rcurrent.ranking AS current_ranking,
                    rcurrent.ranking_points AS current_ranking_points,
                    rprev.ranking AS prev_ranking,
                    rprev.ranking_points AS prev_ranking_points
                FROM (SELECT * FROM calculate_aggregate_ranking(modes, se.created_at) WHERE game_id = 0 ORDER BY player_id) rcurrent
                LEFT JOIN (SELECT * FROM calculate_aggregate_ranking(modes, se.created_at - interval '0.000001') WHERE game_id = 0 ORDER BY player_id) rprev
                    ON rcurrent.player_id = rprev.player_id AND rprev.game_id = 0
                WHERE rcurrent.game_id = 0
            ) r ON TRUE
            WHERE se.score_id =  ANY(uncached_scores)
        )
        SELECT
            'mode'::recent_activity_type AS activity_type,
            modes AS mode_ids,
            se.mode_id AS game_or_mode_id,
            se.score_id AS score_id,
            mr.current_ranking AS ranking,
            mr.current_ranking_points AS ranking_points,
            mr.prev_score_id AS prev_score_id,
            mr.prev_ranking AS prev_ranking,
            mr.prev_ranking_points AS prev_ranking_points,
            passed_players.passed_player_ids
        FROM score_entry se
        LEFT JOIN LATERAL (
            SELECT mr.current_ranking, mr.current_ranking_points, mr.prev_score_id, mr.prev_ranking, mr.prev_ranking_points
            FROM mode_ranking mr
            WHERE se.score_id = mr.base_score_id
                AND se.score_id = mr.current_score_id
        ) mr ON TRUE
        JOIN LATERAL (
            SELECT
                COALESCE(array_remove(array_agg(mri.player_id ORDER BY mri.prev_ranking NULLS LAST), null), ARRAY[]::INTEGER[]) AS passed_player_ids
            FROM mode_ranking mri
            WHERE mri.prev_ranking - mri.current_ranking < 0
                AND mri.player_id != se.player_id
                AND se.score_id = mri.base_score_id -- Make sure we're only looking at the leaderboard for this score
        ) passed_players ON TRUE
        where se.score_id = ANY(uncached_scores)
        UNION ALL
        SELECT
            'game'::recent_activity_type AS activity_type,
            modes AS mode_ids,
            g.game_id as "id!",
            se.score_id as "current_score_id!",
            gr.current_ranking as ranking,
            gr.current_ranking_points as ranking_points,
            NULL as prev_score_id,
            gr.prev_ranking as prev_ranking,
            gr.prev_ranking_points as prev_ranking_points,
            passed_players.passed_player_ids
        from score_entry se
        join mode m on se.mode_id = m.mode_id
        join game g on m.game_id = g.game_id
        LEFT JOIN LATERAL (
            SELECT gri.current_ranking, gri.current_ranking_points, gri.prev_ranking, gri.prev_ranking_points
            FROM game_ranking gri
            WHERE se.score_id = gri.base_score_id
                AND se.player_id = gri.player_id
        ) gr ON TRUE
        JOIN LATERAL (
            SELECT
                COALESCE(array_remove(array_agg(gri.player_id order by gri.prev_ranking nulls last), null), ARRAY[]::INTEGER[]) AS passed_player_ids
            FROM game_ranking gri
            WHERE se.score_id = gri.base_score_id -- Make sure we're only looking at the leaderboard for this score
                AND gri.current_ranking > (SELECT current_ranking FROM game_ranking WHERE se.score_id = base_score_id AND se.player_id = player_id)
                AND gri.prev_ranking <= COALESCE((SELECT prev_ranking FROM game_ranking WHERE se.score_id = base_score_id AND se.player_id = player_id), 10000000)
                AND se.player_id != gri.player_id
        ) passed_players ON TRUE
        WHERE se.score_id = ANY(uncached_scores)
        UNION ALL
        SELECT
            'overall'::recent_activity_type AS activity_type,
            modes AS mode_ids,
            0 as "id!",
            se.score_id as "current_score_id!",
            gr.current_ranking as ranking,
            gr.current_ranking_points as ranking_points,
            NULL as prev_score_id,
            gr.prev_ranking as prev_ranking,
            gr.prev_ranking_points as prev_ranking_points,
            passed_players.passed_player_ids
        FROM score_entry se
        LEFT JOIN LATERAL (
            SELECT gri.current_ranking, gri.current_ranking_points, gri.prev_ranking, gri.prev_ranking_points
            FROM overall_ranking gri
            WHERE se.score_id = gri.base_score_id
                AND se.player_id = gri.player_id
        ) gr ON TRUE
        JOIN LATERAL (
            SELECT
                COALESCE(array_remove(array_agg(gri.player_id order by gri.prev_ranking nulls last), null), ARRAY[]::INTEGER[]) AS passed_player_ids
            FROM overall_ranking gri
            WHERE se.score_id = gri.base_score_id -- Make sure we're only looking at the leaderboard for this score
                AND gri.current_ranking > (SELECT current_ranking FROM overall_ranking WHERE se.score_id = base_score_id AND se.player_id = player_id)
                AND gri.prev_ranking <= COALESCE((SELECT prev_ranking FROM overall_ranking WHERE se.score_id = base_score_id AND se.player_id = player_id), 10000000)
                AND se.player_id != gri.player_id
        ) passed_players ON TRUE
        WHERE se.score_id = ANY(uncached_scores)
        ON CONFLICT (activity_type, score_id, mode_ids)
        DO NOTHING;
    END IF;
END;
$$;


--
-- TOC entry 467 (class 1255 OID 17892)
-- Name: calculate_aggregate_ranking(integer[], timestamp with time zone, public.ranking_algorithm); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.calculate_aggregate_ranking(modes integer[], datetime timestamp with time zone DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP), algorithm public.ranking_algorithm DEFAULT 'naive'::public.ranking_algorithm) RETURNS TABLE(rank_id integer, game_id integer, player_id integer, score_count integer, ranking integer, ranking_points integer)
    LANGUAGE sql STABLE
    AS $$
        WITH ranked_scores AS (SELECT
                                   CASE WHEN  m.mode_id = ANY (modes)
                                       THEN r.ranking_points
                                   ELSE NULL
                                   END AS ranking_points,
                                   m.game_id,
                                   p.player_id,
                                   p.player_name
                               FROM calculate_ranking(modes, datetime, algorithm) r
            JOIN mode m ON r.mode_id = m.mode_id
            JOIN player p ON r.player_id = p.player_id)
        SELECT
            CAST(row_number() OVER(ORDER BY r.game_id, r.ranking_points DESC NULLS LAST, r.player_name) AS INT) AS rank_id,
            r.game_id,
            r.player_id,
            cast(r.score_count as int),
            cast(r.ranking as int),
            cast(r.ranking_points as int)
        FROM
        (SELECT
            r.game_id,
            r.player_id,
            r.player_name,
            COUNT(*) as score_count,
            RANK()
                 OVER (
                     PARTITION BY r.game_id
                     ORDER BY SUM(r.ranking_points) DESC NULLS LAST ) AS ranking,
            SUM(r.ranking_points) AS ranking_points
        FROM ranked_scores r
        GROUP BY r.game_id, r.player_name, r.player_id
        UNION ALL
        SELECT
            0 AS game_id,
            r.player_id,
            r.player_name,
            COUNT(*) as score_count,
            RANK()
                 OVER (
                     ORDER BY SUM(r.ranking_points) DESC NULLS LAST ) AS ranking,
            SUM(r.ranking_points) AS ranking_points
        FROM ranked_scores r
        GROUP BY r.player_name, r.player_id) r;
    $$;


--
-- TOC entry 459 (class 1255 OID 17891)
-- Name: calculate_ranking(integer[], timestamp with time zone, public.ranking_algorithm); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.calculate_ranking(modes integer[] DEFAULT NULL::integer[], datetime timestamp with time zone DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP), algorithm public.ranking_algorithm DEFAULT 'naive'::public.ranking_algorithm) RETURNS TABLE(score_id integer, player_id integer, mode_id integer, ranking_points integer, ranking integer)
    LANGUAGE sql STABLE
    AS $$
	WITH player_best_scores AS (
		SELECT
		    mo.mode_id,
		    mo.weight,
		    mo.margin,
		    a.score_id,
		    a.player_id,
		    a.rank_order_key
		FROM mode mo
		LEFT JOIN linked_mode lm USING (mode_id)
		JOIN LATERAL (
			SELECT DISTINCT ON (mo.mode_id, sii.player_id)
			    sii.score_id,
			    sii.player_id,
			    sii.rank_order_key
			FROM score_entry__as_of2(datetime) sii
			WHERE
				(sii.mode_id = mo.mode_id OR sii.mode_id = lm.linked_id) AND
				is_ranked_score_status(sii.status)
			ORDER BY
			    mo.mode_id,
			    sii.player_id,
			    sii.rank_order_key
		) a ON TRUE
		WHERE (modes IS NULL OR (mo.mode_id = ANY(modes)))
	)
	SELECT
	    score_id,
	    player_id,
	    mode_id,
	    CASE
	        WHEN algorithm = 'naive'::ranking_algorithm THEN
	            calculate_ranking_points(weight, margin, CAST(ranking AS INT))
	        WHEN algorithm = 'percentile'::ranking_algorithm THEN
	            CAST(PERCENT_RANK() OVER (PARTITION BY mode_id ORDER BY ranking DESC) * 10000 AS INT)
	    END as ranking_points,
	    ranking
	FROM (
	    SELECT
	        sii.score_id,
	        p.player_id,
	        p.player_name,
	        mo.mode_id,
	        mo.weight,
	        mo.margin,
	        CAST(
	        	RANK()
	        	OVER (
				    PARTITION BY mo.mode_id
				    ORDER BY sii.rank_order_key
				)
			AS INT) AS ranking
	    FROM mode mo
		LEFT JOIN player_best_scores sii ON sii.mode_id = mo.mode_id
	    JOIN player p USING (player_id)
	    WHERE (modes IS NULL OR mo.mode_id = ANY(modes))
	) b -- Add calculated ranking
	ORDER BY mode_id, ranking, player_name;
$$;


--
-- TOC entry 453 (class 1255 OID 17463)
-- Name: calculate_ranking_points(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.calculate_ranking_points(weight integer, margin integer, rank integer) RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $$
        SELECT
            GREATEST(
                CASE
                    WHEN rank = 1 THEN weight
                    WHEN rank = 2 THEN weight - 3
                    WHEN rank = 3 THEN weight - 5
                    ELSE weight - 6 + margin - margin * (rank - 3)
                END,
            0);
    $$;


--
-- TOC entry 465 (class 1255 OID 17686)
-- Name: insert_achievement(integer, integer, integer, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.insert_achievement(achievement_id_in integer, player_id_in integer, mode_id_in integer, condition_sql character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    EXECUTE
           'INSERT INTO account_achievement (account_id, achievement_id, score_id)'
           'SELECT ' || player_id_in || ', ' || achievement_id_in || ','
                   '(SELECT score_id FROM score_entry se LEFT JOIN grade g USING (grade_id) '
                   'WHERE is_ranked_score_status(se.status) AND se.player_id = ' || player_id_in || ' AND se.mode_id = ' || mode_id_in || ' AND ' || condition_sql || ' ORDER BY created_at, score_id LIMIT 1)'
           'ON CONFLICT (account_id, achievement_id)'
           'DO UPDATE SET score_id = EXCLUDED.score_id;';
END;
$$;


--
-- TOC entry 456 (class 1255 OID 18078)
-- Name: is_ranked_score_status(public.score_status); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.is_ranked_score_status(status public.score_status) RETURNS boolean
    LANGUAGE sql IMMUTABLE
    AS $$
    SELECT status != 'rejected'::score_status;
$$;


--
-- TOC entry 466 (class 1255 OID 17884)
-- Name: jsonb_merge_recurse(jsonb, jsonb); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.jsonb_merge_recurse(orig jsonb, delta jsonb) RETURNS jsonb
    LANGUAGE sql
    AS $$
    SELECT
        jsonb_object_agg(
            coalesce(keyOrig, keyDelta),
            CASE
                WHEN valOrig isnull THEN valDelta
                WHEN valDelta isnull THEN valOrig
                WHEN (jsonb_typeof(valOrig) <> 'object' OR jsonb_typeof(valDelta) <> 'object') THEN valDelta
                ELSE jsonb_merge_recurse(valOrig, valDelta)
            END
        )
    FROM jsonb_each(orig) e1(keyOrig, valOrig)
    FULL JOIN jsonb_each(delta) e2(keyDelta, valDelta) ON keyOrig = keyDelta
$$;


--
-- TOC entry 455 (class 1255 OID 17467)
-- Name: manage_updated_at(regclass); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.manage_updated_at(_tbl regclass) RETURNS void
    LANGUAGE plpgsql
    AS $$
    BEGIN
        EXECUTE format('CREATE TRIGGER set_updated_at BEFORE UPDATE ON %s
                        FOR EACH ROW EXECUTE PROCEDURE set_update_at()', _tbl);
    END;
    $$;


SET default_tablespace = '';

--
-- TOC entry 208 (class 1259 OID 16512)
-- Name: score_entry; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.score_entry (
    score_id integer NOT NULL,
    grade_id integer,
    level integer,
    playtime time(2) without time zone,
    score integer,
    status public.score_status NOT NULL,
    comment character varying,
    player_id integer NOT NULL,
    mode_id integer NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_by integer NOT NULL,
    system_time_start timestamp with time zone DEFAULT transaction_timestamp() NOT NULL,
    system_time_end timestamp with time zone DEFAULT 'infinity'::timestamp with time zone NOT NULL,
    platform_id integer,
    is_announced boolean DEFAULT true NOT NULL,
    verification_comment character varying,
    verified_by integer,
    verified_on timestamp with time zone,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL,
    rank_order_key character varying,
    CONSTRAINT score_entry_system_time_check CHECK ((system_time_start < system_time_end))
);


--
-- TOC entry 209 (class 1259 OID 16527)
-- Name: score_entry_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.score_entry_history (
    score_id integer NOT NULL,
    grade_id integer,
    level integer,
    playtime time(2) without time zone,
    score integer,
    status public.score_status NOT NULL,
    comment character varying,
    player_id integer NOT NULL,
    mode_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    updated_by integer,
    system_time_start timestamp with time zone,
    system_time_end timestamp with time zone,
    platform_id integer,
    is_announced boolean DEFAULT true NOT NULL,
    verification_comment character varying,
    verified_by integer,
    verified_on timestamp with time zone,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL,
    rank_order_key character varying
);


--
-- TOC entry 255 (class 1259 OID 18136)
-- Name: score_entry_with_history; Type: MATERIALIZED VIEW; Schema: public; Owner: -
--

CREATE MATERIALIZED VIEW public.score_entry_with_history AS
 SELECT score_entry.score_id,
    score_entry.grade_id,
    score_entry.level,
    score_entry.playtime,
    score_entry.score,
    score_entry.status,
    score_entry.comment,
    score_entry.player_id,
    score_entry.mode_id,
    score_entry.created_at,
    score_entry.updated_at,
    score_entry.updated_by,
    score_entry.platform_id,
    score_entry.is_announced,
    score_entry.verification_comment,
    score_entry.verified_by,
    score_entry.verified_on,
    score_entry.sys_period,
    score_entry.rank_order_key
   FROM public.score_entry
UNION ALL
 SELECT score_entry_history.score_id,
    score_entry_history.grade_id,
    score_entry_history.level,
    score_entry_history.playtime,
    score_entry_history.score,
    score_entry_history.status,
    score_entry_history.comment,
    score_entry_history.player_id,
    score_entry_history.mode_id,
    score_entry_history.created_at,
    score_entry_history.updated_at,
    score_entry_history.updated_by,
    score_entry_history.platform_id,
    score_entry_history.is_announced,
    score_entry_history.verification_comment,
    score_entry_history.verified_by,
    score_entry_history.verified_on,
    score_entry_history.sys_period,
    score_entry_history.rank_order_key
   FROM public.score_entry_history
  WITH NO DATA;


--
-- TOC entry 460 (class 1255 OID 18145)
-- Name: score_entry__as_of2(timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.score_entry__as_of2(timestamp with time zone) RETURNS SETOF public.score_entry_with_history
    LANGUAGE sql STABLE
    AS $_$
    SELECT * FROM public.score_entry_with_history WHERE COALESCE(LOWER(sys_period), '-Infinity') <= $1 AND COALESCE(UPPER(sys_period), 'Infinity') > $1
$_$;


--
-- TOC entry 454 (class 1255 OID 17466)
-- Name: set_update_at(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.set_update_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        IF (
            NEW IS DISTINCT FROM OLD AND
            NEW.updated_at IS NOT DISTINCT FROM OLD.updated_at
        ) THEN
            NEW.updated_at := current_timestamp;
        END IF;
        RETURN NEW;
    END;
    $$;


--
-- TOC entry 463 (class 1255 OID 17688)
-- Name: update_multiple_player_achievements(integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_multiple_player_achievements(player_ids integer[]) RETURNS void
    LANGUAGE sql
    AS $$
    SELECT update_player_achievements(a.player_id)
    FROM (SELECT UNNEST(player_ids) AS player_id) a;
$$;


--
-- TOC entry 457 (class 1255 OID 17687)
-- Name: update_player_achievements(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.update_player_achievements(player_id_in integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Probably good enough, we can replace this if it truly becomes unmaintainable

    -- Get an S1 grade in TGM1 Master Mode
    PERFORM public.insert_achievement(01, player_id_in, 1, 'g.sort_order >= (SELECT sort_order FROM grade WHERE mode_id = 1 AND grade_display = ''S1'')');

    -- Reach level 300 in TGM1 Master Mode
    PERFORM public.insert_achievement(02, player_id_in, 1, 'se.level >= 300');

    -- Get more than 150,000 points in TAP Normal mode
    PERFORM public.insert_achievement(03, player_id_in, 12, 'se.score > 150000');

    -- Reach level 500 in TGM1 Master Mode
    PERFORM public.insert_achievement(04, player_id_in, 1, 'se.level >= 500');

    -- Reach level 150 in TAP Death
    PERFORM public.insert_achievement(05, player_id_in, 7, 'se.level >= 150');

    -- Get an S1 grade in TAP Master Mode
    PERFORM public.insert_achievement(06, player_id_in, 6, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 6 AND grade_display = ''S1'')');

    -- Reach level 200 in TAP Death
    PERFORM public.insert_achievement(07, player_id_in, 7, 'se.level >= 200');

    -- Reach level 700 in TGM1 Master
    PERFORM public.insert_achievement(08, player_id_in, 1, 'se.level >= 700');

    -- Reach level 500 in TGM1 20G Mode
    PERFORM public.insert_achievement(09, player_id_in, 2, 'se.level >= 500');

    -- Get an S7 grade in TGM1 Master
    PERFORM public.insert_achievement(10, player_id_in, 1, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 1 AND grade_display = ''S7'')');

    -- Reach level 300 in TAP Death
    PERFORM public.insert_achievement(11, player_id_in, 7, 'se.level >= 300');

    -- Reach level 999 in TGM Master
    PERFORM public.insert_achievement(12, player_id_in, 1, 'se.level = 999');

    -- Complete the game with the GM grade in TGM1 Master
    PERFORM public.insert_achievement(13, player_id_in, 1, 'se.grade_id = (SELECT grade_id FROM grade WHERE mode_id = 1 AND grade_display = ''GM'')');

    -- Get an S7 grade in TAP Master Mode
    PERFORM public.insert_achievement(14, player_id_in, 6, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 6 AND grade_display = ''S7'')');

    -- Complete the game with the GM grade in TGM1 20G
    PERFORM public.insert_achievement(15, player_id_in, 2, 'se.grade_id = (SELECT grade_id FROM grade WHERE mode_id = 2 AND grade_display = ''GM'')');

    -- Reach level 400 in TAP Death
    PERFORM public.insert_achievement(16, player_id_in, 7, 'se.level >= 400');

    -- Reach level 999 in TAP Master Mode
    PERFORM public.insert_achievement(17, player_id_in, 6, 'se.level = 999');

    -- Clear the fading credits roll in TAP Master Mode (or enter the invisible credits roll)
    PERFORM public.insert_achievement(18, player_id_in, 6,
            '(g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 6 AND grade_display = ''M'') OR '
            ' se.grade_id IN (SELECT grade_id FROM grade WHERE mode_id = 6 AND grade_display != ''M'' AND grade_display != ''GM'' AND line = ''orange''))');

    -- Get an S9 grade in TAP Master Mode
    PERFORM public.insert_achievement(19, player_id_in, 6, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 6 AND grade_display = ''S9'')');

    -- Reach level 500 in TAP Death
    PERFORM public.insert_achievement(20, player_id_in, 7, 'se.level >= 500');

    -- Get the M grade in TAP Death
    PERFORM public.insert_achievement(21, player_id_in, 7, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 7 AND grade_display = ''M'')');

    -- Reach level 700 in TAP Death
    PERFORM public.insert_achievement(22, player_id_in, 7, 'se.level >= 700');

    -- Get the GM grade in TAP Death
    PERFORM public.insert_achievement(23, player_id_in, 7, 'se.grade_id = (SELECT grade_id FROM grade WHERE mode_id = 7 AND grade_display = ''GM'')');

    -- Survive the credit roll and get GM in TAP Master Mode
    PERFORM public.insert_achievement(24, player_id_in, 6, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 6 AND grade_display = ''GM'')');

    -- Survive the credit roll and get Orange Line GM in TAP Master Mode
    PERFORM public.insert_achievement(25, player_id_in, 6, 'se.grade_id = (SELECT grade_id FROM grade WHERE mode_id = 6 AND grade_display = ''GM'' AND line = ''orange'')');

    -- Get the M grade (or better) in TAP Master
    PERFORM public.insert_achievement(26, player_id_in, 6, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 6 AND grade_display = ''M'')');

    -- Get 400 or more Hanabi in TI Easy
    PERFORM public.insert_achievement(27, player_id_in, 17, 'se.score >= 400');

    -- Become Qualified 3 in TI Master
    PERFORM public.insert_achievement(28, player_id_in, 30, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 30 AND grade_display = ''3'')');

    -- Reach level 500 in TI Master
    PERFORM public.insert_achievement(29, player_id_in, 15, 'se.level >= 500');

    -- Become Qualified S1 in TI Master
    PERFORM public.insert_achievement(30, player_id_in, 30, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 30 AND grade_display = ''S1'')');

    -- Reach level 700 in TI Master
    PERFORM public.insert_achievement(31, player_id_in, 15, 'se.level >= 700');

    -- Become Qualified S5 in TI Master
    PERFORM public.insert_achievement(32, player_id_in, 30, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 30 AND grade_display = ''S5'')');

    -- Clear TI Master mode by reaching level 999
    PERFORM public.insert_achievement(33, player_id_in, 15, 'se.level >= 999');

    -- Become Qualified m1 in TI Master
    PERFORM public.insert_achievement(34, player_id_in, 30, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 30 AND grade_display = ''m1'')');

    -- Become Qualified m5 in TI Master
    PERFORM public.insert_achievement(35, player_id_in, 30, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 30 AND grade_display = ''m5'')');

    -- Become Qualified Master in TI Master
    PERFORM public.insert_achievement(36, player_id_in, 30, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 30 AND grade_display = ''M'')');

    -- Get the MasterM grade in TI Master
    PERFORM public.insert_achievement(37, player_id_in, 15, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 15 AND grade_display = ''MM'')');

    -- Become Qualified MasterM in TI Master
    PERFORM public.insert_achievement(38, player_id_in, 30, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 30 AND grade_display = ''MM'')');

    -- Become Qualified Grand Master in TI Master
    PERFORM public.insert_achievement(39, player_id_in, 30, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 30 AND grade_display = ''GM'')');

    -- Clear TI Master with an Orange Line GM grade
    PERFORM public.insert_achievement(40, player_id_in, 15, 'se.grade_id = (SELECT grade_id FROM grade WHERE mode_id = 15 AND grade_display = ''GM'')');

    -- Complete the 20 regular stages of Sakura mode
    PERFORM public.insert_achievement(41, player_id_in, 18, 'g.sort_order > (SELECT MIN(sort_order) FROM grade WHERE mode_id = 18 AND grade_display = ''20'')');

    -- Complete ALL stages of Sakura mode
    PERFORM public.insert_achievement(42, player_id_in, 18, 'se.grade_id = (SELECT grade_id FROM grade WHERE mode_id = 18 AND grade_display = ''ALL'')');

    -- Get an S1 grade in TI Shirase
    PERFORM public.insert_achievement(43, player_id_in, 16, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 16 AND grade_display = ''S1'')');

    -- Get an S3 grade in TI Shirase
    PERFORM public.insert_achievement(44, player_id_in, 16, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 16 AND grade_display = ''S3'')');

    -- Get an S5 grade in TI Shirase
    PERFORM public.insert_achievement(45, player_id_in, 16, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 16 AND grade_display = ''S5'')');

    -- Break the TI Shirase Torikan at level 500
    PERFORM public.insert_achievement(46, player_id_in, 16, 'se.level > 500');

    -- Get an S10 grade in TI Shirase
    PERFORM public.insert_achievement(47, player_id_in, 16, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 16 AND grade_display = ''S10'')');

    -- Get an S12 grade in TI Shirase
    PERFORM public.insert_achievement(48, player_id_in, 16, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 16 AND grade_display = ''S12'')');

    -- Reach the big credits roll by getting an S13 at the end of TI Shirase
    PERFORM public.insert_achievement(49, player_id_in, 16, 'g.sort_order >= (SELECT MIN(sort_order) FROM grade WHERE mode_id = 16 AND grade_display = ''S13'')');

    -- Conquer the big credits roll and clear TI Shirase with an Orange Line S13
    PERFORM public.insert_achievement(50, player_id_in, 16, 'se.grade_id = (SELECT grade_id FROM grade WHERE mode_id = 16 AND grade_display = ''S13'' AND line = ''orange'')');

    -- Submit a score for all Main modes
    WITH
    	mode_ids AS (SELECT mode_id FROM mode_tag mt WHERE mt.tag_name = 'MAIN'),
    	earliest_scores_for_modes AS (
    		SELECT DISTINCT ON (se.mode_id) mode_id, se.score_id, se.created_at
    		FROM score_entry se
    		JOIN mode_ids USING (mode_id)
    		WHERE se.player_id = player_id_in AND is_ranked_score_status(se.status)
    		ORDER BY se.mode_id, created_at
    	)
    INSERT INTO account_achievement (account_id, achievement_id, score_id)
    SELECT player_id_in, 51, (SELECT score_id FROM earliest_scores_for_modes ORDER BY created_at DESC LIMIT 1)
    WHERE (SELECT COUNT(*) FROM mode_ids) = (SELECT COUNT(*) FROM earliest_scores_for_modes)
    ON CONFLICT (account_id, achievement_id)
    DO UPDATE SET score_id = EXCLUDED.score_id;

    -- Submit a score for all Extended modes
    WITH
        mode_ids AS (SELECT mode_id FROM mode_tag mt WHERE mt.tag_name = 'EXTENDED'),
        earliest_scores_for_modes AS (
            SELECT DISTINCT ON (se.mode_id) mode_id, se.score_id, se.created_at
            FROM score_entry se
            JOIN mode_ids USING (mode_id)
            WHERE se.player_id = player_id_in AND is_ranked_score_status(se.status)
            ORDER BY se.mode_id, created_at
        )
    INSERT INTO account_achievement (account_id, achievement_id, score_id)
    SELECT player_id_in, 52, (SELECT score_id FROM earliest_scores_for_modes ORDER BY created_at DESC LIMIT 1)
    WHERE (SELECT COUNT(*) FROM mode_ids) = (SELECT COUNT(*) FROM earliest_scores_for_modes)
    ON CONFLICT (account_id, achievement_id)
    DO UPDATE SET score_id = EXCLUDED.score_id;
END;
$$;


--
-- TOC entry 458 (class 1255 OID 18090)
-- Name: versioning(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.versioning() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
  sys_period text;
  history_table text;
  manipulate jsonb;
  ignore_unchanged_values bool;
  commonColumns text[];
  time_stamp_to_use timestamptz := current_timestamp;
  range_lower timestamptz;
  transaction_info txid_snapshot;
  existing_range tstzrange;
  holder record;
  holder2 record;
  pg_version integer;
BEGIN
  -- version 0.4.0

  IF TG_WHEN != 'BEFORE' OR TG_LEVEL != 'ROW' THEN
    RAISE TRIGGER_PROTOCOL_VIOLATED USING
    MESSAGE = 'function "versioning" must be fired BEFORE ROW';
  END IF;

  IF TG_OP != 'INSERT' AND TG_OP != 'UPDATE' AND TG_OP != 'DELETE' THEN
    RAISE TRIGGER_PROTOCOL_VIOLATED USING
    MESSAGE = 'function "versioning" must be fired for INSERT or UPDATE or DELETE';
  END IF;

  IF TG_NARGS not in (3,4) THEN
    RAISE INVALID_PARAMETER_VALUE USING
    MESSAGE = 'wrong number of parameters for function "versioning"',
    HINT = 'expected 3 or 4 parameters but got ' || TG_NARGS;
  END IF;

  sys_period := TG_ARGV[0];
  history_table := TG_ARGV[1];
  ignore_unchanged_values := TG_ARGV[3];

  IF ignore_unchanged_values AND TG_OP = 'UPDATE' AND NEW IS NOT DISTINCT FROM OLD THEN
    RETURN OLD;
  END IF;

  -- check if sys_period exists on original table
  SELECT atttypid, attndims INTO holder FROM pg_attribute WHERE attrelid = TG_RELID AND attname = sys_period AND NOT attisdropped;
  IF NOT FOUND THEN
    RAISE 'column "%" of relation "%" does not exist', sys_period, TG_TABLE_NAME USING
    ERRCODE = 'undefined_column';
  END IF;
  IF holder.atttypid != to_regtype('tstzrange') THEN
    IF holder.attndims > 0 THEN
      RAISE 'system period column "%" of relation "%" is not a range but an array', sys_period, TG_TABLE_NAME USING
      ERRCODE = 'datatype_mismatch';
    END IF;

    SELECT rngsubtype INTO holder2 FROM pg_range WHERE rngtypid = holder.atttypid;
    IF FOUND THEN
      RAISE 'system period column "%" of relation "%" is not a range of timestamp with timezone but of type %', sys_period, TG_TABLE_NAME, format_type(holder2.rngsubtype, null) USING
      ERRCODE = 'datatype_mismatch';
    END IF;

    RAISE 'system period column "%" of relation "%" is not a range but type %', sys_period, TG_TABLE_NAME, format_type(holder.atttypid, null) USING
    ERRCODE = 'datatype_mismatch';
  END IF;

  IF TG_OP = 'UPDATE' OR TG_OP = 'DELETE' THEN
    -- Ignore rows already modified in this transaction
    transaction_info := txid_current_snapshot();
    IF OLD.xmin::text >= (txid_snapshot_xmin(transaction_info) % (2^32)::bigint)::text
    AND OLD.xmin::text <= (txid_snapshot_xmax(transaction_info) % (2^32)::bigint)::text THEN
      IF TG_OP = 'DELETE' THEN
        RETURN OLD;
      END IF;

      RETURN NEW;
    END IF;

    SELECT current_setting('server_version_num')::integer
    INTO pg_version;

    -- to support postgres < 9.6
    IF pg_version < 90600 THEN
      -- check if history table exits
      IF to_regclass(history_table::cstring) IS NULL THEN
        RAISE 'relation "%" does not exist', history_table;
      END IF;
    ELSE
      IF to_regclass(history_table) IS NULL THEN
        RAISE 'relation "%" does not exist', history_table;
      END IF;
    END IF;

    -- check if history table has sys_period
    IF NOT EXISTS(SELECT * FROM pg_attribute WHERE attrelid = history_table::regclass AND attname = sys_period AND NOT attisdropped) THEN
      RAISE 'history relation "%" does not contain system period column "%"', history_table, sys_period USING
      HINT = 'history relation must contain system period column with the same name and data type as the versioned one';
    END IF;

    EXECUTE format('SELECT $1.%I', sys_period) USING OLD INTO existing_range;

    IF existing_range IS NULL THEN
      RAISE 'system period column "%" of relation "%" must not be null', sys_period, TG_TABLE_NAME USING
      ERRCODE = 'null_value_not_allowed';
    END IF;

    IF isempty(existing_range) OR NOT upper_inf(existing_range) THEN
      RAISE 'system period column "%" of relation "%" contains invalid value', sys_period, TG_TABLE_NAME USING
      ERRCODE = 'data_exception',
      DETAIL = 'valid ranges must be non-empty and unbounded on the high side';
    END IF;

    IF TG_ARGV[2] = 'true' THEN
      -- mitigate update conflicts
      range_lower := lower(existing_range);
      IF range_lower >= time_stamp_to_use THEN
        time_stamp_to_use := range_lower + interval '1 microseconds';
      END IF;
    END IF;

    WITH history AS
      (SELECT attname, atttypid
      FROM   pg_attribute
      WHERE  attrelid = history_table::regclass
      AND    attnum > 0
      AND    NOT attisdropped),
      main AS
      (SELECT attname, atttypid
      FROM   pg_attribute
      WHERE  attrelid = TG_RELID
      AND    attnum > 0
      AND    NOT attisdropped)
    SELECT
      history.attname AS history_name,
      main.attname AS main_name,
      history.atttypid AS history_type,
      main.atttypid AS main_type
    INTO holder
      FROM history
      INNER JOIN main
      ON history.attname = main.attname
    WHERE
      history.atttypid != main.atttypid;

    IF FOUND THEN
      RAISE 'column "%" of relation "%" is of type % but column "%" of history relation "%" is of type %',
        holder.main_name, TG_TABLE_NAME, format_type(holder.main_type, null), holder.history_name, history_table, format_type(holder.history_type, null)
      USING ERRCODE = 'datatype_mismatch';
    END IF;

    WITH history AS
      (SELECT attname
      FROM   pg_attribute
      WHERE  attrelid = history_table::regclass
      AND    attnum > 0
      AND    NOT attisdropped),
      main AS
      (SELECT attname
      FROM   pg_attribute
      WHERE  attrelid = TG_RELID
      AND    attnum > 0
      AND    NOT attisdropped)
    SELECT array_agg(quote_ident(history.attname)) INTO commonColumns
      FROM history
      INNER JOIN main
      ON history.attname = main.attname
      AND history.attname != sys_period;

    EXECUTE ('INSERT INTO ' ||
      history_table ||
      '(' ||
      array_to_string(commonColumns , ',') ||
      ',' ||
      quote_ident(sys_period) ||
      ') VALUES ($1.' ||
      array_to_string(commonColumns, ',$1.') ||
      ',tstzrange($2, $3, ''[)''))')
       USING OLD, range_lower, time_stamp_to_use;
  END IF;

  IF TG_OP = 'UPDATE' OR TG_OP = 'INSERT' THEN
    manipulate := jsonb_set('{}'::jsonb, ('{' || sys_period || '}')::text[], to_jsonb(tstzrange(time_stamp_to_use, null, '[)')));

    RETURN jsonb_populate_record(NEW, manipulate);
  END IF;

  RETURN OLD;
END;
$_$;


-- --
-- -- TOC entry 210 (class 1259 OID 16533)
-- -- Name: __diesel_schema_migrations; Type: TABLE; Schema: public; Owner: -
-- --

-- CREATE TABLE public.__diesel_schema_migrations (
--     version character varying(50) NOT NULL,
--     run_on timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
-- );


-- --
-- -- TOC entry 197 (class 1259 OID 16385)
-- -- Name: _sqlx_migrations; Type: TABLE; Schema: public; Owner: -
-- --

-- CREATE TABLE IF NOT EXISTS public._sqlx_migrations (
--     version bigint NOT NULL,
--     description text NOT NULL,
--     installed_on timestamp with time zone DEFAULT now() NOT NULL,
--     success boolean NOT NULL,
--     checksum bytea NOT NULL,
--     execution_time bigint NOT NULL
-- );


--
-- TOC entry 199 (class 1259 OID 16449)
-- Name: account; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.account (
    player_id integer NOT NULL,
    player_name character varying NOT NULL,
    email character varying,
    password character varying NOT NULL,
    player_role public.player_role_type DEFAULT 'user'::public.player_role_type NOT NULL,
    active boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    avatar_file character varying,
    system_time_start timestamp with time zone DEFAULT transaction_timestamp() NOT NULL,
    system_time_end timestamp with time zone DEFAULT 'infinity'::timestamp with time zone NOT NULL,
    location_id integer,
    settings jsonb DEFAULT '{}'::jsonb NOT NULL,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL,
    CONSTRAINT player_system_time_check CHECK ((system_time_start < system_time_end))
);


--
-- TOC entry 232 (class 1259 OID 17664)
-- Name: account_achievement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.account_achievement (
    account_achievement_id integer NOT NULL,
    account_id integer NOT NULL,
    achievement_id integer NOT NULL,
    score_id integer
);


--
-- TOC entry 231 (class 1259 OID 17662)
-- Name: account_achievement_account_achievement_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.account_achievement ALTER COLUMN account_achievement_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.account_achievement_account_achievement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 240 (class 1259 OID 17771)
-- Name: account_badge; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.account_badge (
    account_badge_id integer NOT NULL,
    account_id integer NOT NULL,
    badge_id integer NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_by integer
);


--
-- TOC entry 239 (class 1259 OID 17769)
-- Name: account_badge_account_badge_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.account_badge ALTER COLUMN account_badge_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.account_badge_account_badge_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 252 (class 1259 OID 17978)
-- Name: account_claim; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.account_claim (
    account_claim_id integer NOT NULL,
    account_id integer NOT NULL,
    claim_key character varying NOT NULL,
    claim_value character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    system_time_start timestamp with time zone DEFAULT transaction_timestamp() NOT NULL,
    system_time_end timestamp with time zone DEFAULT 'infinity'::timestamp with time zone NOT NULL,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL,
    CONSTRAINT account_claim_system_time_check CHECK ((system_time_start < system_time_end))
);


--
-- TOC entry 251 (class 1259 OID 17976)
-- Name: account_claim_account_claim_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.account_claim ALTER COLUMN account_claim_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.account_claim_account_claim_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 253 (class 1259 OID 18002)
-- Name: account_claim_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.account_claim_history (
    account_claim_id integer NOT NULL,
    account_id integer NOT NULL,
    claim_key character varying NOT NULL,
    claim_value character varying NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    system_time_start timestamp with time zone NOT NULL,
    system_time_end timestamp with time zone NOT NULL,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL
);


--
-- TOC entry 200 (class 1259 OID 16465)
-- Name: account_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.account_history (
    player_id integer NOT NULL,
    player_name character varying NOT NULL,
    email character varying,
    password character varying NOT NULL,
    player_role public.player_role_type NOT NULL,
    active boolean NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    avatar_file character varying,
    system_time_start timestamp with time zone,
    system_time_end timestamp with time zone,
    location_id integer,
    settings jsonb DEFAULT '{}'::jsonb NOT NULL,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL
);


--
-- TOC entry 254 (class 1259 OID 18116)
-- Name: account_with_history2; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.account_with_history2 AS
 SELECT account.player_id,
    account.player_name,
    account.email,
    account.password,
    account.player_role,
    account.active,
    account.created_at,
    account.updated_at,
    account.avatar_file,
    account.location_id,
    account.settings,
    account.sys_period
   FROM public.account
UNION ALL
 SELECT account_history.player_id,
    account_history.player_name,
    account_history.email,
    account_history.password,
    account_history.player_role,
    account_history.active,
    account_history.created_at,
    account_history.updated_at,
    account_history.avatar_file,
    account_history.location_id,
    account_history.settings,
    account_history.sys_period
   FROM public.account_history;


--
-- TOC entry 230 (class 1259 OID 17643)
-- Name: achievement; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.achievement (
    achievement_id integer NOT NULL,
    description text NOT NULL,
    game_id integer,
    mode_id integer,
    sort_order integer DEFAULT 1 NOT NULL
);


--
-- TOC entry 229 (class 1259 OID 17641)
-- Name: achievement_achievement_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.achievement ALTER COLUMN achievement_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.achievement_achievement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 212 (class 1259 OID 16539)
-- Name: api_key; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.api_key (
    api_key_id integer NOT NULL,
    description character varying NOT NULL,
    key character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 211 (class 1259 OID 16537)
-- Name: api_key_api_key_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.api_key ALTER COLUMN api_key_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.api_key_api_key_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 238 (class 1259 OID 17756)
-- Name: badge; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.badge (
    badge_id integer NOT NULL,
    name character varying NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_by integer
);


--
-- TOC entry 237 (class 1259 OID 17754)
-- Name: badge_badge_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.badge ALTER COLUMN badge_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.badge_badge_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 214 (class 1259 OID 16551)
-- Name: external_integration; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.external_integration (
    integration_id integer NOT NULL,
    target public.integration_target NOT NULL,
    player_id integer NOT NULL,
    external_key character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 213 (class 1259 OID 16549)
-- Name: external_integration_integration_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.external_integration ALTER COLUMN integration_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.external_integration_integration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 216 (class 1259 OID 16563)
-- Name: game; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.game (
    game_id integer NOT NULL,
    game_name character varying NOT NULL,
    short_name character varying NOT NULL,
    subtitle character varying NOT NULL
);


--
-- TOC entry 215 (class 1259 OID 16561)
-- Name: game_game_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.game ALTER COLUMN game_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.game_game_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 218 (class 1259 OID 16573)
-- Name: grade; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.grade (
    grade_id integer NOT NULL,
    grade_display character varying NOT NULL,
    mode_id integer NOT NULL,
    line public.grade_line,
    sort_order integer NOT NULL
);


--
-- TOC entry 217 (class 1259 OID 16571)
-- Name: grade_grade_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.grade ALTER COLUMN grade_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.grade_grade_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 242 (class 1259 OID 17804)
-- Name: linked_mode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.linked_mode (
    linked_mode_id integer NOT NULL,
    mode_id integer NOT NULL,
    linked_id integer NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 241 (class 1259 OID 17802)
-- Name: linked_mode_linked_mode_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.linked_mode ALTER COLUMN linked_mode_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.linked_mode_linked_mode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 226 (class 1259 OID 17575)
-- Name: location; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.location (
    location_id integer NOT NULL,
    num_code integer NOT NULL,
    name text NOT NULL,
    alpha_2 character(2) NOT NULL,
    alpha_3 character(3) NOT NULL
);


--
-- TOC entry 225 (class 1259 OID 17573)
-- Name: location_location_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.location ALTER COLUMN location_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.location_location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 220 (class 1259 OID 16583)
-- Name: mode; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mode (
    mode_id integer NOT NULL,
    game_id integer NOT NULL,
    mode_name character varying NOT NULL,
    is_ranked boolean DEFAULT false NOT NULL,
    weight integer NOT NULL,
    margin integer NOT NULL,
    link character varying,
    grade_sort smallint DEFAULT '-1'::integer,
    score_sort smallint,
    level_sort smallint DEFAULT '-1'::integer,
    time_sort smallint DEFAULT 1,
    sort_order integer NOT NULL,
    description character varying,
    submission_range tstzrange DEFAULT '(,)'::tstzrange NOT NULL
);


--
-- TOC entry 234 (class 1259 OID 17692)
-- Name: mode_group; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mode_group (
    mode_group_id integer NOT NULL,
    name character varying NOT NULL,
    description character varying NOT NULL,
    game_id integer DEFAULT 0 NOT NULL,
    mode_ids integer[] DEFAULT ARRAY[]::integer[] NOT NULL
);


--
-- TOC entry 233 (class 1259 OID 17690)
-- Name: mode_group_mode_group_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.mode_group ALTER COLUMN mode_group_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.mode_group_mode_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 219 (class 1259 OID 16581)
-- Name: mode_mode_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.mode ALTER COLUMN mode_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.mode_mode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 236 (class 1259 OID 17738)
-- Name: mode_tag; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mode_tag (
    mode_tag_id integer NOT NULL,
    mode_id integer NOT NULL,
    tag_name character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT mode_tag_tag_name_check CHECK (((tag_name)::text ~ '^[A-Z0-9\-_]+$'::text))
);


--
-- TOC entry 235 (class 1259 OID 17736)
-- Name: mode_tag_mode_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.mode_tag ALTER COLUMN mode_tag_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.mode_tag_mode_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 244 (class 1259 OID 17826)
-- Name: platform; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.platform (
    platform_id integer NOT NULL,
    platform_name character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    mode_id integer,
    game_id integer
);


--
-- TOC entry 243 (class 1259 OID 17824)
-- Name: platform_platform_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.platform ALTER COLUMN platform_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.platform_platform_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 245 (class 1259 OID 17895)
-- Name: player; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.player AS
 SELECT a.player_id,
    a.player_name,
    a.avatar_file,
    a.location_id,
    l.alpha_2 AS location
   FROM (public.account a
     LEFT JOIN public.location l ON ((a.location_id = l.location_id)));


--
-- TOC entry 198 (class 1259 OID 16447)
-- Name: player_player_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.account ALTER COLUMN player_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.player_player_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 222 (class 1259 OID 16597)
-- Name: player_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.player_token (
    token_id integer NOT NULL,
    player_id integer NOT NULL,
    token_type public.user_token_type DEFAULT 'activation'::public.user_token_type NOT NULL,
    token character varying NOT NULL,
    expiration_date timestamp with time zone NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 221 (class 1259 OID 16595)
-- Name: player_token_token_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.player_token ALTER COLUMN token_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.player_token_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 202 (class 1259 OID 16473)
-- Name: proof; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.proof (
    proof_id integer NOT NULL,
    score_id integer NOT NULL,
    proof_type public.proof_type DEFAULT 'other'::public.proof_type NOT NULL,
    link character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    system_time_start timestamp with time zone DEFAULT transaction_timestamp() NOT NULL,
    system_time_end timestamp with time zone DEFAULT 'infinity'::timestamp with time zone NOT NULL,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL,
    CONSTRAINT proof_system_time_check CHECK ((system_time_start < system_time_end))
);


--
-- TOC entry 203 (class 1259 OID 16488)
-- Name: proof_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.proof_history (
    proof_id integer NOT NULL,
    score_id integer NOT NULL,
    proof_type public.proof_type NOT NULL,
    link character varying NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    system_time_start timestamp with time zone,
    system_time_end timestamp with time zone,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL
);


--
-- TOC entry 201 (class 1259 OID 16471)
-- Name: proof_proof_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.proof ALTER COLUMN proof_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.proof_proof_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 228 (class 1259 OID 17619)
-- Name: recent_activity; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.recent_activity (
    recent_activity_id integer NOT NULL,
    activity_type public.recent_activity_type NOT NULL,
    game_or_mode_id integer NOT NULL,
    mode_ids integer[] NOT NULL,
    score_id integer NOT NULL,
    ranking integer,
    ranking_points integer,
    prev_score_id integer,
    prev_ranking integer,
    prev_ranking_points integer,
    passed_player_ids integer[] NOT NULL
);


--
-- TOC entry 227 (class 1259 OID 17617)
-- Name: recent_activity_recent_activity_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.recent_activity ALTER COLUMN recent_activity_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.recent_activity_recent_activity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 205 (class 1259 OID 16496)
-- Name: rival; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rival (
    rival_id integer NOT NULL,
    player_id integer NOT NULL,
    rival_player_id integer NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    system_time_start timestamp with time zone DEFAULT transaction_timestamp() NOT NULL,
    system_time_end timestamp with time zone DEFAULT 'infinity'::timestamp with time zone NOT NULL,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL,
    CONSTRAINT rival_system_time_check CHECK ((system_time_start < system_time_end))
);


--
-- TOC entry 206 (class 1259 OID 16507)
-- Name: rival_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.rival_history (
    rival_id integer NOT NULL,
    player_id integer NOT NULL,
    rival_player_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    system_time_start timestamp with time zone NOT NULL,
    system_time_end timestamp with time zone NOT NULL,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL
);


--
-- TOC entry 204 (class 1259 OID 16494)
-- Name: rival_rival_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.rival ALTER COLUMN rival_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.rival_rival_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 207 (class 1259 OID 16510)
-- Name: score_entry_score_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.score_entry ALTER COLUMN score_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.score_entry_score_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 247 (class 1259 OID 17915)
-- Name: score_vote; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.score_vote (
    score_vote_id integer NOT NULL,
    score_id integer NOT NULL,
    account_id integer NOT NULL,
    status public.score_status NOT NULL,
    verification_comment character varying,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    system_time_start timestamp with time zone DEFAULT transaction_timestamp() NOT NULL,
    system_time_end timestamp with time zone DEFAULT 'infinity'::timestamp with time zone NOT NULL,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL,
    CONSTRAINT score_vote_system_time_check CHECK ((system_time_start < system_time_end))
);


--
-- TOC entry 248 (class 1259 OID 17946)
-- Name: score_vote_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.score_vote_history (
    score_vote_id integer NOT NULL,
    score_id integer NOT NULL,
    account_id integer NOT NULL,
    status public.score_status NOT NULL,
    verification_comment character varying,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    system_time_start timestamp with time zone NOT NULL,
    system_time_end timestamp with time zone NOT NULL,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL
);


--
-- TOC entry 246 (class 1259 OID 17913)
-- Name: score_vote_score_vote_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.score_vote ALTER COLUMN score_vote_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.score_vote_score_vote_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 250 (class 1259 OID 17962)
-- Name: sent_mail; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sent_mail (
    sent_mail_id integer NOT NULL,
    email character varying NOT NULL,
    account_id integer,
    subject character varying NOT NULL,
    body character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 249 (class 1259 OID 17960)
-- Name: sent_mail_sent_mail_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.sent_mail ALTER COLUMN sent_mail_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sent_mail_sent_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 224 (class 1259 OID 16608)
-- Name: sessions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sessions (
    session_id integer NOT NULL,
    session_key uuid NOT NULL,
    user_id integer NOT NULL,
    expires timestamp with time zone NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 223 (class 1259 OID 16606)
-- Name: sessions_session_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.sessions ALTER COLUMN session_id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.sessions_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 257 (class 1259 OID 18148)
-- Name: team; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.team (
    team_id integer NOT NULL,
    name character varying NOT NULL,
    mode_id integer,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL
);


--
-- TOC entry 259 (class 1259 OID 18165)
-- Name: team_account; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.team_account (
    team_account_id integer NOT NULL,
    account_id integer NOT NULL,
    team_id integer NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    sys_period tstzrange DEFAULT tstzrange(CURRENT_TIMESTAMP, NULL::timestamp with time zone) NOT NULL
);


--
-- TOC entry 261 (class 1259 OID 18196)
-- Name: team_account_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.team_account_history (
    team_account_id integer NOT NULL,
    account_id integer NOT NULL,
    team_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    sys_period tstzrange NOT NULL
);


--
-- TOC entry 258 (class 1259 OID 18163)
-- Name: team_account_team_account_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.team_account ALTER COLUMN team_account_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.team_account_team_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 260 (class 1259 OID 18190)
-- Name: team_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.team_history (
    team_id integer NOT NULL,
    name character varying NOT NULL,
    mode_id integer,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    sys_period tstzrange NOT NULL
);


--
-- TOC entry 256 (class 1259 OID 18146)
-- Name: team_team_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

ALTER TABLE public.team ALTER COLUMN team_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.team_team_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 3472 (class 2606 OID 17469)
-- Name: __diesel_schema_migrations __diesel_schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

-- ALTER TABLE ONLY public.__diesel_schema_migrations
--     ADD CONSTRAINT __diesel_schema_migrations_pkey PRIMARY KEY (version);


-- --
-- -- TOC entry 3446 (class 2606 OID 16393)
-- -- Name: _sqlx_migrations _sqlx_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
-- --

-- ALTER TABLE ONLY public._sqlx_migrations
--     ADD CONSTRAINT _sqlx_migrations_pkey PRIMARY KEY (version);


--
-- TOC entry 3526 (class 2606 OID 17670)
-- Name: account_achievement account_achievement_account_id_achievement_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_achievement
    ADD CONSTRAINT account_achievement_account_id_achievement_id_key UNIQUE (account_id, achievement_id);


--
-- TOC entry 3528 (class 2606 OID 17668)
-- Name: account_achievement account_achievement_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_achievement
    ADD CONSTRAINT account_achievement_pkey PRIMARY KEY (account_achievement_id);


--
-- TOC entry 3544 (class 2606 OID 17780)
-- Name: account_badge account_badge_account_id_badge_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_badge
    ADD CONSTRAINT account_badge_account_id_badge_id_key UNIQUE (account_id, badge_id);


--
-- TOC entry 3546 (class 2606 OID 17778)
-- Name: account_badge account_badge_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_badge
    ADD CONSTRAINT account_badge_pkey PRIMARY KEY (account_badge_id);


--
-- TOC entry 3560 (class 2606 OID 17987)
-- Name: account_claim account_claim_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_claim
    ADD CONSTRAINT account_claim_pkey PRIMARY KEY (account_claim_id);


--
-- TOC entry 3524 (class 2606 OID 17651)
-- Name: achievement achievement_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.achievement
    ADD CONSTRAINT achievement_pkey PRIMARY KEY (achievement_id);


--
-- TOC entry 3474 (class 2606 OID 17471)
-- Name: api_key api_key_description_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_key
    ADD CONSTRAINT api_key_description_key UNIQUE (description);


--
-- TOC entry 3476 (class 2606 OID 17473)
-- Name: api_key api_key_key_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_key
    ADD CONSTRAINT api_key_key_key UNIQUE (key);


--
-- TOC entry 3478 (class 2606 OID 16548)
-- Name: api_key api_key_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_key
    ADD CONSTRAINT api_key_pkey PRIMARY KEY (api_key_id);


--
-- TOC entry 3540 (class 2606 OID 17768)
-- Name: badge badge_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.badge
    ADD CONSTRAINT badge_name_key UNIQUE (name);


--
-- TOC entry 3542 (class 2606 OID 17766)
-- Name: badge badge_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.badge
    ADD CONSTRAINT badge_pkey PRIMARY KEY (badge_id);


--
-- TOC entry 3480 (class 2606 OID 17475)
-- Name: external_integration external_integration_external_key_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.external_integration
    ADD CONSTRAINT external_integration_external_key_key UNIQUE (external_key);


--
-- TOC entry 3482 (class 2606 OID 16560)
-- Name: external_integration external_integration_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.external_integration
    ADD CONSTRAINT external_integration_pkey PRIMARY KEY (integration_id);


--
-- TOC entry 3488 (class 2606 OID 16570)
-- Name: game game_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.game
    ADD CONSTRAINT game_pkey PRIMARY KEY (game_id);


--
-- TOC entry 3492 (class 2606 OID 17477)
-- Name: grade grade_id_grade_mode_id_line_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.grade
    ADD CONSTRAINT grade_id_grade_mode_id_line_unique UNIQUE (grade_display, mode_id, line);


--
-- TOC entry 3495 (class 2606 OID 16580)
-- Name: grade grade_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.grade
    ADD CONSTRAINT grade_pkey PRIMARY KEY (grade_id);


--
-- TOC entry 3548 (class 2606 OID 17812)
-- Name: linked_mode linked_mode_mode_id_linked_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.linked_mode
    ADD CONSTRAINT linked_mode_mode_id_linked_id_key UNIQUE (mode_id, linked_id);


--
-- TOC entry 3550 (class 2606 OID 17810)
-- Name: linked_mode linked_mode_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.linked_mode
    ADD CONSTRAINT linked_mode_pkey PRIMARY KEY (linked_mode_id);


--
-- TOC entry 3510 (class 2606 OID 17588)
-- Name: location location_alpha_2_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_alpha_2_key UNIQUE (alpha_2);


--
-- TOC entry 3512 (class 2606 OID 17590)
-- Name: location location_alpha_3_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_alpha_3_key UNIQUE (alpha_3);


--
-- TOC entry 3514 (class 2606 OID 17586)
-- Name: location location_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_name_key UNIQUE (name);


--
-- TOC entry 3516 (class 2606 OID 17584)
-- Name: location location_num_code_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_num_code_key UNIQUE (num_code);


--
-- TOC entry 3518 (class 2606 OID 17582)
-- Name: location location_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_pkey PRIMARY KEY (location_id);


--
-- TOC entry 3530 (class 2606 OID 17703)
-- Name: mode_group mode_group_description_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mode_group
    ADD CONSTRAINT mode_group_description_key UNIQUE (description);


--
-- TOC entry 3532 (class 2606 OID 18135)
-- Name: mode_group mode_group_game_id_name_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mode_group
    ADD CONSTRAINT mode_group_game_id_name_unique UNIQUE (game_id, name);


--
-- TOC entry 3534 (class 2606 OID 17699)
-- Name: mode_group mode_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mode_group
    ADD CONSTRAINT mode_group_pkey PRIMARY KEY (mode_group_id);


--
-- TOC entry 3499 (class 2606 OID 16594)
-- Name: mode mode_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mode
    ADD CONSTRAINT mode_pkey PRIMARY KEY (mode_id);


--
-- TOC entry 3536 (class 2606 OID 17748)
-- Name: mode_tag mode_tag_mode_id_tag_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mode_tag
    ADD CONSTRAINT mode_tag_mode_id_tag_name_key UNIQUE (mode_id, tag_name);


--
-- TOC entry 3538 (class 2606 OID 17746)
-- Name: mode_tag mode_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mode_tag
    ADD CONSTRAINT mode_tag_pkey PRIMARY KEY (mode_tag_id);


--
-- TOC entry 3552 (class 2606 OID 17835)
-- Name: platform platform_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.platform
    ADD CONSTRAINT platform_pkey PRIMARY KEY (platform_id);


--
-- TOC entry 3448 (class 2606 OID 17479)
-- Name: account player_avatar_file_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT player_avatar_file_unique UNIQUE (avatar_file);


--
-- TOC entry 3460 (class 2606 OID 17481)
-- Name: rival player_id_rival_player_id_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rival
    ADD CONSTRAINT player_id_rival_player_id_unique UNIQUE (player_id, rival_player_id);


--
-- TOC entry 3484 (class 2606 OID 17483)
-- Name: external_integration player_id_target_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.external_integration
    ADD CONSTRAINT player_id_target_unique UNIQUE (player_id, target);


--
-- TOC entry 3450 (class 2606 OID 16464)
-- Name: account player_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT player_pkey PRIMARY KEY (player_id);


--
-- TOC entry 3501 (class 2606 OID 16605)
-- Name: player_token player_token_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.player_token
    ADD CONSTRAINT player_token_pkey PRIMARY KEY (token_id);


--
-- TOC entry 3503 (class 2606 OID 17485)
-- Name: player_token player_token_player_id_token_type_unique; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.player_token
    ADD CONSTRAINT player_token_player_id_token_type_unique UNIQUE (player_id, token_type);


--
-- TOC entry 3457 (class 2606 OID 16487)
-- Name: proof proof_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.proof
    ADD CONSTRAINT proof_pkey PRIMARY KEY (proof_id);


--
-- TOC entry 3520 (class 2606 OID 17628)
-- Name: recent_activity recent_activity_activity_type_score_id_mode_ids_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.recent_activity
    ADD CONSTRAINT recent_activity_activity_type_score_id_mode_ids_key UNIQUE (activity_type, score_id, mode_ids);


--
-- TOC entry 3522 (class 2606 OID 17626)
-- Name: recent_activity recent_activity_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.recent_activity
    ADD CONSTRAINT recent_activity_pkey PRIMARY KEY (recent_activity_id);


--
-- TOC entry 3462 (class 2606 OID 16506)
-- Name: rival rival_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rival
    ADD CONSTRAINT rival_pkey PRIMARY KEY (rival_id);


--
-- TOC entry 3464 (class 2606 OID 16526)
-- Name: score_entry score_entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.score_entry
    ADD CONSTRAINT score_entry_pkey PRIMARY KEY (score_id);


--
-- TOC entry 3554 (class 2606 OID 17924)
-- Name: score_vote score_vote_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.score_vote
    ADD CONSTRAINT score_vote_pkey PRIMARY KEY (score_vote_id);


--
-- TOC entry 3556 (class 2606 OID 17926)
-- Name: score_vote score_vote_score_id_account_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.score_vote
    ADD CONSTRAINT score_vote_score_id_account_id_key UNIQUE (score_id, account_id);


--
-- TOC entry 3558 (class 2606 OID 17970)
-- Name: sent_mail sent_mail_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sent_mail
    ADD CONSTRAINT sent_mail_pkey PRIMARY KEY (sent_mail_id);


--
-- TOC entry 3506 (class 2606 OID 16612)
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (session_id);


--
-- TOC entry 3508 (class 2606 OID 17487)
-- Name: sessions sessions_session_key_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_session_key_key UNIQUE (session_key);


--
-- TOC entry 3569 (class 2606 OID 18177)
-- Name: team_account team_account_account_id_team_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.team_account
    ADD CONSTRAINT team_account_account_id_team_id_key UNIQUE (account_id, team_id);


--
-- TOC entry 3571 (class 2606 OID 18175)
-- Name: team_account team_account_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.team_account
    ADD CONSTRAINT team_account_pkey PRIMARY KEY (team_account_id);


--
-- TOC entry 3563 (class 2606 OID 18160)
-- Name: team team_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.team
    ADD CONSTRAINT team_name_key UNIQUE (name);


--
-- TOC entry 3565 (class 2606 OID 18162)
-- Name: team team_name_mode_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.team
    ADD CONSTRAINT team_name_mode_id_key UNIQUE (name, mode_id);


--
-- TOC entry 3567 (class 2606 OID 18158)
-- Name: team team_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.team
    ADD CONSTRAINT team_pkey PRIMARY KEY (team_id);


--
-- TOC entry 3561 (class 1259 OID 18144)
-- Name: calculate_ranking_player_best_scores_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX calculate_ranking_player_best_scores_index ON public.score_entry_with_history USING btree (mode_id, status, sys_period);


--
-- TOC entry 3485 (class 1259 OID 17488)
-- Name: game_game_id_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX game_game_id_uindex ON public.game USING btree (game_id);


--
-- TOC entry 3486 (class 1259 OID 17489)
-- Name: game_game_name_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX game_game_name_uindex ON public.game USING btree (game_name);


--
-- TOC entry 3489 (class 1259 OID 17490)
-- Name: game_short_name_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX game_short_name_uindex ON public.game USING btree (short_name);


--
-- TOC entry 3490 (class 1259 OID 17491)
-- Name: grade_grade_id_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX grade_grade_id_uindex ON public.grade USING btree (grade_id);


--
-- TOC entry 3493 (class 1259 OID 17492)
-- Name: grade_mode_mode_id_fk_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX grade_mode_mode_id_fk_index ON public.grade USING btree (mode_id);


--
-- TOC entry 3496 (class 1259 OID 17493)
-- Name: mode_game_game_id_fk_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX mode_game_game_id_fk_index ON public.mode USING btree (game_id);


--
-- TOC entry 3497 (class 1259 OID 17494)
-- Name: mode_mode_id_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX mode_mode_id_uindex ON public.mode USING btree (mode_id);


--
-- TOC entry 3451 (class 1259 OID 17495)
-- Name: player_player_email_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX player_player_email_uindex ON public.account USING btree (email);


--
-- TOC entry 3452 (class 1259 OID 17496)
-- Name: player_player_id_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX player_player_id_uindex ON public.account USING btree (player_id);


--
-- TOC entry 3453 (class 1259 OID 17497)
-- Name: player_player_name_ci_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX player_player_name_ci_uindex ON public.account USING btree (lower((player_name)::text));


--
-- TOC entry 3454 (class 1259 OID 17498)
-- Name: player_player_name_lower_findex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX player_player_name_lower_findex ON public.account USING btree (lower((player_name)::text));


--
-- TOC entry 3455 (class 1259 OID 17499)
-- Name: player_player_name_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX player_player_name_uindex ON public.account USING btree (player_name);


--
-- TOC entry 3504 (class 1259 OID 17500)
-- Name: player_token_token_id_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX player_token_token_id_uindex ON public.player_token USING btree (token_id);


--
-- TOC entry 3458 (class 1259 OID 17501)
-- Name: proof_proof_id_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX proof_proof_id_uindex ON public.proof USING btree (proof_id);


--
-- TOC entry 3470 (class 1259 OID 18131)
-- Name: score_entry_history_score_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX score_entry_history_score_id ON public.score_entry_history USING btree (score_id, lower(sys_period), upper(sys_period));


--
-- TOC entry 3465 (class 1259 OID 18130)
-- Name: score_entry_status; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX score_entry_status ON public.score_entry USING btree (status);


--
-- TOC entry 3466 (class 1259 OID 17503)
-- Name: score_grade_gradeid_fk_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX score_grade_gradeid_fk_index ON public.score_entry USING btree (grade_id);


--
-- TOC entry 3467 (class 1259 OID 17504)
-- Name: score_mode_modeid_fk_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX score_mode_modeid_fk_index ON public.score_entry USING btree (mode_id);


--
-- TOC entry 3468 (class 1259 OID 17505)
-- Name: score_player_playerid_fk_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX score_player_playerid_fk_index ON public.score_entry USING btree (player_id);


--
-- TOC entry 3469 (class 1259 OID 17506)
-- Name: score_score_id_uindex; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX score_score_id_uindex ON public.score_entry USING btree (score_id);


--
-- TOC entry 3608 (class 2620 OID 17507)
-- Name: account set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.account FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3622 (class 2620 OID 17792)
-- Name: account_badge set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.account_badge FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3627 (class 2620 OID 17993)
-- Name: account_claim set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.account_claim FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3616 (class 2620 OID 17510)
-- Name: api_key set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.api_key FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3621 (class 2620 OID 17791)
-- Name: badge set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.badge FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3617 (class 2620 OID 17511)
-- Name: external_integration set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.external_integration FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3623 (class 2620 OID 17823)
-- Name: linked_mode set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.linked_mode FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3620 (class 2620 OID 17795)
-- Name: mode_tag set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.mode_tag FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3624 (class 2620 OID 17838)
-- Name: platform set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.platform FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3618 (class 2620 OID 17801)
-- Name: player_token set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.player_token FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3610 (class 2620 OID 17509)
-- Name: proof set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.proof FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3612 (class 2620 OID 17512)
-- Name: rival set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.rival FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3614 (class 2620 OID 17508)
-- Name: score_entry set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.score_entry FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3625 (class 2620 OID 17937)
-- Name: score_vote set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.score_vote FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3619 (class 2620 OID 17798)
-- Name: sessions set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.sessions FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3629 (class 2620 OID 18188)
-- Name: team set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.team FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3631 (class 2620 OID 18189)
-- Name: team_account set_updated_at; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER set_updated_at BEFORE UPDATE ON public.team_account FOR EACH ROW EXECUTE PROCEDURE public.set_update_at();


--
-- TOC entry 3609 (class 2620 OID 18094)
-- Name: account versioning_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER versioning_trigger BEFORE INSERT OR DELETE OR UPDATE ON public.account FOR EACH ROW EXECUTE PROCEDURE public.versioning('sys_period', 'account_history', 'true');


--
-- TOC entry 3628 (class 2620 OID 18097)
-- Name: account_claim versioning_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER versioning_trigger BEFORE INSERT OR DELETE OR UPDATE ON public.account_claim FOR EACH ROW EXECUTE PROCEDURE public.versioning('sys_period', 'account_claim_history', 'true');


--
-- TOC entry 3611 (class 2620 OID 18100)
-- Name: proof versioning_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER versioning_trigger BEFORE INSERT OR DELETE OR UPDATE ON public.proof FOR EACH ROW EXECUTE PROCEDURE public.versioning('sys_period', 'proof_history', 'true');


--
-- TOC entry 3613 (class 2620 OID 18109)
-- Name: rival versioning_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER versioning_trigger BEFORE INSERT OR DELETE OR UPDATE ON public.rival FOR EACH ROW EXECUTE PROCEDURE public.versioning('sys_period', 'rival_history', 'true');


--
-- TOC entry 3615 (class 2620 OID 18112)
-- Name: score_entry versioning_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER versioning_trigger BEFORE INSERT OR DELETE OR UPDATE ON public.score_entry FOR EACH ROW EXECUTE PROCEDURE public.versioning('sys_period', 'score_entry_history', 'true');


--
-- TOC entry 3626 (class 2620 OID 18115)
-- Name: score_vote versioning_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER versioning_trigger BEFORE INSERT OR DELETE OR UPDATE ON public.score_vote FOR EACH ROW EXECUTE PROCEDURE public.versioning('sys_period', 'score_vote_history', 'true');


--
-- TOC entry 3630 (class 2620 OID 18202)
-- Name: team versioning_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER versioning_trigger BEFORE INSERT OR DELETE OR UPDATE ON public.team FOR EACH ROW EXECUTE PROCEDURE public.versioning('sys_period', 'team_history', 'true');


--
-- TOC entry 3632 (class 2620 OID 18203)
-- Name: team_account versioning_trigger; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER versioning_trigger BEFORE INSERT OR DELETE OR UPDATE ON public.team_account FOR EACH ROW EXECUTE PROCEDURE public.versioning('sys_period', 'team_account_history', 'true');


--
-- TOC entry 3590 (class 2606 OID 17671)
-- Name: account_achievement account_achievement_account_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_achievement
    ADD CONSTRAINT account_achievement_account_id_fk FOREIGN KEY (account_id) REFERENCES public.account(player_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3591 (class 2606 OID 17676)
-- Name: account_achievement account_achievement_achievement_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_achievement
    ADD CONSTRAINT account_achievement_achievement_id_fk FOREIGN KEY (achievement_id) REFERENCES public.achievement(achievement_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3592 (class 2606 OID 17681)
-- Name: account_achievement account_achievement_score_entry_score_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_achievement
    ADD CONSTRAINT account_achievement_score_entry_score_id_fk FOREIGN KEY (score_id) REFERENCES public.score_entry(score_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3595 (class 2606 OID 17781)
-- Name: account_badge account_badge_account_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_badge
    ADD CONSTRAINT account_badge_account_id_fk FOREIGN KEY (account_id) REFERENCES public.account(player_id);


--
-- TOC entry 3597 (class 2606 OID 18085)
-- Name: account_badge account_badge_account_updated_by_player_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_badge
    ADD CONSTRAINT account_badge_account_updated_by_player_fk FOREIGN KEY (updated_by) REFERENCES public.account(player_id);


--
-- TOC entry 3596 (class 2606 OID 17786)
-- Name: account_badge account_badge_badge_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_badge
    ADD CONSTRAINT account_badge_badge_id_fk FOREIGN KEY (badge_id) REFERENCES public.badge(badge_id);


--
-- TOC entry 3605 (class 2606 OID 17988)
-- Name: account_claim account_claim_account_player_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_claim
    ADD CONSTRAINT account_claim_account_player_id FOREIGN KEY (account_id) REFERENCES public.account(player_id);


--
-- TOC entry 3589 (class 2606 OID 17657)
-- Name: achievement achievement_mode_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.achievement
    ADD CONSTRAINT achievement_mode_id FOREIGN KEY (mode_id) REFERENCES public.mode(mode_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3594 (class 2606 OID 18080)
-- Name: badge badge_account_updated_by_player_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.badge
    ADD CONSTRAINT badge_account_updated_by_player_fk FOREIGN KEY (updated_by) REFERENCES public.account(player_id);


--
-- TOC entry 3572 (class 2606 OID 17591)
-- Name: account fk_player_location_location_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT fk_player_location_location_id FOREIGN KEY (location_id) REFERENCES public.location(location_id);


--
-- TOC entry 3583 (class 2606 OID 17513)
-- Name: grade grade_mode_mode_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.grade
    ADD CONSTRAINT grade_mode_mode_id_fk FOREIGN KEY (mode_id) REFERENCES public.mode(mode_id) ON UPDATE CASCADE;


--
-- TOC entry 3582 (class 2606 OID 17518)
-- Name: external_integration integration_player_player_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.external_integration
    ADD CONSTRAINT integration_player_player_id_fk FOREIGN KEY (player_id) REFERENCES public.account(player_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3599 (class 2606 OID 17818)
-- Name: linked_mode linked_mode_mode_linked_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.linked_mode
    ADD CONSTRAINT linked_mode_mode_linked_id_fk FOREIGN KEY (linked_id) REFERENCES public.mode(mode_id);


--
-- TOC entry 3598 (class 2606 OID 17813)
-- Name: linked_mode linked_mode_mode_mode_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.linked_mode
    ADD CONSTRAINT linked_mode_mode_mode_id_fk FOREIGN KEY (mode_id) REFERENCES public.mode(mode_id);


--
-- TOC entry 3584 (class 2606 OID 17523)
-- Name: mode mode_game_game_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mode
    ADD CONSTRAINT mode_game_game_id_fk FOREIGN KEY (game_id) REFERENCES public.game(game_id) ON UPDATE CASCADE;


--
-- TOC entry 3593 (class 2606 OID 17749)
-- Name: mode_tag mode_tag_mode_mode_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mode_tag
    ADD CONSTRAINT mode_tag_mode_mode_id_fk FOREIGN KEY (mode_id) REFERENCES public.mode(mode_id);


--
-- TOC entry 3601 (class 2606 OID 18209)
-- Name: platform platform_game_game_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.platform
    ADD CONSTRAINT platform_game_game_id_fk FOREIGN KEY (game_id) REFERENCES public.game(game_id);


--
-- TOC entry 3600 (class 2606 OID 18204)
-- Name: platform platform_mode_mode_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.platform
    ADD CONSTRAINT platform_mode_mode_id_fk FOREIGN KEY (mode_id) REFERENCES public.mode(mode_id);


--
-- TOC entry 3585 (class 2606 OID 17528)
-- Name: player_token player_token_player_player_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.player_token
    ADD CONSTRAINT player_token_player_player_id_fk FOREIGN KEY (player_id) REFERENCES public.account(player_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3573 (class 2606 OID 17533)
-- Name: proof proof_score_entry_score_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.proof
    ADD CONSTRAINT proof_score_entry_score_id_fk FOREIGN KEY (score_id) REFERENCES public.score_entry(score_id) ON UPDATE CASCADE;


--
-- TOC entry 3588 (class 2606 OID 17634)
-- Name: recent_activity recent_activity_score_entry_prev_score_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.recent_activity
    ADD CONSTRAINT recent_activity_score_entry_prev_score_id FOREIGN KEY (prev_score_id) REFERENCES public.score_entry(score_id);


--
-- TOC entry 3587 (class 2606 OID 17629)
-- Name: recent_activity recent_activity_score_entry_score_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.recent_activity
    ADD CONSTRAINT recent_activity_score_entry_score_id FOREIGN KEY (score_id) REFERENCES public.score_entry(score_id);


--
-- TOC entry 3574 (class 2606 OID 17538)
-- Name: rival rival_player_player_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rival
    ADD CONSTRAINT rival_player_player_id_fk FOREIGN KEY (player_id) REFERENCES public.account(player_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3575 (class 2606 OID 17543)
-- Name: rival rival_player_rival_player_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.rival
    ADD CONSTRAINT rival_player_rival_player_id_fk FOREIGN KEY (rival_player_id) REFERENCES public.account(player_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3580 (class 2606 OID 17839)
-- Name: score_entry score_entry_platform_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.score_entry
    ADD CONSTRAINT score_entry_platform_id_fk FOREIGN KEY (platform_id) REFERENCES public.platform(platform_id) ON UPDATE CASCADE;


--
-- TOC entry 3576 (class 2606 OID 17548)
-- Name: score_entry score_entry_updated_by_player_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.score_entry
    ADD CONSTRAINT score_entry_updated_by_player_fk FOREIGN KEY (updated_by) REFERENCES public.account(player_id);


--
-- TOC entry 3581 (class 2606 OID 17899)
-- Name: score_entry score_entry_verified_by_account_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.score_entry
    ADD CONSTRAINT score_entry_verified_by_account_fk FOREIGN KEY (verified_by) REFERENCES public.account(player_id) ON UPDATE CASCADE;


--
-- TOC entry 3577 (class 2606 OID 17553)
-- Name: score_entry score_grade_gradeid_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.score_entry
    ADD CONSTRAINT score_grade_gradeid_fk FOREIGN KEY (grade_id) REFERENCES public.grade(grade_id) ON UPDATE CASCADE;


--
-- TOC entry 3578 (class 2606 OID 17558)
-- Name: score_entry score_mode_modeid_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.score_entry
    ADD CONSTRAINT score_mode_modeid_fk FOREIGN KEY (mode_id) REFERENCES public.mode(mode_id) ON UPDATE CASCADE;


--
-- TOC entry 3579 (class 2606 OID 17563)
-- Name: score_entry score_player_playerid_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.score_entry
    ADD CONSTRAINT score_player_playerid_fk FOREIGN KEY (player_id) REFERENCES public.account(player_id) ON UPDATE CASCADE;


--
-- TOC entry 3603 (class 2606 OID 17932)
-- Name: score_vote score_vote_account_player_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.score_vote
    ADD CONSTRAINT score_vote_account_player_id FOREIGN KEY (account_id) REFERENCES public.account(player_id);


--
-- TOC entry 3602 (class 2606 OID 17927)
-- Name: score_vote score_vote_score_entry_score_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.score_vote
    ADD CONSTRAINT score_vote_score_entry_score_id FOREIGN KEY (score_id) REFERENCES public.score_entry(score_id);


--
-- TOC entry 3604 (class 2606 OID 17971)
-- Name: sent_mail sent_mail_account_player_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sent_mail
    ADD CONSTRAINT sent_mail_account_player_id FOREIGN KEY (account_id) REFERENCES public.account(player_id);


--
-- TOC entry 3586 (class 2606 OID 17568)
-- Name: sessions sessions_player_player_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_player_player_id_fk FOREIGN KEY (user_id) REFERENCES public.account(player_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3606 (class 2606 OID 18178)
-- Name: team_account team_account_account_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.team_account
    ADD CONSTRAINT team_account_account_id_fk FOREIGN KEY (account_id) REFERENCES public.account(player_id);


--
-- TOC entry 3607 (class 2606 OID 18183)
-- Name: team_account team_account_team_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.team_account
    ADD CONSTRAINT team_account_team_id_fk FOREIGN KEY (team_id) REFERENCES public.team(team_id);


-- Completed on 2023-06-17 16:44:33

--
-- PostgreSQL database dump complete
--

    end if;
end $outer$;

do $$ begin
if not exists (
    select 1
    from public.game
)
then
    INSERT INTO public.game (game_id, game_name, short_name, subtitle) VALUES (0, 'Overall', 'All', '');
    INSERT INTO public.game VALUES (1, 'Tetris The Grandmaster', 'TGM1', 'テトリス　ザ・グランドマスター');
    INSERT INTO public.game VALUES (2, 'Tetris The Absolute: The Grandmaster 2 PLUS', 'TAP', 'テトリスT.A.ザ・グランドマスター2 PLUS');
    INSERT INTO public.game VALUES (3, 'Tetris The Grandmaster 3: Terror Instinct', 'TI', 'テトリス　ザ・グランドマスター3　-Terror Instinct-');

    INSERT INTO public.mode VALUES (1, 1, 'Master', true, 100, 1, 'https://tetrisconcept.net/threads/tgm1.134/', -1, 0, -1, 1, 10);
    INSERT INTO public.mode VALUES (2, 1, '20G', true, 100, 1, 'https://tetrisconcept.net/threads/tgm-20g-code.75/', -1, 0, -1, 1, 20);
    INSERT INTO public.mode VALUES (6, 2, 'Master', true, 100, 1, 'https://tetrisconcept.net/threads/tap-master.24/', -1, 0, -1, 1, 60);
    INSERT INTO public.mode VALUES (7, 2, 'Death', true, 100, 1, 'https://tetrisconcept.net/threads/tap-death.128/', -1, 0, -1, 1, 70);
    INSERT INTO public.mode VALUES (12, 2, 'Normal', false, 100, 1, 'https://tetrisconcept.net/threads/tap-normal.245/', NULL, -1, 0, 0, 120);
    INSERT INTO public.mode VALUES (3, 1, 'Big', false, 100, 1, 'https://tetrisconcept.net/threads/tgm-big-mode.1127/', -1, 0, -1, 1, 30);
    INSERT INTO public.mode VALUES (4, 1, 'Reverse', false, 100, 1, 'https://tetrisconcept.net/threads/tgm-umop-ap-sdn.185/', -1, 0, -1, 1, 40);
    INSERT INTO public.mode VALUES (5, 1, 'Secret Grade', false, 100, 1, 'https://tetrisconcept.net/threads/tgm-tap-secret-grade.299/', -1, 0, 1, 1, 50);
    INSERT INTO public.mode VALUES (8, 2, 'Death (Series of 5)', false, 100, 1, 'https://tetrisconcept.net/threads/tap-death-series-of-5.2789/', NULL, NULL, -1, NULL, 80);
    INSERT INTO public.mode VALUES (9, 2, 'Doubles', false, 100, 1, 'https://tetrisconcept.net/threads/tap-doubles.258/', NULL, NULL, -1, 1, 90);
    INSERT INTO public.mode VALUES (10, 2, 'Big', false, 100, 1, 'https://tetrisconcept.net/threads/tap-master-big-mode-big-code.78/', -1, 0, -1, 1, 100);
    INSERT INTO public.mode VALUES (11, 2, 'Item', false, 100, 1, 'https://tetrisconcept.net/threads/tap-item-modes-item-code.343/', -1, 0, -1, 1, 110);
    INSERT INTO public.mode VALUES (13, 2, 'TGM+', false, 100, 1, 'https://tetrisconcept.net/threads/tap-tgm.778/', NULL, 0, -1, 1, 130);
    INSERT INTO public.mode VALUES (15, 3, 'Master (C)', true, 100, 1, 'https://tetrisconcept.net/threads/ti-master.572/', -1, NULL, -1, 1, 150);
    INSERT INTO public.mode VALUES (16, 3, 'Shirase (C)', true, 100, 1, 'https://tetrisconcept.net/threads/ti-shirase.305/', -1, NULL, -1, 1, 160);
    INSERT INTO public.mode VALUES (17, 3, 'Easy (C)', false, 100, 1, 'https://tetrisconcept.net/threads/ti-easy.959/', NULL, -1, -1, 1, 170);
    INSERT INTO public.mode VALUES (18, 3, 'Sakura (C)', false, 100, 1, 'https://tetrisconcept.net/threads/ti-sakura.1579/', -1, NULL, -1, 1, 180);
    INSERT INTO public.mode VALUES (20, 3, 'Master (W)', false, 100, 1, 'https://tetrisconcept.net/threads/ti-master.572/', -1, NULL, -1, 1, 220);
    INSERT INTO public.mode VALUES (21, 3, 'Shirase (W)', false, 100, 1, 'https://tetrisconcept.net/threads/ti-shirase.305/', -1, NULL, -1, 1, 230);
    INSERT INTO public.mode VALUES (22, 3, 'Easy (W)', false, 100, 1, 'https://tetrisconcept.net/threads/ti-easy.959/', NULL, -1, -1, 1, 240);
    INSERT INTO public.mode VALUES (23, 3, 'Sakura (W)', false, 100, 1, 'https://tetrisconcept.net/threads/ti-sakura.1579/', -1, NULL, -1, 1, 250);
    INSERT INTO public.mode VALUES (14, 2, 'Secret Grade', false, 100, 1, 'https://tetrisconcept.net/threads/tgm-tap-secret-grade.299/', -1, 0, 1, 1, 140);
    INSERT INTO public.mode VALUES (19, 3, 'Master Secret Grade (C)', false, 100, 1, 'https://tetrisconcept.net/threads/miscellaneous-tetris-achievements.1370/', -1, NULL, 1, 1, 190);
    INSERT INTO public.mode VALUES (24, 3, 'Master Secret Grade (W)', false, 100, 1, 'https://tetrisconcept.net/threads/miscellaneous-tetris-achievements.1370/', -1, NULL, 1, 1, 260);
    INSERT INTO public.mode VALUES (25, 3, 'Big (C)', false, 100, 1, 'https://tetrisconcept.net/threads/ti-master-big-mode.1844/', -1, NULL, -1, 1, 210);
    INSERT INTO public.mode VALUES (26, 3, 'Big (W)', false, 100, 1, 'https://tetrisconcept.net/threads/ti-master-big-mode.1844/', -1, NULL, -1, 1, 280);
    INSERT INTO public.mode VALUES (27, 3, 'Shirase Secret Grade (C)', false, 100, 1, 'https://tetrisconcept.net/threads/miscellaneous-tetris-achievements.1370/', -1, NULL, 1, 1, 200);
    INSERT INTO public.mode VALUES (28, 3, 'Shirase Secret Grade (W)', false, 100, 1, 'https://tetrisconcept.net/threads/miscellaneous-tetris-achievements.1370/', -1, NULL, 1, 1, 270);
    INSERT INTO public."mode"
    (mode_id, game_id, mode_name, is_ranked, weight, margin, link, grade_sort, score_sort, level_sort, time_sort, sort_order, description, submission_range)
    VALUES(30, 3, 'Qualified Master (C)', false, 100, 1, 'https://tetrisconcept.net/threads/ti-master.572/', -1, NULL, NULL, NULL, 155, 'Submit your qualified grade for Master mode. This is your Master grade as shown on the player overview screen when you log in or the grade shown beside the playfield frame.

    These grades are awarded to you when you pass a Promotional Exam.

    See this page for more details: https://tetris.wiki/Tetris_The_Grand_Master_3_Terror-Instinct#User_accounts_and_exams', '(,)');
    INSERT INTO public."mode"
(mode_id, game_id, mode_name, is_ranked, weight, margin, link, grade_sort, score_sort, level_sort, time_sort, sort_order, description, submission_range)
VALUES(31, 3, 'Qualified Master (W)', false, 100, 1, 'https://tetrisconcept.net/threads/ti-master.572/', -1, NULL, NULL, NULL, 225, 'Submit your qualified grade for Master mode. This is your Master grade as shown on the player overview screen when you log in or the grade shown beside the playfield frame.

These grades are awarded to you when you pass a Promotional Exam.

See this page for more details: https://tetris.wiki/Tetris_The_Grand_Master_3_Terror-Instinct#User_accounts_and_exams', '(,)');


    INSERT INTO public.grade VALUES (001, '9', 1, NULL, 10);
    INSERT INTO public.grade VALUES (002, '8', 1, NULL, 20);
    INSERT INTO public.grade VALUES (003, '7', 1, NULL, 30);
    INSERT INTO public.grade VALUES (004, '6', 1, NULL, 40);
    INSERT INTO public.grade VALUES (005, '5', 1, NULL, 50);
    INSERT INTO public.grade VALUES (006, '4', 1, NULL, 60);
    INSERT INTO public.grade VALUES (007, '3', 1, NULL, 70);
    INSERT INTO public.grade VALUES (008, '2', 1, NULL, 80);
    INSERT INTO public.grade VALUES (009, '1', 1, NULL, 90);
    INSERT INTO public.grade VALUES (010, 'S1', 1, NULL, 100);
    INSERT INTO public.grade VALUES (011, 'S2', 1, NULL, 110);
    INSERT INTO public.grade VALUES (012, 'S3', 1, NULL, 120);
    INSERT INTO public.grade VALUES (013, 'S4', 1, NULL, 130);
    INSERT INTO public.grade VALUES (014, 'S5', 1, NULL, 140);
    INSERT INTO public.grade VALUES (015, 'S6', 1, NULL, 150);
    INSERT INTO public.grade VALUES (016, 'S7', 1, NULL, 160);
    INSERT INTO public.grade VALUES (017, 'S8', 1, NULL, 170);
    INSERT INTO public.grade VALUES (018, 'S9', 1, NULL, 180);
    INSERT INTO public.grade VALUES (019, 'GM', 1, NULL, 190);
    INSERT INTO public.grade VALUES (020, '9', 2, NULL, 10);
    INSERT INTO public.grade VALUES (021, '8', 2, NULL, 20);
    INSERT INTO public.grade VALUES (022, '7', 2, NULL, 30);
    INSERT INTO public.grade VALUES (023, '6', 2, NULL, 40);
    INSERT INTO public.grade VALUES (024, '5', 2, NULL, 50);
    INSERT INTO public.grade VALUES (025, '4', 2, NULL, 60);
    INSERT INTO public.grade VALUES (026, '3', 2, NULL, 70);
    INSERT INTO public.grade VALUES (027, '2', 2, NULL, 80);
    INSERT INTO public.grade VALUES (028, '1', 2, NULL, 90);
    INSERT INTO public.grade VALUES (029, 'S1', 2, NULL, 100);
    INSERT INTO public.grade VALUES (030, 'S2', 2, NULL, 110);
    INSERT INTO public.grade VALUES (031, 'S3', 2, NULL, 120);
    INSERT INTO public.grade VALUES (032, 'S4', 2, NULL, 130);
    INSERT INTO public.grade VALUES (033, 'S5', 2, NULL, 140);
    INSERT INTO public.grade VALUES (034, 'S6', 2, NULL, 150);
    INSERT INTO public.grade VALUES (035, 'S7', 2, NULL, 160);
    INSERT INTO public.grade VALUES (036, 'S8', 2, NULL, 170);
    INSERT INTO public.grade VALUES (037, 'S9', 2, NULL, 180);
    INSERT INTO public.grade VALUES (038, 'GM', 2, NULL, 190);
    INSERT INTO public.grade VALUES (039, '9', 3, NULL, 10);
    INSERT INTO public.grade VALUES (040, '8', 3, NULL, 20);
    INSERT INTO public.grade VALUES (041, '7', 3, NULL, 30);
    INSERT INTO public.grade VALUES (042, '6', 3, NULL, 40);
    INSERT INTO public.grade VALUES (043, '5', 3, NULL, 50);
    INSERT INTO public.grade VALUES (044, '4', 3, NULL, 60);
    INSERT INTO public.grade VALUES (045, '3', 3, NULL, 70);
    INSERT INTO public.grade VALUES (046, '2', 3, NULL, 80);
    INSERT INTO public.grade VALUES (047, '1', 3, NULL, 90);
    INSERT INTO public.grade VALUES (048, 'S1', 3, NULL, 100);
    INSERT INTO public.grade VALUES (049, 'S2', 3, NULL, 110);
    INSERT INTO public.grade VALUES (050, 'S3', 3, NULL, 120);
    INSERT INTO public.grade VALUES (051, 'S4', 3, NULL, 130);
    INSERT INTO public.grade VALUES (052, 'S5', 3, NULL, 140);
    INSERT INTO public.grade VALUES (053, 'S6', 3, NULL, 150);
    INSERT INTO public.grade VALUES (054, 'S7', 3, NULL, 160);
    INSERT INTO public.grade VALUES (055, 'S8', 3, NULL, 170);
    INSERT INTO public.grade VALUES (056, 'S9', 3, NULL, 180);
    INSERT INTO public.grade VALUES (057, 'GM', 3, NULL, 190);
    INSERT INTO public.grade VALUES (058, '9', 4, NULL, 10);
    INSERT INTO public.grade VALUES (059, '8', 4, NULL, 20);
    INSERT INTO public.grade VALUES (060, '7', 4, NULL, 30);
    INSERT INTO public.grade VALUES (061, '6', 4, NULL, 40);
    INSERT INTO public.grade VALUES (062, '5', 4, NULL, 50);
    INSERT INTO public.grade VALUES (063, '4', 4, NULL, 60);
    INSERT INTO public.grade VALUES (064, '3', 4, NULL, 70);
    INSERT INTO public.grade VALUES (065, '2', 4, NULL, 80);
    INSERT INTO public.grade VALUES (066, '1', 4, NULL, 90);
    INSERT INTO public.grade VALUES (067, 'S1', 4, NULL, 100);
    INSERT INTO public.grade VALUES (068, 'S2', 4, NULL, 110);
    INSERT INTO public.grade VALUES (069, 'S3', 4, NULL, 120);
    INSERT INTO public.grade VALUES (070, 'S4', 4, NULL, 130);
    INSERT INTO public.grade VALUES (071, 'S5', 4, NULL, 140);
    INSERT INTO public.grade VALUES (072, 'S6', 4, NULL, 150);
    INSERT INTO public.grade VALUES (073, 'S7', 4, NULL, 160);
    INSERT INTO public.grade VALUES (074, 'S8', 4, NULL, 170);
    INSERT INTO public.grade VALUES (075, 'S9', 4, NULL, 180);
    INSERT INTO public.grade VALUES (076, 'GM', 4, NULL, 190);
    INSERT INTO public.grade VALUES (077, '9', 5, NULL, 10);
    INSERT INTO public.grade VALUES (078, '8', 5, NULL, 20);
    INSERT INTO public.grade VALUES (079, '7', 5, NULL, 30);
    INSERT INTO public.grade VALUES (080, '6', 5, NULL, 40);
    INSERT INTO public.grade VALUES (081, '5', 5, NULL, 50);
    INSERT INTO public.grade VALUES (082, '4', 5, NULL, 60);
    INSERT INTO public.grade VALUES (083, '3', 5, NULL, 70);
    INSERT INTO public.grade VALUES (084, '2', 5, NULL, 80);
    INSERT INTO public.grade VALUES (085, '1', 5, NULL, 90);
    INSERT INTO public.grade VALUES (086, 'S1', 5, NULL, 100);
    INSERT INTO public.grade VALUES (087, 'S2', 5, NULL, 110);
    INSERT INTO public.grade VALUES (088, 'S3', 5, NULL, 120);
    INSERT INTO public.grade VALUES (089, 'S4', 5, NULL, 130);
    INSERT INTO public.grade VALUES (090, 'S5', 5, NULL, 140);
    INSERT INTO public.grade VALUES (091, 'S6', 5, NULL, 150);
    INSERT INTO public.grade VALUES (092, 'S7', 5, NULL, 160);
    INSERT INTO public.grade VALUES (093, 'S8', 5, NULL, 170);
    INSERT INTO public.grade VALUES (094, 'S9', 5, NULL, 180);
    INSERT INTO public.grade VALUES (095, 'GM', 5, NULL, 190);
    INSERT INTO public.grade VALUES (096, '9', 6, NULL, 10);
    INSERT INTO public.grade VALUES (097, '8', 6, NULL, 20);
    INSERT INTO public.grade VALUES (098, '7', 6, NULL, 30);
    INSERT INTO public.grade VALUES (099, '6', 6, NULL, 40);
    INSERT INTO public.grade VALUES (100, '5', 6, NULL, 50);
    INSERT INTO public.grade VALUES (101, '4', 6, NULL, 60);
    INSERT INTO public.grade VALUES (102, '3', 6, NULL, 70);
    INSERT INTO public.grade VALUES (103, '2', 6, NULL, 80);
    INSERT INTO public.grade VALUES (104, '1', 6, NULL, 90);
    INSERT INTO public.grade VALUES (105, 'S1', 6, NULL, 100);
    INSERT INTO public.grade VALUES (106, 'S2', 6, NULL, 110);
    INSERT INTO public.grade VALUES (107, 'S3', 6, NULL, 120);
    INSERT INTO public.grade VALUES (108, 'S4', 6, NULL, 130);
    INSERT INTO public.grade VALUES (109, 'S5', 6, NULL, 140);
    INSERT INTO public.grade VALUES (110, 'S6', 6, NULL, 150);
    INSERT INTO public.grade VALUES (111, 'S7', 6, NULL, 160);
    INSERT INTO public.grade VALUES (112, 'S8', 6, NULL, 170);
    INSERT INTO public.grade VALUES (113, 'S9', 6, NULL, 180);
    INSERT INTO public.grade VALUES (114, 'S9', 6, 'orange', 190);
    INSERT INTO public.grade VALUES (115, 'M', 6, 'green', 200);
    INSERT INTO public.grade VALUES (116, 'GM', 6, 'green', 210);
    INSERT INTO public.grade VALUES (117, 'GM', 6, 'orange', 220);
    INSERT INTO public.grade VALUES (118, '9', 10, NULL, 10);
    INSERT INTO public.grade VALUES (119, '8', 10, NULL, 20);
    INSERT INTO public.grade VALUES (120, '7', 10, NULL, 30);
    INSERT INTO public.grade VALUES (121, '6', 10, NULL, 40);
    INSERT INTO public.grade VALUES (122, '5', 10, NULL, 50);
    INSERT INTO public.grade VALUES (123, '4', 10, NULL, 60);
    INSERT INTO public.grade VALUES (124, '3', 10, NULL, 70);
    INSERT INTO public.grade VALUES (125, '2', 10, NULL, 80);
    INSERT INTO public.grade VALUES (126, '1', 10, NULL, 90);
    INSERT INTO public.grade VALUES (127, 'S1', 10, NULL, 100);
    INSERT INTO public.grade VALUES (128, 'S2', 10, NULL, 110);
    INSERT INTO public.grade VALUES (129, 'S3', 10, NULL, 120);
    INSERT INTO public.grade VALUES (130, 'S4', 10, NULL, 130);
    INSERT INTO public.grade VALUES (131, 'S5', 10, NULL, 140);
    INSERT INTO public.grade VALUES (132, 'S6', 10, NULL, 150);
    INSERT INTO public.grade VALUES (133, 'S7', 10, NULL, 160);
    INSERT INTO public.grade VALUES (134, 'S8', 10, NULL, 170);
    INSERT INTO public.grade VALUES (135, 'S9', 10, NULL, 180);
    INSERT INTO public.grade VALUES (136, '9', 11, NULL, 10);
    INSERT INTO public.grade VALUES (137, '8', 11, NULL, 20);
    INSERT INTO public.grade VALUES (138, '7', 11, NULL, 30);
    INSERT INTO public.grade VALUES (139, '6', 11, NULL, 40);
    INSERT INTO public.grade VALUES (140, '5', 11, NULL, 50);
    INSERT INTO public.grade VALUES (141, '4', 11, NULL, 60);
    INSERT INTO public.grade VALUES (142, '3', 11, NULL, 70);
    INSERT INTO public.grade VALUES (143, '2', 11, NULL, 80);
    INSERT INTO public.grade VALUES (144, '1', 11, NULL, 90);
    INSERT INTO public.grade VALUES (145, 'S1', 11, NULL, 100);
    INSERT INTO public.grade VALUES (146, 'S2', 11, NULL, 110);
    INSERT INTO public.grade VALUES (147, 'S3', 11, NULL, 120);
    INSERT INTO public.grade VALUES (148, 'S4', 11, NULL, 130);
    INSERT INTO public.grade VALUES (149, 'S5', 11, NULL, 140);
    INSERT INTO public.grade VALUES (150, 'S6', 11, NULL, 150);
    INSERT INTO public.grade VALUES (151, 'S7', 11, NULL, 160);
    INSERT INTO public.grade VALUES (152, 'S8', 11, NULL, 170);
    INSERT INTO public.grade VALUES (153, 'S9', 11, NULL, 180);
    INSERT INTO public.grade VALUES (154, '9', 14, NULL, 10);
    INSERT INTO public.grade VALUES (155, '8', 14, NULL, 20);
    INSERT INTO public.grade VALUES (156, '7', 14, NULL, 30);
    INSERT INTO public.grade VALUES (157, '6', 14, NULL, 40);
    INSERT INTO public.grade VALUES (158, '5', 14, NULL, 50);
    INSERT INTO public.grade VALUES (159, '4', 14, NULL, 60);
    INSERT INTO public.grade VALUES (160, '3', 14, NULL, 70);
    INSERT INTO public.grade VALUES (161, '2', 14, NULL, 80);
    INSERT INTO public.grade VALUES (162, '1', 14, NULL, 90);
    INSERT INTO public.grade VALUES (163, 'S1', 14, NULL, 100);
    INSERT INTO public.grade VALUES (164, 'S2', 14, NULL, 110);
    INSERT INTO public.grade VALUES (165, 'S3', 14, NULL, 120);
    INSERT INTO public.grade VALUES (166, 'S4', 14, NULL, 130);
    INSERT INTO public.grade VALUES (167, 'S5', 14, NULL, 140);
    INSERT INTO public.grade VALUES (168, 'S6', 14, NULL, 150);
    INSERT INTO public.grade VALUES (169, 'S7', 14, NULL, 160);
    INSERT INTO public.grade VALUES (170, 'S8', 14, NULL, 170);
    INSERT INTO public.grade VALUES (171, 'S9', 14, NULL, 180);
    INSERT INTO public.grade VALUES (172, 'GM', 14, NULL, 190);
    INSERT INTO public.grade VALUES (173, '—', 7, NULL, 10);
    INSERT INTO public.grade VALUES (174, 'M', 7, NULL, 20);
    INSERT INTO public.grade VALUES (175, 'GM', 7, NULL, 30);
    INSERT INTO public.grade VALUES (176, '1', 18, NULL, 10);
    INSERT INTO public.grade VALUES (177, '2', 18, NULL, 20);
    INSERT INTO public.grade VALUES (178, '3', 18, NULL, 30);
    INSERT INTO public.grade VALUES (179, '4', 18, NULL, 40);
    INSERT INTO public.grade VALUES (180, '5', 18, NULL, 50);
    INSERT INTO public.grade VALUES (181, '6', 18, NULL, 60);
    INSERT INTO public.grade VALUES (182, '7', 18, NULL, 70);
    INSERT INTO public.grade VALUES (183, '8', 18, NULL, 80);
    INSERT INTO public.grade VALUES (184, '9', 18, NULL, 90);
    INSERT INTO public.grade VALUES (185, '10', 18, NULL, 100);
    INSERT INTO public.grade VALUES (186, '11', 18, NULL, 110);
    INSERT INTO public.grade VALUES (187, '12', 18, NULL, 120);
    INSERT INTO public.grade VALUES (188, '13', 18, NULL, 130);
    INSERT INTO public.grade VALUES (189, '14', 18, NULL, 140);
    INSERT INTO public.grade VALUES (190, '15', 18, NULL, 150);
    INSERT INTO public.grade VALUES (191, '16', 18, NULL, 160);
    INSERT INTO public.grade VALUES (192, '17', 18, NULL, 170);
    INSERT INTO public.grade VALUES (193, '18', 18, NULL, 180);
    INSERT INTO public.grade VALUES (194, '19', 18, NULL, 190);
    INSERT INTO public.grade VALUES (195, '20', 18, NULL, 200);
    INSERT INTO public.grade VALUES (196, 'EX1', 18, NULL, 210);
    INSERT INTO public.grade VALUES (197, 'EX2', 18, NULL, 220);
    INSERT INTO public.grade VALUES (198, 'EX3', 18, NULL, 230);
    INSERT INTO public.grade VALUES (199, 'EX4', 18, NULL, 240);
    INSERT INTO public.grade VALUES (200, 'EX5', 18, NULL, 250);
    INSERT INTO public.grade VALUES (201, 'EX6', 18, NULL, 260);
    INSERT INTO public.grade VALUES (202, 'EX7', 18, NULL, 270);
    INSERT INTO public.grade VALUES (203, 'ALL', 18, NULL, 280);
    INSERT INTO public.grade VALUES (204, '1', 23, NULL, 10);
    INSERT INTO public.grade VALUES (205, '2', 23, NULL, 20);
    INSERT INTO public.grade VALUES (206, '3', 23, NULL, 30);
    INSERT INTO public.grade VALUES (207, '4', 23, NULL, 40);
    INSERT INTO public.grade VALUES (208, '5', 23, NULL, 50);
    INSERT INTO public.grade VALUES (209, '6', 23, NULL, 60);
    INSERT INTO public.grade VALUES (210, '7', 23, NULL, 70);
    INSERT INTO public.grade VALUES (211, '8', 23, NULL, 80);
    INSERT INTO public.grade VALUES (212, '9', 23, NULL, 90);
    INSERT INTO public.grade VALUES (213, '10', 23, NULL, 100);
    INSERT INTO public.grade VALUES (214, '11', 23, NULL, 110);
    INSERT INTO public.grade VALUES (215, '12', 23, NULL, 120);
    INSERT INTO public.grade VALUES (216, '13', 23, NULL, 130);
    INSERT INTO public.grade VALUES (217, '14', 23, NULL, 140);
    INSERT INTO public.grade VALUES (218, '15', 23, NULL, 150);
    INSERT INTO public.grade VALUES (219, '16', 23, NULL, 160);
    INSERT INTO public.grade VALUES (220, '17', 23, NULL, 170);
    INSERT INTO public.grade VALUES (221, '18', 23, NULL, 180);
    INSERT INTO public.grade VALUES (222, '19', 23, NULL, 190);
    INSERT INTO public.grade VALUES (223, '20', 23, NULL, 200);
    INSERT INTO public.grade VALUES (224, 'EX1', 23, NULL, 210);
    INSERT INTO public.grade VALUES (225, 'EX2', 23, NULL, 220);
    INSERT INTO public.grade VALUES (226, 'EX3', 23, NULL, 230);
    INSERT INTO public.grade VALUES (227, 'EX4', 23, NULL, 240);
    INSERT INTO public.grade VALUES (228, 'EX5', 23, NULL, 250);
    INSERT INTO public.grade VALUES (229, 'EX6', 23, NULL, 260);
    INSERT INTO public.grade VALUES (230, 'EX7', 23, NULL, 270);
    INSERT INTO public.grade VALUES (231, 'ALL', 23, NULL, 280);
    INSERT INTO public.grade VALUES (232, '9', 15, NULL, 10);
    INSERT INTO public.grade VALUES (233, '8', 15, NULL, 20);
    INSERT INTO public.grade VALUES (234, '7', 15, NULL, 30);
    INSERT INTO public.grade VALUES (235, '6', 15, NULL, 40);
    INSERT INTO public.grade VALUES (236, '5', 15, NULL, 50);
    INSERT INTO public.grade VALUES (237, '4', 15, NULL, 60);
    INSERT INTO public.grade VALUES (238, '3', 15, NULL, 70);
    INSERT INTO public.grade VALUES (239, '2', 15, NULL, 80);
    INSERT INTO public.grade VALUES (240, '1', 15, NULL, 90);
    INSERT INTO public.grade VALUES (241, 'S1', 15, NULL, 100);
    INSERT INTO public.grade VALUES (242, 'S2', 15, NULL, 110);
    INSERT INTO public.grade VALUES (243, 'S3', 15, NULL, 120);
    INSERT INTO public.grade VALUES (244, 'S4', 15, NULL, 130);
    INSERT INTO public.grade VALUES (245, 'S5', 15, NULL, 140);
    INSERT INTO public.grade VALUES (246, 'S6', 15, NULL, 150);
    INSERT INTO public.grade VALUES (247, 'S7', 15, NULL, 160);
    INSERT INTO public.grade VALUES (248, 'S8', 15, NULL, 170);
    INSERT INTO public.grade VALUES (249, 'S9', 15, NULL, 180);
    INSERT INTO public.grade VALUES (250, 'm1', 15, NULL, 190);
    INSERT INTO public.grade VALUES (251, 'm2', 15, NULL, 200);
    INSERT INTO public.grade VALUES (252, 'm3', 15, NULL, 210);
    INSERT INTO public.grade VALUES (253, 'm4', 15, NULL, 220);
    INSERT INTO public.grade VALUES (254, 'm5', 15, NULL, 230);
    INSERT INTO public.grade VALUES (255, 'm6', 15, NULL, 240);
    INSERT INTO public.grade VALUES (256, 'm7', 15, NULL, 250);
    INSERT INTO public.grade VALUES (257, 'm8', 15, NULL, 260);
    INSERT INTO public.grade VALUES (258, 'm9', 15, NULL, 270);
    INSERT INTO public.grade VALUES (259, 'M', 15, NULL, 280);
    INSERT INTO public.grade VALUES (260, 'MK', 15, NULL, 290);
    INSERT INTO public.grade VALUES (261, 'MV', 15, NULL, 300);
    INSERT INTO public.grade VALUES (262, 'MO', 15, NULL, 310);
    INSERT INTO public.grade VALUES (263, 'MM', 15, NULL, 320);
    INSERT INTO public.grade VALUES (264, 'GM', 15, NULL, 330);
    INSERT INTO public.grade VALUES (265, '9', 20, NULL, 10);
    INSERT INTO public.grade VALUES (266, '8', 20, NULL, 20);
    INSERT INTO public.grade VALUES (267, '7', 20, NULL, 30);
    INSERT INTO public.grade VALUES (268, '6', 20, NULL, 40);
    INSERT INTO public.grade VALUES (269, '5', 20, NULL, 50);
    INSERT INTO public.grade VALUES (270, '4', 20, NULL, 60);
    INSERT INTO public.grade VALUES (271, '3', 20, NULL, 70);
    INSERT INTO public.grade VALUES (272, '2', 20, NULL, 80);
    INSERT INTO public.grade VALUES (273, '1', 20, NULL, 90);
    INSERT INTO public.grade VALUES (274, 'S1', 20, NULL, 100);
    INSERT INTO public.grade VALUES (275, 'S2', 20, NULL, 110);
    INSERT INTO public.grade VALUES (276, 'S3', 20, NULL, 120);
    INSERT INTO public.grade VALUES (277, 'S4', 20, NULL, 130);
    INSERT INTO public.grade VALUES (278, 'S5', 20, NULL, 140);
    INSERT INTO public.grade VALUES (279, 'S6', 20, NULL, 150);
    INSERT INTO public.grade VALUES (280, 'S7', 20, NULL, 160);
    INSERT INTO public.grade VALUES (281, 'S8', 20, NULL, 170);
    INSERT INTO public.grade VALUES (282, 'S9', 20, NULL, 180);
    INSERT INTO public.grade VALUES (283, 'm1', 20, NULL, 190);
    INSERT INTO public.grade VALUES (284, 'm2', 20, NULL, 200);
    INSERT INTO public.grade VALUES (285, 'm3', 20, NULL, 210);
    INSERT INTO public.grade VALUES (286, 'm4', 20, NULL, 220);
    INSERT INTO public.grade VALUES (287, 'm5', 20, NULL, 230);
    INSERT INTO public.grade VALUES (288, 'm6', 20, NULL, 240);
    INSERT INTO public.grade VALUES (289, 'm7', 20, NULL, 250);
    INSERT INTO public.grade VALUES (290, 'm8', 20, NULL, 260);
    INSERT INTO public.grade VALUES (291, 'm9', 20, NULL, 270);
    INSERT INTO public.grade VALUES (292, 'M', 20, NULL, 280);
    INSERT INTO public.grade VALUES (293, 'MK', 20, NULL, 290);
    INSERT INTO public.grade VALUES (294, 'MV', 20, NULL, 300);
    INSERT INTO public.grade VALUES (295, 'MO', 20, NULL, 310);
    INSERT INTO public.grade VALUES (296, 'MM', 20, NULL, 320);
    INSERT INTO public.grade VALUES (297, 'GM', 20, NULL, 330);
    INSERT INTO public.grade VALUES (298, '—', 16, NULL, 10);
    INSERT INTO public.grade VALUES (299, 'S1', 16, NULL, 20);
    INSERT INTO public.grade VALUES (300, 'S2', 16, NULL, 30);
    INSERT INTO public.grade VALUES (301, 'S3', 16, NULL, 40);
    INSERT INTO public.grade VALUES (302, 'S4', 16, NULL, 50);
    INSERT INTO public.grade VALUES (303, 'S5', 16, NULL, 60);
    INSERT INTO public.grade VALUES (304, 'S6', 16, NULL, 70);
    INSERT INTO public.grade VALUES (305, 'S7', 16, NULL, 80);
    INSERT INTO public.grade VALUES (306, 'S8', 16, NULL, 90);
    INSERT INTO public.grade VALUES (307, 'S9', 16, NULL, 100);
    INSERT INTO public.grade VALUES (308, 'S10', 16, NULL, 110);
    INSERT INTO public.grade VALUES (309, 'S11', 16, NULL, 120);
    INSERT INTO public.grade VALUES (310, 'S12', 16, NULL, 130);
    INSERT INTO public.grade VALUES (311, 'S13', 16, 'green', 140);
    INSERT INTO public.grade VALUES (312, 'S13', 16, 'orange', 150);
    INSERT INTO public.grade VALUES (313, '—', 21, NULL, 10);
    INSERT INTO public.grade VALUES (314, 'S1', 21, NULL, 20);
    INSERT INTO public.grade VALUES (315, 'S2', 21, NULL, 30);
    INSERT INTO public.grade VALUES (316, 'S3', 21, NULL, 40);
    INSERT INTO public.grade VALUES (317, 'S4', 21, NULL, 50);
    INSERT INTO public.grade VALUES (318, 'S5', 21, NULL, 60);
    INSERT INTO public.grade VALUES (319, 'S6', 21, NULL, 70);
    INSERT INTO public.grade VALUES (320, 'S7', 21, NULL, 80);
    INSERT INTO public.grade VALUES (321, 'S8', 21, NULL, 90);
    INSERT INTO public.grade VALUES (322, 'S9', 21, NULL, 100);
    INSERT INTO public.grade VALUES (323, 'S10', 21, NULL, 110);
    INSERT INTO public.grade VALUES (324, 'S11', 21, NULL, 120);
    INSERT INTO public.grade VALUES (325, 'S12', 21, NULL, 130);
    INSERT INTO public.grade VALUES (326, 'S13', 21, 'green', 140);
    INSERT INTO public.grade VALUES (327, 'S13', 21, 'orange', 150);
    INSERT INTO public.grade VALUES (328, '9', 19, NULL, 10);
    INSERT INTO public.grade VALUES (329, '8', 19, NULL, 20);
    INSERT INTO public.grade VALUES (330, '7', 19, NULL, 30);
    INSERT INTO public.grade VALUES (331, '6', 19, NULL, 40);
    INSERT INTO public.grade VALUES (332, '5', 19, NULL, 50);
    INSERT INTO public.grade VALUES (333, '4', 19, NULL, 60);
    INSERT INTO public.grade VALUES (334, '3', 19, NULL, 70);
    INSERT INTO public.grade VALUES (335, '2', 19, NULL, 80);
    INSERT INTO public.grade VALUES (336, '1', 19, NULL, 90);
    INSERT INTO public.grade VALUES (337, 'S1', 19, NULL, 100);
    INSERT INTO public.grade VALUES (338, 'S2', 19, NULL, 110);
    INSERT INTO public.grade VALUES (339, 'S3', 19, NULL, 120);
    INSERT INTO public.grade VALUES (340, 'S4', 19, NULL, 130);
    INSERT INTO public.grade VALUES (341, 'S5', 19, NULL, 140);
    INSERT INTO public.grade VALUES (342, 'S6', 19, NULL, 150);
    INSERT INTO public.grade VALUES (343, 'S7', 19, NULL, 160);
    INSERT INTO public.grade VALUES (344, 'S8', 19, NULL, 170);
    INSERT INTO public.grade VALUES (345, 'S9', 19, NULL, 180);
    INSERT INTO public.grade VALUES (346, 'GM', 19, NULL, 190);
    INSERT INTO public.grade VALUES (347, '9', 24, NULL, 10);
    INSERT INTO public.grade VALUES (348, '8', 24, NULL, 20);
    INSERT INTO public.grade VALUES (349, '7', 24, NULL, 30);
    INSERT INTO public.grade VALUES (350, '6', 24, NULL, 40);
    INSERT INTO public.grade VALUES (351, '5', 24, NULL, 50);
    INSERT INTO public.grade VALUES (352, '4', 24, NULL, 60);
    INSERT INTO public.grade VALUES (353, '3', 24, NULL, 70);
    INSERT INTO public.grade VALUES (354, '2', 24, NULL, 80);
    INSERT INTO public.grade VALUES (355, '1', 24, NULL, 90);
    INSERT INTO public.grade VALUES (356, 'S1', 24, NULL, 100);
    INSERT INTO public.grade VALUES (357, 'S2', 24, NULL, 110);
    INSERT INTO public.grade VALUES (358, 'S3', 24, NULL, 120);
    INSERT INTO public.grade VALUES (359, 'S4', 24, NULL, 130);
    INSERT INTO public.grade VALUES (360, 'S5', 24, NULL, 140);
    INSERT INTO public.grade VALUES (361, 'S6', 24, NULL, 150);
    INSERT INTO public.grade VALUES (362, 'S7', 24, NULL, 160);
    INSERT INTO public.grade VALUES (363, 'S8', 24, NULL, 170);
    INSERT INTO public.grade VALUES (364, 'S9', 24, NULL, 180);
    INSERT INTO public.grade VALUES (365, 'GM', 24, NULL, 190);
    INSERT INTO public.grade VALUES (366, 'S1', 6, 'green', 101);
    INSERT INTO public.grade VALUES (367, 'S1', 6, 'orange', 102);
    INSERT INTO public.grade VALUES (368, 'S2', 6, 'green', 111);
    INSERT INTO public.grade VALUES (369, 'S2', 6, 'orange', 112);
    INSERT INTO public.grade VALUES (370, 'S3', 6, 'green', 121);
    INSERT INTO public.grade VALUES (371, 'S3', 6, 'orange', 122);
    INSERT INTO public.grade VALUES (372, 'S4', 6, 'green', 131);
    INSERT INTO public.grade VALUES (373, 'S4', 6, 'orange', 132);
    INSERT INTO public.grade VALUES (374, 'S5', 6, 'green', 141);
    INSERT INTO public.grade VALUES (375, 'S5', 6, 'orange', 142);
    INSERT INTO public.grade VALUES (376, 'S6', 6, 'green', 151);
    INSERT INTO public.grade VALUES (377, 'S6', 6, 'orange', 152);
    INSERT INTO public.grade VALUES (378, 'S7', 6, 'green', 161);
    INSERT INTO public.grade VALUES (379, 'S7', 6, 'orange', 162);
    INSERT INTO public.grade VALUES (380, 'S8', 6, 'green', 171);
    INSERT INTO public.grade VALUES (381, 'S8', 6, 'orange', 172);
    INSERT INTO public.grade VALUES (382, 'S9', 6, 'green', 185);
    INSERT INTO public.grade VALUES (383, '9', 25, NULL, 10);
    INSERT INTO public.grade VALUES (384, '8', 25, NULL, 20);
    INSERT INTO public.grade VALUES (385, '7', 25, NULL, 30);
    INSERT INTO public.grade VALUES (386, '6', 25, NULL, 40);
    INSERT INTO public.grade VALUES (387, '5', 25, NULL, 50);
    INSERT INTO public.grade VALUES (388, '4', 25, NULL, 60);
    INSERT INTO public.grade VALUES (389, '3', 25, NULL, 70);
    INSERT INTO public.grade VALUES (390, '2', 25, NULL, 80);
    INSERT INTO public.grade VALUES (391, '1', 25, NULL, 90);
    INSERT INTO public.grade VALUES (392, 'S1', 25, NULL, 100);
    INSERT INTO public.grade VALUES (393, 'S2', 25, NULL, 110);
    INSERT INTO public.grade VALUES (394, 'S3', 25, NULL, 120);
    INSERT INTO public.grade VALUES (395, 'S4', 25, NULL, 130);
    INSERT INTO public.grade VALUES (396, 'S5', 25, NULL, 140);
    INSERT INTO public.grade VALUES (397, 'S6', 25, NULL, 150);
    INSERT INTO public.grade VALUES (398, 'S7', 25, NULL, 160);
    INSERT INTO public.grade VALUES (399, 'S8', 25, NULL, 170);
    INSERT INTO public.grade VALUES (400, 'S9', 25, NULL, 180);
    INSERT INTO public.grade VALUES (401, 'm1', 25, NULL, 190);
    INSERT INTO public.grade VALUES (402, 'm2', 25, NULL, 200);
    INSERT INTO public.grade VALUES (403, 'm3', 25, NULL, 210);
    INSERT INTO public.grade VALUES (404, 'm4', 25, NULL, 220);
    INSERT INTO public.grade VALUES (405, 'm5', 25, NULL, 230);
    INSERT INTO public.grade VALUES (406, 'm6', 25, NULL, 240);
    INSERT INTO public.grade VALUES (407, 'm7', 25, NULL, 250);
    INSERT INTO public.grade VALUES (408, 'm8', 25, NULL, 260);
    INSERT INTO public.grade VALUES (409, 'm9', 25, NULL, 270);
    INSERT INTO public.grade VALUES (410, 'M', 25, NULL, 280);
    INSERT INTO public.grade VALUES (411, 'MK', 25, NULL, 290);
    INSERT INTO public.grade VALUES (412, 'MV', 25, NULL, 300);
    INSERT INTO public.grade VALUES (413, 'MO', 25, NULL, 310);
    INSERT INTO public.grade VALUES (414, 'MM', 25, NULL, 320);
    INSERT INTO public.grade VALUES (415, '9', 26, NULL, 10);
    INSERT INTO public.grade VALUES (416, '8', 26, NULL, 20);
    INSERT INTO public.grade VALUES (417, '7', 26, NULL, 30);
    INSERT INTO public.grade VALUES (418, '6', 26, NULL, 40);
    INSERT INTO public.grade VALUES (419, '5', 26, NULL, 50);
    INSERT INTO public.grade VALUES (420, '4', 26, NULL, 60);
    INSERT INTO public.grade VALUES (421, '3', 26, NULL, 70);
    INSERT INTO public.grade VALUES (422, '2', 26, NULL, 80);
    INSERT INTO public.grade VALUES (423, '1', 26, NULL, 90);
    INSERT INTO public.grade VALUES (424, 'S1', 26, NULL, 100);
    INSERT INTO public.grade VALUES (425, 'S2', 26, NULL, 110);
    INSERT INTO public.grade VALUES (426, 'S3', 26, NULL, 120);
    INSERT INTO public.grade VALUES (427, 'S4', 26, NULL, 130);
    INSERT INTO public.grade VALUES (428, 'S5', 26, NULL, 140);
    INSERT INTO public.grade VALUES (429, 'S6', 26, NULL, 150);
    INSERT INTO public.grade VALUES (430, 'S7', 26, NULL, 160);
    INSERT INTO public.grade VALUES (431, 'S8', 26, NULL, 170);
    INSERT INTO public.grade VALUES (432, 'S9', 26, NULL, 180);
    INSERT INTO public.grade VALUES (433, 'm1', 26, NULL, 190);
    INSERT INTO public.grade VALUES (434, 'm2', 26, NULL, 200);
    INSERT INTO public.grade VALUES (435, 'm3', 26, NULL, 210);
    INSERT INTO public.grade VALUES (436, 'm4', 26, NULL, 220);
    INSERT INTO public.grade VALUES (437, 'm5', 26, NULL, 230);
    INSERT INTO public.grade VALUES (438, 'm6', 26, NULL, 240);
    INSERT INTO public.grade VALUES (439, 'm7', 26, NULL, 250);
    INSERT INTO public.grade VALUES (440, 'm8', 26, NULL, 260);
    INSERT INTO public.grade VALUES (441, 'm9', 26, NULL, 270);
    INSERT INTO public.grade VALUES (442, 'M', 26, NULL, 280);
    INSERT INTO public.grade VALUES (443, 'MK', 26, NULL, 290);
    INSERT INTO public.grade VALUES (444, 'MV', 26, NULL, 300);
    INSERT INTO public.grade VALUES (445, 'MO', 26, NULL, 310);
    INSERT INTO public.grade VALUES (446, 'MM', 26, NULL, 320);
    INSERT INTO public.grade VALUES (447, 'S1', 27, NULL, 10);
    INSERT INTO public.grade VALUES (448, 'S2', 27, NULL, 20);
    INSERT INTO public.grade VALUES (449, 'S3', 27, NULL, 30);
    INSERT INTO public.grade VALUES (450, 'S4', 27, NULL, 40);
    INSERT INTO public.grade VALUES (451, 'S5', 27, NULL, 50);
    INSERT INTO public.grade VALUES (452, 'S6', 27, NULL, 60);
    INSERT INTO public.grade VALUES (453, 'S7', 27, NULL, 70);
    INSERT INTO public.grade VALUES (454, 'S8', 27, NULL, 80);
    INSERT INTO public.grade VALUES (455, 'S9', 27, NULL, 90);
    INSERT INTO public.grade VALUES (456, 'm1', 27, NULL, 100);
    INSERT INTO public.grade VALUES (457, 'm2', 27, NULL, 110);
    INSERT INTO public.grade VALUES (458, 'm3', 27, NULL, 120);
    INSERT INTO public.grade VALUES (459, 'm4', 27, NULL, 130);
    INSERT INTO public.grade VALUES (460, 'm5', 27, NULL, 140);
    INSERT INTO public.grade VALUES (461, 'm6', 27, NULL, 150);
    INSERT INTO public.grade VALUES (462, 'm7', 27, NULL, 160);
    INSERT INTO public.grade VALUES (463, 'm8', 27, NULL, 170);
    INSERT INTO public.grade VALUES (464, 'm9', 27, NULL, 180);
    INSERT INTO public.grade VALUES (465, 'GM', 27, NULL, 190);
    INSERT INTO public.grade VALUES (466, 'S1', 28, NULL, 10);
    INSERT INTO public.grade VALUES (467, 'S2', 28, NULL, 20);
    INSERT INTO public.grade VALUES (468, 'S3', 28, NULL, 30);
    INSERT INTO public.grade VALUES (469, 'S4', 28, NULL, 40);
    INSERT INTO public.grade VALUES (470, 'S5', 28, NULL, 50);
    INSERT INTO public.grade VALUES (471, 'S6', 28, NULL, 60);
    INSERT INTO public.grade VALUES (472, 'S7', 28, NULL, 70);
    INSERT INTO public.grade VALUES (473, 'S8', 28, NULL, 80);
    INSERT INTO public.grade VALUES (474, 'S9', 28, NULL, 90);
    INSERT INTO public.grade VALUES (475, 'm1', 28, NULL, 100);
    INSERT INTO public.grade VALUES (476, 'm2', 28, NULL, 110);
    INSERT INTO public.grade VALUES (477, 'm3', 28, NULL, 120);
    INSERT INTO public.grade VALUES (478, 'm4', 28, NULL, 130);
    INSERT INTO public.grade VALUES (479, 'm5', 28, NULL, 140);
    INSERT INTO public.grade VALUES (480, 'm6', 28, NULL, 150);
    INSERT INTO public.grade VALUES (481, 'm7', 28, NULL, 160);
    INSERT INTO public.grade VALUES (482, 'm8', 28, NULL, 170);
    INSERT INTO public.grade VALUES (483, 'm9', 28, NULL, 180);
    INSERT INTO public.grade VALUES (484, 'GM', 28, NULL, 190);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(517, 'GM', 30, NULL, 330);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(516, 'MM', 30, NULL, 320);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(515, 'MO', 30, NULL, 310);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(514, 'MV', 30, NULL, 300);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(513, 'MK', 30, NULL, 290);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(512, 'M', 30, NULL, 280);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(511, 'm9', 30, NULL, 270);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(510, 'm8', 30, NULL, 260);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(509, 'm7', 30, NULL, 250);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(508, 'm6', 30, NULL, 240);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(507, 'm5', 30, NULL, 230);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(506, 'm4', 30, NULL, 220);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(505, 'm3', 30, NULL, 210);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(504, 'm2', 30, NULL, 200);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(503, 'm1', 30, NULL, 190);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(502, 'S9', 30, NULL, 180);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(501, 'S8', 30, NULL, 170);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(500, 'S7', 30, NULL, 160);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(499, 'S6', 30, NULL, 150);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(498, 'S5', 30, NULL, 140);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(497, 'S4', 30, NULL, 130);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(496, 'S3', 30, NULL, 120);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(495, 'S2', 30, NULL, 110);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(494, 'S1', 30, NULL, 100);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(493, '1', 30, NULL, 90);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(492, '2', 30, NULL, 80);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(491, '3', 30, NULL, 70);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(490, '4', 30, NULL, 60);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(489, '5', 30, NULL, 50);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(488, '6', 30, NULL, 40);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(487, '7', 30, NULL, 30);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(486, '8', 30, NULL, 20);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(485, '9', 30, NULL, 10);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(550, 'GM', 31, NULL, 330);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(549, 'MM', 31, NULL, 320);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(548, 'MO', 31, NULL, 310);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(547, 'MV', 31, NULL, 300);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(546, 'MK', 31, NULL, 290);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(545, 'M', 31, NULL, 280);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(544, 'm9', 31, NULL, 270);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(543, 'm8', 31, NULL, 260);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(542, 'm7', 31, NULL, 250);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(541, 'm6', 31, NULL, 240);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(540, 'm5', 31, NULL, 230);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(539, 'm4', 31, NULL, 220);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(538, 'm3', 31, NULL, 210);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(537, 'm2', 31, NULL, 200);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(536, 'm1', 31, NULL, 190);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(535, 'S9', 31, NULL, 180);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(534, 'S8', 31, NULL, 170);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(533, 'S7', 31, NULL, 160);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(532, 'S6', 31, NULL, 150);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(531, 'S5', 31, NULL, 140);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(530, 'S4', 31, NULL, 130);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(529, 'S3', 31, NULL, 120);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(528, 'S2', 31, NULL, 110);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(527, 'S1', 31, NULL, 100);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(526, '1', 31, NULL, 90);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(525, '2', 31, NULL, 80);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(524, '3', 31, NULL, 70);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(523, '4', 31, NULL, 60);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(522, '5', 31, NULL, 50);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(521, '6', 31, NULL, 40);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(520, '7', 31, NULL, 30);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(519, '8', 31, NULL, 20);
    INSERT INTO public.grade
    (grade_id, grade_display, mode_id, line, sort_order)
    VALUES(518, '9', 31, NULL, 10);


    PERFORM setval(pg_get_serial_sequence('public.game', 'game_id'), (select max(game_id) from public.game));
    PERFORM setval(pg_get_serial_sequence('public.mode', 'mode_id'), 200);
    PERFORM setval(pg_get_serial_sequence('public.grade', 'grade_id'), (select max(grade_id) from public.grade));

	INSERT INTO public.location (num_code, name, alpha_2, alpha_3) VALUES
        (4,'Afghanistan','af','afg'),
        (248,'Åland Islands','ax','ala'),
        (8,'Albania','al','alb'),
        (12,'Algeria','dz','dza'),
        (16,'American Samoa','as','asm'),
        (20,'Andorra','ad','and'),
        (24,'Angola','ao','ago'),
        (660,'Anguilla','ai','aia'),
        (10,'Antarctica','aq','ata'),
        (28,'Antigua and Barbuda','ag','atg'),
        (32,'Argentina','ar','arg'),
        (51,'Armenia','am','arm'),
        (533,'Aruba','aw','abw'),
        (36,'Australia','au','aus'),
        (40,'Austria','at','aut'),
        (31,'Azerbaijan','az','aze'),
        (44,'Bahamas','bs','bhs'),
        (48,'Bahrain','bh','bhr'),
        (50,'Bangladesh','bd','bgd'),
        (52,'Barbados','bb','brb'),
        (112,'Belarus','by','blr'),
        (56,'Belgium','be','bel'),
        (84,'Belize','bz','blz'),
        (204,'Benin','bj','ben'),
        (60,'Bermuda','bm','bmu'),
        (64,'Bhutan','bt','btn'),
        (68,'Bolivia (Plurinational State of)','bo','bol'),
        (535,'Bonaire, Sint Eustatius and Saba','bq','bes'),
        (70,'Bosnia and Herzegovina','ba','bih'),
        (72,'Botswana','bw','bwa'),
        (74,'Bouvet Island','bv','bvt'),
        (76,'Brazil','br','bra'),
        (86,'British Indian Ocean Territory','io','iot'),
        (96,'Brunei Darussalam','bn','brn'),
        (100,'Bulgaria','bg','bgr'),
        (854,'Burkina Faso','bf','bfa'),
        (108,'Burundi','bi','bdi'),
        (132,'Cabo Verde','cv','cpv'),
        (116,'Cambodia','kh','khm'),
        (120,'Cameroon','cm','cmr'),
        (124,'Canada','ca','can'),
        (136,'Cayman Islands','ky','cym'),
        (140,'Central African Republic','cf','caf'),
        (148,'Chad','td','tcd'),
        (152,'Chile','cl','chl'),
        (156,'China','cn','chn'),
        (162,'Christmas Island','cx','cxr'),
        (166,'Cocos (Keeling) Islands','cc','cck'),
        (170,'Colombia','co','col'),
        (174,'Comoros','km','com'),
        (178,'Congo','cg','cog'),
        (180,'Congo, Democratic Republic of the','cd','cod'),
        (184,'Cook Islands','ck','cok'),
        (188,'Costa Rica','cr','cri'),
        (384,'Côte d''Ivoire','ci','civ'),
        (191,'Croatia','hr','hrv'),
        (192,'Cuba','cu','cub'),
        (531,'Curaçao','cw','cuw'),
        (196,'Cyprus','cy','cyp'),
        (203,'Czechia','cz','cze'),
        (208,'Denmark','dk','dnk'),
        (262,'Djibouti','dj','dji'),
        (212,'Dominica','dm','dma'),
        (214,'Dominican Republic','do','dom'),
        (218,'Ecuador','ec','ecu'),
        (818,'Egypt','eg','egy'),
        (222,'El Salvador','sv','slv'),
        (226,'Equatorial Guinea','gq','gnq'),
        (232,'Eritrea','er','eri'),
        (233,'Estonia','ee','est'),
        (748,'Eswatini','sz','swz'),
        (231,'Ethiopia','et','eth'),
        (238,'Falkland Islands (Malvinas)','fk','flk'),
        (234,'Faroe Islands','fo','fro'),
        (242,'Fiji','fj','fji'),
        (246,'Finland','fi','fin'),
        (250,'France','fr','fra'),
        (254,'French Guiana','gf','guf'),
        (258,'French Polynesia','pf','pyf'),
        (260,'French Southern Territories','tf','atf'),
        (266,'Gabon','ga','gab'),
        (270,'Gambia','gm','gmb'),
        (268,'Georgia','ge','geo'),
        (276,'Germany','de','deu'),
        (288,'Ghana','gh','gha'),
        (292,'Gibraltar','gi','gib'),
        (300,'Greece','gr','grc'),
        (304,'Greenland','gl','grl'),
        (308,'Grenada','gd','grd'),
        (312,'Guadeloupe','gp','glp'),
        (316,'Guam','gu','gum'),
        (320,'Guatemala','gt','gtm'),
        (831,'Guernsey','gg','ggy'),
        (324,'Guinea','gn','gin'),
        (624,'Guinea-Bissau','gw','gnb'),
        (328,'Guyana','gy','guy'),
        (332,'Haiti','ht','hti'),
        (334,'Heard Island and McDonald Islands','hm','hmd'),
        (336,'Holy See','va','vat'),
        (340,'Honduras','hn','hnd'),
        (344,'Hong Kong','hk','hkg'),
        (348,'Hungary','hu','hun'),
        (352,'Iceland','is','isl'),
        (356,'India','in','ind'),
        (360,'Indonesia','id','idn'),
        (364,'Iran (Islamic Republic of)','ir','irn'),
        (368,'Iraq','iq','irq'),
        (372,'Ireland','ie','irl'),
        (833,'Isle of Man','im','imn'),
        (376,'Israel','il','isr'),
        (380,'Italy','it','ita'),
        (388,'Jamaica','jm','jam'),
        (392,'Japan','jp','jpn'),
        (832,'Jersey','je','jey'),
        (400,'Jordan','jo','jor'),
        (398,'Kazakhstan','kz','kaz'),
        (404,'Kenya','ke','ken'),
        (296,'Kiribati','ki','kir'),
        (408,'Korea (Democratic People''s Republic of)','kp','prk'),
        (410,'Korea, Republic of','kr','kor'),
        (414,'Kuwait','kw','kwt'),
        (417,'Kyrgyzstan','kg','kgz'),
        (418,'Lao People''s Democratic Republic','la','lao'),
        (428,'Latvia','lv','lva'),
        (422,'Lebanon','lb','lbn'),
        (426,'Lesotho','ls','lso'),
        (430,'Liberia','lr','lbr'),
        (434,'Libya','ly','lby'),
        (438,'Liechtenstein','li','lie'),
        (440,'Lithuania','lt','ltu'),
        (442,'Luxembourg','lu','lux'),
        (446,'Macao','mo','mac'),
        (450,'Madagascar','mg','mdg'),
        (454,'Malawi','mw','mwi'),
        (458,'Malaysia','my','mys'),
        (462,'Maldives','mv','mdv'),
        (466,'Mali','ml','mli'),
        (470,'Malta','mt','mlt'),
        (584,'Marshall Islands','mh','mhl'),
        (474,'Martinique','mq','mtq'),
        (478,'Mauritania','mr','mrt'),
        (480,'Mauritius','mu','mus'),
        (175,'Mayotte','yt','myt'),
        (484,'Mexico','mx','mex'),
        (583,'Micronesia (Federated States of)','fm','fsm'),
        (498,'Moldova, Republic of','md','mda'),
        (492,'Monaco','mc','mco'),
        (496,'Mongolia','mn','mng'),
        (499,'Montenegro','me','mne'),
        (500,'Montserrat','ms','msr'),
        (504,'Morocco','ma','mar'),
        (508,'Mozambique','mz','moz'),
        (104,'Myanmar','mm','mmr'),
        (516,'Namibia','na','nam'),
        (520,'Nauru','nr','nru'),
        (524,'Nepal','np','npl'),
        (528,'Netherlands','nl','nld'),
        (540,'New Caledonia','nc','ncl'),
        (554,'New Zealand','nz','nzl'),
        (558,'Nicaragua','ni','nic'),
        (562,'Niger','ne','ner'),
        (566,'Nigeria','ng','nga'),
        (570,'Niue','nu','niu'),
        (574,'Norfolk Island','nf','nfk'),
        (807,'North Macedonia','mk','mkd'),
        (580,'Northern Mariana Islands','mp','mnp'),
        (578,'Norway','no','nor'),
        (512,'Oman','om','omn'),
        (586,'Pakistan','pk','pak'),
        (585,'Palau','pw','plw'),
        (275,'Palestine, State of','ps','pse'),
        (591,'Panama','pa','pan'),
        (598,'Papua New Guinea','pg','png'),
        (600,'Paraguay','py','pry'),
        (604,'Peru','pe','per'),
        (608,'Philippines','ph','phl'),
        (612,'Pitcairn','pn','pcn'),
        (616,'Poland','pl','pol'),
        (620,'Portugal','pt','prt'),
        (630,'Puerto Rico','pr','pri'),
        (634,'Qatar','qa','qat'),
        (638,'Réunion','re','reu'),
        (642,'Romania','ro','rou'),
        (643,'Russian Federation','ru','rus'),
        (646,'Rwanda','rw','rwa'),
        (652,'Saint Barthélemy','bl','blm'),
        (654,'Saint Helena, Ascension and Tristan da Cunha','sh','shn'),
        (659,'Saint Kitts and Nevis','kn','kna'),
        (662,'Saint Lucia','lc','lca'),
        (663,'Saint Martin (French part)','mf','maf'),
        (666,'Saint Pierre and Miquelon','pm','spm'),
        (670,'Saint Vincent and the Grenadines','vc','vct'),
        (882,'Samoa','ws','wsm'),
        (674,'San Marino','sm','smr'),
        (678,'Sao Tome and Principe','st','stp'),
        (682,'Saudi Arabia','sa','sau'),
        (686,'Senegal','sn','sen'),
        (688,'Serbia','rs','srb'),
        (690,'Seychelles','sc','syc'),
        (694,'Sierra Leone','sl','sle'),
        (702,'Singapore','sg','sgp'),
        (534,'Sint Maarten (Dutch part)','sx','sxm'),
        (703,'Slovakia','sk','svk'),
        (705,'Slovenia','si','svn'),
        (90,'Solomon Islands','sb','slb'),
        (706,'Somalia','so','som'),
        (710,'South Africa','za','zaf'),
        (239,'South Georgia and the South Sandwich Islands','gs','sgs'),
        (728,'South Sudan','ss','ssd'),
        (724,'Spain','es','esp'),
        (144,'Sri Lanka','lk','lka'),
        (729,'Sudan','sd','sdn'),
        (740,'Suriname','sr','sur'),
        (744,'Svalbard and Jan Mayen','sj','sjm'),
        (752,'Sweden','se','swe'),
        (756,'Switzerland','ch','che'),
        (760,'Syrian Arab Republic','sy','syr'),
        (158,'Taiwan, Province of China','tw','twn'),
        (762,'Tajikistan','tj','tjk'),
        (834,'Tanzania, United Republic of','tz','tza'),
        (764,'Thailand','th','tha'),
        (626,'Timor-Leste','tl','tls'),
        (768,'Togo','tg','tgo'),
        (772,'Tokelau','tk','tkl'),
        (776,'Tonga','to','ton'),
        (780,'Trinidad and Tobago','tt','tto'),
        (788,'Tunisia','tn','tun'),
        (792,'Turkey','tr','tur'),
        (795,'Turkmenistan','tm','tkm'),
        (796,'Turks and Caicos Islands','tc','tca'),
        (798,'Tuvalu','tv','tuv'),
        (800,'Uganda','ug','uga'),
        (804,'Ukraine','ua','ukr'),
        (784,'United Arab Emirates','ae','are'),
        (826,'United Kingdom of Great Britain and Northern Ireland','gb','gbr'),
        (840,'United States of America','us','usa'),
        (581,'United States Minor Outlying Islands','um','umi'),
        (858,'Uruguay','uy','ury'),
        (860,'Uzbekistan','uz','uzb'),
        (548,'Vanuatu','vu','vut'),
        (862,'Venezuela (Bolivarian Republic of)','ve','ven'),
        (704,'Viet Nam','vn','vnm'),
        (92,'Virgin Islands (British)','vg','vgb'),
        (850,'Virgin Islands (U.S.)','vi','vir'),
        (876,'Wallis and Futuna','wf','wlf'),
        (732,'Western Sahara','eh','esh'),
        (887,'Yemen','ye','yem'),
        (894,'Zambia','zm','zmb'),
        (716,'Zimbabwe','zw','zwe');

    INSERT INTO public.platform (platform_name, game_id)
    VALUES
    ('Arcade (PCB)', 1),
    ('Emulator (MAME)', 1),
    ('Switch (ACA)', 1),
    ('PS4 (ACA)', 1),
    ('Arcade (PCB)', 2),
    ('Emulator (MAME)', 2),
    ('Emulator (FBNeo/Fightcade)', 2),
    ('Switch (ACA)', 2),
    ('PS4 (ACA)', 2);

    INSERT INTO public.achievement (achievement_id, description, game_id, mode_id, sort_order) VALUES
    (1, 'Get an S1 grade in TGM1 Master', 1, 1, 10),
    (2, 'Reach level 300 in TGM1 Master', 1, 1, 20),
    (4, 'Reach level 500 in TGM1 Master', 1, 1, 40),
    (5, 'Reach level 150 in TAP Death', 2, 7, 50),
    (7, 'Reach level 200 in TAP Death', 2, 7, 70),
    (8, 'Reach level 700 in TGM1 Master', 1, 1, 80),
    (9, 'Reach level 500 in TGM1 20G', 1, 2, 90),
    (10, 'Get an S7 grade in TGM1 Master', 1, 1, 100),
    (11, 'Reach level 300 in TAP Death', 2, 7, 110),
    (12, 'Reach level 999 in TGM1 Master', 1, 1, 120),
    (13, 'Complete the game with the GM grade in TGM1 Master ', 1, 1, 130),
    (14, 'Get an S7 grade in TAP Master', 2, 6, 140),
    (15, 'Complete the game with the GM grade in TGM1 20G', 1, 6, 150),
    (16, 'Reach level 400 in TAP Death', 2, 7, 160),
    (17, 'Reach level 999 in TAP Master', 2, 6, 170),
    (18, 'Clear the fading credit roll (or better) in TAP Master', 2, 6, 180),
    (19, 'Get an S9 grade in TAP Master', 2, 6, 190),
    (20, 'Reach level 500 in TAP Death', 2, 7, 200),
    (21, 'Get the M grade in TAP Death', 2, 7, 210),
    (22, 'Reach level 700 in TAP Death', 2, 7, 220),
    (23, 'Get the GM grade in TAP Death', 2, 7, 230),
    (24, 'Survive the credit roll and get GM in TAP Master', 2, 6, 240),
    (25, 'Survive the credit roll and get Orange Line GM in TAP Master', 2, 6, 250),
    (3, 'Get more than 150,000 points in TAP Normal mode', 2, 12, 30),
    (26, 'Get the M grade (or better) in TAP Master', 2, 6, 225),
    (27, 'Get 400 or more Hanabi in TI Easy', 3, 17, 300),
    (28, 'Become Qualified 3 in TI Master', 3, 30, 310),
    (29, 'Reach level 500 in TI Master', 3, 15, 320),
    (30, 'Become Qualified S1 in TI Master', 3, 30, 330),
    (31, 'Reach level 700 in TI Master', 3, 15, 340),
    (32, 'Become Qualified S5 in TI Master', 3, 30, 350),
    (33, 'Clear TI Master mode by reaching level 999', 3, 15, 360),
    (34, 'Become Qualified m1 in TI Master', 3, 30, 370),
    (35, 'Become Qualified m5 in TI Master', 3, 30, 380),
    (36, 'Become Qualified Master in TI Master', 3, 30, 390),
    (37, 'Get the MasterM grade in TI Master', 3, 15, 400),
    (38, 'Become Qualified MasterM in TI Master', 3, 30, 410),
    (39, 'Become Qualified Grand Master in TI Master', 3, 30, 420),
    (40, 'Clear TI Master with an Orange Line GM grade', 3, 15, 430),
    (41, 'Complete the 20 regular stages of Sakura mode', 3, 18, 440),
    (42, 'Complete ALL stages of Sakura mode', 3, 18, 450),
    (43, 'Get an S1 grade in TI Shirase', 3, 16, 460),
    (44, 'Get an S3 grade in TI Shirase', 3, 16, 470),
    (45, 'Get an S5 grade in TI Shirase', 3, 16, 480),
    (46, 'Break the TI Shirase Torikan at level 500', 3, 16, 490),
    (47, 'Get an S10 grade in TI Shirase', 3, 16, 500),
    (48, 'Get an S12 grade in TI Shirase', 3, 16, 510),
    (49, 'Reach the big credits roll by getting an S13 at the end of TI Shirase', 3, 16, 520),
    (50, 'Conquer the big credits roll and clear TI Shirase with an Orange Line S13', 3, 16, 530),
    (51, 'Submit a score for all Main modes', 0, NULL, 540),
    (52, 'Submit a score for all Extended modes', 0, NULL, 550),
    (6, 'Get an S1 grade in TAP Master', 2, 6, 60);

    --
    -- TOC entry 3383 (class 0 OID 0)
    -- Dependencies: 254
    -- Name: achievement_achievement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
    --

    PERFORM pg_catalog.setval('public.achievement_achievement_id_seq', 52, true);

    end if;
end $$;

