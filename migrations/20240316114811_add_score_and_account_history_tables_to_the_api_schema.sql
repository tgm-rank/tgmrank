CREATE OR REPLACE VIEW api.score_entry_with_history AS
    SELECT * FROM public.score_entry_with_history;
GRANT SELECT ON api.score_entry_with_history TO anon_user;

ALTER VIEW public.account_with_history2 RENAME TO account_with_history;

CREATE OR REPLACE VIEW public.player_with_history AS
    SELECT
        a.player_id,
        a.player_name,
        a.avatar_file,
        a.location_id,
        l.alpha_2 AS location,
        a.sys_period
    FROM public.account_with_history a
    LEFT JOIN location l ON a.location_id = l.location_id;

CREATE OR REPLACE VIEW api.player_with_history AS
    SELECT * FROM public.player_with_history;
GRANT SELECT ON api.player_with_history TO anon_user;
