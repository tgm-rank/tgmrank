CREATE INDEX ON public.recent_activity USING btree (mode_ids);
CREATE INDEX ON public.score_entry_with_history USING btree (platform_id);
CREATE INDEX ON public.score_entry_with_history USING btree (player_id);
CREATE INDEX ON public.score_entry_with_history USING btree (score_id);
CREATE INDEX ON public.score_entry_with_history USING btree (status);
