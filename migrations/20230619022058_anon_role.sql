ALTER TABLE score_entry DISABLE TRIGGER versioning_trigger;

DROP FUNCTION public.score_entry__as_of2;
DROP MATERIALIZED VIEW public.score_entry_with_history;

ALTER TABLE score_entry
DROP COLUMN system_time_start,
DROP COLUMN system_time_end,
DROP COLUMN is_announced;

ALTER TABLE score_entry_history
DROP COLUMN system_time_start,
DROP COLUMN system_time_end,
DROP COLUMN is_announced;
ALTER TABLE score_entry ENABLE TRIGGER versioning_trigger;

CREATE MATERIALIZED VIEW public.score_entry_with_history
TABLESPACE pg_default
AS SELECT score_entry.score_id,
    score_entry.grade_id,
    score_entry.level,
    score_entry.playtime,
    score_entry.score,
    score_entry.status,
    score_entry.comment,
    score_entry.player_id,
    score_entry.mode_id,
    score_entry.created_at,
    score_entry.updated_at,
    score_entry.updated_by,
    score_entry.platform_id,
    score_entry.verification_comment,
    score_entry.verified_by,
    score_entry.verified_on,
    score_entry.sys_period,
    score_entry.rank_order_key
   FROM score_entry
UNION ALL
 SELECT score_entry_history.score_id,
    score_entry_history.grade_id,
    score_entry_history.level,
    score_entry_history.playtime,
    score_entry_history.score,
    score_entry_history.status,
    score_entry_history.comment,
    score_entry_history.player_id,
    score_entry_history.mode_id,
    score_entry_history.created_at,
    score_entry_history.updated_at,
    score_entry_history.updated_by,
    score_entry_history.platform_id,
    score_entry_history.verification_comment,
    score_entry_history.verified_by,
    score_entry_history.verified_on,
    score_entry_history.sys_period,
    score_entry_history.rank_order_key
   FROM score_entry_history
WITH DATA;

-- View indexes:
CREATE INDEX calculate_ranking_player_best_scores_index ON public.score_entry_with_history USING btree (mode_id, status, sys_period);

CREATE FUNCTION public.score_entry__as_of2(timestamp with time zone) RETURNS SETOF public.score_entry_with_history
    LANGUAGE sql STABLE
    AS $_$
    SELECT * FROM public.score_entry_with_history WHERE COALESCE(LOWER(sys_period), '-Infinity') <= $1 AND COALESCE(UPPER(sys_period), 'Infinity') > $1
$_$;

ALTER TABLE account_claim DISABLE TRIGGER versioning_trigger;
ALTER TABLE account_claim
DROP COLUMN system_time_start,
DROP COLUMN system_time_end;
ALTER TABLE account_claim_history
DROP COLUMN system_time_start,
DROP COLUMN system_time_end;
ALTER TABLE account_claim ENABLE TRIGGER versioning_trigger;

ALTER TABLE account DISABLE TRIGGER versioning_trigger;
ALTER TABLE account
DROP COLUMN system_time_start,
DROP COLUMN system_time_end;
ALTER TABLE account_history
DROP COLUMN system_time_start,
DROP COLUMN system_time_end;
ALTER TABLE account ENABLE TRIGGER versioning_trigger;

ALTER TABLE proof DISABLE TRIGGER versioning_trigger;
ALTER TABLE proof
DROP COLUMN system_time_start,
DROP COLUMN system_time_end;
ALTER TABLE proof_history
DROP COLUMN system_time_start,
DROP COLUMN system_time_end;
ALTER TABLE proof ENABLE TRIGGER versioning_trigger;

ALTER TABLE score_vote DISABLE TRIGGER versioning_trigger;
ALTER TABLE score_vote
DROP COLUMN system_time_start,
DROP COLUMN system_time_end;
ALTER TABLE score_vote_history
DROP COLUMN system_time_start,
DROP COLUMN system_time_end;
ALTER TABLE score_vote ENABLE TRIGGER versioning_trigger;

-- Start setting up new schema and roles
ALTER DEFAULT PRIVILEGES REVOKE EXECUTE ON FUNCTIONS FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM public;

CREATE ROLE anon_user NOLOGIN NOINHERIT;
CREATE SCHEMA api;

REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA public FROM anon_user;
REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public FROM anon_user;
REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public FROM anon_user;

GRANT USAGE ON SCHEMA api TO anon_user;

CREATE OR REPLACE VIEW api.game AS
SELECT * FROM public.game;
GRANT SELECT ON api.game TO anon_user;

CREATE OR REPLACE VIEW api.mode AS
SELECT * FROM public.mode;
GRANT SELECT ON api.mode TO anon_user;

CREATE OR REPLACE VIEW api.score_entry AS
SELECT * FROM public.score_entry;
GRANT SELECT ON api.score_entry TO anon_user;

CREATE OR REPLACE VIEW api.player AS
SELECT * FROM public.player;
GRANT SELECT ON api.player TO anon_user;

CREATE OR REPLACE VIEW api.platform AS
SELECT platform_id, platform_name FROM public.platform;
GRANT SELECT ON api.platform TO anon_user;

CREATE OR REPLACE VIEW api.grade AS
SELECT * FROM public.grade;
GRANT SELECT ON api.grade TO anon_user;

CREATE OR REPLACE VIEW api.proof AS
SELECT * FROM public.proof;
GRANT SELECT ON api.proof TO anon_user;

-- This table will act as a "type" for the mode_ranking function
CREATE TABLE public.mode_ranking (
	score_id INTEGER,
	player_id INTEGER,
	mode_id INTEGER,
	ranking_points INTEGER,
	ranking INTEGER,
	PRIMARY KEY(mode_id, player_id, score_id),
    CONSTRAINT score_entry FOREIGN KEY (score_id) REFERENCES public.score_entry(score_id),
    CONSTRAINT player FOREIGN KEY (player_id) REFERENCES public.account(player_id),
    CONSTRAINT mode FOREIGN KEY (mode_id) REFERENCES public.mode(mode_id)
);

CREATE OR REPLACE VIEW api.mode_ranking AS
SELECT * FROM public.mode_ranking;
GRANT SELECT ON api.mode_ranking TO anon_user;

DROP FUNCTION public.calculate_ranking;
CREATE OR REPLACE FUNCTION public.mode_ranking(modes integer[] DEFAULT NULL::INTEGER[], datetime TIMESTAMP WITH TIME ZONE DEFAULT timezone('UTC'::TEXT, CURRENT_TIMESTAMP), algorithm ranking_algorithm DEFAULT 'naive'::ranking_algorithm)
    RETURNS SETOF public.mode_ranking
    LANGUAGE sql
    STABLE
AS $function$
	WITH player_best_scores AS (
		SELECT
		    mo.mode_id,
		    mo.weight,
		    mo.margin,
		    a.score_id,
		    a.player_id,
		    a.rank_order_key
		FROM public.mode mo
		LEFT JOIN linked_mode lm USING (mode_id)
		JOIN LATERAL (
			SELECT DISTINCT ON (mo.mode_id, sii.player_id)
			    sii.score_id,
			    sii.player_id,
			    sii.rank_order_key
			FROM score_entry__as_of2(datetime) sii
			WHERE
				(sii.mode_id = mo.mode_id OR sii.mode_id = lm.linked_id) AND
				is_ranked_score_status(sii.status)
			ORDER BY
			    mo.mode_id,
			    sii.player_id,
			    sii.rank_order_key
		) a ON TRUE
		WHERE (modes IS NULL OR (mo.mode_id = ANY(modes)))
	)
	SELECT
	    score_id,
	    player_id,
	    mode_id,
	    CASE
	        WHEN algorithm = 'naive'::ranking_algorithm THEN
	            calculate_ranking_points(weight, margin, CAST(ranking AS INT))
	        WHEN algorithm = 'percentile'::ranking_algorithm THEN
	            CAST(PERCENT_RANK() OVER (PARTITION BY mode_id ORDER BY ranking DESC) * 10000 AS INT)
	    END AS ranking_points,
	    ranking
	FROM (
	    SELECT
	        sii.score_id,
	        p.player_id,
	        p.player_name,
	        mo.mode_id,
	        mo.weight,
	        mo.margin,
	        CAST(
	        	RANK()
	        	OVER (
				    PARTITION BY mo.mode_id
				    ORDER BY sii.rank_order_key
				)
			AS INT) AS ranking
	    FROM mode mo
		LEFT JOIN player_best_scores sii ON sii.mode_id = mo.mode_id
	    JOIN player p USING (player_id)
	    WHERE (modes IS NULL OR mo.mode_id = ANY(modes))
	) b -- Add calculated ranking
	ORDER BY mode_id, ranking, player_name;
$function$
;

CREATE FUNCTION api.mode_ranking(modes INTEGER[], as_of TIMESTAMP WITH TIME ZONE DEFAULT timezone('UTC'::TEXT, CURRENT_TIMESTAMP), algorithm ranking_algorithm DEFAULT 'naive'::ranking_algorithm) RETURNS SETOF mode_ranking AS $$
    SELECT * FROM public.mode_ranking(modes, as_of, algorithm);
$$ LANGUAGE SQL IMMUTABLE SECURITY DEFINER;
GRANT EXECUTE ON FUNCTION api.mode_ranking TO anon_user;


-- This table will act as a "type" for the game_ranking function
CREATE TABLE public.game_ranking (
    rank_id INTEGER,
    game_id INTEGER,
    player_id INTEGER,
    score_count INTEGER,
    ranking INTEGER,
    ranking_points INTEGER,
    CONSTRAINT player FOREIGN KEY (player_id) REFERENCES public.account(player_id),
    CONSTRAINT game FOREIGN KEY (game_id) REFERENCES public.game(game_id)
);

CREATE OR REPLACE VIEW api.game_ranking AS
SELECT * FROM public.game_ranking;
GRANT SELECT ON api.game_ranking TO anon_user;

DROP FUNCTION public.calculate_aggregate_ranking;
CREATE OR REPLACE FUNCTION public.game_ranking(modes INTEGER[], datetime TIMESTAMP WITH TIME ZONE DEFAULT timezone('UTC'::TEXT, CURRENT_TIMESTAMP), algorithm ranking_algorithm DEFAULT 'naive'::ranking_algorithm)
    RETURNS SETOF public.game_ranking
    LANGUAGE sql
    STABLE
AS $function$
        WITH ranked_scores AS (SELECT
                                   CASE WHEN m.mode_id = ANY (modes)
                                       THEN r.ranking_points
                                   ELSE NULL
                                   END AS ranking_points,
                                   m.game_id,
                                   p.player_id,
                                   p.player_name
                               FROM mode_ranking(modes, datetime, algorithm) r
            JOIN mode m ON r.mode_id = m.mode_id
            JOIN player p ON r.player_id = p.player_id)
        SELECT
            CAST(row_number() OVER(ORDER BY r.game_id, r.ranking_points DESC NULLS LAST, r.player_name) AS INTEGER) AS rank_id,
            r.game_id,
            r.player_id,
            CAST(r.score_count as INTEGER),
            CAST(r.ranking as INTEGER),
            CAST(r.ranking_points as INTEGER)
        FROM
        (SELECT
            r.game_id,
            r.player_id,
            r.player_name,
            COUNT(*) as score_count,
            RANK()
                 OVER (
                     PARTITION BY r.game_id
                     ORDER BY SUM(r.ranking_points) DESC NULLS LAST ) AS ranking,
            SUM(r.ranking_points) AS ranking_points
        FROM ranked_scores r
        GROUP BY r.game_id, r.player_name, r.player_id
        UNION ALL
        SELECT
            0 AS game_id,
            r.player_id,
            r.player_name,
            COUNT(*) as score_count,
            RANK()
                 OVER (
                     ORDER BY SUM(r.ranking_points) DESC NULLS LAST ) AS ranking,
            SUM(r.ranking_points) AS ranking_points
        FROM ranked_scores r
        GROUP BY r.player_name, r.player_id) r;
    $function$
;

CREATE FUNCTION api.game_ranking(modes integer[], as_of TIMESTAMP WITH TIME ZONE DEFAULT timezone('UTC'::TEXT, CURRENT_TIMESTAMP), algorithm ranking_algorithm DEFAULT 'naive'::ranking_algorithm) RETURNS SETOF public.game_ranking AS $$
    SELECT * FROM public.game_ranking(modes, as_of, algorithm);
$$ LANGUAGE SQL IMMUTABLE SECURITY DEFINER;
GRANT EXECUTE ON FUNCTION api.game_ranking TO anon_user;


CREATE OR REPLACE FUNCTION public.cache_recent_activity(modes INTEGER[], scores INTEGER[]) RETURNS void
    LANGUAGE plpgsql
    SET join_collapse_limit TO '1'
    AS $$
DECLARE
    uncached_scores INTEGER[];
BEGIN
    SELECT array_agg(s.score_id)
    INTO uncached_scores
    FROM (SELECT UNNEST(scores) AS score_id) s
    LEFT JOIN (SELECT DISTINCT score_id FROM recent_activity r WHERE r.mode_ids = modes) r
        USING (score_id)
    WHERE r.score_id IS NULL;

    IF uncached_scores IS NOT NULL
    THEN
        INSERT INTO recent_activity
        (activity_type, mode_ids, game_or_mode_id, score_id, ranking, ranking_points, prev_score_id, prev_ranking, prev_ranking_points, passed_player_ids)
        WITH mode_ranking AS (
            SELECT se.score_id AS base_score_id, r.*
            FROM score_entry se
            JOIN LATERAL (
                SELECT player_id,
                    rcurrent.score_id AS current_score_id,
                    rcurrent.ranking AS current_ranking,
                    rcurrent.ranking_points AS current_ranking_points,
                    rprev.score_id AS prev_score_id,
                    rprev.ranking AS prev_ranking,
                    rprev.ranking_points AS prev_ranking_points
                FROM mode_ranking(array[se.mode_id], se.created_at) rcurrent
                LEFT JOIN mode_ranking(array[se.mode_id], se.created_at - interval '0.000001') rprev
                    USING (player_id)
            ) r ON TRUE
            WHERE se.score_id = ANY(uncached_scores)
        ),
        game_ranking AS (
            SELECT se.score_id AS base_score_id, r.*
            FROM score_entry se
            JOIN mode m USING (mode_id)
            JOIN game g USING (game_id)
            JOIN LATERAL (
                SELECT rcurrent.player_id,
                    rcurrent.ranking AS current_ranking,
                    rcurrent.ranking_points AS current_ranking_points,
                    rprev.ranking AS prev_ranking,
                    rprev.ranking_points AS prev_ranking_points
                FROM (SELECT * FROM game_ranking(modes, se.created_at) ORDER BY player_id) rcurrent
                LEFT JOIN (SELECT * FROM game_ranking(modes, se.created_at - interval '0.000001') ORDER BY player_id) rprev
                    ON rcurrent.player_id = rprev.player_id AND rprev.game_id = g.game_id
                WHERE rcurrent.game_id = g.game_id
            ) r ON TRUE
            WHERE se.score_id =  ANY(uncached_scores)
        ),
        overall_ranking AS (
            SELECT se.score_id AS base_score_id, r.*
            FROM score_entry se
            JOIN LATERAL (
                SELECT rcurrent.player_id,
                    rcurrent.ranking AS current_ranking,
                    rcurrent.ranking_points AS current_ranking_points,
                    rprev.ranking AS prev_ranking,
                    rprev.ranking_points AS prev_ranking_points
                FROM (SELECT * FROM game_ranking(modes, se.created_at) WHERE game_id = 0 ORDER BY player_id) rcurrent
                LEFT JOIN (SELECT * FROM game_ranking(modes, se.created_at - interval '0.000001') WHERE game_id = 0 ORDER BY player_id) rprev
                    ON rcurrent.player_id = rprev.player_id AND rprev.game_id = 0
                WHERE rcurrent.game_id = 0
            ) r ON TRUE
            WHERE se.score_id =  ANY(uncached_scores)
        )
        SELECT
            'mode'::recent_activity_type AS activity_type,
            modes AS mode_ids,
            se.mode_id AS game_or_mode_id,
            se.score_id AS score_id,
            mr.current_ranking AS ranking,
            mr.current_ranking_points AS ranking_points,
            mr.prev_score_id AS prev_score_id,
            mr.prev_ranking AS prev_ranking,
            mr.prev_ranking_points AS prev_ranking_points,
            passed_players.passed_player_ids
        FROM score_entry se
        LEFT JOIN LATERAL (
            SELECT mr.current_ranking, mr.current_ranking_points, mr.prev_score_id, mr.prev_ranking, mr.prev_ranking_points
            FROM mode_ranking mr
            WHERE se.score_id = mr.base_score_id
                AND se.score_id = mr.current_score_id
        ) mr ON TRUE
        JOIN LATERAL (
            SELECT
                COALESCE(array_remove(array_agg(mri.player_id ORDER BY mri.prev_ranking NULLS LAST), NULL), ARRAY[]::INTEGER[]) AS passed_player_ids
            FROM mode_ranking mri
            WHERE mri.prev_ranking - mri.current_ranking < 0
                AND mri.player_id != se.player_id
                AND se.score_id = mri.base_score_id -- Make sure we're only looking at the leaderboard for this score
        ) passed_players ON TRUE
        where se.score_id = ANY(uncached_scores)
        UNION ALL
        SELECT
            'game'::recent_activity_type AS activity_type,
            modes AS mode_ids,
            g.game_id AS "id!",
            se.score_id AS "current_score_id!",
            gr.current_ranking AS ranking,
            gr.current_ranking_points AS ranking_points,
            NULL AS prev_score_id,
            gr.prev_ranking AS prev_ranking,
            gr.prev_ranking_points AS prev_ranking_points,
            passed_players.passed_player_ids
        FROM score_entry se
        JOIN mode m ON se.mode_id = m.mode_id
        JOIN game g ON m.game_id = g.game_id
        LEFT JOIN LATERAL (
            SELECT gri.current_ranking, gri.current_ranking_points, gri.prev_ranking, gri.prev_ranking_points
            FROM game_ranking gri
            WHERE se.score_id = gri.base_score_id
                AND se.player_id = gri.player_id
        ) gr ON TRUE
        JOIN LATERAL (
            SELECT
                COALESCE(array_remove(array_agg(gri.player_id order by gri.prev_ranking nulls last), NULL), ARRAY[]::INTEGER[]) AS passed_player_ids
            FROM game_ranking gri
            WHERE se.score_id = gri.base_score_id -- Make sure we're only looking at the leaderboard for this score
                AND gri.current_ranking > (SELECT current_ranking FROM game_ranking WHERE se.score_id = base_score_id AND se.player_id = player_id)
                AND gri.prev_ranking <= COALESCE((SELECT prev_ranking FROM game_ranking WHERE se.score_id = base_score_id AND se.player_id = player_id), 10000000)
                AND se.player_id != gri.player_id
        ) passed_players ON TRUE
        WHERE se.score_id = ANY(uncached_scores)
        UNION ALL
        SELECT
            'overall'::recent_activity_type AS activity_type,
            modes AS mode_ids,
            0 AS "id!",
            se.score_id AS "current_score_id!",
            gr.current_ranking AS ranking,
            gr.current_ranking_points AS ranking_points,
            NULL AS prev_score_id,
            gr.prev_ranking AS prev_ranking,
            gr.prev_ranking_points AS prev_ranking_points,
            passed_players.passed_player_ids
        FROM score_entry se
        LEFT JOIN LATERAL (
            SELECT gri.current_ranking, gri.current_ranking_points, gri.prev_ranking, gri.prev_ranking_points
            FROM overall_ranking gri
            WHERE se.score_id = gri.base_score_id
                AND se.player_id = gri.player_id
        ) gr ON TRUE
        JOIN LATERAL (
            SELECT
                COALESCE(array_remove(array_agg(gri.player_id order by gri.prev_ranking nulls last), NULL), ARRAY[]::INTEGER[]) AS passed_player_ids
            FROM overall_ranking gri
            WHERE se.score_id = gri.base_score_id -- Make sure we're only looking at the leaderboard for this score
                AND gri.current_ranking > (SELECT current_ranking FROM overall_ranking WHERE se.score_id = base_score_id AND se.player_id = player_id)
                AND gri.prev_ranking <= COALESCE((SELECT prev_ranking FROM overall_ranking WHERE se.score_id = base_score_id AND se.player_id = player_id), 10000000)
                AND se.player_id != gri.player_id
        ) passed_players ON TRUE
        WHERE se.score_id = ANY(uncached_scores)
        ON CONFLICT (activity_type, score_id, mode_ids)
        DO NOTHING;
    END IF;
END;
$$;
