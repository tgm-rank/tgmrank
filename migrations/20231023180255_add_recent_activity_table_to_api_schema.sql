CREATE OR REPLACE VIEW api.recent_activity AS
SELECT * FROM public.recent_activity;
GRANT SELECT ON api.recent_activity TO anon_user;
