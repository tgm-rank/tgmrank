ALTER TABLE public.score_entry RENAME CONSTRAINT score_mode_modeid_fk TO "mode";
ALTER TABLE public.score_entry RENAME CONSTRAINT score_player_playerid_fk TO player;
ALTER TABLE public.score_entry RENAME CONSTRAINT score_entry_verified_by_account_fk TO verified_by;
