DO $outer$ BEGIN
IF NOT EXISTS (
    SELECT 1
    FROM public.mode_group
)
THEN
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('main', 'TGM1 Main Ranked Modes', 1, '{1,2}');
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('extended', 'TGM1 Extended Ranked Modes', 1, '{1,2,3,4,29,12}');
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('main', 'TAP Main Ranked Modes', 2, '{6,7}');
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('extended', 'TAP Extended Ranked Modes', 2, '{6,7,13,11,9,14,10,12}');
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('main', 'TI Main Ranked Modes', 3, '{15,16}');
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('extended', 'TI Extended Ranked Modes', 3, '{15,16,17,18,25,19,30}');
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('main', 'Overall Main Ranked Modes', 0, '{1,2,6,7,15,16}');
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('extended', 'Overall Extended Ranked Modes', 0, '{1,2,3,4,29,12,6,7,13,11,9,14,8,15,16,17,18,25,19,30}');
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('world', 'TI World Ranked Modes', 3, '{20,21}');
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('worldExtended', 'TI World Ranked Modes (Extended)', 3, '{20,21,22,23,24,26,31}');
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('secret', 'Overall Main Secret Grade Ranked Modes', 0, '{5,14,19}');
    INSERT INTO public.mode_group("name", description, game_id, mode_ids)
	    VALUES('big', 'Overall Main Big Ranked Modes', 0, '{3,10,25}');

    REFRESH MATERIALIZED VIEW api.mode_mv;
END IF;
END $outer$;
