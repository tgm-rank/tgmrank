CREATE TABLE public.mode_alias (
    mode_alias_id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    mode_id INTEGER NOT NULL,
    alias VARCHAR NOT NULL,
    UNIQUE (mode_id, alias),
    CONSTRAINT mode FOREIGN KEY (mode_id) REFERENCES public.mode(mode_id)
);

CREATE OR REPLACE VIEW api.mode_alias AS
SELECT * FROM public.mode_alias;
GRANT SELECT ON api.mode_alias TO anon_user;

CREATE OR REPLACE FUNCTION public.slugify("value" TEXT)
RETURNS TEXT AS $$
  WITH "lowercase" AS (
    SELECT lower("value") AS "value"
  ),
  "hyphenated" AS (
    SELECT regexp_replace("value", '[^a-z0-9_]+', '-', 'gi') AS "value"
    FROM "lowercase"
  ),
  "trimmed" AS (
    SELECT regexp_replace(regexp_replace("value", '-+$', ''), '^-', '') AS "value"
    FROM "hyphenated"
  )
  SELECT "value" FROM "trimmed";
$$ LANGUAGE SQL STRICT IMMUTABLE;

INSERT INTO public.mode_alias (mode_id, alias)
SELECT mode_id, public.slugify(mode_name)
FROM public.mode;

UPDATE public.mode_alias
SET alias = 'tgm-plus'
WHERE mode_id = 13;

UPDATE public.mode_alias
SET alias = 'tgm-plus-secret-grade'
WHERE mode_id = 41;

INSERT INTO public.mode_alias (mode_id, alias)
VALUES
((SELECT mode_id FROM public.mode WHERE mode_name = 'Death (Series of 5)'), 'death-series'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Master (C)'), 'master'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Master (C)'), 'master-classic'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Qualified Master (C)'), 'qualified-master'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Qualified Master (C)'), 'qualified-master-classic'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Shirase (C)'), 'shirase'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Shirase (C)'), 'shirase-classic'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Easy (C)'), 'easy'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Easy (C)'), 'easy-classic'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Sakura (C)'), 'sakura'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Sakura (C)'), 'sakura-classic'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Master Secret Grade (C)'), 'master-secret-grade'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Master Secret Grade (C)'), 'master-secret-grade-classic'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Shirase Secret Grade (C)'), 'shirase-secret-grade'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Shirase Secret Grade (C)'), 'shirase-secret-grade-classic'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Big (C)'), 'big'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Big (C)'), 'big-classic'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Master (W)'), 'master-world'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Qualified Master (W)'), 'qualified-master-world'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Shirase (W)'), 'shirase-world'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Easy (W)'), 'easy-world'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Sakura (W)'), 'sakura-world'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Master Secret Grade (W)'), 'master-secret-grade-world'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Shirase Secret Grade (W)'), 'shirase-secret-grade-world'),
((SELECT mode_id FROM public.mode WHERE mode_name = 'Big (W)'), 'big-world');
