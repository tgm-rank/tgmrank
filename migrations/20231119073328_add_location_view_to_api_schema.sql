CREATE OR REPLACE VIEW api.location AS
SELECT * FROM public.location;
GRANT SELECT ON api.location TO anon_user;
