DROP VIEW api.current_mode_ranking;

CREATE OR REPLACE VIEW api.ranking
AS
  SELECT *
  FROM current_mode_ranking;
GRANT SELECT ON api.ranking TO anon_user;

CREATE OR REPLACE FUNCTION api.ranking(api.score_entry)
RETURNS SETOF api.ranking ROWS 1 AS
$$
  SELECT *
  FROM api.ranking
  WHERE score_id = $1.score_id
$$ STABLE LANGUAGE SQL;
GRANT EXECUTE ON FUNCTION api.ranking(api.score_entry) TO anon_user;
