CREATE OR REPLACE
FUNCTION api.tstzrange_to_json(tstzrange) RETURNS json AS $$
  SELECT
    json_build_object(
        'lower', lower($1),
        'upper', upper($1)
    );
$$ LANGUAGE SQL;

-- CREATE CAST (tstzrange AS json) WITH FUNCTION api.tstzrange_to_json(tstzrange) AS ASSIGNMENT;

-- GRANT EXECUTE ON FUNCTION api.tstzrange_to_json TO anon_user;
