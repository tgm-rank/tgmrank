CREATE OR REPLACE FUNCTION public.mode_ranking(modes integer[] DEFAULT NULL::integer[], datetime timestamp with time zone DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP), algorithm ranking_algorithm DEFAULT 'naive'::ranking_algorithm)
 RETURNS SETOF mode_ranking
 LANGUAGE sql
 STABLE
AS $function$
    WITH player_best_scores AS (
        SELECT
            mo.mode_id,
            mo.weight,
            mo.margin,
            a.score_id,
            a.player_id,
            p.player_name,
            CAST(
                RANK()
                OVER (
                    PARTITION BY mo.mode_id
                    ORDER BY a.rank_order_key
                ) AS INT) AS ranking
        FROM public.mode mo
        LEFT JOIN public.linked_mode lm USING (mode_id)
        LEFT JOIN LATERAL (
            SELECT DISTINCT ON (mo.mode_id, sii.player_id)
                sii.score_id,
                sii.player_id,
                sii.rank_order_key
            FROM public.score_entry__as_of2(datetime) sii
            WHERE
                (sii.mode_id = mo.mode_id OR sii.mode_id = lm.linked_id) AND
                public.is_ranked_score_status(sii.status)
            ORDER BY
                mo.mode_id,
                sii.player_id,
                sii.rank_order_key
        ) a ON TRUE
        JOIN public.player p USING (player_id)
    )
    SELECT
        score_id,
        player_id,
        mode_id,
        CASE
            WHEN algorithm = 'naive'::ranking_algorithm THEN
                calculate_ranking_points(weight, margin, CAST(ranking AS INT))
            WHEN algorithm = 'percentile'::ranking_algorithm THEN
                CAST(PERCENT_RANK() OVER (PARTITION BY mode_id ORDER BY ranking DESC) * 10000 AS INT)
            WHEN algorithm = 'simple_max'::ranking_algorithm THEN
                (
                    SELECT CAST(MAX(c) + 1 - ranking AS INT)
                    FROM (
                        SELECT COUNT(*) AS c
                        FROM player_best_scores
                        GROUP BY mode_id
                    ) entrants_per_mode
                )
        END AS ranking_points,
        ranking
    FROM player_best_scores s
    WHERE (modes IS NULL OR (s.mode_id = ANY(modes)))
    ORDER BY mode_id, ranking, player_name;
$function$
;
