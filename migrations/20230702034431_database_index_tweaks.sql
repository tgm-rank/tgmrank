CREATE INDEX ON public.account USING btree (player_id);
CREATE INDEX ON public.mode USING btree (mode_id);
CREATE INDEX ON public.recent_activity (score_id);
CREATE INDEX ON public.recent_activity USING btree (mode_ids);
CREATE INDEX ON public.score_entry_with_history (status);
