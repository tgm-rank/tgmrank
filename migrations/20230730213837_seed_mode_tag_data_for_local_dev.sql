do $$ begin
if not exists (
    select 1
    from public.mode
    where mode_id = 29
)
then
    INSERT INTO public.mode VALUES (29, 1, 'Score Attack', false, 100, 1, 'https://tetrisconcept.net/threads/tgm-score-attack.3101/', NULL, -1, 0, 0, 45, '', '(,)');

    INSERT INTO mode_tag (mode_id, tag_name)
    VALUES
    (1, 'MAIN'),
    (2, 'MAIN'),
    (6, 'MAIN'),
    (7, 'MAIN'),
    (15, 'MAIN'),
    (16, 'MAIN'),
    (1, 'EXTENDED'),
    (2, 'EXTENDED'),
    (3, 'EXTENDED'),
    (4, 'EXTENDED'),
    (29, 'EXTENDED'),
    (12, 'EXTENDED'),
    (6, 'EXTENDED'),
    (7, 'EXTENDED'),
    (13, 'EXTENDED'),
    (11, 'EXTENDED'),
    (9, 'EXTENDED'),
    (14, 'EXTENDED'),
    (8, 'EXTENDED'),
    (15, 'EXTENDED'),
    (16, 'EXTENDED'),
    (17, 'EXTENDED'),
    (18, 'EXTENDED'),
    (25, 'EXTENDED'),
    (19, 'EXTENDED'),
    (30, 'EXTENDED');
end if;
end $$;

SELECT 1;
