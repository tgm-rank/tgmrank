CREATE INDEX ON public.account USING btree (location_id);
CREATE INDEX ON public.account_badge USING btree (account_id);
CREATE INDEX ON public.grade USING btree (sort_order);
CREATE INDEX ON public.mode_tag USING btree (mode_id);
CREATE INDEX ON public.rival USING btree (player_id);
