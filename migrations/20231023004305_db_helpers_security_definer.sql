CREATE OR REPLACE FUNCTION api.lookup_mode_id(game_filter VARCHAR, mode_filter VARCHAR)
    RETURNS INTEGER
    LANGUAGE sql
    STABLE
AS $function$
    SELECT public.lookup_mode_id(game_filter, mode_filter)
$function$ SECURITY DEFINER
;
