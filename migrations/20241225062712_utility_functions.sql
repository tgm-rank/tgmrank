CREATE OR REPLACE FUNCTION api.get_group_mode_ids(game_name TEXT, group_name TEXT) RETURNS INTEGER[] AS $$
  SELECT mg.mode_ids
  FROM api.mode_group mg
  JOIN api.game g USING (game_id)
  WHERE mg.name ILIKE group_name AND g.short_name ILIKE game_name
$$ LANGUAGE SQL IMMUTABLE SECURITY DEFINER;
GRANT EXECUTE ON FUNCTION api.get_group_mode_ids TO anon_user;
