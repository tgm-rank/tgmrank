CREATE OR REPLACE FUNCTION public.validate_score_entry(score_id_in INTEGER, validation_function_in TEXT DEFAULT NULL)
RETURNS SETOF validation_result
AS $$
DECLARE
    validation_function text;
    validation_sql text;
BEGIN
  validation_function := (
    SELECT COALESCE(validation_function_in, m.validation_function)
    FROM mode m
    JOIN score_entry se USING (mode_id)
    WHERE se.score_id = score_id_in
  );

  -- validation function should accept a score_entry as input and return a set of validation_result
  IF validation_function IS NOT NULL THEN
    -- Example query:
    -- SELECT field, error FROM public.validate_mode_death_series_of_5((SELECT ROW(se.*)::public.score_entry FROM score_entry se WHERE score_id = 49));
    validation_sql := 'SELECT field, error FROM ' || validation_function || '((SELECT ROW(se.*)::public.score_entry FROM score_entry se WHERE score_id = $1))';

    RETURN QUERY EXECUTE validation_sql USING score_id_in;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.validate_field_grade(grade_id_in INTEGER, mode_id_in INTEGER)
RETURNS SETOF validation_result
AS $$
BEGIN
  IF NOT EXISTS(SELECT 1 FROM public.grade g WHERE g.mode_id = mode_id_in AND g.grade_id = grade_id_in) THEN
    RETURN QUERY SELECT 'grade_id', 'Grade is invalid for mode';
    RETURN;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.validate_mode_death_series_of_5(score_entry public.score_entry)
RETURNS SETOF validation_result
AS $$
BEGIN
  RETURN QUERY
    SELECT * FROM validate_field_level(score_entry.LEVEL, '[25,4995]')
    UNION ALL
    SELECT * FROM validate_field_time_empty(score_entry.playtime)
    UNION ALL
    SELECT * FROM validate_field_score_empty(score_entry.score)
    UNION ALL
    SELECT * FROM validate_field_grade_empty(score_entry.grade_id);
END;
$$ LANGUAGE plpgsql;

UPDATE mode SET validation_function = 'public.validate_mode_death_series_of_5' WHERE mode_id = 8;
