ALTER TABLE mode_ranking
ADD COLUMN as_of TIMESTAMPTZ;

ALTER TABLE mode_ranking
DROP CONSTRAINT mode_ranking_pkey,
ADD CONSTRAINT mode_ranking_pkey PRIMARY KEY (mode_id, player_id, score_id, as_of);

CREATE OR REPLACE FUNCTION public.mode_ranking(
    modes integer[] DEFAULT NULL::integer[],
    datetime timestamp with time zone DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP),
    algorithm ranking_algorithm DEFAULT 'naive'::ranking_algorithm,
    game_filter VARCHAR DEFAULT NULL,
    mode_filter VARCHAR DEFAULT NULL
)
    RETURNS SETOF public.mode_ranking
    LANGUAGE sql
    STABLE
AS $function$
    WITH player_best_scores AS (
        SELECT
            mo.mode_id,
            mo.weight,
            mo.margin,
            a.score_id,
            a.player_id,
            p.player_name,
            CAST(
                RANK()
                OVER (
                    PARTITION BY mo.mode_id
                    ORDER BY a.rank_order_key
                ) AS INT) AS ranking
        FROM public.mode mo
        JOIN public.game g USING (game_id)
        LEFT JOIN public.linked_mode lm USING (mode_id)
        LEFT JOIN LATERAL (
            SELECT DISTINCT ON (mo.mode_id, sii.player_id)
                sii.score_id,
                sii.player_id,
                sii.rank_order_key
            FROM score_entry__as_of2(datetime) sii
            WHERE
                (sii.mode_id = mo.mode_id OR sii.mode_id = lm.linked_id) AND
                public.is_ranked_score_status(sii.status)
            ORDER BY
                mo.mode_id,
                sii.player_id,
                sii.rank_order_key
        ) a ON TRUE
        JOIN public.player p USING (player_id)
        WHERE
            (modes IS NULL OR (mo.mode_id = ANY(modes))) AND
            -- Filters for mode_filter/game_filter
            (mode_filter IS NULL OR (
                (mo.game_id = public.safe_int_cast(game_filter, -1) OR g.short_name ILIKE game_filter) AND
                mo.mode_id IN (SELECT mode_id FROM public.mode_alias WHERE alias = mode_filter)
                ))
    )
    SELECT
        score_id,
        player_id,
        mode_id,
        CASE
            WHEN algorithm = 'naive'::ranking_algorithm THEN
                calculate_ranking_points(weight, margin, CAST(ranking AS INT))
            WHEN algorithm = 'percentile'::ranking_algorithm THEN
                CAST(PERCENT_RANK() OVER (PARTITION BY mode_id ORDER BY ranking DESC) * 10000 AS INT)
        END AS ranking_points,
        ranking,
        datetime
    FROM player_best_scores
    ORDER BY mode_id, ranking, player_name;
$function$
;

CREATE OR REPLACE FUNCTION public.mode_ranking_update(score_id_in INTEGER)
    RETURNS void
    LANGUAGE plpgsql
AS $function$
BEGIN
    DELETE FROM public.mode_ranking
    WHERE as_of IN (
        SELECT DISTINCT ON (se.rank_order_key)
            se.updated_at
        FROM public.score_entry_with_history se
        WHERE se.score_id = score_id_in
    );

    WITH as_of_timestamps AS (
        SELECT DISTINCT ON (se.rank_order_key)
            se.updated_at
        FROM public.score_entry_with_history se
        WHERE se.score_id = score_id_in
    )
    INSERT INTO public.mode_ranking (score_id, player_id, mode_id, ranking_points, ranking, as_of)
    SELECT mr.*
    FROM as_of_timestamps aot
    JOIN LATERAL public.mode_ranking(NULL, aot.updated_at) mr ON TRUE;
END;
$function$;

CREATE OR REPLACE FUNCTION public.mode_ranking_update_many(score_ids integer[])
 RETURNS void
 LANGUAGE sql
AS $function$
    SELECT DISTINCT ON (s.score_id)
        public.mode_ranking_update(s.score_id)
    FROM public.score_entry_with_history s
    WHERE s.score_id = ANY(score_ids);
$function$
;
