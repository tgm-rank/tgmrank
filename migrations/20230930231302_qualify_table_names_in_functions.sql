CREATE OR REPLACE FUNCTION public.mode_ranking(
    modes integer[] DEFAULT NULL::integer[],
    datetime timestamp with time zone DEFAULT timezone('UTC'::text, CURRENT_TIMESTAMP),
    algorithm ranking_algorithm DEFAULT 'naive'::ranking_algorithm,
    game_filter VARCHAR DEFAULT NULL,
    mode_filter VARCHAR DEFAULT NULL
)
    RETURNS SETOF public.mode_ranking
    LANGUAGE sql
    STABLE
AS $function$
    WITH player_best_scores AS (
        SELECT
            mo.mode_id,
            mo.weight,
            mo.margin,
            a.score_id,
            a.player_id,
            p.player_name,
            CAST(
                RANK()
                OVER (
                    PARTITION BY mo.mode_id
                    ORDER BY a.rank_order_key
                ) AS INT) AS ranking
        FROM public.mode mo
        JOIN public.game g USING (game_id)
        LEFT JOIN public.linked_mode lm USING (mode_id)
        LEFT JOIN LATERAL (
            SELECT DISTINCT ON (mo.mode_id, sii.player_id)
                sii.score_id,
                sii.player_id,
                sii.rank_order_key
            FROM public.score_entry__as_of2(datetime) sii
            WHERE
                (sii.mode_id = mo.mode_id OR sii.mode_id = lm.linked_id) AND
                public.is_ranked_score_status(sii.status)
            ORDER BY
                mo.mode_id,
                sii.player_id,
                sii.rank_order_key
        ) a ON TRUE
        JOIN public.player p USING (player_id)
        WHERE
            (modes IS NULL OR (mo.mode_id = ANY(modes))) AND
            -- Filters for mode_filter/game_filter
            (mode_filter IS NULL OR (
                (mo.game_id = public.safe_int_cast(game_filter, -1) OR g.short_name ILIKE game_filter) AND
                mo.mode_id IN (SELECT mode_id FROM public.mode_alias WHERE alias = mode_filter)
                ))
    )
    SELECT
        score_id,
        player_id,
        mode_id,
        CASE
            WHEN algorithm = 'naive'::ranking_algorithm THEN
                calculate_ranking_points(weight, margin, CAST(ranking AS INT))
            WHEN algorithm = 'percentile'::ranking_algorithm THEN
                CAST(PERCENT_RANK() OVER (PARTITION BY mode_id ORDER BY ranking DESC) * 10000 AS INT)
        END AS ranking_points,
        ranking,
        datetime
    FROM player_best_scores
    ORDER BY mode_id, ranking, player_name;
$function$
;

DROP FUNCTION IF EXISTS public.mode_ranking_update;
DROP FUNCTION IF EXISTS public.mode_ranking_update_many;
