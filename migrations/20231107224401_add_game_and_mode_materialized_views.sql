CREATE MATERIALIZED VIEW api.mode_mv AS
    SELECT m.*, mt.tags, ma.aliases
    FROM api.mode m
    LEFT JOIN (
        SELECT mode_id, array_agg(LOWER(tag_name)) AS tags
        FROM api.mode_tag mt
        GROUP BY mode_id
    ) mt USING (mode_id)
    LEFT JOIN (
        SELECT mode_id, array_agg(alias) as aliases
        FROM api.mode_alias ma
        GROUP BY mode_id
    ) ma USING (mode_id)
    ORDER BY m.game_id, m.sort_order
WITH DATA;

CREATE INDEX mode_mv_mode_id_idx ON api.mode_mv(mode_id);

GRANT SELECT ON api.mode_mv TO anon_user;
