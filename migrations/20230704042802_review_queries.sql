DROP FUNCTION public.score_entry__as_of2;
CREATE FUNCTION public.score_entry__as_of2(timestamp with time zone) RETURNS SETOF public.score_entry
    LANGUAGE sql STABLE
    AS $_$
    SELECT * FROM public.score_entry_with_history WHERE COALESCE(LOWER(sys_period), '-Infinity') <= $1 AND COALESCE(UPPER(sys_period), 'Infinity') > $1
$_$;

CREATE OR REPLACE FUNCTION public.mode_ranking(modes integer[] DEFAULT NULL::INTEGER[], datetime TIMESTAMP WITH TIME ZONE DEFAULT timezone('UTC'::TEXT, CURRENT_TIMESTAMP), algorithm ranking_algorithm DEFAULT 'naive'::ranking_algorithm)
    RETURNS SETOF public.mode_ranking
    LANGUAGE sql
    STABLE
AS $function$
	WITH player_best_scores AS (
		SELECT
		    mo.mode_id,
		    mo.weight,
		    mo.margin,
		    a.score_id,
		    a.player_id,
            p.player_name,
	        CAST(
	        	RANK()
	        	OVER (
				    PARTITION BY mo.mode_id
				    ORDER BY a.rank_order_key
				) AS INT) AS ranking
		FROM public.mode mo
		LEFT JOIN public.linked_mode lm USING (mode_id)
		LEFT JOIN LATERAL (
			SELECT DISTINCT ON (mo.mode_id, sii.player_id)
			    sii.score_id,
			    sii.player_id,
			    sii.rank_order_key
			FROM public.score_entry__as_of2(datetime) sii
			WHERE
				(sii.mode_id = mo.mode_id OR sii.mode_id = lm.linked_id) AND
				public.is_ranked_score_status(sii.status)
			ORDER BY
			    mo.mode_id,
			    sii.player_id,
			    sii.rank_order_key
		) a ON TRUE
        JOIN public.player p USING (player_id)
		WHERE (modes IS NULL OR (mo.mode_id = ANY(modes)))
	)
	SELECT
	    score_id,
	    player_id,
	    mode_id,
	    CASE
	        WHEN algorithm = 'naive'::ranking_algorithm THEN
	            calculate_ranking_points(weight, margin, CAST(ranking AS INT))
	        WHEN algorithm = 'percentile'::ranking_algorithm THEN
	            CAST(PERCENT_RANK() OVER (PARTITION BY mode_id ORDER BY ranking DESC) * 10000 AS INT)
	    END AS ranking_points,
	    ranking
	FROM player_best_scores
	ORDER BY mode_id, ranking, player_name;
$function$
;

CREATE MATERIALIZED VIEW public.current_mode_ranking
AS SELECT * FROM public.mode_ranking()
WITH DATA;

CREATE FUNCTION api.current_mode_ranking() RETURNS SETOF public.mode_ranking AS $$
    SELECT * FROM public.current_mode_ranking;
$$ LANGUAGE SQL IMMUTABLE SECURITY DEFINER;
GRANT EXECUTE ON FUNCTION api.current_mode_ranking TO anon_user;
