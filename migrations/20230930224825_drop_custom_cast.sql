DROP CAST IF EXISTS (tstzrange AS json);

DROP FUNCTION IF EXISTS api.tstzrange_to_json(tstzrange);
