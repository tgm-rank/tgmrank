ALTER TABLE public.mode
ADD COLUMN IF NOT EXISTS validation_function TEXT;

DROP TYPE IF EXISTS public.validation_result;
CREATE TYPE public.validation_result AS (
    field text,
    error text
);

CREATE OR REPLACE FUNCTION public.validate_field_grade_required(grade_id INTEGER)
RETURNS SETOF validation_result
AS $$
BEGIN
  IF grade_id IS NULL THEN
    RETURN QUERY SELECT 'grade_id', 'Grade is required';
    RETURN;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.validate_field_time_required(playtime TIME(2))
RETURNS SETOF validation_result
AS $$
BEGIN
  IF playtime IS NULL THEN
    RETURN QUERY SELECT 'playtime', 'Time is required';
    RETURN;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.validate_field_level_required(level INTEGER)
RETURNS SETOF validation_result
AS $$
BEGIN
  IF level IS NULL THEN
    RETURN QUERY SELECT 'level', 'Level is required';
    RETURN;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.validate_field_score_required(score INTEGER)
RETURNS SETOF validation_result
AS $$
BEGIN
  IF score IS NULL THEN
    RETURN QUERY SELECT 'score', 'Score is required';
    RETURN;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.validate_field_grade_empty(grade_id INTEGER)
RETURNS SETOF validation_result
AS $$
BEGIN
  IF grade_id IS NOT NULL THEN
    RETURN QUERY SELECT 'grade_id', 'Grade must be empty';
    RETURN;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.validate_field_time_empty(playtime TIME(2))
RETURNS SETOF validation_result
AS $$
BEGIN
  IF playtime IS NOT NULL THEN
    RETURN QUERY SELECT 'playtime', 'Time must be empty';
    RETURN;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.validate_field_level_empty(level INTEGER)
RETURNS SETOF validation_result
AS $$
BEGIN
  IF level IS NOT NULL THEN
    RETURN QUERY SELECT 'level', 'Level must be empty';
    RETURN;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.validate_field_score_empty(score INTEGER)
RETURNS SETOF validation_result
AS $$
BEGIN
  IF score IS NOT NULL THEN
    RETURN QUERY SELECT 'score', 'Score must be empty';
    RETURN;
  END IF;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION public.validate_field_level(level INTEGER, range INT4RANGE)
RETURNS SETOF validation_result
AS $$
BEGIN
  IF NOT (range @> level) THEN
    RETURN QUERY SELECT 'level', 'Invalid level';
    RETURN;
  END IF;
END;
$$ LANGUAGE plpgsql;
