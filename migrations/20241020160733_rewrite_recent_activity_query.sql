CREATE OR REPLACE FUNCTION public.cache_recent_activity(modes INTEGER[], scores INTEGER[]) RETURNS void
    LANGUAGE plpgsql
    SET join_collapse_limit TO '1'
    AS $$
DECLARE
    uncached_scores INTEGER[];
BEGIN
    SELECT array_agg(s.score_id)
    INTO uncached_scores
    FROM (SELECT UNNEST(scores) AS score_id) s
    LEFT JOIN (SELECT DISTINCT score_id FROM public.recent_activity r WHERE r.mode_ids = modes) r
        USING (score_id)
    WHERE r.score_id IS NULL;

    IF uncached_scores IS NOT NULL
    THEN
        INSERT INTO public.recent_activity
        (activity_type, mode_ids, game_or_mode_id, score_id, ranking, ranking_points, prev_score_id, prev_ranking, prev_ranking_points, passed_player_ids)
        SELECT
            'mode'::recent_activity_type AS activity_type,
            modes AS mode_ids,
            se.mode_id AS game_or_mode_id,
            se.score_id AS score_id,
            rcurrent.ranking AS ranking,
            rcurrent.ranking_points AS ranking_points,
            rprev.score_id AS prev_score_id,
            rprev.ranking AS prev_ranking,
            rprev.ranking_points AS prev_ranking_points,
            (
                SELECT COALESCE(ARRAY_REMOVE(ARRAY_AGG(passed_players.player_id ORDER BY passed_players.ranking NULLS LAST), NULL), ARRAY[]::INTEGER[])
                FROM public.mode_ranking(ARRAY[se.mode_id], se.created_at) passed_players
                WHERE
                passed_players.ranking <= COALESCE(rprev.ranking, 999999) AND
                passed_players.ranking > rcurrent.ranking AND
                passed_players.player_id != se.player_id
            ) AS passed_player_ids
            FROM public.score_entry se
            LEFT JOIN public.mode_ranking(ARRAY[se.mode_id], se.created_at) rcurrent
                ON se.score_id = rcurrent.score_id
            LEFT JOIN public.mode_ranking(ARRAY[se.mode_id], se.created_at - interval '0.000001') rprev
                ON se.player_id = rprev.player_id
            WHERE
                se.score_id = ANY(uncached_scores)
            UNION ALL
            SELECT
            'game'::recent_activity_type AS activity_type,
            modes AS mode_ids,
            g.game_id AS game_or_mode_id,
            se.score_id AS score_id,
            rcurrent.ranking AS ranking,
            rcurrent.ranking_points AS ranking_points,
            NULL AS prev_score_id,
            rprev.ranking AS prev_ranking,
            rprev.ranking_points AS prev_ranking_points,
            (
                SELECT COALESCE(ARRAY_REMOVE(ARRAY_AGG(passed_players.player_id ORDER BY passed_players.ranking NULLS LAST), NULL), ARRAY[]::INTEGER[])
                FROM public.game_ranking(modes, se.created_at) passed_players
                WHERE
                    passed_players.ranking <= COALESCE(rprev.ranking, 999999) AND
                    passed_players.ranking > rcurrent.ranking AND
                    passed_players.player_id != se.player_id AND
                    passed_players.game_id = g.game_id
            ) AS passed_player_ids
            FROM public.score_entry se
            JOIN public.mode m USING (mode_id)
            JOIN public.game g USING (game_id)
            LEFT JOIN public.game_ranking(modes, se.created_at) rcurrent
                ON se.player_id = rcurrent.player_id AND rcurrent.game_id = g.game_id
            LEFT JOIN public.game_ranking(modes, se.created_at - interval '0.000001') rprev
                ON se.player_id = rprev.player_id AND rprev.game_id = g.game_id
            WHERE
                se.score_id = ANY(uncached_scores)
            UNION ALL
            SELECT
            'overall'::recent_activity_type AS activity_type,
            modes AS mode_ids,
            0 AS game_or_mode_id,
            se.score_id AS score_id,
            rcurrent.ranking AS ranking,
            rcurrent.ranking_points AS ranking_points,
            NULL AS prev_score_id,
            rprev.ranking AS prev_ranking,
            rprev.ranking_points AS prev_ranking_points,
            (
                SELECT COALESCE(ARRAY_REMOVE(ARRAY_AGG(passed_players.player_id ORDER BY passed_players.ranking NULLS LAST), NULL), ARRAY[]::INTEGER[])
                FROM public.game_ranking(modes, se.created_at) passed_players
                WHERE
                    passed_players.ranking <= COALESCE(rprev.ranking, 999999) AND
                    passed_players.ranking > rcurrent.ranking AND
                    passed_players.player_id != se.player_id AND
                    passed_players.game_id = 0
            ) AS passed_player_ids
            FROM public.score_entry se
            LEFT JOIN public.game_ranking(modes, se.created_at) rcurrent
                ON se.player_id = rcurrent.player_id AND rcurrent.game_id = 0
            LEFT JOIN public.game_ranking(modes, se.created_at - interval '0.000001') rprev
                ON se.player_id = rprev.player_id AND rprev.game_id = 0
            WHERE
                se.score_id = ANY(uncached_scores)
        ON CONFLICT (activity_type, score_id, mode_ids)
        DO NOTHING;
    END IF;
END;
$$;
