CREATE OR REPLACE VIEW api.mode_tag AS
    SELECT * FROM public.mode_tag;
GRANT SELECT ON api.mode_tag TO anon_user;

CREATE OR REPLACE VIEW api.account_achievement AS
    SELECT * FROM public.account_achievement;
GRANT SELECT ON api.account_achievement TO anon_user;

CREATE OR REPLACE VIEW api.achievement AS
    SELECT * FROM public.achievement;
GRANT SELECT ON api.achievement TO anon_user;

CREATE OR REPLACE VIEW api.account_badge AS
    SELECT * FROM public.account_badge;
GRANT SELECT ON api.account_badge TO anon_user;

CREATE OR REPLACE VIEW api.badge AS
    SELECT * FROM public.badge;
GRANT SELECT ON api.badge TO anon_user;

CREATE OR REPLACE VIEW api.rival AS
    SELECT * FROM public.rival;
GRANT SELECT ON api.rival TO anon_user;
