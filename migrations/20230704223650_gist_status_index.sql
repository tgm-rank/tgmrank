CREATE EXTENSION IF NOT EXISTS btree_gist;

DROP INDEX IF EXISTS score_entry_with_history_status_idx;
DROP INDEX IF EXISTS score_entry_with_history_status_idx1;

CREATE INDEX ON public.score_entry_with_history USING gist (status);
