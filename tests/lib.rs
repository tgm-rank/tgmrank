extern crate rand;
extern crate serde;
extern crate tgmrank;

mod achievement_tests;
mod api_login_tests;
mod api_tests;
mod avatar_tests;
mod badge_tests;
mod one_off_leaderboard_tests;
mod player_tests;
mod ranking_tests;
mod recent_activity_tests;
mod test_utility;
