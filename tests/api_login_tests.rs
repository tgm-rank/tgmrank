use reqwest::StatusCode;

use tgmrank::models::forms::{AddScoreForm, PlayerUpdateForm, VerifyScoreForm};
use tgmrank::models::{PlayerTokenEntity, Role, ScoreStatus, UserTokenType};
use tgmrank::services::PlayerService;
use tgmrank::{controllers::models::LoginModel, models::forms::UserRegisterForm};

use crate::test_utility::{
    assert_login_verify_failure, assert_login_verify_success, random_string, Client, TestResult,
};

#[actix_web::test]
async fn login_fails_when_username_is_not_found() -> TestResult<()> {
    let client = Client::new()?;
    let response = client.login("bad_username", "bad_password").await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    assert_user_cannot_access_protected_stuff(&client).await?;

    Ok(())
}

#[actix_web::test]
async fn login_succeeds() -> TestResult<()> {
    let client = Client::new()?;

    let (response, user) = client.random_new_user().await?;
    assert_eq!(response.status(), StatusCode::OK);

    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    let response = client.login_verify().await?;
    let login_model = assert_login_verify_success(response).await?;

    assert_eq!(login_model.username, user.username);

    Ok(())
}

#[actix_web::test]
async fn login_fails_when_password_is_wrong() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, user) = client.random_new_user().await?;

    let wrong_password = "Hello World!";
    let response = client.login(&user.username, &wrong_password).await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    Ok(())
}

#[actix_web::test]
async fn user_can_reset_forgotten_password() -> TestResult<()> {
    let client = Client::new()?;

    let db = client.make_db_pool().await?;

    let new_valid_password = random_string(8);
    let (_response, user) = client.random_new_user().await?;
    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    let login_data: LoginModel = response.json().await?;

    client.initiate_password_reset(&user.username).await?;
    let token = PlayerTokenEntity::get_by_player(
        &mut *db.acquire().await?,
        login_data.user_id,
        &UserTokenType::ForgotPassword,
    )
    .await?
    .token;
    let response = client
        .do_password_reset(&user.username, &token, &new_valid_password)
        .await?;
    assert_eq!(response.status(), StatusCode::OK);

    assert_user_cannot_access_protected_stuff(&client).await?;

    // Old password should not work
    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    let response = client.login(&user.username, &new_valid_password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    Ok(())
}

#[actix_web::test]
async fn user_cannot_reset_password_without_knowing_current_password() -> TestResult<()> {
    let client = Client::new()?;

    let new_valid_password = random_string(8);
    let (_response, user) = client.random_new_user().await?;
    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    let response = client
        .update_user(PlayerUpdateForm {
            current_password: Some("Incorrect password".to_string()),
            new_password: Some(new_valid_password.clone()),
            ..Default::default()
        })
        .await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    let _response = client.logout().await?;

    // New password should not work
    let response = client.login(&user.username, &new_valid_password).await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    // Old password should still work
    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    Ok(())
}

#[actix_web::test]
async fn user_can_reset_password() -> TestResult<()> {
    let client = Client::new()?;
    let client2 = Client::new()?;

    let new_valid_password = random_string(8);
    let (_response, user) = client.random_new_user().await?;
    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    let response = client2.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    let response = client
        .update_user(PlayerUpdateForm {
            current_password: Some(user.password.clone()),
            new_password: Some(new_valid_password.clone()),
            ..Default::default()
        })
        .await?;
    assert_eq!(response.status(), StatusCode::OK);

    // User that did not reset password should be logged out
    assert_user_cannot_access_protected_stuff(&client2).await?;

    // User should still be logged in after resetting their password
    let response = client.login_verify().await?;
    assert_login_verify_success(response).await?;

    let _response = client.logout().await?;

    // Old password should not work
    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    let response = client.login(&user.username, &new_valid_password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    Ok(())
}

#[actix_web::test]
async fn user_cannot_reset_password_to_a_too_short_password() -> TestResult<()> {
    let client = Client::new()?;

    let too_short_new_password = "1234567";
    let (_response, user) = client.random_new_user().await?;
    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    let response = client
        .update_user(PlayerUpdateForm {
            current_password: Some(user.password.to_owned()),
            new_password: Some(too_short_new_password.to_string()),
            ..Default::default()
        })
        .await?;
    assert_eq!(response.status(), StatusCode::BAD_REQUEST);

    Ok(())
}

#[actix_web::test]
async fn user_cannot_access_protected_endpoints_when_logged_out() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, user) = client.random_new_user().await?;
    let _response = client.login(&user.username, &user.password).await?;
    let _response = client.logout().await?;

    assert_user_cannot_access_protected_stuff(&client).await?;

    Ok(())
}

#[actix_web::test]
async fn user_cannot_access_admin_thing() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, user) = client.random_new_user().await?;
    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    let response = client.refresh().await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    Ok(())
}

#[actix_web::test]
async fn changing_a_user_into_an_admin_removes_their_current_session() -> TestResult<()> {
    let client = Client::new()?;

    let user = UserRegisterForm {
        username: "admin".to_owned(),
        email: "admin@example.com".to_owned(),
        password: "12345678".to_owned(),
    };
    let _response = client.create_new_user(&user).await?;

    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    let login_model: LoginModel = response.json().await?;
    assert_eq!(login_model.role, Role::User);

    let response = client.refresh().await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    // Update user to admin behind the scenes
    let db = client.make_db_pool().await?;
    let player =
        PlayerService::find_player(&mut *db.acquire().await?, &login_model.username, false).await?;
    PlayerService::update_role(&mut *db.acquire().await?, player, Role::Admin).await?;

    assert_user_cannot_access_protected_stuff(&client).await?;

    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::OK);

    let response = client.refresh().await?;
    assert_eq!(response.status(), StatusCode::OK);

    Ok(())
}

#[actix_web::test]
async fn banning_user_invalidates_session() -> TestResult<()> {
    let client = Client::new()?;

    let (response, user) = client.random_new_user().await?;
    let login_model: LoginModel = response.json().await?;

    let _response = client.login(&user.username, &user.password).await?;

    assert_eq!(login_model.role, Role::User);

    let db = client.make_db_pool().await?;
    let player =
        PlayerService::find_player(&mut *db.acquire().await?, &login_model.username, false).await?;
    PlayerService::update_role(&mut *db.acquire().await?, player, Role::Banned).await?;

    assert_user_cannot_access_protected_stuff(&client).await?;

    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    Ok(())
}

#[actix_web::test]
async fn regular_user_cannot_update_player_role() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, user) = client.random_new_user().await?;
    let (response, _) = client.random_new_user().await?;
    let login_model: LoginModel = response.json().await?;

    let response = client
        .admin_update_user_role(login_model.user_id, Role::Banned)
        .await?;
    assert!(response.status().is_client_error());

    client.login(&user.username, &user.password).await?;
    let response = client
        .admin_update_user_role(login_model.user_id, Role::Banned)
        .await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    Ok(())
}

#[actix_web::test]
async fn admin_can_update_player_role() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, admin) = client.random_new_user().await?;
    client.update_user_role(&admin, Role::Admin).await?;

    let (response, user) = client.random_new_user().await?;
    let login_model: LoginModel = response.json().await?;

    client.login(&admin.username, &admin.password).await?;
    let response = client
        .admin_update_user_role(login_model.user_id, Role::Banned)
        .await?;

    let status = response.status();
    assert!(status.is_success());

    let response = client.login(&user.username, &user.password).await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    Ok(())
}

pub async fn assert_user_cannot_access_protected_stuff(client: &Client) -> TestResult<()> {
    let response = client.login_verify().await?;
    assert_login_verify_failure(response).await?;

    let response = client.add_score(&AddScoreForm::default()).await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    let response = client
        .verify_score(
            1,
            &VerifyScoreForm {
                status: ScoreStatus::Verified,
                comment: None,
            },
        )
        .await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    let response = client.upload_avatar("avatar.png", "image/png").await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    Ok(())
}
