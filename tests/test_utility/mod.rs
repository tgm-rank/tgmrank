use anyhow::bail;
use std::io::Read;

use futures::StreamExt;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use reqwest::StatusCode;

use tgmrank::controllers::player_controller::RivalForm;
use tgmrank::models::filters::RecentActivityQuery;
use tgmrank::models::forms::{
    AddScoreForm, PlayerLocationUpdateForm, PlayerUpdateForm, UpdateScoreForm, UserRegisterForm,
    VerifyScoreForm,
};
use tgmrank::modules::{DbPool, Settings};
use tgmrank::{controllers::game_controller::GameRankingQuery, models::Role};
use tgmrank::{controllers::mode_controller::ModeRankingQuery, services::PlayerService};
use tgmrank::{
    controllers::models::{
        ErrorModel, GenericModel, ModeRankingModel, OverallRankingEntryModel, PlayerModel,
        RecentActivityModel,
    },
    models::forms::PlayerUsernameUpdateForm,
};

mod data_generator;
pub use data_generator::*;
use tgmrank::controllers::badge_controller::NewBadgeForm;
use tgmrank::controllers::models::{LoginModel, PlatformModel};

pub type TestResult<T> = Result<T, anyhow::Error>;

pub struct Client {
    pub settings: Settings,
    pub client: reqwest::Client,
}

impl Client {
    pub fn new() -> TestResult<Self> {
        Ok(Self {
            settings: Settings::new("Settings.env")?,
            client: reqwest::Client::builder()
                .cookie_store(true)
                .build()
                .expect("Could not build http self.client"),
        })
    }

    pub async fn make_db_pool(&self) -> TestResult<DbPool> {
        let db_pool = self.settings.database.make_sqlx_connection_pool().await;
        Ok(db_pool)
    }

    pub async fn ping(&self) -> TestResult<reqwest::Response> {
        let response = self.client.get(&self.make_url("/v1/ping")).send().await?;
        Ok(response)
    }

    pub async fn get_site_version(&self) -> TestResult<reqwest::Response> {
        let response = self
            .client
            .get(&self.make_url("/v1/version"))
            .send()
            .await?;
        Ok(response)
    }

    pub async fn refresh(&self) -> TestResult<reqwest::Response> {
        let response = self
            .client
            .get(&self.make_url("/v1/admin/refresh"))
            .send()
            .await?;
        Ok(response)
    }

    pub async fn create_new_user(&self, form: &UserRegisterForm) -> TestResult<reqwest::Response> {
        let response = self
            .client
            .post(&self.make_url("/v1/register"))
            .json(&form)
            .send()
            .await?;

        Ok(response)
    }

    pub async fn create_admin(&self) -> TestResult<UserRegisterForm> {
        let db = self.make_db_pool().await?;

        let (response, form) = self.random_new_user().await?;
        let login_model: LoginModel = response.json().await?;

        let db = &mut *db.acquire().await?;
        let player = PlayerService::find_by_id(db, login_model.user_id).await?;
        let _player = PlayerService::update_role(db, player, Role::Admin).await?;

        Ok(form)
    }

    pub async fn admin_update_user_role(
        &self,
        user_id: i32,
        role: Role,
    ) -> TestResult<reqwest::Response> {
        let response = self
            .client
            .post(&self.make_url(&format!("/v1/player/{}/role/{}", user_id, role.to_string())))
            .header("content-length", "0")
            .send()
            .await?;

        Ok(response)
    }

    pub async fn update_user_role(&self, user: &UserRegisterForm, role: Role) -> TestResult<()> {
        let db = self.make_db_pool().await?;
        let db = &mut *db.acquire().await?;
        let player = PlayerService::find_player(db, &user.username, false).await?;
        PlayerService::update_role(db, player, role).await?;

        Ok(())
    }

    pub async fn random_new_user(&self) -> TestResult<(reqwest::Response, UserRegisterForm)> {
        let valid_username = random_string(5);
        let valid_password = "12345678";
        let valid_email = format!("{}@example.com", valid_username);

        let registration_form = UserRegisterForm {
            username: valid_username.to_string(),
            email: valid_email.to_string(),
            password: valid_password.to_string(),
        };

        let response = self.create_new_user(&registration_form).await?;

        Ok((response, registration_form))
    }

    pub async fn login(&self, username: &str, password: &str) -> TestResult<reqwest::Response> {
        use tgmrank::models::forms::LoginForm;

        let login_form = LoginForm {
            username: username.to_string(),
            password: password.to_string(),
            remember: None,
        };

        let response = self
            .client
            .post(&self.make_url("/v1/login"))
            .json(&login_form)
            .send()
            .await?;

        Ok(response)
    }

    pub async fn login_verify(&self) -> TestResult<reqwest::Response> {
        Ok(self
            .client
            .get(&self.make_url("/v1/login/verify"))
            .send()
            .await?)
    }

    pub async fn logout(&self) -> TestResult<reqwest::Response> {
        Ok(self.client.get(&self.make_url("/v1/logout")).send().await?)
    }

    pub async fn add_score(&self, score_to_add: &AddScoreForm) -> TestResult<reqwest::Response> {
        let response = self
            .client
            .post(&self.make_url("/v1/score"))
            .json(score_to_add)
            .send()
            .await?;
        Ok(response)
    }

    pub async fn add_scores(&self, scores_to_add: &[AddScoreForm]) -> TestResult<()> {
        futures::stream::iter(scores_to_add)
            .for_each_concurrent(10, |s| async move {
                let _result = self.add_score(&s).await.unwrap();
            })
            .await;
        Ok(())
    }

    pub async fn update_score(&self, update: &UpdateScoreForm) -> TestResult<reqwest::Response> {
        let response = self
            .client
            .patch(&self.make_url("/v1/score"))
            .json(update)
            .send()
            .await?;
        Ok(response)
    }

    pub async fn verify_score(
        &self,
        score_id: i32,
        form: &VerifyScoreForm,
    ) -> TestResult<reqwest::Response> {
        let response = self
            .client
            .post(&self.make_url(&format!("/v1/score/{}/verify", score_id)))
            .json(form)
            .send()
            .await?;
        Ok(response)
    }

    pub async fn get_ranking(&self, game_id: i32) -> TestResult<Vec<OverallRankingEntryModel>> {
        self.get_ranking2(game_id, &GameRankingQuery::default())
            .await
    }

    pub async fn get_ranking2(
        &self,
        game_id: i32,
        query: &GameRankingQuery,
    ) -> TestResult<Vec<OverallRankingEntryModel>> {
        let path = &format!("/v1/game/{}/ranking", game_id);
        let response = self
            .client
            .get(&self.make_url(path))
            .query(query)
            .send()
            .await?;
        let body = response.text().await?;

        Ok(serde_json::from_str(&body)?)
    }

    pub async fn get_mode_ranking(&self, mode_id: i32) -> TestResult<Vec<ModeRankingModel>> {
        let path = &format!("/v1/mode/{}/ranking", mode_id);
        let response = self.client.get(&self.make_url(path)).send().await?;
        Ok(response.json().await?)
    }

    pub async fn get_mode_ranking2(
        &self,
        mode_id: i32,
        query: &ModeRankingQuery,
    ) -> TestResult<Vec<ModeRankingModel>> {
        let path = &format!("/v1/mode/{}/ranking", mode_id);
        let response = self
            .client
            .get(&self.make_url(path))
            .query(query)
            .send()
            .await?;
        Ok(response.json().await?)
    }

    pub async fn get_platforms_for_mode(&self, mode_id: i32) -> TestResult<Vec<PlatformModel>> {
        let path = &format!("/v1/mode/{}/platform", mode_id);
        let response = self.client.get(&self.make_url(path)).send().await?;
        Ok(response.json().await?)
    }

    pub async fn get_player_scores(&self, player_id: i32) -> TestResult<Vec<ModeRankingModel>> {
        let path = &format!("/v1/player/{}/scores", player_id);
        let response = self.client.get(&self.make_url(path)).send().await?;
        Ok(response.json().await?)
    }

    pub async fn get_rivals_for_player_raw(&self, player_id: i32) -> TestResult<reqwest::Response> {
        let path = &format!("/v1/player/{}/rivals", player_id);
        let response = self.client.get(&self.make_url(path)).send().await?;
        Ok(response)
    }

    pub async fn get_rivals_for_player(&self, player_id: i32) -> TestResult<Vec<PlayerModel>> {
        let response = self.get_rivals_for_player_raw(player_id).await?;
        Ok(response.json().await?)
    }

    pub async fn get_rivaled_by_for_player(&self, player_id: i32) -> TestResult<Vec<PlayerModel>> {
        let path = &format!("/v1/player/{}/rivaledBy", player_id);
        let response = self.client.get(&self.make_url(path)).send().await?;
        Ok(response.json().await?)
    }

    pub async fn post_rival_for_player_raw(
        &self,
        player_id: i32,
        rival_player_id: i32,
    ) -> TestResult<reqwest::Response> {
        let path = &format!("/v1/player/{}/rival", player_id);
        let rival_form = RivalForm { rival_player_id };
        let response = self
            .client
            .post(&self.make_url(path))
            .json(&rival_form)
            .send()
            .await?;
        Ok(response)
    }

    pub async fn post_rival_for_player(
        &self,
        player_id: i32,
        rival_player_id: i32,
    ) -> TestResult<PlayerModel> {
        let response = self
            .post_rival_for_player_raw(player_id, rival_player_id)
            .await?;
        Ok(response.json().await?)
    }

    pub async fn delete_rival_for_player_raw(
        &self,
        player_id: i32,
        rival_player_id: i32,
    ) -> TestResult<reqwest::Response> {
        let path = &format!("/v1/player/{}/rival/player/{}", player_id, rival_player_id);
        let response = self.client.delete(&self.make_url(path)).send().await?;
        Ok(response)
    }

    pub async fn delete_rival_for_player(
        &self,
        player_id: i32,
        rival_player_id: i32,
    ) -> TestResult<GenericModel> {
        let response = self
            .delete_rival_for_player_raw(player_id, rival_player_id)
            .await?;
        Ok(response.json().await?)
    }

    pub async fn get_score(&self, score_id: i32) -> TestResult<ModeRankingModel> {
        let path = &format!("/v1/score/{}", score_id);
        let response = self.client.get(&self.make_url(path)).send().await?;
        Ok(response.json().await?)
    }

    pub async fn get_recent_activity(
        &self,
        query: &RecentActivityQuery,
    ) -> TestResult<Vec<RecentActivityModel>> {
        let response = self
            .client
            .get(&self.make_url("/v1/score/activity"))
            .query(query)
            .send()
            .await?;
        Ok(response.json().await?)
    }

    pub async fn initiate_password_reset(
        &self,
        player_name: &str,
    ) -> TestResult<reqwest::Response> {
        use tgmrank::models::forms::ForgotPasswordForm;

        let forgot_password_form = ForgotPasswordForm {
            username: player_name.to_string(),
        };

        let response = self
            .client
            .post(&self.make_url("/v1/forgot_password"))
            .json(&forgot_password_form)
            .send()
            .await?;

        Ok(response)
    }

    pub async fn do_password_reset(
        &self,
        player_name: &str,
        reset_token: &str,
        new_password: &str,
    ) -> TestResult<reqwest::Response> {
        use tgmrank::models::forms::ResetPasswordForm;

        let reset_password_form = ResetPasswordForm {
            password: new_password.to_string(),
        };

        let response = self
            .client
            .post(&self.make_url(&format!(
                "/v1/reset_password/{}/{}",
                player_name, reset_token
            )))
            .json(&reset_password_form)
            .send()
            .await?;

        Ok(response)
    }

    pub async fn update_user(
        &self,
        update_form: PlayerUpdateForm,
    ) -> TestResult<reqwest::Response> {
        let response = self
            .client
            .patch(&self.make_url("/v1/player"))
            .json(&update_form)
            .send()
            .await?;

        Ok(response)
    }

    pub async fn update_user_location(
        &self,
        update_form: PlayerLocationUpdateForm,
    ) -> TestResult<reqwest::Response> {
        let response = self
            .client
            .put(&self.make_url("/v1/player/location"))
            .json(&update_form)
            .send()
            .await?;

        Ok(response)
    }

    pub async fn get_username_history(&self, player_id: i32) -> TestResult<reqwest::Response> {
        let response = self
            .client
            .get(&self.make_url(&format!("/v1/player/{}/username", player_id)))
            .send()
            .await?;

        Ok(response)
    }

    pub async fn update_username(
        &self,
        update_form: PlayerUsernameUpdateForm,
    ) -> TestResult<reqwest::Response> {
        let response = self
            .client
            .put(&self.make_url("/v1/player/username"))
            .json(&update_form)
            .send()
            .await?;

        Ok(response)
    }

    pub async fn get_player_achievements(&self, player_id: i32) -> TestResult<reqwest::Response> {
        let path = &format!("/v1/player/{}/achievement", player_id);
        let response = self.client.get(&self.make_url(path)).send().await?;

        Ok(response)
    }

    pub async fn create_badge(&self, name: &str) -> TestResult<reqwest::Response> {
        let path = "/v1/badge";
        let response = self
            .client
            .post(&self.make_url(path))
            .json(&NewBadgeForm {
                name: name.to_string(),
            })
            .send()
            .await?;

        Ok(response)
    }

    pub async fn add_badge_for_player(
        &self,
        player_id: i32,
        badge_id: i32,
    ) -> TestResult<reqwest::Response> {
        let path = &format!("/v1/player/{}/badge/{}", player_id, badge_id);
        let request = self
            .client
            .post(&self.make_url(path))
            .header("content-length", 0)
            .header("Host", "example.abc.123");
        let response = request.send().await?;
        Ok(response)
    }

    pub async fn remove_badge_for_player(
        &self,
        player_id: i32,
        badge_id: i32,
    ) -> TestResult<reqwest::Response> {
        let path = &format!("/v1/player/{}/badge/{}", player_id, badge_id);
        let response = self
            .client
            .delete(&self.make_url(path))
            .header("Host", "example.abc.123")
            .send()
            .await?;
        Ok(response)
    }

    pub async fn get_player_badges(&self, player_id: i32) -> TestResult<reqwest::Response> {
        let path = &format!("/v1/player/{}/badge", player_id);
        let response = self
            .client
            .get(&self.make_url(path))
            .header("Host", "example.abc.123")
            .send()
            .await?;

        Ok(response)
    }

    pub async fn get_achievement_list(&self) -> TestResult<reqwest::Response> {
        let path = "/v1/achievement";
        let response = self.client.get(&self.make_url(path)).send().await?;

        Ok(response)
    }

    pub async fn upload_avatar(
        &self,
        filename: &str,
        mime_str: &str,
    ) -> TestResult<reqwest::Response> {
        let mut buffer: Vec<u8> = Vec::new();
        std::fs::File::open(format!("tests/test_data/{}", filename))?.read_to_end(&mut buffer)?;

        let part = reqwest::multipart::Part::bytes(buffer)
            .mime_str(mime_str)?
            .file_name(filename.to_string());

        let form = reqwest::multipart::Form::new().part(
            tgmrank::controllers::avatar_controller::AVATAR_FILE_FORM_KEY,
            part,
        );

        let response = self
            .client
            .post(&self.make_url("/v1/avatar"))
            .multipart(form)
            .send()
            .await?;

        Ok(response)
    }

    pub fn make_url(&self, path: &str) -> String {
        format!(
            "http://localhost:{}{}",
            self.settings.application.port, path
        )
    }
}

pub fn random_string(count: usize) -> String {
    let chars: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(count)
        .map(char::from)
        .collect();

    chars
}

pub fn assert_data_errors_has_field<'a>(
    error_model: &'a ErrorModel,
    field_name: &str,
) -> &'a ErrorModel {
    assert_eq!(error_model.data_errors.is_some(), true);
    let data_errors = error_model.data_errors.as_ref().unwrap();

    let error = data_errors
        .iter()
        .find(|e| e.field.as_ref().unwrap_or(&"".to_string()) == field_name);
    assert_eq!(
        error.is_some(),
        true,
        "Could not find data error for field {}",
        field_name
    );

    error.unwrap()
}

pub async fn assert_login_verify_success(response: reqwest::Response) -> TestResult<LoginModel> {
    assert_eq!(response.status(), StatusCode::OK);

    let login: LoginModel = get_json(&response.text().await?)?;
    Ok(login)
}

pub async fn assert_login_verify_failure(response: reqwest::Response) -> TestResult<GenericModel> {
    assert_eq!(response.status(), StatusCode::OK);

    let model: GenericModel = get_json(&response.text().await?)?;
    assert_eq!(model.success, false);
    Ok(model)
}

pub fn get_json<'a, T>(text: &'a str) -> TestResult<T>
where
    T: serde::de::Deserialize<'a>,
{
    let deserialized = serde_json::from_str::<T>(text);

    match deserialized {
        Ok(obj) => Ok(obj),
        Err(e) => bail!(
            "Text could not be parsed as json model({:?}): {:?}",
            e,
            text
        ),
    }
}
