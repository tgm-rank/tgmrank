use rand::{thread_rng, Rng};
use sqlx::postgres::types::PgRange;
use tgmrank::models::forms::ProofLinkForm;
use tgmrank::models::NewModeEntity;
use tgmrank::models::{forms::AddScoreForm, Mode, ProofType};

use super::random_string;

pub struct DataGenerator();

const PROOF_IMAGES: [&'static str; 5] = [
    "https://i.imgur.com/heHjPSRm.jpg",
    "https://i.imgur.com/lvRKeYmm.jpg",
    "https://i.imgur.com/RRYF6Jtm.jpg",
    "https://i.imgur.com/DBpFpp8m.jpg",
    "https://i.imgur.com/vUvV8nFm.jpg",
];

impl DataGenerator {
    pub fn test_score() -> AddScoreForm {
        AddScoreForm {
            proof: vec![Self::get_random_proof()],
            ..Default::default()
        }
    }

    pub fn random_tgm1_gm() -> AddScoreForm {
        let mut rng = thread_rng();
        let score = rng.gen_range(126_000..200_000);

        AddScoreForm {
            mode_id: Mode::Tgm1Master as i32,
            grade_id: Some(19),
            level: Some(999),
            playtime: Some(DataGenerator::generate_time(10, 13)),
            score: Some(score),
            ..DataGenerator::test_score()
        }
    }

    pub fn random_tap_master_clear() -> AddScoreForm {
        let mut rng = thread_rng();

        // S1 to S9, green/orange line
        let grade_id = rng.gen_range(366..383);

        AddScoreForm {
            mode_id: Mode::TapMaster as i32,
            grade_id: Some(grade_id),
            level: Some(999),
            playtime: Some(DataGenerator::generate_time(8, 9)),
            score: None,
            ..DataGenerator::test_score()
        }
    }

    pub fn generate_time(minute_min: i32, minute_max_exclusive: i32) -> String {
        let mut rng = thread_rng();
        let minutes = rng.gen_range(minute_min..minute_max_exclusive);
        let seconds = rng.gen_range(0..45);
        let cs = rng.gen_range(0..100);

        format!("{:02}:{:02}:{:02}", minutes, seconds, cs)
    }

    pub fn get_random_proof() -> ProofLinkForm {
        let mut rng = thread_rng();
        ProofLinkForm {
            proof_type: ProofType::Image,
            link: PROOF_IMAGES[rng.gen_range(0..PROOF_IMAGES.len())].to_string(),
        }
    }

    pub fn test_mode() -> NewModeEntity {
        NewModeEntity {
            game_id: 1,
            mode_name: random_string(8),
            is_ranked: false,
            weight: 50,
            margin: 100,
            link: None,
            grade_sort: None,
            score_sort: None,
            level_sort: None,
            time_sort: None,
            sort_order: 1000,
            description: None,
            submission_range: PgRange::from((
                std::ops::Bound::Unbounded,
                std::ops::Bound::Unbounded,
            )),
            tags: None,
        }
    }
}
