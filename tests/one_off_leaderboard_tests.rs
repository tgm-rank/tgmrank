mod one_off_leaderboard_tests {
    use sqlx::postgres::types::PgRange;
    use tgmrank::{
        controllers::{mode_controller::ModeRankingQuery, models::ModeRankingModel},
        models::{forms::AddScoreForm, ModeEntity, ModeTagRef, NewModeEntity},
        modules::DbPool,
    };
    use time::{Duration, OffsetDateTime};

    use crate::test_utility::{random_string, Client, DataGenerator, TestResult};

    #[actix_web::test]
    async fn cannot_submit_score_for_expired_leaderboard() -> TestResult<()> {
        let client = Client::new()?;

        let db = client.make_db_pool().await?;

        let mut connection = db.acquire().await?;
        let new_mode = ModeEntity::insert_mode(
            &mut connection,
            &NewModeEntity {
                mode_name: format!("TooLateSubmission_{}", random_string(8)),
                submission_range: PgRange::from((
                    std::ops::Bound::Unbounded,
                    std::ops::Bound::Included(OffsetDateTime::now_utc() - Duration::seconds(8)),
                )),
                tags: Some(vec![ModeTagRef::Event.to_string()]),
                ..DataGenerator::test_mode()
            },
        )
        .await?;

        let (_response, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;
        let response = client
            .add_score(
                &(AddScoreForm {
                    mode_id: new_mode.mode_id,
                    ..DataGenerator::random_tgm1_gm()
                }),
            )
            .await?;

        assert!(response.status().is_client_error());

        Ok(())
    }

    #[actix_web::test]
    async fn cannot_submit_score_too_early() -> TestResult<()> {
        let client = Client::new()?;

        let db = client.make_db_pool().await?;
        let mut connection = db.acquire().await?;
        let new_mode = ModeEntity::insert_mode(
            &mut connection,
            &NewModeEntity {
                mode_name: format!("TooEarlySubmission_{}", random_string(8)),
                submission_range: PgRange::from((
                    std::ops::Bound::Included(OffsetDateTime::now_utc() + Duration::seconds(10)),
                    std::ops::Bound::Unbounded,
                )),
                tags: Some(vec![ModeTagRef::Event.to_string()]),
                ..DataGenerator::test_mode()
            },
        )
        .await?;

        let (_response, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;
        let response = client
            .add_score(
                &(AddScoreForm {
                    mode_id: new_mode.mode_id,
                    ..DataGenerator::random_tgm1_gm()
                }),
            )
            .await?;

        assert!(response.status().is_client_error());

        Ok(())
    }

    #[actix_web::test]
    async fn can_submit_score_for_one_off_leaderboard() -> TestResult<()> {
        let client = Client::new()?;

        let db = client.make_db_pool().await?;
        let mut connection = db.acquire().await?;
        let new_mode = ModeEntity::insert_mode(
            &mut connection,
            &NewModeEntity {
                mode_name: format!("WithinSubmissionWindow_{}", random_string(8)),
                submission_range: PgRange::from((
                    std::ops::Bound::Included(OffsetDateTime::now_utc() - Duration::seconds(16)),
                    std::ops::Bound::Included(OffsetDateTime::now_utc() + Duration::seconds(21)),
                )),
                tags: Some(vec![ModeTagRef::Event.to_string()]),
                ..DataGenerator::test_mode()
            },
        )
        .await?;

        let (_response, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;
        let response = client
            .add_score(
                &(AddScoreForm {
                    mode_id: new_mode.mode_id,
                    ..DataGenerator::random_tgm1_gm()
                }),
            )
            .await?;

        assert!(response.status().is_success());

        Ok(())
    }

    #[actix_web::test]
    async fn carnival_of_death_should_validate_death_scores() -> TestResult<()> {
        let client = Client::new()?;

        let db = client.make_db_pool().await?;
        let mut connection = db.acquire().await?;
        let new_mode = ModeEntity::insert_mode(
            &mut connection,
            &NewModeEntity {
                game_id: 2,
                mode_name: format!("Carnival of Death [Test] {}", random_string(8)),
                level_sort: Some(-1),
                time_sort: Some(1),
                submission_range: PgRange::from((
                    std::ops::Bound::Included(OffsetDateTime::now_utc() - Duration::seconds(16)),
                    std::ops::Bound::Included(OffsetDateTime::now_utc() + Duration::seconds(21)),
                )),
                tags: Some(vec![ModeTagRef::Carnival.to_string()]),
                ..DataGenerator::test_mode()
            },
        )
        .await?;

        let (_response, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;
        let response = client
            .add_score(
                &(AddScoreForm {
                    mode_id: new_mode.mode_id,
                    level: Some(1000),
                    playtime: Some("05:59:99".to_string()),
                    ..DataGenerator::test_score()
                }),
            )
            .await?;

        assert!(response.status().is_client_error());

        let response = client
            .add_score(
                &(AddScoreForm {
                    mode_id: new_mode.mode_id,
                    level: Some(999),
                    playtime: Some("05:59:99".to_string()),
                    ..DataGenerator::test_score()
                }),
            )
            .await?;

        assert!(response.status().is_success());

        Ok(())
    }

    async fn make_linked_leaderboards(db: &mut DbPool) -> TestResult<(ModeEntity, ModeEntity)> {
        let uniquifier = random_string(8);
        let mut transaction = db.begin().await?;

        let parent_mode = ModeEntity::insert_mode(
            &mut transaction,
            &NewModeEntity {
                game_id: 3,
                mode_name: format!("Parent Leaderboard {}", uniquifier),
                ..DataGenerator::test_mode()
            },
        )
        .await?;

        let child_mode = ModeEntity::insert_mode(
            &mut transaction,
            &NewModeEntity {
                game_id: 3,
                mode_name: format!("Child Leaderboard {}", uniquifier),
                ..DataGenerator::test_mode()
            },
        )
        .await?;

        transaction.commit().await?;

        Ok((parent_mode, child_mode))
    }

    async fn find_score_on_mode_ranking(
        client: &Client,
        score_id: i32,
        mode_id: i32,
        expect_found: bool,
    ) -> TestResult<()> {
        let ranking = client
            .get_mode_ranking2(mode_id, &ModeRankingQuery::default())
            .await?;

        let score_is_found = ranking.iter().find(|s| s.score_id == score_id).is_some();

        assert!(score_is_found == expect_found);

        Ok(())
    }

    #[actix_web::test]
    async fn linked_leaderboard_scores_on_child_leaderboard_should_show_up_on_parent_leaderboard(
    ) -> TestResult<()> {
        let client = Client::new()?;
        let mut db = client.make_db_pool().await?;

        let (parent_mode, child_mode) = make_linked_leaderboards(&mut db).await?;

        ModeEntity::link_modes(
            &mut *db.acquire().await?,
            parent_mode.mode_id,
            child_mode.mode_id,
        )
        .await?;

        let (_response, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let score: ModeRankingModel = client
            .add_score(
                &(AddScoreForm {
                    mode_id: child_mode.mode_id,
                    ..DataGenerator::test_score()
                }),
            )
            .await?
            .json()
            .await?;

        find_score_on_mode_ranking(&client, score.score_id, parent_mode.mode_id, true).await?;
        find_score_on_mode_ranking(&client, score.score_id, child_mode.mode_id, true).await?;

        Ok(())
    }

    #[actix_web::test]
    async fn linked_leaderboard_scores_on_parent_leaderboard_should_not_show_up_on_child_leaderboard(
    ) -> TestResult<()> {
        let client = Client::new()?;
        let mut db = client.make_db_pool().await?;

        let (parent_mode, child_mode) = make_linked_leaderboards(&mut db).await?;

        ModeEntity::link_modes(
            &mut *db.acquire().await?,
            parent_mode.mode_id,
            child_mode.mode_id,
        )
        .await?;

        let (_response, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let score: ModeRankingModel = client
            .add_score(
                &(AddScoreForm {
                    mode_id: parent_mode.mode_id,
                    ..DataGenerator::test_score()
                }),
            )
            .await?
            .json()
            .await?;

        find_score_on_mode_ranking(&client, score.score_id, child_mode.mode_id, false).await?;
        find_score_on_mode_ranking(&client, score.score_id, parent_mode.mode_id, true).await?;

        Ok(())
    }

    #[actix_web::test]
    async fn leaderboards_in_overall_game_should_rank_properly() -> TestResult<()> {
        let client = Client::new()?;

        let db = client.make_db_pool().await?;
        let mut connection = db.acquire().await?;
        let new_mode = ModeEntity::insert_mode(
            &mut connection,
            &NewModeEntity {
                game_id: 0,
                mode_name: format!("One-off Test {}", random_string(8)),
                level_sort: Some(-1),
                time_sort: Some(1),
                submission_range: PgRange::from((
                    std::ops::Bound::Included(OffsetDateTime::now_utc() - Duration::seconds(16)),
                    std::ops::Bound::Included(OffsetDateTime::now_utc() + Duration::seconds(21)),
                )),
                tags: Some(vec![ModeTagRef::Event.to_string()]),
                ..DataGenerator::test_mode()
            },
        )
        .await?;

        let (_response, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let score: ModeRankingModel = client
            .add_score(
                &(AddScoreForm {
                    mode_id: new_mode.mode_id,
                    ..DataGenerator::test_score()
                }),
            )
            .await?
            .json()
            .await?;

        find_score_on_mode_ranking(&client, score.score_id, new_mode.mode_id, true).await?;

        Ok(())
    }
}
