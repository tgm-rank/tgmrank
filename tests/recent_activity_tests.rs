use anyhow::Context;
use reqwest::StatusCode;
use tgmrank::models::{filters::GenericQuery, forms::AddScoreForm, Mode, Role, ScoreStatus};
use tgmrank::{controllers::models::ModeRankingModel, models::filters::RecentActivityQuery};

use crate::test_utility::{Client, DataGenerator, TestResult};
use itertools::Itertools;
use tgmrank::models::forms::VerifyScoreForm;
use tgmrank::utility::WrappedInt;

#[actix_web::test]
async fn game_and_overall_recent_activity_for_unranked_modes_should_be_unranked() -> TestResult<()>
{
    let client = Client::new()?;

    let (_, user1) = client.random_new_user().await?;
    client.login(&user1.username, &user1.password).await?;
    let score_for_unranked_mode = AddScoreForm {
        mode_id: Mode::TiShiraseSecretGradeWorld as i32,
        grade_id: Some(483), // m9
        level: Some(174),
        playtime: Some("01:23.66".to_string()),
        ..DataGenerator::test_score()
    };

    let score: ModeRankingModel = client
        .add_score(&score_for_unranked_mode)
        .await?
        .json()
        .await?;

    let recent_scores = client
        .get_recent_activity(&RecentActivityQuery {
            filter: GenericQuery {
                score_status: Some(vec![ScoreStatus::Pending]),
                ..Default::default()
            },
            ..Default::default()
        })
        .await?;

    let activity = recent_scores
        .iter()
        .find(|rs| rs.score.score_id == score.score_id)
        .context("Could not find recent activity for score")?;

    assert!(activity.delta.overall.rank.is_none());
    assert!(activity.delta.overall.ranking_points.is_none());

    assert!(activity.delta.game.rank.is_none());
    assert!(activity.delta.game.ranking_points.is_none());

    assert!(activity.delta.mode.rank.is_some());
    assert!(activity.delta.mode.ranking_points.is_some());

    Ok(())
}

#[actix_web::test]
async fn multiple_pbs_should_show_progress() -> TestResult<()> {
    let client = Client::new()?;

    let (_, user1) = client.random_new_user().await?;
    client.login(&user1.username, &user1.password).await?;
    {
        let score_to_add = AddScoreForm {
            level: Some(998),
            grade_id: Some(18),
            ..DataGenerator::random_tgm1_gm()
        };
        client.add_score(&score_to_add).await?;

        let better_score = DataGenerator::random_tgm1_gm();
        client.add_score(&better_score).await?;
    }
    client.logout().await?;

    let recent_scores = client
        .get_recent_activity(&RecentActivityQuery {
            filter: GenericQuery {
                player_name: Some(user1.username),
                score_status: Some(vec![ScoreStatus::Pending]),
                ..Default::default()
            },
            ..Default::default()
        })
        .await?;

    assert_eq!(recent_scores.len(), 2);

    let pb = recent_scores
        .first()
        .context("Could not find recent activity for score")?;
    assert!(pb.score.rank.is_some());
    assert!(pb.previous_score.is_some());

    let prev_pb = recent_scores
        .last()
        .context("Could not find recent activity for score")?;
    assert!(prev_pb.score.rank.is_some());
    assert!(prev_pb.previous_score.is_none());

    assert!(pb.previous_score.as_ref().map(|ps| ps.score_id) == Some(prev_pb.score.score_id));

    Ok(())
}

#[actix_web::test]
async fn non_pbs_should_be_shown_with_previous_score_id() -> TestResult<()> {
    let client = Client::new()?;

    let (_, user1) = client.random_new_user().await?;
    client.login(&user1.username, &user1.password).await?;

    let pb_to_add = AddScoreForm {
        level: Some(998),
        grade_id: Some(18),
        ..DataGenerator::random_tgm1_gm()
    };
    let pb_response: ModeRankingModel = client.add_score(&pb_to_add).await?.json().await?;

    let non_pb_to_add = AddScoreForm {
        level: Some(997),
        grade_id: Some(18),
        ..pb_to_add
    };
    let non_pb_response: ModeRankingModel = client.add_score(&non_pb_to_add).await?.json().await?;

    client.logout().await?;

    let recent_scores = client
        .get_recent_activity(&RecentActivityQuery {
            filter: GenericQuery {
                player_name: Some(user1.username),
                score_status: Some(vec![ScoreStatus::Pending]),
                ..Default::default()
            },
            ..Default::default()
        })
        .await?;

    let pb_recent_activity = recent_scores
        .iter()
        .find(|rs| rs.score.score_id == pb_response.score_id);

    assert!(pb_recent_activity.is_some());
    assert!(pb_recent_activity.unwrap().previous_score.is_none());

    let non_pb_recent_activity = recent_scores
        .iter()
        .find(|rs| rs.score.score_id == non_pb_response.score_id);

    assert!(non_pb_recent_activity.is_some());
    assert!(non_pb_recent_activity.unwrap().previous_score.is_some());

    let non_pb_previous_score = non_pb_recent_activity
        .unwrap()
        .previous_score
        .as_ref()
        .unwrap();
    assert!(non_pb_previous_score.score_id == pb_response.score_id);

    Ok(())
}

#[actix_web::test]
async fn ranking_and_recent_activity_is_calculated_for_mode() -> TestResult<()> {
    let client = Client::new()?;

    let (_, user1) = client.random_new_user().await?;
    let (_, user2) = client.random_new_user().await?;

    let tap_master_mode_id = 6;

    client.login(&user1.username, &user1.password).await?;
    {
        let score_to_add = AddScoreForm {
            mode_id: tap_master_mode_id,
            grade_id: Some(382), // Green S9
            level: Some(999),
            playtime: Some("10:05.71".into()),
            score: Some(123_456),
            ..DataGenerator::test_score()
        };

        let response = client.add_score(&score_to_add).await?;
        assert_eq!(response.status(), StatusCode::OK);
    }
    client.logout().await?;

    client.login(&user2.username, &user2.password).await?;
    {
        let score_to_add = AddScoreForm {
            mode_id: tap_master_mode_id,
            grade_id: Some(116), // Green GM
            level: Some(999),
            playtime: Some("08:42.31".into()),
            score: Some(147_258),
            ..DataGenerator::test_score()
        };

        let response = client.add_score(&score_to_add).await?;
        assert_eq!(response.status(), StatusCode::OK);
    }
    client.logout().await?;

    let mode_ranking = client.get_mode_ranking(tap_master_mode_id).await?;

    let rank1 = mode_ranking
        .iter()
        .find(|s| s.player.as_ref().map(|p| &p.player_name) == Some(&user1.username))
        .expect("Could not find user's rank");
    let rank2 = mode_ranking
        .iter()
        .find(|s| s.player.as_ref().map(|p| &p.player_name) == Some(&user2.username))
        .expect("Could not find user's rank");

    assert!(rank1.rank > rank2.rank);
    assert!(rank1.ranking_points < rank2.ranking_points);

    let mut recent_scores = client
        .get_recent_activity(&RecentActivityQuery {
            filter: GenericQuery {
                player_name: Some(user2.username),
                score_status: Some(vec![ScoreStatus::Pending]),
                ..Default::default()
            },
            ..Default::default()
        })
        .await?;

    let delta = recent_scores.swap_remove(0).delta;
    let found_player1 = delta
        .mode
        .players_offset
        .unwrap_or_default()
        .iter()
        .any(|o| o.player_name == user1.username);
    assert_eq!(found_player1, true, "User2 did not pass user1?");

    let found_player1 = delta
        .game
        .players_offset
        .unwrap_or_default()
        .iter()
        .any(|o| o.player_name == user1.username);
    assert_eq!(found_player1, true, "User2 did not pass user1?");

    let found_player1 = delta
        .overall
        .players_offset
        .unwrap_or_default()
        .iter()
        .any(|o| o.player_name == user1.username);
    assert_eq!(found_player1, true, "User2 did not pass user1?");

    Ok(())
}

#[actix_web::test]
async fn filter_by_rank_when_rank_threshold_is_given() -> TestResult<()> {
    let client = Client::new()?;

    let (_, user1) = client.random_new_user().await?;
    client.login(&user1.username, &user1.password).await?;
    let amazing_tgm1_score = AddScoreForm {
        playtime: Some("08:30.10".to_string()),
        ..DataGenerator::random_tgm1_gm()
    };

    let score: ModeRankingModel = client.add_score(&amazing_tgm1_score).await?.json().await?;

    let rank_threshold = 1;
    let mode_ids = vec![1, 2, 6, 7, 15, 16]
        .into_iter()
        .map(WrappedInt::from)
        .collect_vec();

    let recent_scores = client
        .get_recent_activity(&RecentActivityQuery {
            filter: GenericQuery {
                mode_ids: Some(mode_ids.clone()),
                score_status: Some(vec![ScoreStatus::Pending]),
                ..Default::default()
            },
            filter_mode_ids: Some(mode_ids),
            rank_threshold: Some(rank_threshold),
            ..Default::default()
        })
        .await?;

    assert!(recent_scores.len() >= 1);
    assert_eq!(recent_scores[0].score.score_id, score.score_id);

    let all_scores_within_threshold = recent_scores.iter().all(|rs| {
        rs.delta.overall.rank.unwrap_or(999) <= rank_threshold
            || rs.delta.game.rank.unwrap_or(999) <= rank_threshold
            || rs.delta.mode.rank.unwrap_or(999) <= rank_threshold
    });
    assert!(all_scores_within_threshold);

    Ok(())
}

#[actix_web::test]
async fn should_be_sorted_by_date() -> TestResult<()> {
    let client = Client::new()?;

    for _i in 0..3 {
        let (_, user1) = client.random_new_user().await?;
        client.login(&user1.username, &user1.password).await?;
        {
            let response = client.add_score(&DataGenerator::random_tgm1_gm()).await?;
            assert_eq!(response.status(), StatusCode::OK);
        }
        client.logout().await?;
    }

    let recent_scores = client
        .get_recent_activity(&RecentActivityQuery {
            filter: GenericQuery {
                score_status: Some(vec![ScoreStatus::Pending]),
                ..Default::default()
            },
            ..RecentActivityQuery::default()
        })
        .await?;

    assert!(
        recent_scores
            .into_iter()
            .map(|rs| rs.score.created_at)
            .rev()
            .is_sorted(),
        "Recent activity is not sorted by submission date descending"
    );

    Ok(())
}

#[actix_web::test]
async fn rejected_scores_should_be_removed_from_recent_activity() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, user) = client.random_new_user().await?;
    client.update_user_role(&user, Role::Verifier).await?;
    client.login(&user.username, &user.password).await?;
    let score: ModeRankingModel = client
        .add_score(&DataGenerator::random_tgm1_gm())
        .await?
        .json()
        .await?;

    let recent_scores = client
        .get_recent_activity(&RecentActivityQuery {
            filter: GenericQuery {
                score_status: Some(vec![ScoreStatus::Pending]),
                ..Default::default()
            },
            ..Default::default()
        })
        .await?;

    assert!(
        recent_scores
            .iter()
            .any(|rs| rs.score.score_id == score.score_id),
        "Did not find score in recent activity"
    );

    let reject_score_form = VerifyScoreForm {
        status: ScoreStatus::Rejected,
        comment: None,
    };

    let updated_score: ModeRankingModel = client
        .verify_score(score.score_id, &reject_score_form)
        .await?
        .json()
        .await?;

    assert_eq!(updated_score.status, ScoreStatus::Rejected);

    let recent_scores = client
        .get_recent_activity(&RecentActivityQuery {
            filter: GenericQuery {
                score_status: Some(vec![ScoreStatus::Pending]),
                ..Default::default()
            },
            ..Default::default()
        })
        .await?;

    assert!(
        !recent_scores
            .iter()
            .any(|rs| rs.score.score_id == score.score_id),
        "Unexpectedly found rejected score in recent activity"
    );

    Ok(())
}

#[actix_web::test]
async fn only_return_recent_activity_entries_for_requested_modes() -> TestResult<()> {
    let client = Client::new()?;

    let (_, user) = client.random_new_user().await?;
    client.login(&user.username, &user.password).await?;
    let score_for_other_mode = AddScoreForm {
        mode_id: Mode::TapDeathSeries as i32,
        level: Some(2856),
        ..DataGenerator::test_score()
    };

    let score_to_find: ModeRankingModel = client
        .add_score(&score_for_other_mode)
        .await?
        .json()
        .await?;

    let unrelated_score: ModeRankingModel = client
        .add_score(&DataGenerator::random_tap_master_clear())
        .await?
        .json()
        .await?;

    let recent_scores = client
        .get_recent_activity(&RecentActivityQuery {
            filter_mode_ids: Some(vec![(WrappedInt(Mode::TapDeathSeries as i32))]),
            filter: GenericQuery {
                score_status: Some(vec![ScoreStatus::Pending]),
                ..Default::default()
            },
            ..Default::default()
        })
        .await?;

    recent_scores
        .iter()
        .find(|rs| rs.score.score_id == score_to_find.score_id)
        .context("Could not find recent activity for score")?;

    let recent_activity = recent_scores
        .iter()
        .find(|rs| rs.score.score_id == unrelated_score.score_id);

    assert!(
        recent_activity.is_none(),
        "Should not have found score {} in recent activity for mode {}",
        unrelated_score.score_id,
        Mode::TapDeathSeries as i32
    );

    assert!(recent_activity
        .iter()
        .map(|ra| ra.mode_id)
        .all(|mode_id| mode_id == Mode::TapDeathSeries as i32));

    Ok(())
}

mod player_recent_activity_tests {
    use tgmrank::models::filters::{GenericQuery, RecentActivityQuery};

    use crate::test_utility::{Client, DataGenerator, TestResult};
    use tgmrank::models::ScoreStatus;

    #[actix_web::test]
    async fn player_recent_activity_should_only_show_recent_activity_for_requested_player(
    ) -> TestResult<()> {
        let client = Client::new()?;

        let (_, user1) = client.random_new_user().await?;
        client.login(&user1.username, &user1.password).await?;

        client.add_score(&DataGenerator::random_tgm1_gm()).await?;

        let recent_scores = client
            .get_recent_activity(&RecentActivityQuery {
                filter: GenericQuery {
                    player_name: Some(user1.username.to_string()),
                    score_status: Some(vec![ScoreStatus::Pending]),
                    ..Default::default()
                },
                ..Default::default()
            })
            .await?;

        assert_eq!(recent_scores.len(), 1);

        let mut players = recent_scores.into_iter().map(|s| s.score.player);
        assert!(players.all(|player| matches!(player, Some(p) if p.player_name == user1.username)));

        Ok(())
    }
}

mod tiebreaker_tests {
    use anyhow::Context;
    use tgmrank::{
        controllers::models::{ModeRankingModel, PlayerModel},
        models::{filters::RecentActivityQuery, forms::AddScoreForm},
    };

    use crate::test_utility::{Client, DataGenerator, TestResult};
    use tgmrank::models::filters::GenericQuery;
    use tgmrank::models::ScoreStatus;

    #[actix_web::test]
    async fn players_offset_should_not_include_players_you_tie_with() -> TestResult<()> {
        let client = Client::new()?;

        let tied_score = &DataGenerator::random_tap_master_clear();

        let (_response, user1) = client.random_new_user().await?;
        client.login(&user1.username, &user1.password).await?;
        client.add_score(tied_score).await?;
        client.logout().await?;

        let (_response, user2) = client.random_new_user().await?;
        client.login(&user2.username, &user2.password).await?;
        let score: ModeRankingModel = client.add_score(tied_score).await?.json().await?;

        let recent_scores = client
            .get_recent_activity(&RecentActivityQuery {
                filter: GenericQuery {
                    score_status: Some(vec![ScoreStatus::Pending]),
                    ..Default::default()
                },
                ..RecentActivityQuery::default()
            })
            .await?;

        let score_activity = recent_scores
            .iter()
            .find(|rs| rs.score.score_id == score.score_id)
            .context("Could not find new score entry in recent activity")?;

        if let Some(mode_offset) = &score_activity.delta.mode.players_offset {
            assert!(
                !mode_offset.iter().any(|p| p.player_name == user1.username),
                "Players offset should not include tied player",
            );
        }

        if let Some(mode_offset) = &score_activity.delta.game.players_offset {
            assert!(
                !mode_offset.iter().any(|p| p.player_name == user1.username),
                "Players offset should not include tied player",
            );
        }

        if let Some(mode_offset) = &score_activity.delta.overall.players_offset {
            assert!(
                !mode_offset.iter().any(|p| p.player_name == user1.username),
                "Players offset should not include tied player",
            );
        }

        Ok(())
    }

    #[actix_web::test]
    async fn players_offset_should_include_players_you_were_tied_with_if_you_break_a_tie(
    ) -> TestResult<()> {
        let client = Client::new()?;

        let score_to_add = DataGenerator::random_tap_master_clear();

        let (_response, user_with_bad_score) = client.random_new_user().await?;

        client
            .login(&user_with_bad_score.username, &user_with_bad_score.password)
            .await?;
        let worse_clear = AddScoreForm {
            playtime: Some(DataGenerator::generate_time(9, 10)),
            ..score_to_add.clone()
        };
        client.add_score(&worse_clear).await?;
        client.logout().await?;

        let mut players = vec![];
        for _i in 0..3 {
            let (_response, user) = client.random_new_user().await?;

            client.login(&user.username, &user.password).await?;
            client.add_score(&score_to_add).await?;
            client.logout().await?;

            players.push(user);
        }

        players.push(user_with_bad_score);

        let tie_breaking_player = players.swap_remove(0);
        let better_clear = AddScoreForm {
            playtime: Some(DataGenerator::generate_time(7, 8)),
            ..score_to_add
        };

        client
            .login(&tie_breaking_player.username, &tie_breaking_player.password)
            .await?;
        let better_score: ModeRankingModel = client.add_score(&better_clear).await?.json().await?;

        let recent_scores = client
            .get_recent_activity(&RecentActivityQuery {
                filter: GenericQuery {
                    score_status: Some(vec![ScoreStatus::Pending, ScoreStatus::Verified]),
                    ..Default::default()
                },
                ..Default::default()
            })
            .await?;

        let better_score_activity = recent_scores
            .iter()
            .find(|rs| rs.score.score_id == better_score.score_id)
            .unwrap();

        let expected_players_offset = &players.iter().map(|p| &*p.username).collect::<Vec<&str>>();

        if let Some(po) = better_score_activity.delta.mode.players_offset.as_ref() {
            assert_passed_players(&po, expected_players_offset)
        }

        if let Some(po) = better_score_activity.delta.game.players_offset.as_ref() {
            assert_passed_players(&po, expected_players_offset)
        }

        if let Some(po) = better_score_activity.delta.overall.players_offset.as_ref() {
            assert_passed_players(&po, expected_players_offset)
        }

        Ok(())
    }

    #[actix_web::test]
    async fn can_get_recent_activity_by_score_id() -> TestResult<()> {
        let client = Client::new()?;

        let (_, user1) = client.random_new_user().await?;
        client.login(&user1.username, &user1.password).await?;

        let score: ModeRankingModel = client
            .add_score(&DataGenerator::random_tap_master_clear())
            .await?
            .json()
            .await?;

        let recent_scores = client
            .get_recent_activity(&RecentActivityQuery {
                score_id: Some(score.score_id),
                filter: GenericQuery {
                    score_status: Some(vec![ScoreStatus::Pending]),
                    ..Default::default()
                },
                ..RecentActivityQuery::default()
            })
            .await?;

        recent_scores
            .iter()
            .find(|rs| rs.score.score_id == score.score_id)
            .context("Could not find recent activity for score")?;

        assert_eq!(recent_scores.len(), 1);

        Ok(())
    }

    fn assert_passed_players(players_offset: &[PlayerModel], expected_players_offset: &[&str]) {
        let passed_player_names: Vec<&str> = players_offset
            .iter()
            .map(|p| p.player_name.as_ref())
            .collect();

        assert!(
            passed_player_names
                .iter()
                .all(|p| expected_players_offset.contains(&p.as_ref())),
            "Tied players are not in passed players"
        );
    }
}
