mod badge_tests {
    use futures::StreamExt;
    use tgmrank::controllers::{models::LoginModel, models::PlayerBadgeModel};

    use crate::test_utility::{random_string, Client, TestResult};
    use reqwest::StatusCode;
    use tgmrank::controllers::models::BadgeModel;

    async fn create_badge(client: &Client) -> TestResult<BadgeModel> {
        let response = client
            .create_badge(&format!("TestBadge {}", random_string(4)))
            .await?;

        assert_eq!(response.status(), StatusCode::OK);

        let badge_model: BadgeModel = response.json().await?;

        Ok(badge_model)
    }

    async fn create_n_badges(client: &Client, n: usize) -> TestResult<Vec<BadgeModel>> {
        futures::stream::iter(0..n)
            .then(|_| create_badge(client))
            .collect::<Vec<TestResult<_>>>()
            .await
            .into_iter()
            .collect::<TestResult<_>>()
    }

    async fn add_badges_for_player(
        client: &Client,
        player_id: i32,
        badges: &[BadgeModel],
    ) -> TestResult<()> {
        futures::stream::iter(badges)
            .then(|b| client.add_badge_for_player(player_id, b.badge_id))
            .collect::<Vec<TestResult<_>>>()
            .await;
        Ok(())
    }

    #[actix_web::test]
    async fn player_badges_are_returned() -> TestResult<()> {
        let client = Client::new()?;

        let admin = client.create_admin().await?;
        client.login(&admin.username, &admin.password).await?;
        let new_badges = create_n_badges(&client, 3).await?;

        let (response, _) = client.random_new_user().await?;
        let player: LoginModel = response.json().await?;
        let player_id = player.user_id;

        let model: PlayerBadgeModel = client.get_player_badges(player_id).await?.json().await?;
        assert_eq!(model.badges.len(), 0);

        add_badges_for_player(&client, player_id, &new_badges).await?;

        let model: PlayerBadgeModel = client.get_player_badges(player_id).await?.json().await?;
        assert_eq!(model.badges.len(), new_badges.len());
        for expected_badge in new_badges {
            assert!(model.badges.contains(&expected_badge.name));
        }

        Ok(())
    }

    #[actix_web::test]
    async fn player_badges_are_per_player() -> TestResult<()> {
        let client = Client::new()?;

        let (response, _) = client.random_new_user().await?;
        let player1: LoginModel = response.json().await?;

        let (response, _) = client.random_new_user().await?;
        let player2: LoginModel = response.json().await?;

        let model: PlayerBadgeModel = client
            .get_player_badges(player1.user_id)
            .await?
            .json()
            .await?;
        assert_eq!(model.badges.len(), 0);

        let model: PlayerBadgeModel = client
            .get_player_badges(player2.user_id)
            .await?
            .json()
            .await?;
        assert_eq!(model.badges.len(), 0);

        let admin = client.create_admin().await?;
        client.login(&admin.username, &admin.password).await?;
        let new_badge = create_badge(&client).await?;

        client
            .add_badge_for_player(player1.user_id, new_badge.badge_id)
            .await?;

        println!(
            "{:?} {:?} {:?}",
            admin.username, player1.user_id, player2.user_id
        );

        let model: PlayerBadgeModel = client
            .get_player_badges(player1.user_id)
            .await?
            .json()
            .await?;
        assert_eq!(model.badges.len(), 1);
        assert!(model.badges.contains(&new_badge.name));

        let model: PlayerBadgeModel = client
            .get_player_badges(player2.user_id)
            .await?
            .json()
            .await?;
        assert_eq!(model.badges.len(), 0);

        client
            .remove_badge_for_player(player1.user_id, new_badge.badge_id)
            .await?;

        let model: PlayerBadgeModel = client
            .get_player_badges(player1.user_id)
            .await?
            .json()
            .await?;
        assert_eq!(model.badges.len(), 0);

        Ok(())
    }
}
