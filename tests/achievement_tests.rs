use tgmrank::{
    controllers::models::{
        AccountAchievementModel, AchievementModel, LoginModel, ModeRankingModel,
    },
    models::{forms::AddScoreForm, Mode, Role, ScoreStatus},
};

use crate::test_utility::{Client, DataGenerator, TestResult};
use tgmrank::models::forms::VerifyScoreForm;

#[actix_web::test]
async fn can_get_achievements_list() -> TestResult<()> {
    let client = Client::new()?;
    let achievement_list_response = client.get_achievement_list().await?;
    assert!(achievement_list_response.status().is_success());

    achievement_list_response
        .json::<Vec<AchievementModel>>()
        .await?;

    Ok(())
}

#[actix_web::test]
async fn achievements_are_calculated_on_score_submission() -> TestResult<()> {
    let client = Client::new()?;

    let (_, user) = client.random_new_user().await?;

    let login_model: LoginModel = client
        .login(&user.username, &user.password)
        .await?
        .json()
        .await?;

    let achievements = client.get_player_achievements(login_model.user_id).await?;
    assert!(achievements.status().is_success());
    let achievements: Vec<AccountAchievementModel> = achievements.json().await?;

    let score: ModeRankingModel = client
        .add_score(&DataGenerator::random_tgm1_gm())
        .await?
        .json()
        .await?;
    assert!(!achievements.iter().any(|a| a.score_id.is_some()));

    let achievements = client.get_player_achievements(login_model.user_id).await?;
    assert!(achievements.status().is_success());

    let achievements: Vec<AccountAchievementModel> = achievements.json().await?;
    assert!(achievements
        .iter()
        .any(|a| a.score_id == Some(score.score_id)));

    Ok(())
}

#[actix_web::test]
async fn achievement_should_use_the_first_score_that_meets_the_achievement_condition(
) -> TestResult<()> {
    let client = Client::new()?;

    let (_, user) = client.random_new_user().await?;

    let login_model: LoginModel = client
        .login(&user.username, &user.password)
        .await?
        .json()
        .await?;

    let form = &AddScoreForm {
        playtime: Some("13:20.10".to_string()),
        ..DataGenerator::random_tgm1_gm()
    };
    let score: ModeRankingModel = client.add_score(form).await?.json().await?;

    let form = &AddScoreForm {
        playtime: Some("12:59.25".to_string()),
        ..DataGenerator::random_tgm1_gm()
    };
    let better_score: ModeRankingModel = client.add_score(form).await?.json().await?;

    let achievements: Vec<AccountAchievementModel> = client
        .get_player_achievements(login_model.user_id)
        .await?
        .json()
        .await?;

    assert!(achievements
        .iter()
        .filter(|a| a.score_id.is_some())
        .all(|a| a.score_id == Some(score.score_id)));

    assert!(!achievements
        .iter()
        .any(|a| a.score_id == Some(better_score.score_id)));

    Ok(())
}

#[actix_web::test]
async fn rejected_scores_do_not_count_toward_achievements() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, user) = client.random_new_user().await?;
    client.update_user_role(&user, Role::Verifier).await?;

    let login_model: LoginModel = client
        .login(&user.username, &user.password)
        .await?
        .json()
        .await?;

    let score: ModeRankingModel = client
        .add_score(&AddScoreForm {
            mode_id: Mode::TapDeath as i32,
            grade_id: Some(174), // M
            level: Some(555),
            playtime: Some("03:43.22".to_string()),
            ..DataGenerator::test_score()
        })
        .await?
        .json()
        .await?;

    let achievements: Vec<AccountAchievementModel> = client
        .get_player_achievements(login_model.user_id)
        .await?
        .json()
        .await?;
    assert!(achievements
        .iter()
        .any(|a| a.score_id == Some(score.score_id)));

    client
        .verify_score(
            score.score_id,
            &VerifyScoreForm {
                status: ScoreStatus::Rejected,
                comment: None,
            },
        )
        .await?;

    let achievements: Vec<AccountAchievementModel> = client
        .get_player_achievements(login_model.user_id)
        .await?
        .json()
        .await?;
    assert!(!achievements.iter().any(|a| a.score_id.is_some()));

    Ok(())
}
