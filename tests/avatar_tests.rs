use reqwest::StatusCode;

use tgmrank::controllers::models::LoginModel;

use crate::test_utility::{Client, DataGenerator, TestResult};

#[actix_web::test]
async fn avatar_upload_works() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, user) = client.random_new_user().await?;

    let response = client.login(&user.username, &user.password).await?;
    let login_model: LoginModel = response.json().await?;
    assert_eq!(login_model.avatar, None);

    let response = client.upload_avatar("avatar.png", "image/png").await?;
    let status = response.status();
    let content = response.text().await?;
    assert_eq!(status, StatusCode::OK);
    assert_ne!(content, "");

    let response = client.login_verify().await?;
    let login_model: LoginModel = response.json().await?;
    assert_ne!(login_model.avatar, None);

    Ok(())
}

#[actix_web::test]
async fn avatar_upload_updates_ranking_fields() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, user) = client.random_new_user().await?;

    client.login(&user.username, &user.password).await?;
    {
        let score_to_add = DataGenerator::random_tgm1_gm();
        client.add_score(&score_to_add).await?;
    }

    let ranking = client.get_ranking(0).await?;
    let player_rank_model = ranking
        .iter()
        .find(|r| r.player.player_name == user.username);
    assert!(matches!(player_rank_model, Some(_)));

    let player_avatar = player_rank_model.and_then(|m| m.player.avatar.as_ref());
    assert!(matches!(player_avatar, None));

    client.upload_avatar("avatar.png", "image/png").await?;

    let ranking = client.get_ranking(0).await?;
    let player_rank_model = ranking
        .iter()
        .find(|r| r.player.player_name == user.username);
    assert!(matches!(player_rank_model, Some(_)));

    let player_avatar = player_rank_model.and_then(|m| m.player.avatar.as_ref());
    assert!(matches!(player_avatar, Some(_)));

    Ok(())
}

#[actix_web::test]
async fn must_be_logged_in() -> TestResult<()> {
    let client = Client::new()?;

    let response = client.upload_avatar("avatar.png", "image/png").await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    Ok(())
}

#[actix_web::test]
async fn too_large_image_fails() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, user) = client.random_new_user().await?;
    let _response = client.login(&user.username, &user.password).await?;

    let response = client
        .upload_avatar("large_image.jpg", "image/jpeg")
        .await?;
    assert_ne!(response.status(), StatusCode::OK);

    let response = client.login_verify().await?;
    let login_model: LoginModel = response.json().await?;
    assert_eq!(login_model.avatar, None);

    Ok(())
}

#[actix_web::test]
async fn bad_content_type_fails() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, user) = client.random_new_user().await?;
    let _response = client.login(&user.username, &user.password).await?;

    let response = client.upload_avatar("avatar.png", "text/html").await?;
    assert_ne!(response.status(), StatusCode::OK);

    let response = client.login_verify().await?;
    let login_model: LoginModel = response.json().await?;
    assert_eq!(login_model.avatar, None);

    Ok(())
}
