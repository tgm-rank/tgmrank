use itertools::Itertools;
use reqwest::StatusCode;

use tgmrank::controllers::models::{ModeRankingModel, OverallRankingEntryModel};
use tgmrank::models::forms::{AddScoreForm, ProofLinkForm, UpdateScoreForm};
use tgmrank::models::ProofType;
use tgmrank::{controllers::models::LoginModel, models::Mode};

use crate::test_utility::{Client, DataGenerator, TestResult};

#[actix_web::test]
async fn points_are_calculated_for_all_ranked_modes_for_game() -> TestResult<()> {
    let client = Client::new()?;

    let (response, user1) = client.random_new_user().await?;
    let user1_model: LoginModel = response.json().await?;

    client.login(&user1.username, &user1.password).await?;
    {
        let scores = vec![
            AddScoreForm {
                mode_id: Mode::Tgm1Master as i32,
                grade_id: Some(19), // GM
                level: Some(999),
                playtime: Some("12:50.87".into()),
                score: Some(164_212),
                ..DataGenerator::test_score()
            },
            AddScoreForm {
                mode_id: Mode::Tgm120g as i32,
                grade_id: Some(38), // GM
                level: Some(999),
                playtime: Some("12:21.45".into()),
                score: Some(163_855),
                ..DataGenerator::test_score()
            },
            AddScoreForm {
                mode_id: Mode::Tgm1Big as i32,
                grade_id: Some(53), // GM
                level: Some(733),
                playtime: Some("07:23.05".into()),
                score: Some(73998),
                ..DataGenerator::test_score()
            },
        ];

        client.add_scores(&scores).await?;
    }
    client.logout().await?;

    let overall_ranking = client.get_ranking(0).await?;
    let overall_points = overall_ranking
        .iter()
        .find(|o| o.player.player_name == user1.username)
        .expect("User1 not found in overall ranking")
        .ranking_points
        .expect("User1 does not have any overall points");

    let game_ranking = client.get_ranking(Mode::Tgm1Master as i32).await?;
    let game_points = game_ranking
        .iter()
        .find(|g| g.player.player_name == user1.username)
        .expect("User1 not found in game ranking")
        .ranking_points
        .expect("User1 does not have any game ranking points");

    let player_scores = client.get_player_scores(user1_model.user_id).await?;
    let calculated_points: i32 = player_scores
        .into_iter()
        .filter(|s| s.mode_id == 1 || s.mode_id == 2)
        .filter_map(|s| s.ranking_points)
        .sum();

    assert_eq!(
        calculated_points, game_points,
        "Game points does not equal sum of ranked points"
    );
    assert_eq!(
        overall_points, game_points,
        "Overall points does not equal game points"
    );

    Ok(())
}

#[actix_web::test]
async fn points_are_calculated_from_ranked_modes_across_games() -> TestResult<()> {
    let client = Client::new()?;

    let (response, user1) = client.random_new_user().await?;
    let user1_model: LoginModel = response.json().await?;

    client.login(&user1.username, &user1.password).await?;
    {
        let scores = vec![
            AddScoreForm {
                mode_id: Mode::Tgm120g as i32,
                grade_id: Some(32),
                level: Some(456),
                playtime: Some("04:05.06".into()),
                score: Some(44_444),
                ..DataGenerator::test_score()
            },
            AddScoreForm {
                mode_id: Mode::TapDeath as i32,
                grade_id: Some(173),
                level: Some(456),
                playtime: Some("03:05.06".into()),
                score: Some(123_456),
                ..DataGenerator::test_score()
            },
            AddScoreForm {
                mode_id: Mode::TiMasterClassic as i32,
                grade_id: Some(248), // S8
                level: Some(456),
                playtime: Some("04:05.06".into()),
                score: None,
                ..DataGenerator::test_score()
            },
        ];

        client.add_scores(&scores).await?;
    }
    client.logout().await?;

    let overall_ranking = client.get_ranking(0).await?;
    let overall_points = overall_ranking
        .iter()
        .find(|o| o.player.player_name == user1.username)
        .expect("User1 not found in overall ranking")
        .ranking_points
        .expect("User1 does not have any points");

    let player_scores = client.get_player_scores(user1_model.user_id).await?;
    let calculated_score: i32 = player_scores
        .into_iter()
        .filter_map(|s| s.ranking_points)
        .sum();

    assert_eq!(
        overall_points, calculated_score,
        "calculated ranking points does not match overall ranking points"
    );

    Ok(())
}

#[actix_web::test]
async fn no_overall_points_for_unranked_mode() -> TestResult<()> {
    let client = Client::new()?;

    let (_, user1) = client.random_new_user().await?;

    client.login(&user1.username, &user1.password).await?;
    {
        let score_to_add = AddScoreForm {
            mode_id: Mode::TapTgmplus as i32,
            level: Some(872),
            playtime: Some("07:31.52".into()),
            score: Some(123_456),
            proof: Vec::new(),
            ..Default::default()
        };

        client.add_score(&score_to_add).await?;
    }
    client.logout().await?;

    let overall_ranking = client.get_ranking(0).await?;
    let user_overall_ranking = overall_ranking
        .iter()
        .find(|o| o.player.player_name == user1.username);

    assert_eq!(user_overall_ranking.is_none(), true);

    Ok(())
}

#[actix_web::test]
async fn should_not_commit_scores_on_dry_run() -> TestResult<()> {
    let client = Client::new()?;

    let (_, user1) = client.random_new_user().await?;

    client.login(&user1.username, &user1.password).await?;

    let score_to_add = AddScoreForm {
        dry_run: Some(true),
        ..DataGenerator::random_tgm1_gm()
    };
    let add_score_result = client.add_score(&score_to_add).await?;
    assert!(add_score_result.status().is_success());

    client.logout().await?;

    let overall_ranking = client.get_ranking(0).await?;
    let user_overall_ranking = overall_ranking
        .iter()
        .find(|o| o.player.player_name == user1.username);

    assert!(user_overall_ranking.is_none());

    let game_ranking = client.get_ranking(1).await?;
    assert!(game_ranking
        .iter()
        .find(|o| o.player.player_name == user1.username)
        .is_none());

    let mode_ranking = client.get_mode_ranking(score_to_add.mode_id).await?;
    let mode_players = mode_ranking
        .into_iter()
        .filter_map(|r| r.player)
        .collect_vec();

    assert!(mode_players
        .iter()
        .find(|p| p.player_name == user1.username)
        .is_none());

    Ok(())
}

#[actix_web::test]
async fn users_should_be_able_to_update_score_comment_and_proof() -> TestResult<()> {
    let client = Client::new()?;

    let (_, user1) = client.random_new_user().await?;

    let initial_comment = "initial comment";

    client.login(&user1.username, &user1.password).await?;

    let score_to_add = AddScoreForm {
        mode_id: Mode::TiShiraseClassic as i32,
        grade_id: Some(309), // S11
        level: Some(1152),
        playtime: Some("04:52.28".into()),
        comment: Some(initial_comment.to_string()),
        ..DataGenerator::test_score()
    };

    let add_score_response = client.add_score(&score_to_add).await?;
    let added_score_text = add_score_response.text().await?;
    let added_score: ModeRankingModel = serde_json::from_str(&added_score_text)?;

    let actual_comment = added_score.comment.unwrap();
    assert_eq!(&actual_comment, initial_comment);

    assert_eq!(added_score.proof[0].proof_type, ProofType::Image);

    let new_comment = "updated!";
    let update = UpdateScoreForm {
        score_id: added_score.score_id,
        comment: Some(new_comment.to_string()),
        proof: vec![ProofLinkForm {
            proof_type: ProofType::Other,
            link: "https://doc.rust-lang.org/std/index.html".to_string(),
        }],
        ..Default::default()
    };

    let update_score_response = client.update_score(&update).await?;
    let update_score_text = update_score_response.text().await?;
    let _update_score_model: ModeRankingModel = serde_json::from_str(&update_score_text)?;

    client.logout().await?;

    let score = client.get_score(added_score.score_id).await?;

    let actual_comment = score.comment.unwrap();
    assert_eq!(&actual_comment, new_comment);

    assert_eq!(score.proof[0].proof_type, ProofType::Other);

    Ok(())
}

#[actix_web::test]
async fn submitting_new_pb_removes_old_score_from_ranking() -> TestResult<()> {
    let client = Client::new()?;

    let (_, user) = client.random_new_user().await?;

    client.login(&user.username, &user.password).await?;
    let score_to_add = AddScoreForm {
        grade_id: Some(18), // S9
        playtime: Some("15:21.28".into()),
        ..DataGenerator::random_tgm1_gm()
    };
    let response = client.add_score(&score_to_add).await?;
    let initial_score: ModeRankingModel = response.json().await?;

    let score_to_add = AddScoreForm {
        grade_id: Some(19), // GM
        playtime: Some("13:14.52".into()),
        ..DataGenerator::random_tgm1_gm()
    };
    let response = client.add_score(&score_to_add).await?;
    let better_score: ModeRankingModel = response.json().await?;

    let mode_ranking = client.get_mode_ranking(1).await?;

    let has_latest_score = mode_ranking.iter().any(|s| {
        let player_id = s.player.as_ref().map(|p| p.player_id);

        s.score_id == better_score.score_id
            && better_score.player.as_ref().map(|p| p.player_id) == player_id
    });
    assert!(
        has_latest_score,
        "User's latest score was not found in mode ranking"
    );

    // TODO Assert on the non-deserialized response
    let _old_score_still_retrievable = client.get_score(initial_score.score_id).await?;

    Ok(())
}

#[actix_web::test]
async fn unauthorized_when_adding_a_score_without_being_logged_in() -> TestResult<()> {
    let client = Client::new()?;

    let score_to_add = DataGenerator::random_tap_master_clear();

    let response = client.add_score(&score_to_add).await?;
    assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

    Ok(())
}

#[actix_web::test]
async fn aggregated_rankings_are_sorted() -> TestResult<()> {
    fn assert_ranking_is_sorted(ranking: &[OverallRankingEntryModel]) {
        let is_ranking_sorted = ranking.is_sorted_by_key(|r| r.rank_id);
        assert_eq!(is_ranking_sorted, true);
    }

    let client = Client::new()?;

    let ranking = client.get_ranking(0).await?;
    assert_ranking_is_sorted(&ranking);

    for i in 0..3 {
        let game_ranking = client.get_ranking(i).await?;
        assert_ranking_is_sorted(&game_ranking);
    }

    Ok(())
}

#[actix_web::test]
async fn tied_scores_have_the_same_rank() -> TestResult<()> {
    let client = Client::new()?;

    let tied_score = DataGenerator::random_tgm1_gm();

    let (_, user1) = client.random_new_user().await?;
    client.login(&user1.username, &user1.password).await?;
    let response = client.add_score(&tied_score).await?;
    let score1: ModeRankingModel = response.json().await?;
    client.logout().await?;

    let (_, user2) = client.random_new_user().await?;
    client.login(&user2.username, &user2.password).await?;
    let response = client.add_score(&tied_score).await?;
    let score2: ModeRankingModel = response.json().await?;

    assert_eq!(score1.rank, score2.rank);
    assert_eq!(score1.ranking_points, score2.ranking_points);

    Ok(())
}

#[actix_web::test]
async fn disallow_duplicate_score_entries() -> TestResult<()> {
    let client = Client::new()?;

    let some_score = DataGenerator::random_tgm1_gm();

    let (_, user) = client.random_new_user().await?;
    client.login(&user.username, &user.password).await?;

    let response = client.add_score(&some_score).await?;
    assert_eq!(response.status().is_success(), true);

    let response = client.add_score(&some_score).await?;
    assert_eq!(response.status().is_client_error(), true);

    Ok(())
}

mod score_validation_tests {
    use tgmrank::models::{forms::AddScoreForm, Mode};

    use crate::test_utility::{Client, DataGenerator, TestResult};

    #[actix_web::test]
    async fn relaxed_conditions_for_shirase_to_account_for_regrets() -> TestResult<()> {
        let client = Client::new()?;

        let some_score = AddScoreForm {
            mode_id: Mode::TiShiraseWorld as i32,
            grade_id: Some(316), // S3, subtracted 1 due to regret
            level: Some(418),
            playtime: Some("03:36.88".to_string()),
            ..DataGenerator::test_score()
        };

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&some_score).await?;
        assert!(response.status().is_success());

        Ok(())
    }

    #[actix_web::test]
    async fn tap_master_should_reject_scores_with_level_999_but_no_grade_line() -> TestResult<()> {
        let client = Client::new()?;

        let some_score = AddScoreForm {
            mode_id: Mode::TapMaster as i32,
            grade_id: Some(113), // S9, no line
            level: Some(999),
            playtime: Some("10:58.24".to_string()),
            ..DataGenerator::test_score()
        };

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&some_score).await?;
        println!("{:?}", response);
        assert!(response.status().is_client_error());

        Ok(())
    }
}

mod proof_tests {
    use tgmrank::controllers::models::{ErrorModel, ModeRankingModel};
    use tgmrank::models::forms::{AddScoreForm, ProofLinkForm, UpdateScoreForm};
    use tgmrank::models::ProofType;

    use crate::test_utility::{assert_data_errors_has_field, Client, DataGenerator, TestResult};

    #[actix_web::test]
    async fn malformatted_urls_should_respond_with_an_error() -> TestResult<()> {
        let client = Client::new()?;

        let some_score = AddScoreForm {
            proof: vec![ProofLinkForm {
                proof_type: ProofType::Other,
                link: "BYOU_CIEA".to_string(),
            }],
            ..DataGenerator::random_tgm1_gm()
        };

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&some_score).await?;
        assert_eq!(response.status().is_client_error(), true);

        let error_model: ErrorModel = response.json().await?;
        assert_data_errors_has_field(&error_model, "proof[0].link");

        Ok(())
    }

    #[actix_web::test]
    async fn broken_proof_link_should_respond_with_an_error() -> TestResult<()> {
        let client = Client::new()?;

        let some_score = AddScoreForm {
            proof: vec![ProofLinkForm {
                proof_type: ProofType::Image,
                link: "https://this.site.does.not.exist".to_string(),
            }],
            ..DataGenerator::random_tgm1_gm()
        };

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&some_score).await?;
        assert_eq!(response.status().is_client_error(), true);

        let error_model: ErrorModel = response.json().await?;
        assert_data_errors_has_field(&error_model, "proof[0].link");

        Ok(())
    }

    #[actix_web::test]
    async fn image_proof_to_non_images_should_respond_with_an_error() -> TestResult<()> {
        let client = Client::new()?;

        let some_score = AddScoreForm {
            proof: vec![ProofLinkForm {
                proof_type: ProofType::Image,
                link: "https://doc.rust-lang.org/std/index.html".to_string(),
            }],
            ..DataGenerator::random_tgm1_gm()
        };

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&some_score).await?;
        assert_eq!(response.status().is_client_error(), true);

        let error_model: ErrorModel = response.json().await?;
        assert_data_errors_has_field(&error_model, "proof[0].link");

        Ok(())
    }

    #[actix_web::test]
    async fn image_proof_should_work() -> TestResult<()> {
        let client = Client::new()?;

        let some_score = AddScoreForm {
            proof: vec![ProofLinkForm {
                proof_type: ProofType::Image,
                link: "https://www.rust-lang.org/static/images/rust-logo-blk.png".to_string(),
            }],
            ..DataGenerator::random_tgm1_gm()
        };

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&some_score).await?;
        assert!(response.status().is_success());

        Ok(())
    }

    #[actix_web::test]
    async fn video_links_to_youtube_or_twitch_should_work() -> TestResult<()> {
        let client = Client::new()?;

        let some_score = AddScoreForm {
            proof: vec![ProofLinkForm {
                proof_type: ProofType::Video,
                link: "https://www.youtube.com/watch?v=mIYS5sEkm78".to_string(),
            }],
            ..DataGenerator::random_tgm1_gm()
        };

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&some_score).await?;
        assert!(response.status().is_success());

        Ok(())
    }

    #[actix_web::test]
    async fn video_links_to_non_youtube_or_twitch_sites_should_respond_with_an_error(
    ) -> TestResult<()> {
        let client = Client::new()?;

        let some_score = AddScoreForm {
            proof: vec![ProofLinkForm {
                proof_type: ProofType::Video,
                link: "https://vimeo.com/285779587".to_string(),
            }],
            ..DataGenerator::random_tgm1_gm()
        };

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&some_score).await?;
        assert_eq!(response.status().is_client_error(), true);

        let error_model: ErrorModel = response.json().await?;
        assert_data_errors_has_field(&error_model, "proof[0].link");

        Ok(())
    }

    #[actix_web::test]
    async fn video_proof_to_image_should_respond_with_an_error() -> TestResult<()> {
        let client = Client::new()?;

        let some_score = AddScoreForm {
            proof: vec![ProofLinkForm {
                proof_type: ProofType::Video,
                link: "https://doc.rust-lang.org/rust-logo1.53.0.png".to_string(),
            }],
            ..DataGenerator::random_tgm1_gm()
        };

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&some_score).await?;
        assert_eq!(response.status().is_client_error(), true);

        let error_model: ErrorModel = response.json().await?;
        assert_data_errors_has_field(&error_model, "proof[0].link");

        Ok(())
    }

    #[actix_web::test]
    async fn score_updates_should_be_able_to_replace_proof() -> TestResult<()> {
        let client = Client::new()?;

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&DataGenerator::random_tgm1_gm()).await?;
        let score: ModeRankingModel = response.json().await?;
        assert_eq!(score.proof.len(), 1);

        let response = client
            .update_score(&UpdateScoreForm {
                score_id: score.score_id,
                proof: vec![
                    ProofLinkForm {
                        proof_type: ProofType::Image,
                        link: "https://www.rust-lang.org/static/images/rust-logo-blk.png"
                            .to_string(),
                    },
                    ProofLinkForm {
                        proof_type: ProofType::Other,
                        link: "https://www.rust-lang.org/policies/licenses".to_string(),
                    },
                ],
                ..Default::default()
            })
            .await?;
        let score: ModeRankingModel = response.json().await?;
        assert_eq!(score.proof.len(), 2);
        assert_eq!(
            score.proof[0].link,
            "https://www.rust-lang.org/static/images/rust-logo-blk.png"
        );
        assert_eq!(
            score.proof[1].link,
            "https://www.rust-lang.org/policies/licenses"
        );

        let response = client
            .update_score(&UpdateScoreForm {
                score_id: score.score_id,
                proof: vec![
                    ProofLinkForm {
                        proof_type: ProofType::Image,
                        link: "https://www.rust-lang.org/static/images/ferris-error.png"
                            .to_string(),
                    },
                    ProofLinkForm {
                        proof_type: ProofType::Other,
                        link: "https://www.rust-lang.org/policies/licenses".to_string(),
                    },
                ],
                ..Default::default()
            })
            .await?;
        let score: ModeRankingModel = response.json().await?;
        assert_eq!(score.proof.len(), 2);
        assert_eq!(
            score.proof[0].link,
            "https://www.rust-lang.org/policies/licenses"
        );
        assert_eq!(
            score.proof[1].link,
            "https://www.rust-lang.org/static/images/ferris-error.png"
        );

        Ok(())
    }

    #[actix_web::test]
    async fn streamable_should_be_valid_proof() -> TestResult<()> {
        let client = Client::new()?;

        let some_score = AddScoreForm {
            proof: vec![ProofLinkForm {
                proof_type: ProofType::Video,
                link: "https://streamable.com/ntklj1".to_string(),
            }],
            ..DataGenerator::random_tgm1_gm()
        };

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&some_score).await?;
        assert_eq!(response.status().is_success(), true);

        Ok(())
    }

    #[actix_web::test]
    async fn broken_streamable_links_should_error() -> TestResult<()> {
        let client = Client::new()?;

        let some_score = AddScoreForm {
            proof: vec![ProofLinkForm {
                proof_type: ProofType::Video,
                link: "https://streamable.com/helloworld3429348".to_string(),
            }],
            ..DataGenerator::random_tgm1_gm()
        };

        let (_, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.add_score(&some_score).await?;
        assert_eq!(response.status().is_client_error(), true);

        Ok(())
    }
}

mod rejected_scores_tests {
    use tgmrank::{
        controllers::{models::LoginModel, models::ModeRankingModel},
        models::{Mode, Role, ScoreStatus},
    };

    use crate::test_utility::{Client, DataGenerator, TestResult};
    use tgmrank::models::forms::{AddScoreForm, VerifyScoreForm};
    use tgmrank::models::SentMail;

    #[actix_web::test]
    async fn leaderboards_should_not_rank_rejected_scores() -> TestResult<()> {
        let client = Client::new()?;

        let (_response, user) = client.random_new_user().await?;
        client.update_user_role(&user, Role::Verifier).await?;

        let user: LoginModel = client
            .login(&user.username, &user.password)
            .await?
            .json()
            .await?;
        let score: ModeRankingModel = client
            .add_score(&DataGenerator::random_tgm1_gm())
            .await?
            .json()
            .await?;

        let mode_ranking = client.get_mode_ranking(Mode::Tgm1Master as i32).await?;
        assert!(
            mode_ranking.iter().any(|r| r.score_id == score.score_id),
            "Could not find score in mode ranking"
        );

        let game_ranking = client.get_ranking(0).await?;
        assert!(
            game_ranking
                .iter()
                .any(|r| r.player.player_id == user.user_id),
            "Could not find player in game ranking"
        );

        let updated_score: ModeRankingModel = client
            .verify_score(
                score.score_id,
                &VerifyScoreForm {
                    status: ScoreStatus::Rejected,
                    comment: None,
                },
            )
            .await?
            .json()
            .await?;

        assert_eq!(score.score_id, updated_score.score_id);
        assert_eq!(updated_score.status, ScoreStatus::Rejected);

        let mode_ranking = client.get_mode_ranking(Mode::Tgm1Master as i32).await?;
        assert!(
            !mode_ranking.iter().any(|r| r.score_id == score.score_id),
            "Unexpectedly found rejected score in ranking"
        );
        let game_ranking = client.get_ranking(0).await?;
        assert!(
            !game_ranking
                .iter()
                .any(|r| r.player.player_id == user.user_id),
            "Unexpectedly found player in game ranking"
        );

        Ok(())
    }

    #[actix_web::test]
    async fn ranking_should_have_verification_fields() -> TestResult<()> {
        let client = Client::new()?;

        let (_response, user) = client.random_new_user().await?;

        let submitter_user: LoginModel = client
            .login(&user.username, &user.password)
            .await?
            .json()
            .await?;

        let score: ModeRankingModel = client
            .add_score(&DataGenerator::random_tgm1_gm())
            .await?
            .json()
            .await?;
        client.logout().await?;

        let (_response, user) = client.random_new_user().await?;
        client.update_user_role(&user, Role::Verifier).await?;
        client.login(&user.username, &user.password).await?;

        let verification_comment = "My verification comment";
        let updated_score: ModeRankingModel = client
            .verify_score(
                score.score_id,
                &VerifyScoreForm {
                    status: ScoreStatus::Rejected,
                    comment: Some(verification_comment.to_string()),
                },
            )
            .await?
            .json()
            .await?;

        assert_eq!(score.score_id, updated_score.score_id);
        assert_eq!(score.status, ScoreStatus::Pending);
        assert_eq!(updated_score.status, ScoreStatus::Rejected);
        assert!(updated_score.verification.is_some());

        let verification = updated_score.verification.unwrap();
        assert_eq!(verification.verified_by.player_name, user.username);
        assert_eq!(verification.comment, Some(verification_comment.to_string()));

        let db = client.make_db_pool().await?;
        let sent_emails = SentMail::fetch_all(&mut *db.acquire().await?).await?;
        let rejection_email = sent_emails
            .into_iter()
            .find(|e| e.account_id == Some(submitter_user.user_id));

        assert!(
            rejection_email.is_some(),
            "Could not find rejection email for user {}",
            submitter_user.user_id
        );
        assert!(rejection_email.unwrap().subject.contains("Rejected"));

        Ok(())
    }

    #[actix_web::test]
    async fn users_should_be_able_to_resubmit_rejected_scores_with_updated_proof() -> TestResult<()>
    {
        let client = Client::new()?;

        let (_response, user) = client.random_new_user().await?;
        client.update_user_role(&user, Role::Verifier).await?;
        let _user: LoginModel = client
            .login(&user.username, &user.password)
            .await?
            .json()
            .await?;

        let score_to_submit = DataGenerator::random_tgm1_gm();
        let score: ModeRankingModel = client.add_score(&score_to_submit).await?.json().await?;

        let updated_score: ModeRankingModel = client
            .verify_score(
                score.score_id,
                &VerifyScoreForm {
                    status: ScoreStatus::Rejected,
                    comment: None,
                },
            )
            .await?
            .json()
            .await?;

        assert_eq!(score.score_id, updated_score.score_id);
        assert_eq!(updated_score.status, ScoreStatus::Rejected);

        let score_with_updated_proof = AddScoreForm {
            proof: vec![DataGenerator::get_random_proof()],
            ..score_to_submit
        };

        let response = client.add_score(&score_with_updated_proof).await?;
        assert!(response.status().is_success());

        Ok(())
    }
}

mod temporal_rankings_tests {
    use tgmrank::controllers::game_controller::GameRankingQuery;
    use tgmrank::controllers::mode_controller::ModeRankingQuery;
    use tgmrank::controllers::models::LoginModel;
    use tgmrank::controllers::models::ModeRankingModel;
    use tgmrank::models::forms::AddScoreForm;
    use time::OffsetDateTime;

    use crate::test_utility::{Client, DataGenerator, TestResult};

    #[actix_web::test]
    async fn leaderboards_should_show_rankings_as_of_some_date_time() -> TestResult<()> {
        let client = Client::new()?;

        // Add a score better than initial score, but worse than next PB
        let (_response, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;
        client
            .add_score(
                &(AddScoreForm {
                    grade_id: Some(18), // S9
                    ..DataGenerator::random_tgm1_gm()
                }),
            )
            .await?;
        client.logout().await?;

        let (response, user) = client.random_new_user().await?;
        let user_model: LoginModel = response.json().await?;

        client.login(&user.username, &user.password).await?;
        let response = client
            .add_score(
                &(AddScoreForm {
                    grade_id: Some(17), // S8
                    score: Some(111_111),
                    ..DataGenerator::random_tgm1_gm()
                }),
            )
            .await?;

        let text = response.text().await?;
        let initial_score: ModeRankingModel = serde_json::from_str(&text)?;

        let dt_after_initial_score_but_before_next_score = OffsetDateTime::now_utc();

        let response = client
            .add_score(
                &(AddScoreForm {
                    grade_id: Some(19), // GM
                    ..DataGenerator::random_tgm1_gm()
                }),
            )
            .await?;
        let _better_score: ModeRankingModel = response.json().await?;

        let ranking = client
            .get_mode_ranking2(
                1,
                &ModeRankingQuery {
                    as_of: dt_after_initial_score_but_before_next_score
                        .format(&time::format_description::well_known::Iso8601::DEFAULT)
                        .ok(),
                    ..Default::default()
                },
            )
            .await?;

        let has_initial_score = ranking.iter().any(|s| s.score_id == initial_score.score_id);
        assert!(
            has_initial_score,
            "User's initial score was not found in temporal mode ranking"
        );

        let ranking = client
            .get_ranking2(
                1,
                &GameRankingQuery {
                    as_of: dt_after_initial_score_but_before_next_score
                        .format(&time::format_description::well_known::Iso8601::DEFAULT)
                        .ok(),
                    ..Default::default()
                },
            )
            .await?;

        let player_ranking_entry = ranking
            .iter()
            .find(|s| s.player.player_id == user_model.user_id)
            .expect("Could not find player in game leaderboard");

        assert_eq!(
            player_ranking_entry.ranking_points, initial_score.ranking_points,
            "Incorrect ranking for player in game leaderboard"
        );

        Ok(())
    }
}

mod platorm_tests {
    use tgmrank::{
        controllers::models::ModeRankingModel,
        models::{
            forms::{AddScoreForm, UpdateScoreForm},
            Mode, ModeEntity, NewModeEntity, NewPlatformEntity, PlatformEntity,
        },
    };

    use crate::test_utility::{random_string, Client, DataGenerator, TestResult};

    #[actix_web::test]
    async fn should_be_able_to_submit_platform() -> TestResult<()> {
        let client = Client::new()?;

        let db = client.make_db_pool().await?;

        let mut connection = db.acquire().await?;
        let new_mode = ModeEntity::insert_mode(
            &mut connection,
            &NewModeEntity {
                mode_name: format!("PlatformTest_{}", random_string(8)),
                tags: Some(vec![]),
                ..DataGenerator::test_mode()
            },
        )
        .await?;

        let new_platform = PlatformEntity::insert(
            &mut connection,
            &NewPlatformEntity {
                platform_name: format!("Some platform {}", random_string(8)),
                mode_id: new_mode.mode_id,
            },
        )
        .await?;

        let platforms = client.get_platforms_for_mode(new_mode.mode_id).await?;
        let platform = platforms
            .iter()
            .find(|p| {
                p.platform_id == new_platform.platform_id
                    && p.platform_name == new_platform.platform_name
            })
            .expect("New platform could not be found");

        let (_response, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        // Submit with game platform
        {
            let game_platform = platforms
                .iter()
                .find(|p| p.platform_id != new_platform.platform_id)
                .expect("Game platform could not be found");

            let response = client
                .add_score(
                    &(AddScoreForm {
                        mode_id: new_mode.mode_id,
                        platform_id: Some(game_platform.platform_id),
                        ..DataGenerator::random_tgm1_gm()
                    }),
                )
                .await?;
            assert!(response.status().is_success());

            let model: ModeRankingModel = response.json().await?;
            let model_platform = model.platform.map(|p| p.platform_name);
            assert_eq!(model_platform.as_ref(), Some(&game_platform.platform_name));
        }

        // Submit with mode-specific platform
        {
            let response = client
                .add_score(
                    &(AddScoreForm {
                        mode_id: new_mode.mode_id,
                        platform_id: Some(platform.platform_id),
                        ..DataGenerator::random_tgm1_gm()
                    }),
                )
                .await?;
            assert!(response.status().is_success());

            let model: ModeRankingModel = response.json().await?;
            let model_platform = model.platform.map(|p| p.platform_name);
            assert_eq!(model_platform.as_ref(), Some(&platform.platform_name));
        }

        // Submit without platform
        {
            let response = client
                .add_score(
                    &(AddScoreForm {
                        mode_id: new_mode.mode_id,
                        platform_id: None,
                        ..DataGenerator::random_tgm1_gm()
                    }),
                )
                .await?;
            assert!(response.status().is_success());
            let model: ModeRankingModel = response.json().await?;
            assert!(model.platform.is_none());
        }

        // Submit with invalid platform
        {
            let response = client
                .add_score(
                    &(AddScoreForm {
                        mode_id: new_mode.mode_id,
                        platform_id: Some(1928374),
                        ..DataGenerator::random_tgm1_gm()
                    }),
                )
                .await?;
            assert!(!response.status().is_success());
        }

        Ok(())
    }

    #[actix_web::test]
    async fn should_be_able_to_remove_platform() -> TestResult<()> {
        let client = Client::new()?;

        let platforms = client
            .get_platforms_for_mode(Mode::Tgm1Master as i32)
            .await?;

        let (_response, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client
            .add_score(
                &(AddScoreForm {
                    mode_id: Mode::Tgm1Master as i32,
                    proof: vec![DataGenerator::get_random_proof()],
                    ..DataGenerator::random_tgm1_gm()
                }),
            )
            .await?;
        assert!(response.status().is_success());
        let score: ModeRankingModel = response.json().await?;
        assert!(score.platform.is_none());

        {
            let new_platform = platforms.first().map(|p| p.platform_id);
            let response = client
                .update_score(
                    &(UpdateScoreForm {
                        platform_id: new_platform,
                        score_id: score.score_id,
                        proof: vec![DataGenerator::get_random_proof()],
                        ..Default::default()
                    }),
                )
                .await?;
            assert!(response.status().is_success());
            let model: ModeRankingModel = response.json().await?;
            assert_eq!(model.platform.map(|p| p.platform_id), new_platform);
        }

        // Remove platform
        {
            let response = client
                .update_score(
                    &(UpdateScoreForm {
                        platform_id: None,
                        score_id: score.score_id,
                        proof: vec![DataGenerator::get_random_proof()],
                        ..Default::default()
                    }),
                )
                .await?;
            assert!(response.status().is_success());
            let model: ModeRankingModel = response.json().await?;
            assert!(model.platform.is_none());
        }

        Ok(())
    }
}
