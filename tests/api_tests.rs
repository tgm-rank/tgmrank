use reqwest::header::HeaderValue;
use reqwest::StatusCode;

use tgmrank::controllers::models::ErrorModel;
use tgmrank::models::forms::AddScoreForm;
use tgmrank::utility::TgmRankError;

use crate::test_utility::{assert_data_errors_has_field, Client, DataGenerator, TestResult};

#[actix_web::test]
async fn site_version() -> TestResult<()> {
    use tgmrank::controllers::models::VersionModel;

    let client = Client::new()?;
    let response = client.get_site_version().await?;
    assert!(response.status().is_success());

    let version_response: VersionModel = response.json().await?;
    assert!(!version_response.version.is_empty());

    Ok(())
}

#[actix_web::test]
async fn ping_works() -> TestResult<()> {
    use tgmrank::controllers::models::PingModel;

    let client = Client::new()?;

    let response = client.ping().await?;
    assert_eq!(response.status(), StatusCode::OK);

    let ping_response: PingModel = response.json().await?;
    assert_eq!(ping_response.message, "pong");

    Ok(())
}

#[actix_web::test]
async fn api_sets_cache_control_header() -> TestResult<()> {
    let client = Client::new()?;
    let response = client.ping().await?;
    let cache_control_header = response.headers().get("Cache-Control");
    assert!(cache_control_header.is_some());

    let expected_header_value = HeaderValue::from_str("no-store")?;
    assert_eq!(cache_control_header.unwrap(), expected_header_value);

    Ok(())
}

#[actix_web::test]
async fn not_found_path_returns_error() -> TestResult<()> {
    let client = Client::new()?;

    let response = client
        .client
        .get(&client.make_url("/v1/Bad/Path"))
        .send()
        .await?;
    assert_eq!(response.status(), StatusCode::NOT_FOUND);

    let error_model: ErrorModel = response.json().await?;
    assert_eq!(error_model.error.is_some(), true);
    assert_eq!(
        error_model.error.unwrap(),
        TgmRankError::NotFoundError.to_string()
    );

    Ok(())
}

#[actix_web::test]
async fn incorrect_type_for_path_segment_should_return_error_as_json() -> TestResult<()> {
    let client = Client::new()?;

    let response = client
        .client
        .get(&client.make_url("/v1/player/hello"))
        .send()
        .await?;
    assert_eq!(response.status(), StatusCode::NOT_FOUND);

    let error_model: ErrorModel = response.json().await?;
    assert_eq!(error_model.error.is_some(), true);

    Ok(())
}

#[actix_web::test]
async fn trailing_slashes_should_be_fine() -> TestResult<()> {
    let client = Client::new()?;

    let response = client
        .client
        .get(&client.make_url("/v1/game/1/"))
        .send()
        .await?;
    assert_eq!(response.status(), StatusCode::OK);

    Ok(())
}

#[actix_web::test]
async fn slashes_should_be_collapsed() -> TestResult<()> {
    let client = Client::new()?;

    let response = client
        .client
        .get(&client.make_url("/v1////////game/1"))
        .send()
        .await?;
    assert_eq!(response.status(), StatusCode::OK);

    Ok(())
}

#[actix_web::test]
async fn incorrect_type_for_query_parameter_should_return_error_as_json() -> TestResult<()> {
    let client = Client::new()?;

    let response = client
        .client
        .get(&client.make_url("/v1/mode/2/ranking?asOf=hi"))
        .send()
        .await?;
    assert_eq!(response.status().is_success(), false);

    let error_model: ErrorModel = response.json().await?;
    assert_eq!(error_model.error.is_some(), true);

    Ok(())
}

#[actix_web::test]
async fn add_score_validation_errors_returns_multiple_validation_errors() -> TestResult<()> {
    let client = Client::new()?;

    let (_response, user) = client.random_new_user().await?;
    client.login(&user.username, &user.password).await?;
    {
        let too_big_level: i32 = 123_456;
        let invalid_playtime = "123:456.789";
        let too_big_score: i32 = 1_234_567_890;

        let very_bad_score_form = AddScoreForm {
            mode_id: 1,
            grade_id: Some(123),
            level: Some(too_big_level),
            playtime: Some(invalid_playtime.into()),
            score: Some(too_big_score),
            ..DataGenerator::test_score()
        };

        let response = client.add_score(&very_bad_score_form).await?;

        let error_model: ErrorModel = response.json().await?;
        assert_eq!(error_model.data_errors.is_some(), true);

        let data_errors = error_model.data_errors.as_ref().unwrap();
        assert_eq!(data_errors.len(), 3);

        assert_data_errors_has_field(&error_model, "level");
        assert_data_errors_has_field(&error_model, "playtime");
        assert_data_errors_has_field(&error_model, "score");
    }
    client.logout().await?;

    Ok(())
}

#[actix_web::test]
async fn pagination_errors_should_have_nested_error_fields() -> TestResult<()> {
    let client = Client::new()?;
    let response = client
        .client
        .get(&client.make_url("/v1/game/1/ranking?page=0&pageSize=0"))
        .send()
        .await?;

    let error_model: ErrorModel = response.json().await?;
    assert_eq!(error_model.data_errors.is_some(), true);

    let data_errors = error_model.data_errors.as_ref().unwrap();
    assert_eq!(data_errors.len(), 2);

    assert_data_errors_has_field(&error_model, "page");
    assert_data_errors_has_field(&error_model, "pageSize");

    Ok(())
}

mod cors_tests {
    use std::collections::HashMap;

    use reqwest::header;
    use reqwest::header::HeaderValue;
    use reqwest::{Method, Request, Url};

    use crate::test_utility::Client;
    use crate::test_utility::TestResult;

    #[actix_web::test]
    async fn options_request_should_work() -> TestResult<()> {
        let client = Client::new()?;
        let method_url_map: HashMap<Method, String> = [
            (Method::GET, client.make_url("/v1/ping")),
            (Method::POST, client.make_url("/v1/login")),
            (Method::PATCH, client.make_url("/v1/score")),
            (
                Method::DELETE,
                client.make_url("/v1/player/123/rival/player/456"),
            ),
            (Method::PUT, client.make_url("/v1/player/location")),
        ]
        .iter()
        .cloned()
        .collect();

        for (method, url) in &method_url_map {
            let mut request = Request::new(Method::OPTIONS, Url::parse(&url)?);

            let headers = request.headers_mut();
            headers.append(
                header::ORIGIN,
                HeaderValue::from_str(&client.settings.application.external_url)?,
            );
            headers.append(
                header::ACCESS_CONTROL_REQUEST_METHOD,
                HeaderValue::from_str(method.as_str())?,
            );

            let response = client.client.execute(request).await?;

            assert!(response.status().is_success());
        }

        Ok(())
    }
}
