mod update_tests {
    use tgmrank::controllers::models::{LoginModel, PlayerUsernameHistoryModel};
    use tgmrank::models::forms::PlayerUsernameUpdateForm;

    use crate::test_utility::{Client, DataGenerator, TestResult};

    async fn validate_username_history(
        client: &Client,
        player_id: i32,
        expected_usernames: &[&str],
    ) -> TestResult<()> {
        let username_history: Vec<PlayerUsernameHistoryModel> =
            client.get_username_history(player_id).await?.json().await?;

        assert_eq!(username_history.len(), expected_usernames.len());

        for (actual, expected_username) in username_history.into_iter().zip(expected_usernames) {
            assert_eq!(&actual.username, expected_username);
        }

        Ok(())
    }

    #[actix_web::test]
    async fn updated_username_should_show_up_in_leaderboards() -> TestResult<()> {
        let client = Client::new()?;

        let (_response, user) = client.random_new_user().await?;
        let login_model: LoginModel = client
            .login(&user.username, &user.password)
            .await?
            .json()
            .await?;

        client.add_score(&DataGenerator::random_tgm1_gm()).await?;

        validate_username_history(&client, login_model.user_id, &[&login_model.username]).await?;

        let some_new_username = "updated_username_01";
        let response = client
            .update_username(PlayerUsernameUpdateForm {
                username: some_new_username.to_string(),
            })
            .await?;

        assert!(response.status().is_success());

        validate_username_history(
            &client,
            login_model.user_id,
            &[some_new_username, &login_model.username],
        )
        .await?;

        let mode_ranking = client.get_mode_ranking(1).await?;
        let found_score = mode_ranking
            .into_iter()
            .any(|r| r.player.is_some() && r.player.unwrap().player_name == some_new_username);

        assert!(found_score);

        let game_ranking = client.get_ranking(1).await?;
        let found_score = game_ranking
            .into_iter()
            .any(|r| r.player.player_name == some_new_username);

        assert!(found_score);

        Ok(())
    }

    #[actix_web::test]
    async fn cannot_update_username_multiple_times_in_a_short_time_period() -> TestResult<()> {
        let client = Client::new()?;

        let (_response, user) = client.random_new_user().await?;
        let _response = client.login(&user.username, &user.password).await?;

        let response = client
            .update_username(PlayerUsernameUpdateForm {
                username: "new username 01".to_string(),
            })
            .await?;
        assert!(response.status().is_success());

        let _response = client.login("new username 01", &user.password).await?;

        let response = client
            .update_username(PlayerUsernameUpdateForm {
                username: "new username 02".to_string(),
            })
            .await?;
        assert!(response.status().is_client_error());

        Ok(())
    }

    #[actix_web::test]
    async fn cannot_update_username_to_username_that_already_exists() -> TestResult<()> {
        let client = Client::new()?;

        let (_response, user1) = client.random_new_user().await?;

        let (_response, user2) = client.random_new_user().await?;
        let _response = client.login(&user2.username, &user2.password).await?;

        let response = client
            .update_username(PlayerUsernameUpdateForm {
                username: user1.username,
            })
            .await?;

        assert!(response.status().is_server_error());

        Ok(())
    }
}

mod rivals_tests {
    use crate::test_utility::{Client, TestResult};
    use reqwest::StatusCode;
    use tgmrank::controllers::models::LoginModel;

    #[actix_web::test]
    async fn cannot_add_or_delete_rivals_when_not_logged_in() -> TestResult<()> {
        let client = Client::new()?;

        let response = client.post_rival_for_player_raw(123, 456).await?;
        assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

        let response = client.delete_rival_for_player_raw(123, 456).await?;
        assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

        Ok(())
    }

    #[actix_web::test]
    async fn cannot_add_or_delete_rivals_for_other_users() -> TestResult<()> {
        let client = Client::new()?;

        let (_response, user) = client.random_new_user().await?;
        client.login(&user.username, &user.password).await?;

        let response = client.post_rival_for_player_raw(123, 456).await?;
        assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

        let response = client.delete_rival_for_player_raw(123, 456).await?;
        assert_eq!(response.status(), StatusCode::UNAUTHORIZED);

        Ok(())
    }

    #[actix_web::test]
    async fn cannot_add_self_as_a_rival() -> TestResult<()> {
        let client = Client::new()?;

        let (_response, user) = client.random_new_user().await?;
        let response = client.login(&user.username, &user.password).await?;
        let login_details: LoginModel = response.json().await?;

        let response = client
            .post_rival_for_player_raw(login_details.user_id, login_details.user_id)
            .await?;
        assert_eq!(response.status(), StatusCode::BAD_REQUEST);

        Ok(())
    }

    #[actix_web::test]
    async fn add_or_delete_rivals_for_self() -> TestResult<()> {
        let client = Client::new()?;

        let (response, user_form) = client.random_new_user().await?;
        let user: LoginModel = response.json().await?;

        let (response, _) = client.random_new_user().await?;
        let rival_user: LoginModel = response.json().await?;

        client
            .login(&user_form.username, &user_form.password)
            .await?;

        let player_response = client
            .post_rival_for_player(user.user_id, rival_user.user_id)
            .await?;
        assert_eq!(player_response.player_id, rival_user.user_id);

        let rival_list_response = client.get_rivals_for_player(user.user_id).await?;
        let found_rival = rival_list_response
            .iter()
            .find(|r| r.player_id == rival_user.user_id);
        assert_eq!(found_rival.is_some(), true);

        let rivaled_by_list = client.get_rivaled_by_for_player(rival_user.user_id).await?;
        let found_rivaled_by = rivaled_by_list.iter().find(|r| r.player_id == user.user_id);
        assert_eq!(found_rivaled_by.is_some(), true);

        let _response = client
            .delete_rival_for_player(user.user_id, rival_user.user_id)
            .await?;

        let rival_list_response = client.get_rivals_for_player(user.user_id).await?;
        assert_eq!(rival_list_response.is_empty(), true);

        Ok(())
    }
}

mod location_tests {
    use anyhow::Context;
    use reqwest::StatusCode;

    use tgmrank::models::filters::{GenericQuery, RecentActivityQuery};
    use tgmrank::models::forms::PlayerLocationUpdateForm;

    use crate::test_utility::{Client, DataGenerator, TestResult};
    use tgmrank::controllers::game_controller::GameRankingQuery;
    use tgmrank::controllers::mode_controller::ModeRankingQuery;
    use tgmrank::controllers::models::LoginModel;
    use tgmrank::models::ScoreStatus;

    #[actix_web::test]
    async fn can_set_valid_location() -> TestResult<()> {
        let new_location = "au";
        let client = Client::new()?;

        let (_response, user) = client.random_new_user().await?;
        let _response = client.login(&user.username, &user.password).await?;

        let response = client
            .update_user_location(PlayerLocationUpdateForm {
                new_location: Some(new_location.into()),
            })
            .await?;
        assert_eq!(response.status(), StatusCode::OK);

        let response = client.login_verify().await?;
        assert_eq!(response.status(), StatusCode::OK);

        let login_model: LoginModel = response.json().await?;
        assert_eq!(login_model.location, Some(new_location.into()));

        Ok(())
    }

    #[actix_web::test]
    async fn can_unset_location() -> TestResult<()> {
        let new_location = "ch";
        let client = Client::new()?;

        let (_response, user) = client.random_new_user().await?;
        let _response = client.login(&user.username, &user.password).await?;

        let _response = client
            .update_user_location(PlayerLocationUpdateForm {
                new_location: Some(new_location.into()),
            })
            .await?;

        let _response = client
            .update_user_location(PlayerLocationUpdateForm { new_location: None })
            .await?;

        let response = client.login_verify().await?;
        assert_eq!(response.status(), StatusCode::OK);

        let login_model: LoginModel = response.json().await?;
        assert_eq!(login_model.location, None);

        Ok(())
    }

    #[actix_web::test]
    async fn cannot_set_invalid_location() -> TestResult<()> {
        let client = Client::new()?;

        let (_response, user) = client.random_new_user().await?;
        let _response = client.login(&user.username, &user.password).await?;

        let invalid_locations = vec!["z", "zz", "zzz", "zzzz"];

        for location_code in invalid_locations {
            let response = client
                .update_user_location(PlayerLocationUpdateForm {
                    new_location: Some(location_code.into()),
                })
                .await?;
            assert!(response.status().is_client_error());
        }

        Ok(())
    }

    #[actix_web::test]
    async fn location_update_should_be_shown_in_rankings() -> TestResult<()> {
        let location_code = "pl";
        let client = Client::new()?;

        let (_response, user) = client.random_new_user().await?;

        client.login(&user.username, &user.password).await?;
        {
            client.add_score(&DataGenerator::random_tgm1_gm()).await?;
        }

        let ranking = client.get_ranking(0).await?;
        let player_location = &ranking
            .iter()
            .find(|r| r.player.player_name == user.username)
            .context("Could not find player in rankings")?
            .player
            .location;
        assert_eq!(player_location, &None);

        client
            .update_user_location(PlayerLocationUpdateForm {
                new_location: Some(location_code.into()),
            })
            .await?;

        let ranking = client.get_ranking(0).await?;
        let player_location = &ranking
            .iter()
            .find(|r| r.player.player_name == user.username)
            .context("Could not find player in rankings")?
            .player
            .location;
        assert_eq!(player_location, &Some(location_code.to_string()));

        Ok(())
    }

    #[actix_web::test]
    async fn rankings_by_location_should_only_show_scores_from_location() -> TestResult<()> {
        let location_code = "th".to_string();
        let client = Client::new()?;

        let (_response, user_with_location) = client.random_new_user().await?;
        client
            .login(&user_with_location.username, &user_with_location.password)
            .await?;
        {
            client
                .update_user_location(PlayerLocationUpdateForm {
                    new_location: Some(location_code.to_string()),
                })
                .await?;

            client.add_score(&DataGenerator::random_tgm1_gm()).await?;
        }

        let (_response, other_user) = client.random_new_user().await?;
        client
            .login(&other_user.username, &other_user.password)
            .await?;
        {
            client.add_score(&DataGenerator::random_tgm1_gm()).await?;
        }

        let ranking = client
            .get_ranking2(
                0,
                &GameRankingQuery {
                    locations: Some(vec![location_code.clone()]),
                    ..Default::default()
                },
            )
            .await?;

        assert_eq!(ranking.len(), 1);
        assert!(ranking
            .iter()
            .all(|r| r.player.location == Some(location_code.clone())));

        let ranking = client
            .get_ranking2(
                1,
                &GameRankingQuery {
                    locations: Some(vec![location_code.clone()]),
                    ..Default::default()
                },
            )
            .await?;

        assert_eq!(ranking.len(), 1);
        assert!(ranking
            .iter()
            .all(|r| r.player.location == Some(location_code.clone())));

        let ranking = client
            .get_mode_ranking2(
                1,
                &ModeRankingQuery {
                    locations: Some(vec![location_code.clone()]),
                    ..Default::default()
                },
            )
            .await?;

        assert_eq!(ranking.len(), 1);
        assert!(ranking
            .iter()
            .all(|r| r.player.as_ref().map(|p| p.location.as_ref()).flatten()
                == Some(&location_code)));

        let recent_scores = client
            .get_recent_activity(&RecentActivityQuery {
                filter: GenericQuery {
                    locations: Some(vec![location_code.clone()]),
                    score_status: Some(vec![ScoreStatus::Pending]),
                    ..Default::default()
                },
                ..Default::default()
            })
            .await?;

        assert_eq!(recent_scores.len(), 1);
        assert!(recent_scores.iter().all(|r| r
            .score
            .player
            .as_ref()
            .map(|p| p.location.as_ref())
            .flatten()
            == Some(&location_code)));

        Ok(())
    }
}
