FROM registry.gitlab.com/tgm-rank/tgmrank/builder:latest AS builder

# The builder image should have most of the files already, update them!
ADD . ./

RUN cargo build --release
RUN cargo test --release --lib

# Build stage 2: Make the final container with just the necessary pieces
FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder /volume/tgmrank/target/x86_64-unknown-linux-musl/release/tgmrank .
COPY --from=builder /volume/tgmrank/public public
COPY --from=builder /volume/tgmrank/Settings.env .
