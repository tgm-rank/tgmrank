# TGM Rank

Tetris the Grandmaster (TGM) is a puzzle game series by Arika based on Sega Tetris. Hallmarks of the series include fast gameplay, instant gravity, and invisible pieces. It's fun. It's been featured at Games Done Quick events a handful of times.

This is the backend for [theabsolute.plus](https://theabsolute.plus), a repository of scores and records held across the community; a place to compare scores and inspire competition.

## Dev Setup

### Native

#### Required stuff

* Rust (nightly-2020-11-17)
  * Installation using [Rustup](https://www.rust-lang.org/tools/install) is recommended
* postgresql 11
  * Set up connection string in `Settings.env`
* openssl

#### Pre-requisites

Install [Rustup](https://www.rust-lang.org/tools/install)

Install the nightly toolchain:

```bash
rustup toolchain install nightly-2020-11-17
```

(Optional) Make it the default

```bash
rustup default nightly-2020-11-17
```

##### Setup postgresql 11

The [periods](https://github.com/xocolatl/periods) Postgresql extension is required. The postgresql container for this project already has it bundled.

#### Running this project

```bash
git clone git@gitlab.com:tgm-rank/tgmrank.git
cd tgmrank

cargo run
```

The application should be running @ `localhost:1300`

Creating/running database migrations:

See [sqlx-cli](https://github.com/launchbadge/sqlx/tree/master/sqlx-cli):

```bash
cargo install sqlx-cli --no-default-features --features postgres
```

### Docker

Clone this repository and `tgmrank-frontend`, then inside `tgmrank`, then run

```bash
docker-compose up --build
```

The `docker-compose` script sets up four containers:

* db: Postgresql database
* web: This project!
* frontend: refers to the container built from the `tgmrank-frontend` project
* proxy: nginx layer that reverse-proxies to the frontend and backend and does a few other things too.

Frontend: Hosted @ `localhost:1234`, nginx forwards requests from `localhost:80`.
Backend: Hosted @ `localhost:1301`, nginx forwards requests from `localhost:1300`.

Stop relevant services if necessary during development. For example, if you're developing a feature for just the frontend, simply stop the containerized version of the frontend.

```bash
docker-compose stop frontend
```

Everything should be set up to run the frontend service locally while still connecting to the containerized backend.

## Miscellaneous Information

### Setup

I use a combination of both setups for development. Docker hosts the database whether I'm running the application in docker or not.

`Settings.env` contains the base database connection string, `docker-compose.override.yml` updates the connection string for dev environments.

### Integration Tests

Integration tests depend on having both a native build environment and Docker. It deletes and rebuilds the current database (in Docker), then runs the tests from a local build connected to the new database.

## License

[MIT](/LICENSE)
