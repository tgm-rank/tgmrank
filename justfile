#!/usr/bin/env just --justfile

start-docker:
  sudo systemctl start docker.socket docker.service

start: start-docker
  sudo docker compose up -d db
  sudo docker compose up -d

stop:
  sudo docker compose down
  sudo systemctl stop docker.socket docker.service

teardown-env:
  sudo docker compose down
  sudo docker volume rm tgmrank_pgdata

test-integration: teardown-env start
  sleep 5s
  cargo test --test integration -- --test-threads=1

prepare-sql:
  cargo sqlx prepare -- --lib

build-builder:
  sudo docker build -f builder.Dockerfile -t registry.gitlab.com/tgm-rank/tgmrank/builder:latest .

push-builder:
  sudo docker push registry.gitlab.com/tgm-rank/tgmrank/builder
